#pragma once
#include "Entity.h"
enum Tier
{
	Gold, Plat, Diamond, TIERSIZE
};

enum ItemType
{
	BLOCK, DAMAGE, HP, MOVESPEED, ITEMTYPESIZE = 6 , NOITEM
};

enum StatusEffect
{
	FIRE, ICE, POISON, STATUSEFFECTSIZE, NOEFFECT
};

class Character;

class Items : public Entity 
{
public:
	Items(Tier level, float time, SGD::HTexture pic, StatusEffect effect,
		ItemType it, bool isPickedUp = false);

	~Items();

	//Class Functions
	void UpdateItem(float dt);
	virtual void Render(void)		override;
	bool HandleCollision(const IEntity* other);
	void ItemFlashText(ItemType it, StatusEffect sEffect, SGD::Point pos);


	void TierOne(void);
	void TierTwo(void);
	void TierThree(void);
	void ApplyStatusEffect(Character* whom, float delta);
	void FireEffect(float delta);
	void IceEffect(float delta);
	void PoisonEffect(float delta);

	//Accessor 
	Tier			GetTier(void)				const { return tier; }
	float			GetTimer(void)				const { return timer; }
	bool			IsEffected(void)			const { return hasStatus; }
	bool			IsHeld(void)				const { return isPickedUp; }
	Character*		GetOwner(void)				const { return owner; }
	ItemType		GetItemType(void)			const { return queItem; }
	float			GetBlockIncrease(void)		const { return blockIncrease; }
	float			GetDamageIncrase(void)		const { return damageIncrease; }
	float			GetHPIncrease(void)			const { return hpIncrease; }
	float			GetMSIncrease(void)			const { return msIncrease; }
	float			GetLifeSpan(void)			const { return lifeSpan; }

	//Mutators
	void SetTier(Tier t)					{ tier = t; }
	void SetTimer(float t)					{ timer = t; }
	void SetIsEffected(bool b)				{ hasStatus = b; }
	void SetPickedUp(bool b)				{ isPickedUp = b; }
	void SetOwner(Character* c)				{ owner = c; }
	void SetBlockIncrease(float b)	{ blockIncrease = b; }
	void SetDamageIncrease(float dmg)		{ damageIncrease = dmg; }
	void SetHPIncrase(float hp)				{ hpIncrease = hp; }
	void SetMSIncrease(float ms)			{ msIncrease = ms; }
	void SetItemType(ItemType it)			{ queItem = it; }

	void ResetMoveSpeed();
private:
	Tier tier;
	float timer;
	bool hasStatus;
	bool isPickedUp = false;
	Character* owner = nullptr;
	ItemType queItem;
	StatusEffect sEffect;
	// the stats that the item are gong to increase
	float blockIncrease;
	float damageIncrease;
	float hpIncrease;
	float msIncrease;
	float lifeSpan;
	bool once = true;
	bool fireBurn = true;
	bool bad = true;
	float ms2;
	float OGDamage;
	float OGBlock;
	float OGHP;
	
	float maxTime;



};

