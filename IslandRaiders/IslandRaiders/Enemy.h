#pragma once
#include "../SGD Wrappers/SGD_InputManager.h"
#include "Character.h"
#include "EnemyAI.h"
#include "Items.h"
#include "SkillStamp.h"

class Player;

class Enemy : public Character
{
protected:
	bool dead = false;
	bool once = true;
	bool marked = false;
	//enemy ID
	// enemyID;

	//AI states object
	AIStamp aiStamp;
	SkillStamp skillStamp;

	//chance to drop items ( 0 -> 100)
	int dropRate = 2;
	//int dropRateScaleValue = 99; 
	//Each monster has one item it may drop on death
	Tier itemTier;
	ItemType queBuff;
	StatusEffect status = NOEFFECT;

	//
	Player *killer= nullptr;

public:
	Enemy(const char* name);
	virtual ~Enemy();
	virtual void   Update(float dt) ;
	virtual void   Render();
	virtual bool   HandleCollision(const IEntity* other);
	virtual void   HandleEvent(const SGD::Event* pEvent);

	//class functions
	virtual unsigned int  GetType()		const override { return ENT_ENEMY; };
	void DropWhatItem();

	// Accessors
	int GetDropRate(void)			const		{ return dropRate; }
	//unsigned int GetEnemyID(void)	const		{ return enemyID; }
	const AIStamp* GetAIStamp(void) const		{ return &aiStamp; }
	Tier GetItemTier(void)			const		{ return itemTier; }
	ItemType GetQueBuff(void)		const		{ return queBuff; }
	StatusEffect GetStatus(void)	const		{ return status; }

	AIStamp* GetAIStamp(void)					{ return &aiStamp; }
	EnemyAIType GetAIType(void)		const		{ return aiStamp.aiType; }
	CombatType GetCombatType(void)	const		{ return aiStamp.combatType; }

	const SkillStamp* GetSkillStamp(void) const { return &skillStamp; }
	SkillStamp* GetSkillStamp(void) { return &skillStamp; }

	// Mutators 
	void SetTier(unsigned short t)		{ aiStamp.tier = t; }
	//void SetEnemyID(int id)				{ enemyID = id; }
	void SetAIType(EnemyAIType ai)		{ aiStamp.aiType = ai; }
	void SetCombatType(CombatType ai)	{ aiStamp.combatType = ai; }
	void SetDropRate(int f)				{ dropRate = f; }
};

