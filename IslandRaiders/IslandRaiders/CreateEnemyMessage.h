#pragma once
#include "..\SGD Wrappers\SGD_Message.h"

class Entity;

class ComboAttackMessage : public SGD::Message
{
public:
	ComboAttackMessage(Entity* owner);
	~ComboAttackMessage();

	Entity* GetOwner(void) const { return postMan; }

private:
	Entity* postMan;
};

