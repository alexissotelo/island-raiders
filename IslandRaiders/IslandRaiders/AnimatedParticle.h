#pragma once
#include "../SGD Wrappers/SGD_Declarations.h"
#include "AnimationTimestamp.h"
#include "Entity.h"

class Character;
class AnimatedParticle : public Entity
{
public:

	AnimatedParticle(Character* _owner, const char* _name, float _span, SGD::Point pos, SGD::Vector vel = SGD::Vector(0,0));
	~AnimatedParticle();

	virtual void   Update(float dt);
	virtual void   Render();
	virtual bool   HandleCollision(const IEntity* other);
	virtual void HandleEvent(const SGD::Event* pEvent);
	unsigned int GetType(void) const { return ENT_ANIM_PARTICLE; }


	//SGD::Rectangle	GetCollisionRect(void)		const;
	//SGD::Rectangle	GetActiveRect(void)			const;

	//accessors
	float GetLifeSpan(void) const { return m_fLifespan; }
	Character* GetOwner(void) const { return m_pOwner; }

	//mutators
	void SetLifeSpan(float lifespan) { m_fLifespan = (lifespan > 0.0f) ? lifespan : 0.0f; }
	void SetOwner(Character* ptr);

private:
	float m_fLifespan;
	Character* m_pOwner;
	bool m_bMotionless;
};

