#include "FollowAI.h"
#include "Enemy.h"
#include "PlayGameState.h"


/**************************************************************
|	Function: FollowAI Constructor [default]
|	Purpose: allocate an object into memory and initialize members
|	Parameter: void
**************************************************************/
FollowAI::FollowAI(void)
{
	//empty
}

/**************************************************************
|	Function: FollowAI Destructor [default]
|	Purpose: deallocate an object out of memory and delete all dynamic members
**************************************************************/
FollowAI::~FollowAI(void)
{
	//no dynamic memory
}

/**************************************************************
|	Function: FollowAI::Update [virtual]
|	Purpose: move an enemy object to a waypoint, once the characters
|			has reached a waypoint search for a player within range and
|			target them, else generate another waypoint
|	Parameter:
|				float dt - the amount of time that has elapsed since the last update
|				AIStamp& stamp - the current state of the enemy object
|	Return Value: void
**************************************************************/
void FollowAI::Update(float dt, AIStamp& stamp)
{
	//type cast
	Enemy* owner = dynamic_cast<Enemy*>(stamp.owner);

	//do I have a waypoint?
	if (stamp.ptWaypoint.x == -1 || stamp.ptWaypoint.y == -1)
	{
		AIManager::GetInstance()->FindClosestTarget(stamp);
		stamp.SwitchAIMode(EnemyAIType::Chaser);
		return;
	}


	//create a vector to the waypoint
	SGD::Vector toWaypoint = stamp.ptWaypoint - stamp.owner->GetPosition();

	//calculate the distance
	float distance = toWaypoint.ComputeLength();

	//generate the waypoint when close
	if (distance <= 5)
	{
		//update a time -- stand still
		stamp.fStartTimer += dt;

		if (stamp.fStartTimer >= 2.0f)
		{
			//search for players in range
			if (AIManager::FindClosestTarget(stamp))
			{
				//chase em
				stamp.aiType = EnemyAIType::Chaser;
				//skip
			}
			else
				//generate a random waypoint to go to
				AIManager::GenerateWaypoint(stamp);
		}

		//stop moving
		owner->SetVelocity({ 0.f, 0.f });
		owner->GetTimeStamp()->SwitchAnimation("idle");

		//skip
		return;
	}

	owner->GetTimeStamp()->SwitchAnimation("walk");

	//normalzie
	toWaypoint.Normalize();

	//scale
	toWaypoint.x *= owner->GetMoveSpeed();
	toWaypoint.y *= owner->GetMoveSpeed();

	//set the velocity
	owner->SetVelocity(toWaypoint);

	//set the direction
	if (owner->GetVelocity().x > 0)
		owner->GetTimeStamp()->bForwards = true;
	else
		owner->GetTimeStamp()->bForwards = false;


}

