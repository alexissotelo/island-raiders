#pragma once
#include "IGameState.h"
#include <vector>
#include <string>
#include "../SGD Wrappers/SGD_AudioManager.h"
#include "../SGD Wrappers/SGD_GraphicsManager.h"
#include "../SGD Wrappers/SGD_InputManager.h"

class BitmapFont;
class Button;

class OptionsGameState : public IGameState
{
public:
	static OptionsGameState* GetInstance(void);

	static void DeleteInstance(void);

	virtual void Enter();
	virtual void Exit();
	virtual bool Update(float dt);
	virtual void Render(float dt);
	void ArcadeCursorMovement(float dt);


private:
	float timer = 0.0f;
	float timerFreq = 0.4f;
	int cursor = 0;
	int volCount = 0;
	bool picking = false;
	std::vector<Button*> menuButtons;
	int soundEffectVolPlus = 100;
	int musicVolPlus = 100;
	bool fullscreen;
	float defaultX;
	float defaultY;
	static OptionsGameState* pInstance;
	const BitmapFont* pFont = nullptr;

	SGD::GraphicsManager* pGraphics = nullptr;
	SGD::AudioManager* pAudio = nullptr;
	SGD::InputManager* pInput = nullptr;
	SGD::HTexture backGround = SGD::INVALID_HANDLE;

	OptionsGameState() = default;
	~OptionsGameState() = default;
	OptionsGameState(const OptionsGameState&) = delete;
	OptionsGameState operator=(const OptionsGameState&) = delete;
};

