#include "CreditsGameState.h"
#include "BitmapFont.h"
#include "..\Source\Game.h"
#include "MenuGameState.h"

#include "GUI.h"

CreditsGameState* CreditsGameState::pInstance = nullptr;


CreditsGameState* CreditsGameState::GetInstance(void)
{
	if (pInstance == nullptr)
		pInstance = new CreditsGameState;

	return pInstance;
}

void CreditsGameState::DeleteInstance(void)
{
	delete pInstance;
	pInstance = nullptr;
}
void CreditsGameState::ArcadeCursorMovement(float dt)
{
	SGD::Vector dir = pInput->GetLeftJoystick(0);
	if (dir.y != 0)
	{
		timer += dt;
		if (timer > timerFreq)
		{
			timer = 0.f;
			if (dir.y < 0)
			{
				nCursor--;
				if (nCursor < 0)
					nCursor = 4;
			}
			if (dir == SGD::Vector{ 0, 1 })
			{
				timer = 0.f;
				nCursor++;
				if (nCursor > 4)
					nCursor = 0;

			}

		}
	}
}
void CreditsGameState::Enter()
{
	pFont = GUIManager::GetInstance()->GetFont();
	pGraphics = SGD::GraphicsManager::GetInstance();
	pAudio = SGD::AudioManager::GetInstance();
	pInput = SGD::InputManager::GetInstance();
	//backGround = pGraphics->LoadTexture(L"resource/Graphics/placeholder images/map.png");
	backGround = pGraphics->LoadTexture(L"resources/graphics/backgrounds/bg.png");
	botScreen = Game::GetInstance()->GetScreenHeight();
	titleScreen = pGraphics->LoadTexture(L"resources/graphics/backgrounds/logo.png");

	const char* arr[] =
	{ "Credits", "\n", "\n", "\n", "Executive Producer", "\n", "John Oleske", "Associate Prodcuer", "Shawn Paris",
	"\n", "Programmers", "\n", "David Mejia", "Jordan Bracken", "Marcos Sotelo", "\n\n", "Artists", "\n","Caris Frazier", "David Mejia", "Marcos Sotelo"
	};
	arrSize = sizeof(arr) / sizeof(arr[0]);
	for (int i = 0; i < arrSize; i++)
	{
		words w;
		if (i == 0)
			w.wordPosition = botScreen;
		else
			w.wordPosition = credits[i - 1].wordPosition + 50;

		if (arr[i] == "Executive Producer" || arr[i] == "Credits" || arr[i] == "Programmers" || arr[i] == "Artists")
			w.wordSize = 2.0f;

		w.wordX = (Game::GetInstance()->GetScreenWidth() / 2) - 100;
		if (arr[i] == "Executive Producer" || arr[i] == "Programmers")
			w.wordX = (Game::GetInstance()->GetScreenWidth() / 2) - 200;

		w.wordToDisplay = arr[i];
		credits.push_back(w);
	}

}
void CreditsGameState::Exit()
{
	pGraphics->UnloadTexture(backGround);
	pGraphics->UnloadTexture(titleScreen);
	DeleteInstance();
}
bool CreditsGameState::Update(float dt)
{
	hiddenTimer += dt;
	if (hiddenTimer > hiddenTimerMax)
		hidden = true;
	if (pInput->IsKeyPressed(SGD::Key::Escape) || pInput->IsButtonPressed(0, 2))
	{
		Game::GetInstance()->ChangeState(MenuGameState::GetInstance());
		return true;
	}

	for (int i = 0; i < arrSize; i++)
	{
		credits[i].wordPosition -= 2.0f;
	}
	if (credits[arrSize - 1].wordPosition == -30.0f)
	{
		SetPositions();
	}

#pragma region no l00k pl0x
#pragma region really no look
#pragma region ok fine look
	if (hidden)
	{
		for (size_t i = 0; i < credits.size(); i++)
		{
			if (credits[i].wordToDisplay == "David Mejia")
				credits[i].wordToDisplay = "Batman";
			if (credits[i].wordToDisplay == "Jordan Bracken")
				credits[i].wordToDisplay = "Jim Gordon";
			if (credits[i].wordToDisplay == "Marcos Sotelo")
				credits[i].wordToDisplay = "Harvey Dent";
		}
	}
#pragma endregion

#pragma endregion

#pragma endregion

	return true;
}
void CreditsGameState::Render(float dt)
{

	pGraphics->DrawTexture(backGround, { 0.0f, 0.0f }/*, 0.f, {}, {}, {}*/);
	pGraphics->DrawTexture(titleScreen, { 0.f, 0.f }, 0.f, {}, {200, 255, 255, 255});


	for (int i = 0; i < arrSize; i++)
	{
		pFont->DrawXML(credits[i].wordToDisplay, { credits[i].wordX, credits[i].wordPosition },
			credits[i].wordSize, { 255, 255, 255 });
	}

}

void CreditsGameState::SetPositions(void)
{
	for (int i = 0; i < arrSize; i++)
	{
		if (i == 0)
			credits[i].wordPosition = botScreen;
		else
			credits[i].wordPosition = credits[i - 1].wordPosition + 50;
	}
}
