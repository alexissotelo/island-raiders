#pragma once
#include "../SGD Wrappers/SGD_Handle.h"
#include <vector>

class AudioQueue
{

public:

	//static singletons
	static AudioQueue* GetInstance(void);
	static void DeleteInstance(void);

	void AddSound(SGD::HAudio, bool _loop = false);
	void AddSound(const char* name, bool _loop = false);
	void AddSound(const wchar_t* name, bool _loop = false);
	void Update(void);
	void clear(void);
	unsigned int size(void) const;

private:
	//class vector
	std::vector<SGD::HAudio> soundQueue;
	// singleton instance
	static AudioQueue* pInstance;


	//no access allowed
	AudioQueue() = default;
	~AudioQueue() = default;
	AudioQueue(const AudioQueue&) = delete;
	const AudioQueue& operator=(const AudioQueue&) = delete;

};
