#pragma once
#include "Animation.h"
#include "AnimationTimestamp.h"
#include "..\SGD Wrappers\SGD_Handle.h"
#include <map>
#include <string>
#include <vector>


/**************************************************************
|	Function: 
|	Purpose: 
|	Parameter: 
|	Return Value: 
**************************************************************/

class AnimationTimestamp;

class AnimationSystem
{
	typedef std::vector<std::string> NameTable;
public:
	//singleton
	static AnimationSystem* GetInstance(void);
	static void DeleteInstance(void);

	//init and destruct
	static void Initialize();
	void Terminate(void);
	//game updating
	void Render(AnimationTimestamp* animationTimeStamp, SGD::Point position);
	bool Update(float dt, AnimationTimestamp* timeStamp);

	//bracket operator
	const Animation* GetAnimation(std::string _string){ return m_mAnimation[_string]; }

	//accessor for names
	const NameTable* GetMissileNames	(void)	const { return &m_vMissiles; }
	const NameTable* GetParticleNames(void)		const { return &m_vParticles; }
	const NameTable* GetLaserNames(void)		const{ return &m_vLasers; }
	const NameTable* GetBeamNames(void)			const { return &m_vBeams; }

private:

	friend class Animation;
	std::map<std::string, Animation* > m_mAnimation; //collection of animation
	std::map<std::string, SGD::HTexture > m_mSpriteSheet; //collection of sprite sheet

	NameTable m_vMissiles;	//collection of missile names
	NameTable m_vParticles;	//collection of particle names
	NameTable m_vLasers;		//collection of laser names
	NameTable m_vBeams;		//collection of beam names

	std::vector<std::string> m_vImageFilePaths;
	//singleton access
	static AnimationSystem* pInstance;


	//helpers
	void GenerateAllAnimations();
	bool LoadAnimation(const char* playerName);
	void PopulateName(const char* fileName, NameTable* table);

	//defaults
	AnimationSystem() = default;
	~AnimationSystem() = default;
	AnimationSystem(const AnimationSystem&) = delete;

	//SGD::HTexture screen[4];

};

