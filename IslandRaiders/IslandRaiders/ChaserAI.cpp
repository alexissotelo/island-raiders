#include "ChaserAI.h"
#include "EnemyAI.h"
#include "Enemy.h"
#include "Player.h"
#include "PlayGameState.h"
#include "../SGD Wrappers/SGD_Geometry.h"

#include <vector>


/**************************************************************
|	Function: ChaserAI Constructor [default]
|	Purpose: allocate an object into memory and initialize members
|	Parameter: void
**************************************************************/
ChaserAI::ChaserAI(void)
{
	//empty
}
/**************************************************************
|	Function: ChaserAI Constructor [default]
|	Purpose: deallocate an object out of memory and delete dynamic members
**************************************************************/
ChaserAI::~ChaserAI(void)
{
	//remove members from memory
}

/**************************************************************
|	Function: ChaserAI::Update [virtual]
|	Purpose: move the enemy towards the player, and pause after
|			time either to stand there or attack randomly
|	Parameter:
|			float dt - the elapsed time since the last update
|			AIStamp& stamp - the current state of the enemy
|	Return Value: void
**************************************************************/
void ChaserAI::Update(float dt, AIStamp& stamp)
{

	//stamp.owner->GetTimeStamp()->SwitchAnimation("idle");
	//return;


	//target
	Character* own = stamp.owner;

	//no target? 
	if (stamp.target == nullptr)
	{
		//find one
		AIManager::GetInstance()->FindPreferredTarget(stamp);
		//leave -- you'll get em next time
		return;
	}

	//stand still if im not moving - obvi 
	if (own->GetTimeStamp()->isDone)
		own->GetTimeStamp()->SwitchAnimation("idle");

	//if the timer is on, decrease the time and do not chase
	if (stamp.fPause > 0.0f)
	{
		//update the pause timer -- dont chase
		stamp.fPause -= dt;
		//reset to zero once the timer is done
		if (stamp.fPause < 0.0f)
			stamp.fPause = 0.0f;

		//no chase
		return;
	}

	//run for at most 5 seconds
	stamp.fStartTimer += dt;

	//the enemy has been moving for some time...give em some down time
	//reset timers and change animations
	if (stamp.fStartTimer >= 3.5f)
	{
		//offset the timer
		int offset = -2 + rand() % 4; //range  =		(-2, -1, 0, -1) : max timer runs 5 secs
		stamp.fStartTimer = (float)offset;

		//start pause timer
		stamp.fPause = 2.0f;

		//stop moving
		own->SetVelocity(SGD::Vector(0.0f, 0.0f));

		//randomly stop or attack
		int act = rand() % 2;

		//switch animation based on action
		if (act)
			//set the animation to walk
			own->GetTimeStamp()->SwitchAnimation("idle");
		else
			//set the animationt attack
			own->GetTimeStamp()->SwitchAnimation("attack");

		//skip 
		return;
	}

	//IF WE MADE IT THIS FAR THEN CHASE!!!

	if (stamp.combatType == CombatType::Melee)
		MeleeChase(dt, stamp); //if check for the type
	else if (stamp.combatType == CombatType::Range /*|| stamp.combatType == CombatType::AOE*/)
		RangeChase(dt, stamp);
}

/**************************************************************
|	Function: ChaserAI::MeleeChase [private]
|	Purpose: enemy movements suited specifically for melee type
|				enemies
|	Parameter:
|			float dt - the elapsed time since the last update
|			AIStamp& stamp - the current state of the enemy
|	Return Value: void
**************************************************************/
void ChaserAI::MeleeChase(float dt, AIStamp& stamp)
{
	//owner
	Character* own = stamp.owner;

	//create  a vector to the player
	SGD::Vector toTarget = stamp.target->GetPosition() - own->GetPosition();

	//calculate the distance
	float distance = toTarget.ComputeLength();

	//normalize the vector
	toTarget.Normalize();

	//50 pixels - attack if the target is close
	if (distance <= 50 && own->GetFireTime() >= own->GetFireFreq())
	{
		stamp.fStartTimer = 0.0f;
		//start pause timer
		stamp.fPause = 2.0f;
		//stop moving
		own->SetVelocity(toTarget);
		//owner->SetVelocity(SGD::Vector(0.0f, 0.0f));
		//set the animation to walk
		own->GetTimeStamp()->SwitchAnimation("attack");
		own->SetFireTimer(0.0f);
	}
	else //walk to the target
	{
		//set the velocity
		float speed = own->GetMoveSpeed();
		toTarget.x *= speed;
		toTarget.y *= speed;
		own->SetVelocity(toTarget);

		//set the animation to walk
		own->GetTimeStamp()->SwitchAnimation("walk");
	}

	//set the direction for animations
	if (own->GetVelocity().x > 0)
		own->GetTimeStamp()->bForwards = true;
	else
		own->GetTimeStamp()->bForwards = false;

}

/**************************************************************
|	Function: ChaserAI::RangeChase [private]
|	Purpose: enemy movement suited specifically for ranged type enemies
|	Parameter:
|			float dt - the elapsed time since the last update
|			AIStamp& stamp - the current state of the enemy
|	Return Value: void
**************************************************************/
void ChaserAI::RangeChase(float dt, AIStamp& stamp)
{
	//actions: find the closest target (DONE inside of FOLLOW AI)
	//move until you have reached their Y position
	//shoot in their direction
	//path find

	//owner
	Character* own = stamp.owner;

	//create  a vector to the player
	SGD::Vector toTarget = stamp.target->GetPosition() - own->GetPosition();
	//facing
	float direction = toTarget.x;
	//we dont care about the x-value
	toTarget.x = 0.0f;

	//move up until the y position is reached
	//calculate the distance
	float distance = toTarget.ComputeLength();
	//normalize the vector
	toTarget.Normalize();


	//100 pixels - shoot if the target is close enough on the yAxis
	if (distance <= 10)
	{
		//follow a random point
		//stamp.SwitchAIMode(EnemyAIType::Follow);
		//reset the start timer
		stamp.fStartTimer = 0.0f;
		//start pause timer
		stamp.fPause = .750f;

		//stop moving
		own->SetVelocity(toTarget);

		//set the animation to walk
		own->GetTimeStamp()->SwitchAnimation("attack");
		own->SetFireTimer(0.0f);

	}
	else //walk to the target
	{
		//set the velocity
		float speed = own->GetMoveSpeed();
		toTarget.x *= speed;
		toTarget.y *= speed;
		own->SetVelocity(toTarget);

		//set the animation to walk
		own->GetTimeStamp()->SwitchAnimation("walk");
	}

	//set the direction for animations
	if (direction > 0)
		own->GetTimeStamp()->bForwards = true;
	else
		own->GetTimeStamp()->bForwards = false;

}
