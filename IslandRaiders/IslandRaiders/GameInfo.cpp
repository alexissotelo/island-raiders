#include "GameInfo.h"
#include "Entity.h"
#include "..\tinyxml\tinyxml.h"
#include "SaveLoadState.h"

#pragma region Game Info
//pointer
GameInfo* GameInfo::m_pInstance = nullptr;

//statics
GameInfo*  GameInfo::GetInstance(void)
{
	if (!m_pInstance)
		m_pInstance = new GameInfo();

	return m_pInstance;
}

void  GameInfo::DeleteInstance(void)
{
	delete m_pInstance;
	m_pInstance = nullptr;
}

void GameInfo::Terminate(void)
{
	unlockLevels = 0;
	for (unsigned int index = 0; index < m_vPlayers.size(); ++index)
	{
		delete m_vPlayers[index];
		m_vPlayers[index] = nullptr;
	}

	//clear
	m_vPlayers.clear();
	//no players
	m_numPlayers = 0;
}

void GameInfo::AddPlayerInfo(PlayerInfo* ptr)
{
	//push into the vector

	m_vPlayers.push_back(ptr);
}
#pragma endregion

#pragma region PlayerInfo

//PlayerInfo::PlayerInfo()
//{
//	m_cName = nullptr;
//	m_classSkill = nullptr;
//	m_numController = -1;
//	//m_playerLevel = 1;
//}

PlayerInfo::~PlayerInfo()
{
	//null all names
	SetCharacterClass(nullptr);
	SetCharacterName(nullptr);
}
PlayerInfo::PlayerInfo(const PlayerInfo& src)
{
	m_cName = nullptr;
	m_classSkill = nullptr;
	m_playerLevel = src.m_playerLevel;

	*this = src;
}
PlayerInfo& PlayerInfo::operator=(const PlayerInfo& src)
{
	if (this != &src)
	{
		SetCharacterClass(src.m_classSkill);
		SetCharacterName(src.m_cName);
		SetControllerNumber(src.m_numController);
		m_playerLevel = src.m_playerLevel;
	}
	return *this;
}
PlayerInfo::PlayerInfo(const char* characterName, const char* className, int ID, int controller, unsigned int level)
{
	SetCharacterClass(className);
	SetCharacterName(characterName);
	SetControllerNumber(controller);

	m_playerLevel = level;
	SetCharacterID(ID);
}

void PlayerInfo::SetCharacterName(const char* name)
{
	if (m_cName)
		delete[] m_cName;

	if (!name)
		m_cName = nullptr;
	else
	{
		int size = strlen(name) + 1;
		m_cName = new char[size];
		strcpy_s(m_cName, size, name);
	}

}
void PlayerInfo::SetCharacterClass(const char* className)
{
	if (m_classSkill)
		delete[] m_classSkill;

	if (!className)
		m_classSkill = nullptr;
	else
	{
		int size = strlen(className) + 1;
		m_classSkill = new char[size];
		strcpy_s(m_classSkill, size, className);
	}
}
#pragma endregion

bool	GameInfo::SaveProfile(const char *filepath)
{
	TiXmlDocument doc;
	std::string path = "resources/xml/";
	path += filepath;
	path += ".xml";


	TiXmlDeclaration * decl = new TiXmlDeclaration("1.0", "", "");
	doc.LinkEndChild(decl);

	TiXmlElement * element = new TiXmlElement("slotone");
	size_t x = GetPlayers().size();
	element->SetAttribute("sizeofparty", (int)x);
	element->SetAttribute("unlockLevels", unlockLevels);

	for (size_t i = 0; i < x; i++)
	{
		TiXmlElement * xElement = new TiXmlElement("Character");
		xElement->SetAttribute("name", GetPlayers()[i]->GetCharacterName());
		xElement->SetAttribute("classSkill", GetPlayers()[i]->GetCharacterClass());
		xElement->SetAttribute("currentlvl", (int)GetPlayers()[i]->GetPlayerLevel());
		xElement->SetAttribute("characterController", GetPlayers()[i]->GetContollerNumber());
		xElement->SetAttribute("PlayerType", GetPlayers()[i]->GetCharacterID());
		element->LinkEndChild(xElement);

	}

	doc.LinkEndChild(element);

	doc.SaveFile(path.c_str());

	return true;
}
bool	GameInfo::CheckProFile(const char *filepath, int slot)
{
	TiXmlDocument doc;
	std::string path = "resources/xml/";
	path += filepath;
	path += ".xml";
	
	doc.LoadFile(path.c_str());
	TiXmlElement* root = doc.RootElement();

	if (root == nullptr)
		return false;
	else
	{
		//TiXmlElement* xElement = root->FirstChildElement("Character");
		TiXmlElement* whoDat = root->FirstChildElement("Character");
		int x = 0;
		while (whoDat)
		{
			int lvl,wlvl;
			const char* name = whoDat->Attribute("name");
			whoDat->Attribute("currentlvl", &lvl);
			whoDat->Attribute("WorldLevel", &wlvl);
			m_worldLevel = wlvl;

			PlayerInfo* p = new PlayerInfo(name, "test", 0, 0, lvl);
			SaveLoadState::GetInstance()->SavedInfo.resize(slot + 1);
			SaveLoadState::GetInstance()->SavedInfo[slot].push_back(p);
			++x;
			whoDat = whoDat->NextSiblingElement("Character");
		}
		return true;
	}
}

int GameInfo::GetNumPlayersXML(const char* filepath)
{
	TiXmlDocument doc;
	std::string path = "resources/xml/";
	path += filepath;
	path += ".xml";

	doc.LoadFile(path.c_str());
	TiXmlElement* root = doc.RootElement();

	if (root == nullptr)
		return -1;
	else
		root->Attribute("sizeofparty", &numPlayersXML);

	return numPlayersXML;
}
bool	GameInfo::LoadProfile(const char *filepath)
{

	TiXmlDocument doc;
	std::string path = "resources/xml/";
	path += filepath;
	path += ".xml";

	doc.LoadFile(path.c_str());
	TiXmlElement* root = doc.RootElement();
	if (root == nullptr)
		return false;
	while (root)
	{
		int size;
		root->Attribute("sizeofparty", &size);
		root->Attribute("unlockLevels",&unlockLevels);
		TiXmlElement* whoDat = root->FirstChildElement("Character");
		for (int x = 0; x < size; x++)
		{
			int lvl;
			int cNumber;
			int cID;
			const char* name = whoDat->Attribute("name");
			const char* characterClass = whoDat->Attribute("classSkill");

			whoDat->Attribute("currentlvl", &lvl);
			whoDat->Attribute("characterController", &cNumber);
			whoDat->Attribute("PlayerType", &cID);
			PlayerInfo *p = new PlayerInfo(name, characterClass, cID, cNumber, lvl);
			AddPlayerInfo(p);
			whoDat = whoDat->NextSiblingElement("Character");
		}
		break;
	}
	return true;
}