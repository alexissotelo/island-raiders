#include "Camera.h"
#include "../Source/Game.h"
#include "../SGD Wrappers/SGD_GraphicsManager.h"
#include "../SGD Wrappers/SGD_Event.h"
#include "../SGD Wrappers/SGD_Listener.h"

#include "LevelManager.h"
#include <math.h>

namespace{

	float Clamp(float x, float min, float max)
	{
		if (x < min) x = min;
		else if (x > max) x = max;
		return x;
	}
	int Clamp(int x, int min, int max)
	{
		if (x < min) x = min;
		else if (x > max) x = max;
		return x;
	}
	SGD::Point lerp(float t, SGD::Point a, SGD::Point b)
	{
		//return (1 - t)* a + t*b;
		float x = 1 - t;
		a.x *= x;
		a.y *= x;
		b.x *= t;
		b.y *= t;
		SGD::Point p = { a.x + b.x, a.y + b.y };
		return p;
	}
	float LerpFloat(float a, float b, float t)
	{
		return (1.f - t)* a + t*b;
	}
}

Camera::Camera()
{
	pPlayState = PlayGameState::GetInstance();
	levelManager = LevelManager::GetInstance();

	currentSection = levelManager->GetLevelSection(currSectionIndex);
	RegisterForEvent("CAM2SECTION");
}
Camera::~Camera()
{
	UnregisterFromEvent("CAM2SECTION");
}
void Camera::Reset()
{
	currSectionIndex = nextSectionIndex = 0;
	EnterLevelSection();
}

SGD::Point camPivot;
SGD::Point nextWaypoint;

/*virtual*/ void Camera::Update(float dt) //final
{
	SGD::Point currSectionPos = currentSection->GetPosition();
	SGD::Point nextSectionPos = nextSection->GetPosition();

	auto party = PlayGameState::GetInstance()->GetParty();
	unsigned int partySize = party.size();

	if (partySize > 1)
	{
		float x = 0.f;
		float y = 0.f;
		for (size_t i = 0; i < partySize; i++)
		{
			x += party[i]->GetPosition().x;
			y += party[i]->GetPosition().y;
		}
		x /= partySize;
		y /= partySize;

		camPivot = { x, y };
	}
	else
	{
		camPivot = party[0]->GetPosition();
	}

	//***
	float radius = 200;
	SGD::Point nextSectionCenter = nextSectionPos+ size*0.6f;
	nextWaypoint = nextSectionCenter;

	float distToNextSection = (nextSectionCenter - camPivot).ComputeLength();
	float t = radius / distToNextSection;
	if (t > 0.999f)
	{
		EnterLevelSection();
		t = 0.f;
	}
	//********

	targetPos = camPivot;
	targetPos.Offset(-GetSize().width *0.4f, 0.f);
	if (!lock || currSectionIndex >= levelManager->GetNumberOfSections())
		//lock or last section
	{
		targetPos.x = Clamp(targetPos.x, currSectionPos.x, nextSectionPos.x);
	}
	else
	{
		//curr
		float currSectionWidth = nextSectionPos.x - currSectionPos.x;
		if (currSectionWidth > 800)currSectionWidth = 800;
		targetPos.x = Clamp(targetPos.x, currSectionPos.x, nextSectionPos.x - currSectionWidth);
	}
	targetPos.y = LerpFloat(currSectionPos.y, nextSectionPos.y,t);
	position = lerp(dt, position, targetPos);
}
/*virtual*/ void Camera::Render()
{

	std::vector<Player*> party = PlayGameState::GetInstance()->GetParty();

	if (currSectionIndex > 1)//render previous section
	{
		levelManager->GetLevelSection(currSectionIndex - 2)->RenderBackground();
	}
	currentSection->RenderBackground();
	if (currentSection != nextSection)
		nextSection->RenderBackground();

	if (Game::GetInstance()->bDebug)
	{
		camPivot.Offset({ -position.x, -position.y });
		//pivot
		SGD::GraphicsManager::GetInstance()->DrawRectangle(
			SGD::Rectangle(camPivot, SGD::Size(10, 10)), {});

		SGD::GraphicsManager::GetInstance()->DrawLine(
		{ party[0]->GetPosition().x - position.x, party[0]->GetPosition().y - position.y },
		{ nextWaypoint.x - position.x, nextWaypoint.y - position.y }, { 255, 255, 0 });
	}
}
void Camera::RenderForeground()
{
	if (currSectionIndex > 1)//render previous section
	{
		levelManager->GetLevelSection(currSectionIndex - 2)->RenderForeground();
	}
	currentSection->RenderForeground();
	if (currentSection != nextSection)
		nextSection->RenderForeground();
}
/*virtual*/ void Camera::HandleEvent(const SGD::Event* pEvent) //final
{
	if (pEvent->GetEventID() == "CAM2SECTION")
	{
		//get section to which collision belongs
		//int sectionIndex = (int)pEvent->GetData();
		EnterLevelSection();
	}
}

void Camera::EnterLevelSection()
{
	int nSections = levelManager->GetNumberOfSections();
	if (currSectionIndex >= nSections)
		return;
	currentSection->UnregisterFromEvent("SPAWNENEMIES");//unregister prev section

	currentSection = levelManager->GetLevelSection(nextSectionIndex);
	if (currSectionIndex < nSections - 1)
	{
		if (++nextSectionIndex > nSections - 1)
			nextSectionIndex = nSections - 1;
	}
	nextSection = levelManager->GetLevelSection(nextSectionIndex);
	currSectionIndex++;
	levelManager->SetEnemiesLeft(currentSection->GetAmountOfEnemies());
	currentSection->RegisterForEvent("SPAWNENEMIES");
}