#include "OptionsGameState.h"
#include "BitmapFont.h"
#include "..\Source\Game.h"
#include "MenuGameState.h"
#include <sstream>

#include "../IslandRaiders/GUI.h"


OptionsGameState* OptionsGameState::pInstance = nullptr;


OptionsGameState* OptionsGameState::GetInstance(void)
{
	if (pInstance == nullptr)
		pInstance = new OptionsGameState;

	return pInstance;
}

void OptionsGameState::DeleteInstance(void)
{
	delete pInstance;
	pInstance = nullptr;
}

void OptionsGameState::Enter()
{
	pFont = GUIManager::GetInstance()->GetFont();
	pGraphics = SGD::GraphicsManager::GetInstance();
	pAudio = SGD::AudioManager::GetInstance();
	pInput = SGD::InputManager::GetInstance();
	backGround = pGraphics->LoadTexture(L"resource/Graphics/placeholder images/map.png");
	defaultX = Game::GetInstance()->GetScreenWidth();
	defaultY = Game::GetInstance()->GetScreenHeight();


}
void OptionsGameState::Exit()
{
	pGraphics->UnloadTexture(backGround);
	DeleteInstance();
}

void OptionsGameState::ArcadeCursorMovement(float dt)
{
	SGD::Vector dir = pInput->GetLeftJoystick(0);
	if (!picking)
	{
		if (dir.y != 0.0f)
		{
			timer += dt;
			if (timer > timerFreq)
			{
				timer = 0.f;
				if (dir.y < 0.0f)
				{
					timer = 0.f;
					cursor--;
					if (cursor < 0)
						cursor = 4;
				}
				if (dir.y > 0.0f)
				{
					timer = 0.f;
					cursor++;
					if (cursor > 4)
						cursor = 0;
				}

			}
		}
	}
	else
	{
		if (dir.x != 0.0f)
		{
			timer += dt;
			if (timer > timerFreq)
			{
				if (dir.x < 0.0f)
				{
					timer = 0.0f;
					volCount--;
					if (volCount < 0)
						volCount = 1;
				}
				else if (dir.x > 0.0f)
				{
					timer = 0.0f;
					volCount++;
					if (volCount > 1)
						volCount = 0;
				}

			}
		}
	}
}


bool OptionsGameState::Update(float dt)
{
	if (pInput->IsKeyPressed(SGD::Key::Escape) || pInput->IsButtonPressed(0, 2))
	{
		Game::GetInstance()->ChangeState(MenuGameState::GetInstance());
		return true;
	}
	if (Game::GetInstance()->ARCADEMODE)
	{
		ArcadeCursorMovement(dt);
	}
	else// if (!Game::GetInstance()->ARCADEMODE)
	{
		if (!picking)
		{
			if (pInput->IsKeyPressed(SGD::Key::Down) == true || pInput->IsDPadPressed(0, SGD::DPad::Down))
			{
				cursor++;
				if (cursor > 2)
					cursor = 0;
			}

			if (pInput->IsKeyPressed(SGD::Key::Up) == true || pInput->IsDPadPressed(0, SGD::DPad::Up))
			{
				cursor--;
				if (cursor < 0)
					cursor = 2;
			}
		}
		else
		{
			if (pInput->IsKeyPressed(SGD::Key::Left) == true || pInput->IsDPadPressed(0, SGD::DPad::Left))
			{
				volCount++;
				if (volCount > 1)
					volCount = 0;
			}

			if (pInput->IsKeyPressed(SGD::Key::Right) == true || pInput->IsDPadPressed(0, SGD::DPad::Right))
			{
				volCount--;
				if (volCount < 0)
					volCount = 1;
			}
		}
	}
	if (picking)
	{
		if (pInput->IsKeyPressed(SGD::Key::Enter) == true || pInput->IsButtonPressed(0, 1))
		{
			if (cursor == 0)
			{
				if (volCount == 0)
				{
					musicVolPlus = pAudio->GetMasterVolume(SGD::AudioGroup::Music);
					musicVolPlus += 10;
					if (musicVolPlus > 100)
						musicVolPlus = 100;
					pAudio->SetMasterVolume(SGD::AudioGroup::Music, musicVolPlus);
				}
				else if (volCount == 1)
				{
					musicVolPlus = pAudio->GetMasterVolume(SGD::AudioGroup::Music);
					musicVolPlus -= 10;
					if (musicVolPlus < 0)
						musicVolPlus = 0;
					pAudio->SetMasterVolume(SGD::AudioGroup::Music, musicVolPlus);
				}
			}
			else if (cursor == 1)
			{
				if (volCount == 0)
				{
					soundEffectVolPlus = pAudio->GetMasterVolume(SGD::AudioGroup::SoundEffects);

					soundEffectVolPlus += 10;
					if (soundEffectVolPlus > 100)
						soundEffectVolPlus = 100;
					pAudio->SetMasterVolume(SGD::AudioGroup::SoundEffects, soundEffectVolPlus);
				}
				else if (volCount == 1)
				{
					soundEffectVolPlus = pAudio->GetMasterVolume(SGD::AudioGroup::SoundEffects);
					soundEffectVolPlus -= 10;
					if (soundEffectVolPlus < 0)
						soundEffectVolPlus = 0;
					pAudio->SetMasterVolume(SGD::AudioGroup::SoundEffects, soundEffectVolPlus);
				}
			}
			else if (cursor == 2)
			{
				if (volCount == 0)
				{
					fullscreen = false;
					pGraphics->Resize(SGD::Size{ defaultX, defaultY }, fullscreen);
				}
				else if (volCount == 1)
				{
					fullscreen = true;
					pGraphics->Resize(SGD::Size{ 800, 600 }, fullscreen);
				}
			}
		}
	}

	if (pInput->IsKeyPressed(SGD::Key::Enter) == true || pInput->IsButtonPressed(0, 1))
		picking = true;
	if (pInput->IsKeyPressed(SGD::Key::T) == true || pInput->IsButtonPressed(0, 2))
		picking = false;
	return true;
}
void OptionsGameState::Render(float dt)
{
	pGraphics->DrawTexture(backGround, { 0.0f, 0.0f }, {}, {}, {}, { 4.0f, 4.0f });

	float width = Game::GetInstance()->GetScreenWidth();
	pFont->DrawXML("Options Screen", { 50, 50 }, 2.0f, { 255, 255, 255 });
	pFont->DrawXML("Background Sound", { 50, 125 }, 1.0f, { 255, 255, 255 });
	pFont->DrawXML("Sound Effects", { 50, 200 }, 1.0f, { 255, 255, 255 });
	pFont->DrawXML("FullScreen", { 50, 275 }, 1.0f, { 255, 255, 255 });
	std::stringstream stream;
	stream << musicVolPlus;
	std::stringstream stream2;
	stream2 << soundEffectVolPlus;



	if (!picking)
		pFont->DrawXML("X", SGD::Point{ 25.0f, 125.0f + 75 * cursor }, 1.0f, { 255, 255, 255 });
	else
	{
		pFont->DrawXML("X", SGD::Point{ 80.0f + 300.0f * volCount, 155.0f + 75.0f * cursor }, 1.0f, { 255, 255, 255 });
		pFont->DrawXML("+", SGD::Point{ 115.0f, 155.0f }, 1.0f, { 255, 255, 255 });
		pFont->DrawXML("+", SGD::Point{ 115.0f, 225.0f }, 1.0f, { 255, 255, 255 });
		pFont->DrawXML("-", SGD::Point{ 350.0f, 155.0f }, 1.0f, { 255, 255, 255 });
		pFont->DrawXML("-", SGD::Point{ 350.0f, 225.0f }, 1.0f, { 255, 255, 255 });
		pFont->DrawXML("On", SGD::Point{ 115.0f, 300.0f }, 1.0f, { 255, 255, 255 });
		pFont->DrawXML("Off", SGD::Point{ 325.0f, 300.0f }, 1.0f, { 255, 255, 255 });
		pFont->DrawXML(stream.str().c_str(), SGD::Point{ 233.0f, 155.0f }, 1.0f, { 255, 255, 255 });
		pFont->DrawXML(stream2.str().c_str(), SGD::Point{ 233.0f, 225.0f }, 1.0f, { 255, 255, 255 });



	}
}

