#include "EnemyAI.h"
#include "Enemy.h"
#include "BossAI.h"
#include "IdleAI.h"
#include "PursuitAI.h"
#include "CombatAI.h"
#include "DefenseAI.h"
#include "KnockbackAI.h"
#include "FlinchAI.h"
#include "FleeAI.h"
#include "LoadingManager.h"

#include "PlayGameState.h"
#include "EntityManager.h"
#include "../SGD Wrappers/SGD_Geometry.h"


/**************************************************************
|	Function: AIStamp Constructor [default]
|	Purpose: allocate an object into memory and initialiize all variables
**************************************************************/
AIStamp::AIStamp(void)
{
	//null out all
	for (unsigned int i = 0; i < 3; ++i)
	{
		enemySkills[i].first = nullptr;
		enemySkills[i].second = 0.0f;
	}
}

/**************************************************************
|	Function: AIStamp Destructor [default]
|	Purpose: deallocate an obejct out of memory and delete all dynamic members
**************************************************************/
AIStamp::~AIStamp(void)
{
	//no dynamic members
	SetAttackSound(nullptr);
}

/**************************************************************
|	Function: AIStamp::SwitchAIMode
|	Purpose: switch an enemies state and reset all timers when
|			switched ti another state
|	Parameter:
|			EnemyAIType _aiType -  an enumeration value to switch
|								the enemy state to
|	Return Value: void
**************************************************************/
void AIStamp::SwitchAIMode(EnemyAIType _aiType)
{
	//no resetting
	if (aiType == _aiType)
		return;

	//change the AI type
	aiType = _aiType;
	//reset the timers
	fPause = -1.0f; //a pause timer for the enmy while 
	fStartTimer = 0.0f;
	owner->GetTimeStamp()->m_clrRender = { 255, 255, 255, 255 };
}

void AIStamp::SetAttackSound(const char* const _sound)
{
	if (attackSoundFilePath)
		delete [] attackSoundFilePath;

	if (_sound)
	{
		//set the name
		int size = strlen(_sound) + 1;
		attackSoundFilePath = new char[size];
		strcpy_s(attackSoundFilePath, size, _sound);
	}

}

/**************************************************************
|	Memeber: AIManager::pInstance
|	Purpose: an instance of the manager that is the only way of accessing
|			class members
**************************************************************/
AIManager* AIManager::pInstance = nullptr;

/**************************************************************
|	Function: AIManager::GetInstance [static]
|	Purpose: creates a new instance of the AI Manager only if one
|			one does not exist, otherwise return the instance of the class
|	Parameter: void
|	Return Value: AIManager* - pointer to the manager class
**************************************************************/
AIManager* AIManager::GetInstance(void)
{
	if (!pInstance)
		pInstance = new AIManager();

	return pInstance;
}

/**************************************************************
|	Function: AIManager::DeleteInstance [static]
|	Purpose: remove from memory the AI Manager instance and reset
|			to NULL
|	Parameter: void
|	Return Value: void
**************************************************************/
void AIManager::DeleteInstance(void)
{
	delete pInstance;
	pInstance = nullptr;
}

//update - update all enemies 
/**************************************************************
|	Function: AIManager::Update
|	Purpose: updates all enemy entities using their AIStamp as a
|			reference of their current status
|	Parameter:
|			float dt - the tiem elapsed since last updating
|			AIStamp& stamp - a refernce to the current status
|							of a character
|	Return Value: void
**************************************************************/
void AIManager::Update(float dt, AIStamp& stamp)
{
	vEnemyAI[stamp.aiType]->Update(dt, stamp);
}

/**************************************************************
|	Function: AIManager::Initialize
|	Purpose: creates all instances of an AI and stores it within
|			the managers container
|	Parameter: void
|	Return Value: void
**************************************************************/
void AIManager::Initialize(void)
{
	
	EnemyAI* ptr = nullptr;

	//boss	
	ptr = new BossAI();
	pInstance->vEnemyAI[ptr->GetAIType()] = ptr;

	//flinching
	ptr = new FlinchAI();
	pInstance->vEnemyAI[ptr->GetAIType()] = ptr;

	//knockback
	ptr = new KnockbackAI();
	pInstance->vEnemyAI[ptr->GetAIType()] = ptr;

	//knockback
	ptr = new StunAI();
	pInstance->vEnemyAI[ptr->GetAIType()] = ptr;

	//idle
	ptr = new IdleAI();
	pInstance->vEnemyAI[ptr->GetAIType()] = ptr;

	//pursuit
	ptr = new PursuitAI();
	pInstance->vEnemyAI[ptr->GetAIType()] = ptr;

	//pursuit
	ptr = new CombatAI();
	pInstance->vEnemyAI[ptr->GetAIType()] = ptr;

	//defense
	ptr = new DefenseAI();
	pInstance->vEnemyAI[ptr->GetAIType()] = ptr;

	//flee
	ptr = new FleeAI();
	pInstance->vEnemyAI[ptr->GetAIType()] = ptr;

	//inform a thread is done
	ThreadData* threadData = ThreadData::GetInstance();
	threadData->countMutex.lock();
	--threadData->numberOfThreads;
	threadData->countCondition.notify_all();
	threadData->countMutex.unlock();
}

/**************************************************************
|	Function: AIManager::Terminate
|	Purpose: to delete from memory all AI instances/types
|	Parameter: void
|	Return Value: void
**************************************************************/
void AIManager::Terminate(void)
{
	for (unsigned int index = 0; index < (unsigned int)EnemyAIType::Count; ++index)
	{
		delete vEnemyAI[(EnemyAIType)index];
		vEnemyAI[(EnemyAIType)index] = nullptr;
	}

	vEnemyAI.clear();
}

/**************************************************************
|	Function: AIManager::FindTarget [static]
|	Purpose: finds a player entity of the preferred type and sets
|			the enemy target if the preferred is found, otherwise
|			the enemy targets a random character
|	Parameter:
|			AIStamp& _aiStamp - a refernce to the current state of the
|							enemy containing the preferred character type
|							and target to be set
|	Return Value: void
**************************************************************/
bool AIManager::FindPreferredTarget(AIStamp& _aiStamp)
{
	//local game pointer
	PlayGameState* pGame = PlayGameState::GetInstance();

	//search for a preferred player
	Entity* prefPlayer = nullptr;

	//vector for all available preferred 
	std::vector<int> peeps;
	//..within the vector of players
	for (unsigned int i = 0; i < pGame->GetParty().size(); ++i)
	{
		//current player
		Player* currPlayer = dynamic_cast<Player*>(pGame->GetPartyMember(i));

		//chase em?
		if ((PlayerType)currPlayer->GetPlayerType() == _aiStamp.preferedTarget)
			peeps.push_back(i);
	}

	//player to target
	int  playerToTarget;

	if (peeps.size() == 0)
		//nobody I wanted?
		return false;

	//found someone I like?
	playerToTarget = rand() % peeps.size();
	//set the preferred player
	prefPlayer = pGame->GetPartyMember(playerToTarget);
	//set the player
	SetTarget(prefPlayer, _aiStamp);

	//success!
	return true;
}

/**************************************************************
|	Function: AIManager::SetTarget [static]
|	Purpose: set the enemies target without altering any reference counting
|	Parameter:
|				Entity* _target - a pointer to the character to target
|				AIStamp& stamp -  a structure containing the target to set
|	Return Value:
**************************************************************/
void AIManager::SetTarget(Entity* _target, AIStamp& stamp)
{
	//NO REF COUNTING!!

	//set the target reference
	stamp.target = _target;
}

/**************************************************************
|	Function: AIManager::FindClosestTarget [static]
|	Purpose: to locate the closest player in the area and set the
|			enemy target
|	Parameter:
|				AIStamp& stamp - a refernce holding the target pointer to set
|	Return Value: returns false when no player is found, otherwise return true
**************************************************************/
bool AIManager::FindClosestTarget(AIStamp& stamp)
{
	//local game pointer
	PlayGameState* pGame = PlayGameState::GetInstance();

	//vector for all available preferred 
	std::vector<int> peeps;
	//vector of enemies
	EntityManager* localEnt = PlayGameState::GetInstance()->GetManager();
	std::vector<IEntity*> vOpp = (stamp.owner->GetType() == ENT_ENEMY) ? localEnt->GetEntityVector(ENT_PLAYER) : localEnt->GetEntityVector(ENT_ENEMY);

	//..within the vector of players
	for (unsigned int i = 0; i <vOpp.size(); ++i)
	{
		//current player
		Character* currPlayer = dynamic_cast<Character*>(vOpp[i]);

		//create a vector 
		SGD::Vector toPlayer = currPlayer->GetPosition() - stamp.owner->GetPosition();

		//calculate the length
		float distance = toPlayer.ComputeLength();

		//within distance?
		float range = 800.f;// (stamp.combatType == CombatType::Melee && stamp.tier < 2) ? 200.f : 450.f;
		if (distance <= range)
			peeps.push_back(i);
	}

	if (peeps.size() == 0)
		return false;

	//player to target
	int  playerToTarget;
	//found someone I like?
	playerToTarget = rand() % peeps.size();

	//
	int targets = peeps[playerToTarget];

	//set the player = 
	AIManager::SetTarget((Entity*)vOpp[targets], stamp);

	//success
	return true;
}
/**************************************************************
|	Function: AIManager::FindWeakestTarget [static]
|	Purpose: to locate the weakest player in the party and set the
|			enemy target, health based on ratio not actual value
|	Parameter:
|				AIStamp& stamp - a refernce holding the target pointer to set
|	Return Value: returns false when no player is found, otherwise return true
**************************************************************/
bool AIManager::FindWeakestTarget(AIStamp& stamp)
{
	//local game pointer
	PlayGameState* pGame = PlayGameState::GetInstance();

	//vector for all available preferred 
	float lowest = 1.0f; //fully healed character
	int lowestOwner = -1; //no player

	//vector of enemies
	EntityManager* localEnt = PlayGameState::GetInstance()->GetManager();
	std::vector<IEntity*> vOpp = (stamp.owner->GetType() == ENT_ENEMY) ? localEnt->GetEntityVector(ENT_PLAYER) : localEnt->GetEntityVector(ENT_ENEMY);
	

	//..within the vector of players
	for (unsigned int i = 0; i < vOpp.size(); ++i)
	{
		//current player
		Character* currPlayer = dynamic_cast<Character*>(vOpp[i]);

		//calculate the ratio of the player health
		float hp = currPlayer->GetHealth() / currPlayer->GetMaxHealth();

		//lower?
		if (hp < lowest)
		{
			//record the lowest
			lowest = hp;
			lowestOwner = i;
		}
	}

	//not found?
	if (lowest == 1.0f || lowestOwner == -1)
		return false;

	//set the player
	AIManager::SetTarget((Entity*)vOpp[lowestOwner], stamp);

	//success
	return true;
}

#if 0

bool AIManager::FindClosestEnemy(AIStamp& stamp)

{
	//local game pointer
	PlayGameState* pGame = PlayGameState::GetInstance();

	//vector for all available preferred 
	std::vector<int> peeps;
	//..within the vector of players
	for (unsigned int i = 0; i < pGame->GetParty().size(); ++i)
	{
		//current player
		Player* currPlayer = dynamic_cast<Player*>(pGame->GetPartyMember(i));

		//create a vector 
		SGD::Vector toPlayer = currPlayer->GetPosition() - stamp.owner->GetPosition();

		//calculate the length
		float distance = toPlayer.ComputeLength();

		//within distance?
		float range = (stamp.combatType == CombatType::Melee) ? 200.f : 450.f;
		if (distance <= range)
			peeps.push_back(i);

	}

	if (peeps.size() == 0)
		return false;

	//player to target
	int  playerToTarget;

	//found someone I like?
	playerToTarget = rand() % peeps.size();

	//set the player
	AIManager::SetTarget(pGame->GetPartyMember(playerToTarget), stamp);

	//success
	return true;
}

bool AIManager::FindWeakestEnemy(AIStamp& stamp)
{
	//local game pointer
	PlayGameState* pGame = PlayGameState::GetInstance();

	//vector for all available preferred 
	float lowest = 1.0f; //fully healed character
	int lowestOwner = -1; //no player

	//..within the vector of players
	for (unsigned int i = 0; i < pGame->GetParty().size(); ++i)
	{
		//current player
		Player* currPlayer = dynamic_cast<Player*>(pGame->GetPartyMember(i));

		//calculate the ratio of the player health
		float hp = currPlayer->GetHealth() / currPlayer->GetMaxHealth();

		//lower?
		if (hp < lowest)
		{
			//record the lowest
			lowest = hp;
			lowestOwner = i;
		}
	}

	//not found?
	if (lowest == 1.0f || lowestOwner == -1)
		return false;

	//set the player
	AIManager::SetTarget(pGame->GetPartyMember(lowestOwner), stamp);

	//success
	return true;
}
#endif
