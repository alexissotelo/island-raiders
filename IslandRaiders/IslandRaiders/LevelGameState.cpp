#include "LevelGameState.h"
#include "BitmapFont.h"
#include "..\Source\Game.h"
#include "PlayGameState.h"
#include "SaveLoadState.h"
#include "Player.h"
#include "GUI.h"
#include "GameInfo.h"
#include "AudioQueue.h"

LevelGameState* LevelGameState::pInstance = nullptr;


LevelGameState* LevelGameState::GetInstance(void)
{
	if (pInstance == nullptr)
		pInstance = new LevelGameState;

	return pInstance;
}
void LevelGameState::DeleteInstance(void)
{
	delete pInstance;
	pInstance = nullptr;
}

void LevelGameState::Enter()
{
	pFont = GUIManager::GetInstance()->GetFont();
	pGraphics = SGD::GraphicsManager::GetInstance();
	pAudio = SGD::AudioManager::GetInstance();
	pInput = SGD::InputManager::GetInstance();
	backGround = pGraphics->LoadTexture(L"resource/Graphics/placeholder images/WORLDMAP.png");
	bird = pGraphics->LoadTexture(L"resource/Graphics/placeholder images/BIRD.png");
	shipIcon = pGraphics->LoadTexture(L"resource/Graphics/placeholder images/COLORSHIPICON.png");
	CursorIcon = pGraphics->LoadTexture(L"resource/Graphics/placeholder images/ARROW.png");
	grayShipIcon = pGraphics->LoadTexture(L"resource/Graphics/placeholder images/BLACKSHIPICON.png");

	//AudioQueue::GetInstance()->AddSound(pAudio->LoadAudio(L"resource/Audio/BackGroundMusic/BACKGROUND_PIRATE.xwm"));

	// way points for the bird to travel too
	// each subscript is the level 
	// [0] = Training Level ... [5] = Kraken Level
	waypoints[0] = SGD::Point(72, 325);//training
	waypoints[1] = SGD::Point(216, 423);//fire
	waypoints[2] = SGD::Point(600, 346);//ice
	waypoints[3] = SGD::Point(227, 83);//poison
	waypoints[4] = SGD::Point(467, 447);//boss lvl

	cursorPoints[0] = SGD::Point(72, 335);
	cursorPoints[1] = SGD::Point(216, 433);
	cursorPoints[2] = SGD::Point(600, 356);
	cursorPoints[3] = SGD::Point(227, 93);
	cursorPoints[4] = SGD::Point(467, 457);

#if UNLOCK_ALL_LEVELS
	maxLvlCanPick = 4; cursor = 0;
#else
	maxLvlCanPick = GameInfo::GetInstance()->GetUnlockLevels();
	cursor = maxLvlCanPick;
#endif
}

void LevelGameState::ArcadeCursorMovement(float dt)
{
	SGD::Vector dir = pInput->GetLeftJoystick(0);
	if (dir.y != 0)
	{
		timer += dt;
		if (timer > timerFreq)
		{
			timer = 0.0f;
			if (dir.y < 0)
			{
				timer = 0.0f;
				cursor--;
				if (cursor < 0)
					cursor = 0;
				timer = 0.0f;
			}
			else if (dir.y > 0)
			{
				timer = 0.f;
				cursor++;
				if (cursor > maxLvlCanPick)
					cursor = maxLvlCanPick;
				timer = 0.0f;
			}

		}
	}
}

void LevelGameState::Exit()
{
	//set the level
	GameInfo::GetInstance()->SetWorldLevel(lvlPicked);

	//unload all textures
	pGraphics->UnloadTexture(backGround);
	pGraphics->UnloadTexture(bird);
	pGraphics->UnloadTexture(shipIcon);
	pGraphics->UnloadTexture(CursorIcon);
	pGraphics->UnloadTexture(grayShipIcon);

	//delete
	DeleteInstance();

	//no more sound
	AudioQueue::GetInstance()->clear();


}
bool LevelGameState::Update(float dt)
{
	if (!startMovement)
	{
		if (Game::GetInstance()->ARCADEMODE)
		{
			ArcadeCursorMovement(dt);
		}
		else
		{
			if (pInput->IsDPadPressed(0, SGD::DPad::Left) || pInput->IsKeyPressed(SGD::Key::A))
			{
				cursor--;
				if (cursor < 0)
					cursor = 0;
			}

			if (pInput->IsDPadPressed(0, SGD::DPad::Right) || pInput->IsKeyPressed(SGD::Key::D))
			{
				cursor++;
				if (cursor > maxLvlCanPick)
					cursor = maxLvlCanPick;
			}
		}

		if (pInput->IsButtonPressed(0, 1) || pInput->IsKeyPressed(SGD::Key::Enter))
		{
			startMovement = true;
			lvlPicked = cursor;
		}

		if (pInput->IsButtonPressed(0, 2) || pInput->IsKeyPressed(SGD::Key::Escape))
		{
			Game::GetInstance()->ChangeState(SaveLoadState::GetInstance());
			return true;
		}
	}
	if (startMovement)
		MoveBird(dt);

	if (changeState)
		Game::GetInstance()->ChangeState(PlayGameState::GetInstance());
	return true;//keep playing

}

void LevelGameState::MoveBird(float dt)
{
	float distanceBetween;
	if (currPoint + 1 > 5)
		currPoint = -1;
	//starting level = 0 : training level
	int x = currPoint;
	if (cursor == 0)
	{
		position = lerp(dt, position, waypoints[lvlPicked]);
		distanceBetween = (waypoints[currPoint] - position).ComputeLength();
		if (distanceBetween < 20.0f)
		{
			currPoint++;
			startMovement = false;
			changeState = true;
		}
	}
	else // if it is any level but level 0 : training level
	{
		position = lerp(dt, position, waypoints[lvlPicked]);
		distanceBetween = (waypoints[lvlPicked] - position).ComputeLength();
		if (distanceBetween < 20.0f)
		{
			currPoint++;
			if (currPoint == cursor)
			{
				startMovement = false;
				changeState = true;
			}
			else
			{
				MoveBird(dt);
			}
		}
	}
}

//void LevelGameState::DoTheMovin()

void LevelGameState::Render(float dt)
{
	pGraphics->DrawTexture(backGround, SGD::Point(0, 0));
	for (int i = 0; i < 5; i++)
	{
		if (i <= maxLvlCanPick)
			pGraphics->DrawTexture(shipIcon, waypoints[i], 0.0f, {}, {}, { .5f, .5f });
		else
			pGraphics->DrawTexture(grayShipIcon, waypoints[i], 0.0f, {}, {}, { .5f, .5f });

	}
	pGraphics->DrawTexture(CursorIcon, cursorPoints[cursor], 0.0f, {}, {}, { .7f, .7f });

	if (startMovement)
		pGraphics->DrawTexture(bird, position);
}

SGD::Point LevelGameState::lerp(float t, SGD::Point a, SGD::Point b)
{
	//return (1 - t)* a + t*b;
	float x = 1 - t;
	a.x *= x;
	a.y *= x;
	b.x *= t;
	b.y *= t;
	SGD::Point p = { a.x + b.x, a.y + b.y };
	return p;
}