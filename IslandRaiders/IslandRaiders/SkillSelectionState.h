#pragma once
#include "IGameState.h"
#include "../SGD Wrappers/SGD_AudioManager.h"
#include "../SGD Wrappers/SGD_GraphicsManager.h"
#include "../SGD Wrappers/SGD_InputManager.h"
#include <vector>

class BitmapFont;
class Player;
class SkillSelectionState : public IGameState
{
public:
	static SkillSelectionState* GetInstance();

	static void DeleteInstance(void);
	virtual void Enter();
	virtual void Exit();
	virtual bool Update(float dt);
	virtual void Render(float dt);
	//Skills
	void UpdateSkills(int who);
	void UpdateSkillOne(unsigned int level);
	void UpdateSkillTwo(unsigned int level);
	void UpdateSkillThree(unsigned int level);
	void SetWayPoints(void);
	void UnloadTextures(void);
	void ArcadeCursorMovement(float dt);
	bool ArcadeCharacterSelection(float dt);

	//void UpdateParty(std::vector<Character*> _party);
private:
	float timer = 0.0f;
	float timerFreq = 0.4f;
	static SkillSelectionState* pInstance;
	SGD::GraphicsManager* pGraphics = nullptr;
	SGD::AudioManager* pAudio = nullptr;
	SGD::InputManager* pInput = nullptr;
	const BitmapFont* pFont = nullptr;

	SGD::HTexture icons[6];
	SGD::HTexture backGround = SGD::INVALID_HANDLE;
	SGD::HTexture skillTreeSword = SGD::INVALID_HANDLE;
	SGD::HTexture skillTreeMage = SGD::INVALID_HANDLE;
	SGD::HTexture skillTreeGunner = SGD::INVALID_HANDLE;
	std::vector<SGD::HTexture> partyIMG;
	std::vector<SGD::HTexture> skillIMG;
	SGD::HTexture redX;
	SGD::HTexture redBox;
	//std::vector<Player*> copyParty;
	std::vector<unsigned int> partyType;


	SGD::Point waypoints[2];
	SGD::Point waypointsTierTwoLeft[2];
	SGD::Point waypointsTierTwoRight[2];
	bool tierOne = false;
	bool tierTwoLeft = false;
	bool tierTwoRight = false;

	bool skillSelectConfirm = false;
	int	skillSelectCursor = 0;

	float offSet = 0.0f;
	int curPicker = 0;
	int numPlayers;
	int numPlayersConfirmed;
	int cursor = 0;

	void DisplayText(int who, int what, SGD::Point SP);

	SkillSelectionState() = default;
	~SkillSelectionState() = default;
	SkillSelectionState(const SkillSelectionState&) = delete;
	SkillSelectionState operator=(const SkillSelectionState&) = delete;
};


