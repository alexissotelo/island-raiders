#include "CreateItemMessage.h"
#include "MessageID.h"
#include "Enemy.h"

CreateItemMessage::CreateItemMessage(Enemy* e)
: SGD::Message(MessageID::MSG_CREATE_ITEM)
{
	postMan = e;
	postMan->AddRef();

}


CreateItemMessage::~CreateItemMessage()
{
	postMan->Release();
	postMan = nullptr;
}
