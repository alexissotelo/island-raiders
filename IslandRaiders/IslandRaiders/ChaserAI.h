#pragma once
#include "EnemyAI.h"
#include "Player.h"

class Enemy;

class ChaserAI : public EnemyAI
{
public:
	//ctor and dtor
	ChaserAI(); //consturctor demanding an owner and type of a preferred player
	virtual ~ChaserAI(); //destroy any dynamic memory

	//update - must override
	virtual void Update(float dt, AIStamp& stamp);
	//ai identifier
	virtual EnemyAIType GetAIType() const { return EnemyAIType::Chaser; } //enemy action identifier

private:
	//helper functions
	void MeleeChase(float dt, AIStamp& stamp);
	void RangeChase(float dt, AIStamp& stamp);
};

