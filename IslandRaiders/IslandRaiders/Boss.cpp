#include "Boss.h"

#include "../SGD Wrappers/SGD_MessageManager.h"
#include "../SGD Wrappers/SGD_EventManager.h"
#include "../SGD Wrappers/SGD_AudioManager.h"
#include "../SGD Wrappers/SGD_Event.h"

#include "AudioQueue.h"
#include "CreateAnimatedParticle.h"
#include "AnimatedParticle.h"
#include "GUI.h"

#include <sstream>

Boss::Boss(const char* name) : Enemy(name)
{
	RegisterForEvent("WARP");

	if (GetName() == "infernokong")
		aTimeStamp.SwitchAnimation("roar");

}


Boss::~Boss()
{
}

bool Boss::HandleCollision(const IEntity* ptr)
{
	if (ptr->GetType() != ENT_PLAYER)
		return false;

	const Player* other = dynamic_cast<const Player*>(ptr);

	if (!other)
		return false;

	bool returnValue = false;

	SGD::Rectangle attack = this->GetActiveRect();
	SGD::Rectangle collide = other->GetCollisionRect();

	if (attack.IsIntersecting(collide))
	{
#pragma region Deal Damage

		//boost of the attack damage
		float boost = stat.bQuickAttack ? stat.quickPercent : stat.heavyPercent;
		//random chance of critically hitting 
		bool criticalhit = (rand() % 5 == 0);
		boost *= criticalhit ? 1.5f : 1.f;
		//attack damage based on attack bonuses
		float atkDamage = stat.damage * boost;

		//create a damage event
		SGD::Event dmg = { "DAMAGE", &atkDamage, this };
		//send it out
		dmg.SendEventNow(other);

		++aTimeStamp.m_CurrentFrame;
		aTimeStamp.m_fFrameTimer = 0.f;

#pragma endregion
#pragma region Sandwarp

		if (aTimeStamp.m_sAction == "sandwarp")
		{
			SGD::Event toss = { "KNOCKBACK", nullptr, this };
			toss.SendEventNow(other);

			++aTimeStamp.m_CurrentFrame;
			aTimeStamp.m_fFrameTimer = 0.0f;

			returnValue = true;
		}
#pragma endregion

	}
	return returnValue;
}

void Boss::HandleEvent(const SGD::Event* pEvent)
{

#pragma region Damage

	//hurt
	if (pEvent->GetEventID() == "DAMAGE")
	{

#pragma region Take Damage
		//deal damage to the boss
		AttackInfo *atkInfo = (AttackInfo*)pEvent->GetData();
		float atk = (atkInfo->damage > 0) ? atkInfo->damage : -atkInfo->damage;

		//defending?
		if (this->aTimeStamp.m_sAction == "guard")
		{
			atk = atk * (1 - stat.defense);
			stat.health -= atk;
			stat.stamina -= 0.2f;
			AudioQueue::GetInstance()->AddSound(SGD::AudioManager::GetInstance()->LoadAudio(L"resources/audio/sound effects/OnHit2.wav"));

		}
		else
		{
			//send it to th correct character
			stat.health -= atk;
			//cap at 1.0f
			if (stat.stamina > 1.0f)
				stat.stamina = 1.0f;
			//play a sound
			AudioQueue::GetInstance()->AddSound(SGD::AudioManager::GetInstance()->LoadAudio(L"resources/audio/sound effects/OnHit.wav"));

		}

		//POP UP DAMAGE
		SGD::Color color = { 255, 255, 0 };
		float scale = 0.75f;
		if (atkInfo->isCriticalHit)
		{
			color = { 255, 0, 0 };
			scale = 1.1f;
		}

		std::stringstream txt;
		txt << (int)atk;
		SGD::Point pos = GetPosition();
		pos.Offset(0, -GetRenderRect().ComputeHeight() * 0.75f);

		GUIManager::GetInstance()->PopUpFade(
			txt.str().c_str(), pos, scale, 1.2f, color);

#pragma endregion

#pragma region Flinch

		//bosses should not flinch
		if (GetName() != "infernokong" && GetName() != "icedragonking" && GetName() != "drake"
			&& GetName() != "leviathan" && GetName() != "crocodile")
		{
			//not flinching?
			if (aiStamp.aiType != EnemyAIType::Flinch)
				//flinch
				aiStamp.SwitchAIMode(EnemyAIType::Flinch);

			//flinching?
			else if (aiStamp.aiType == EnemyAIType::Flinch)
			{
				//randomly respond
				int respond = rand() % 3;

				//fall
				if (!respond)
					//flinch
					aiStamp.SwitchAIMode(EnemyAIType::Knockback);
				//throw hands
				else
					//flinch
					aiStamp.SwitchAIMode(EnemyAIType::Combat);

			}
			else if (aiStamp.aiType == EnemyAIType::Stun)
				//flinch
				aiStamp.SwitchAIMode(EnemyAIType::Knockback);

			//stop abilities
			for (unsigned int i = 0; i < 3; ++i)
			{
				if (aiStamp.enemySkills[i].second < 0.0f)
					aiStamp.enemySkills[i].second = 10.0f;
			}
		}
#pragma endregion

	}

#pragma endregion

#pragma region Knockback
	//knocked tf out
	else if (pEvent->GetEventID() == "KNOCKBACK")
	{
		//no knockbacks
		if (aiStamp.aiType == EnemyAIType::Stun)
			aiStamp.SwitchAIMode(EnemyAIType::Knockback);
		else
			GetCharacterStatus()->StartEffect(Status::STUN, 3.0f, 0.f);

		//aiStamp.SwitchAIMode(EnemyAIType::Stun);

	}
#pragma endregion

#pragma region MARKED

	else if (pEvent->GetEventID() == "MARKED")
	{
		marked = !marked;
	}

#pragma endregion

#pragma region DEATH

	//a player died
	else if (pEvent->GetEventID() == "PLAYER_DEATH")
	{
		AIStamp* stamp = GetAIStamp();
		if (stamp->target == pEvent->GetSender())
		{
			stamp->target = nullptr;
			stamp->SwitchAIMode(EnemyAIType::Idle);
		}
	}

#pragma endregion

#pragma region POKE
	if (pEvent->GetEventID() == "POKE")
	{
		AnimatedParticle* particle = (AnimatedParticle*)pEvent->GetSender();

		if (particle->GetName() == "sandspike")
		{
			//send damage
			SGD::Event damage = { "DAMAGE",&stat.damage, this };
			damage.QueueEvent();
		}
	}
#pragma endregion

#pragma region Status

		else if (pEvent->GetEventID() == "POISON")
		{
			float* effDamage = (float*)pEvent->GetData();
			GetCharacterStatus()->StartEffect(Status::POISON, effDamage[1], effDamage[0]);
		}

		else if (pEvent->GetEventID() == "STUN")
		{
			float* effDamage = (float*)pEvent->GetData();
			GetCharacterStatus()->StartEffect(Status::STUN, *effDamage * 0.5f, 0.f);
		}
#pragma endregion

#pragma region Stamina Check

		//stamina check
		if (stat.stamina <= 0.0f)
		{
			///stun
			aiStamp.SwitchAIMode(EnemyAIType::Stun);
			//reset stamina
			stat.stamina = 1.0f;
		}

#pragma endregion

#pragma region Crocodile

		if (pEvent->GetEventID() == "WARP")
		{
			SGD::Vector toPlayer;
			SGD::Point ptSpawn;
			if (aiStamp.target)
			{
				toPlayer = aiStamp.target->GetPosition() - GetPosition();
				ptSpawn = aiStamp.target->GetPosition();
			}
			else
			{
				ptSpawn = GetPosition();
				toPlayer = { 1.f, 0.f };
			}

			//offset
			if (toPlayer.x < 0)
				ptSpawn.x -= 1.0f;
			else if (toPlayer.x > 0)
				ptSpawn.x += 1.0f;

			//warp to the target
			SetPosition(ptSpawn);

			//leave marker
			CreateAnimatedParticle* msg = new CreateAnimatedParticle(this, "indicator", 1.5f, { 200, 255, 255, 255 }, ptSpawn);
			SGD::MessageManager::GetInstance()->QueueMessage(msg);
		}


#pragma endregion

		if (stat.health < 0.0f)
			stat.health = 0.0f;

	}

