#pragma once
#include "../SGD Wrappers/SGD_Message.h"
#include "../SGD Wrappers/SGD_Geometry.h"

class Character;
class CreateFamiliar : public SGD::Message
{
public:
	CreateFamiliar(const char* name, Character* owner, SGD::Point position);
	~CreateFamiliar();

	Character* GetOwner(void) const { return m_pOwner; }
	const char* GetName(void) const { return m_cName; }
	SGD::Point GetPosition(void) const { return m_ptPosition; }
private:
	Character* m_pOwner;
	SGD::Point m_ptPosition;
	char* m_cName;
};

