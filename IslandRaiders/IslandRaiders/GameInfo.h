#pragma once
#include <vector>

class Entity;
class PlayerInfo
{
	int m_numController = -1; //bad controller
	int characterID = -1; //fuck
	char* m_cName = nullptr; //name of the character
	char* m_classSkill = nullptr; //name of the player class
	unsigned int m_playerLevel = 1; //current level of the player
	unsigned char m_cSkill = 1;
	int choice;

public:
	//constructor for simplicity sake
	//PlayerInfo();
	~PlayerInfo();
	PlayerInfo(const PlayerInfo&);
	PlayerInfo& operator=(const PlayerInfo&);
	PlayerInfo(const char* characterName, const char* className, int ID, int contoller, unsigned int level);

	//accessors
	int GetContollerNumber(void) const { return m_numController; }
	unsigned int GetPlayerLevel(void) const { return m_playerLevel; }
	const char* GetCharacterName(void) const { return m_cName; }
	const char* GetCharacterClass(void) const { return m_classSkill; }
	int GetCharacterID(void) const { return characterID; }
	unsigned char GetSkills(void) const { return m_cSkill; }
	int GetChoice(void) const { return choice; }
	//int GetWorldLevel(void) const { return worldLevel; }

	//mutators
	//void SetWorldLevel(int lvl) { worldLevel = lvl; }
	void SetControllerNumber(int controller) { m_numController = controller; }
	void SetPlayerLevel(unsigned int lvl) { m_playerLevel = lvl; }
	void SetCharacterID(int ID) { characterID = ID; }
	void SetCharacterName(const char* name);
	void SetCharacterClass(const char* className);
	void SetSkills(unsigned char _skill) { m_cSkill = _skill; }
	void SetChoice(int c) { choice = c; }


};

class GameInfo
{
public:

	//singleton methods
	static GameInfo* GetInstance(void);
	static void DeleteInstance(void);
	void Terminate(void);

	//accessors
	std::vector<PlayerInfo*> GetPlayers(void) const { return m_vPlayers; }
	unsigned int GetCurrentLevel(void) const { return m_worldLevel; }
	unsigned int GetUnlockLevels(void) const { return unlockLevels; }

	//mutators
	void SetWorldLevel(unsigned int lvl) { m_worldLevel = lvl; }
	void IncreaseUnlockLevels(void) { if(unlockLevels < 4) unlockLevels++; }

	//methods
	void AddPlayerInfo(PlayerInfo* info);

	//Class functions save and loading
	bool SaveProfile(const char * path);
	bool LoadProfile(const char * filepath);
	bool CheckProFile(const char* filepath, int slot);
	int GetNumPlayersXML(const char* filepath);

private:
	//no touchy touch
	GameInfo() = default;
	~GameInfo() = default;
	GameInfo(const GameInfo&) = delete;
	GameInfo& operator=(const GameInfo&) = delete;
	int loopCounter = 0;
	//instances 
	static GameInfo* m_pInstance;

	//game information
	unsigned int m_numPlayers = 0;
	unsigned  int m_worldLevel = 0;
	int unlockLevels = 0;

	std::vector<PlayerInfo*> m_vPlayers;

	int numPlayersXML = -1;
};

