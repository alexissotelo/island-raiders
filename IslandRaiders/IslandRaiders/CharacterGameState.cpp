#include "CharacterGameState.h"

#include "PlayGameState.h"
#include "..\Source\Game.h"
#include "BitmapFont.h"
#include "LevelGameState.h"
#include "GameInfo.h"
#include "SaveLoadState.h"
#include <sstream>
#include "AudioQueue.h"
#include "GUI.h"

CharacterGameState* CharacterGameState::pInstance = nullptr;

void CharacterGameState::CreateParty()
{

}

SGD::Color playerColors[] = { { 158, 11, 15 }, { 60, 74, 255 }, { 255, 255, 43 } };

void CharacterGameState::Enter()
{
	//clear the character stats
	GameInfo::GetInstance()->Terminate();
	//local graphical pointer

	SGD::GraphicsManager* pGraphics = SGD::GraphicsManager::GetInstance();

	font = GUIManager::GetInstance()->GetFont();
	backGround = pGraphics->LoadTexture(L"resources/graphics/backgrounds/bg.png");

	icons[0] = pGraphics->LoadTexture(L"resources/graphics/icons/mihawk.png");
	icons[1] = pGraphics->LoadTexture(L"resources/graphics/icons/mystogan.png");
	icons[2] = pGraphics->LoadTexture(L"resources/graphics/icons/lal.png");
	icons[3] = pGraphics->LoadTexture(L"resources/graphics/icons/erza.png");
	icons[4] = pGraphics->LoadTexture(L"resources/graphics/icons/fran.png");
	icons[5] = pGraphics->LoadTexture(L"resources/graphics/icons/xanxus.png");

	names[0] = "Mihawk";
	names[1] = "Mystogan";
	names[2] = "LaL";
	names[3] = "Erza";
	names[4] = "Fran";
	names[5] = "Xanxus";

	classNames[0] = "Swordsman";
	classNames[1] = "Mage";
	classNames[2] = "Gunner";
	classNames[3] = "Swordsman";
	classNames[4] = "Mage";
	classNames[5] = "Gunner";

	X = pGraphics->LoadTexture(L"resources/graphics/icons/redX.png");


	for (unsigned int i = 0; i < 4; ++i)
	{
		data[i].m_numController = i;
		data[i].m_numCharacter = i;

		if (pInput->IsControllerConnected(i))
			controller = true;
	}

}
void CharacterGameState::Exit()
{
	//save all character information within the singleton function
	GameInfo* pInfo = GameInfo::GetInstance();

	for (unsigned int i = 0; i < 4; ++i)
	{
		//save active players only
		if (!data[i].m_bConfirm)
			continue;

		//create a player struct to update
		PlayerInfo* playerInfo = new PlayerInfo(
			data[i].m_cName, "base", data[i].m_numCharacter, data[i].m_numController,1);

		//class name
		std::string charName = data[i].m_cName;

		if (charName == "mihawk" || charName == "erza")
			playerInfo->SetCharacterClass("BasicSword");
		else if (charName == "mystogan" || charName == "fran")
			playerInfo->SetCharacterClass("BasicAlch");
		else if (charName == "lal" || charName == "xanxus")
			playerInfo->SetCharacterClass("BasicGunner");


		//add into the singleton
		pInfo->AddPlayerInfo(playerInfo);

	}

	//unload all images
	UnloadTextures();
	//delete instance
	DeleteInstance();

	//no background music
	AudioQueue::GetInstance()->clear();

}
void CharacterGameState::ArcadeCursorMovement(float dt)
{
	SGD::Vector dir = pInput->GetLeftJoystick(0);
	for (int i = 0; i < Game::GetInstance()->NUMARCADEPLAYERS; i++)
	{
		if (dir.x != 0)
		{
			timer += dt;
			if (timer > timerFreq)
			{
				timer = 0.f;
				if (dir.x < 0)
				{
					timer = 0.0f;
					--data[i].m_numCharacter;
					if (data[i].m_numCharacter < 0)
						data[i].m_numCharacter = NUM_CHARACTERS - 1;
				}
				else if (dir.x > 0)
				{
					timer = 0.f;
					++data[i].m_numCharacter;
					if (data[i].m_numCharacter >= NUM_CHARACTERS)
						data[i].m_numCharacter = 0;

				}

			}
		}
	}
}
bool CharacterGameState::ArcadeCharacterSelection(float dt)
{
	for (int i = 0; i < 2; i++)
	{
		if (pInput->IsButtonPressed(0, 1) && (Game::GetInstance()->NUMARCADEPLAYERS == numPlayersConfirmed))
		{
			//start up the game
			Game::GetInstance()->ChangeState(LevelGameState::GetInstance());
			//success!
			return true;
		}

		//pulling out?
		if (pInput->IsButtonPressed(i, 2))
		{
			//already selected?
			if (data[i].m_bConfirm)
			{
				//quitter
				data[i].m_bConfirm = false;
				//no helping the team
				--numPlayersConfirmed;
			}
			else if (!(data[0].m_bInclude == data[1].m_bInclude == data[2].m_bInclude))
			{
				Game::GetInstance()->ChangeState(SaveLoadState::GetInstance());
				return true;
			}
		}
		//no changing once confirmed
		if (data[i].m_bConfirm || !data[i].m_bInclude)
			//skip -- no fun
			continue;
		ArcadeCursorMovement(dt);

		//enter
		if (pInput->IsButtonPressed(i, 1))
		{
			SelectCharacter(i, data[i].m_numCharacter);
			data[i].m_bConfirm = true;
			numPlayersConfirmed++;
		}
	}
	return true;
}
bool CharacterGameState::Update(float dt)
{


	//local input pointer
	SGD::InputManager* pInput = SGD::InputManager::GetInstance();

	if (Game::GetInstance()->ARCADEMODE)
	{
		if (pInput->IsButtonDown(0, 6))
		{

			data[0].m_bInclude = !data[0].m_bInclude;

			if (data[0].m_bInclude)
				Game::GetInstance()->NUMARCADEPLAYERS++;
			else
				Game::GetInstance()->NUMARCADEPLAYERS--;

		}
		if (pInput->IsButtonDown(1, 6))
		{
			data[1].m_bInclude = !data[1].m_bInclude;

			if (data[1].m_bInclude)
				Game::GetInstance()->NUMARCADEPLAYERS++;
			else
				Game::GetInstance()->NUMARCADEPLAYERS--;

		}

		ArcadeCursorMovement(dt);
		return ArcadeCharacterSelection(dt);
	}

#pragma region Keyboard
	else if (!controller && !Game::GetInstance()->ARCADEMODE)
	{
		if (pInput->CheckForNewControllers())
		{
			controller = true;
		}
		else if (!controller)
			controller = pInput->IsControllerConnected(0);

		if (pInput->IsKeyPressed(SGD::Key::Enter) && data[0].m_bConfirm)
		{
			//start up the game
			Game::GetInstance()->ChangeState(LevelGameState::GetInstance());
			//success!
			return true;
		}
		//pulling out?
		if (pInput->IsKeyPressed(SGD::Key::Backspace))
		{
			//already selected?
			if (data[0].m_bConfirm)
			{
				//quitter
				data[0].m_bConfirm = false;
				//no helping the team
				--numPlayersConfirmed;
			}
			else if (!data[0].m_bConfirm)
			{
				Game::GetInstance()->ChangeState(SaveLoadState::GetInstance());
				return true;
			}
		}

		//start button to inlcude
		if (pInput->IsKeyPressed(SGD::Key::S))
		{
			if (!pInput->IsControllerConnected(0))
			{
				if (!data[0].m_bInclude)
				{
					//new player
					++numPlayers;
					//let him play
					data[0].m_bInclude = true;
				}
			}
		}
		//move left
		if (pInput->IsKeyPressed(SGD::Key::LeftArrow))
		{
			--data[0].m_numCharacter;
			if (data[0].m_numCharacter < 0)
				data[0].m_numCharacter = NUM_CHARACTERS - 1;
		}
		//move right
		else if (pInput->IsKeyPressed(SGD::Key::RightArrow))
		{
			++data[0].m_numCharacter;
			if (data[0].m_numCharacter >= NUM_CHARACTERS)
				data[0].m_numCharacter = 0;
		}
		//enter
		else if (pInput->IsKeyPressed(SGD::Key::Enter))
		{
			SelectCharacter(0, data[0].m_numCharacter);

			data[0].m_bConfirm = true;
			numPlayersConfirmed++;
		}
	}

#pragma endregion

#pragma region Controller

	else if (controller && !Game::GetInstance()->ARCADEMODE)
	{
		//loop through all controllers
		for (unsigned int i = 0; i < 4; ++i)
		{
			//skip the new fucker
			if (pInput->CheckForNewControllers())
				//leave
				continue;

			if (pInput->IsControllerConnected(i))
			{

				if ((pInput->IsButtonPressed(i, 1) || pInput->IsKeyPressed(SGD::Key::Enter)) && numPlayers == numPlayersConfirmed && numPlayers != 0)
				{
					//start up the game
					Game::GetInstance()->ChangeState(LevelGameState::GetInstance());
					//success!
					return true;
				}

				//pulling out?
				if (pInput->IsButtonPressed(i, 2))
				{
					//already selected?
					if (data[i].m_bConfirm)
					{
						//quitter
						data[i].m_bConfirm = false;
						//no helping the team
						--numPlayersConfirmed;
					}
					//can i select?
					else if (data[i].m_bInclude)
					{
						//dont include me
						data[i].m_bInclude = false;
						//decrease player size
						--numPlayers;
					}
					else if (!(data[0].m_bInclude == data[1].m_bInclude == data[2].m_bInclude))
					{
						Game::GetInstance()->ChangeState(SaveLoadState::GetInstance());
						return true;
					}
				}
				//start button to inlcude
				if (pInput->IsButtonPressed(i, 9) || pInput->IsKeyPressed(SGD::Key::S))
				{

					if (!data[i].m_bInclude)
					{
						//new player
						++numPlayers;
						//let him play
						data[i].m_bInclude = true;
					}
				}
				//no changing once confirmed
				if (data[i].m_bConfirm || !data[i].m_bInclude)
					//skip -- no fun
					continue;


				//move left
				if (pInput->IsDPadPressed(i, SGD::DPad::Left) || pInput->IsKeyPressed(SGD::Key::LeftArrow))
				{
					--data[i].m_numCharacter;
					if (data[i].m_numCharacter < 0)
						data[i].m_numCharacter = NUM_CHARACTERS - 1;
				}
				//move right
				else if (pInput->IsDPadPressed(i, SGD::DPad::Right) || pInput->IsKeyPressed(SGD::Key::RightArrow))
				{
					++data[i].m_numCharacter;
					if (data[i].m_numCharacter >= NUM_CHARACTERS)
						data[i].m_numCharacter = 0;
				}
				//enter
				else if (pInput->IsButtonPressed(i, 1) || pInput->IsKeyPressed(SGD::Key::Enter))
				{
					SelectCharacter(i, data[i].m_numCharacter);
					data[i].m_bConfirm = true;
					numPlayersConfirmed++;
				}
			}
		}
	}
#pragma endregion

	//keep playing
	return true;
}

void CharacterGameState::Render(float dt)
{
	//local input pointer
	SGD::GraphicsManager* pGraphics = SGD::GraphicsManager::GetInstance();


	//render background
	pGraphics->DrawTexture(backGround, { 0.0f, 0.0f }/*, {}, {}, {}, { 3.5f, 3.0f }*/); //3.5 x 3.0

	font->DrawXML("Press Start To Join The Game", { 240, 25 }, 1.f, {});

	for (size_t i = 0; i < 3; i++)
	{
		pGraphics->DrawRectangle({ { i*200.f+140, 180.f }, SGD::Size(175, 300) }, playerColors[i]);
	}

	if (controller && !Game::GetInstance()->ARCADEMODE)//wtf?
		RenderController();
	else if (!controller)
		RenderKeyboard();
	else if (Game::GetInstance()->ARCADEMODE)
		RenderArcade();
}
void CharacterGameState::RenderArcade()
{
	SGD::GraphicsManager* pGraphics = SGD::GraphicsManager::GetInstance();

	//screen size
	SGD::Size screen = { Game::GetInstance()->GetScreenWidth(), Game::GetInstance()->GetScreenHeight() };

	//render at the center
	SGD::Point ptCenter = { (screen.width * 0.255f) - 29.0f, 260 };
	for (int i = 0; i < 2; ++i)
	{
		if (data[i].m_bInclude)
		{
			SGD::Point pt = { ptCenter.x - 20, ptCenter.y + 130 };
			SGD::Point topPT = { ptCenter.x - 10, ptCenter.y - 60 };
			font->DrawXML(names[data[i].m_numCharacter].c_str(), topPT, 1.0f, { 255, 255, 255 });
			//render the image
			pGraphics->DrawTexture(icons[data[i].m_numCharacter], ptCenter, 0.0f, {}, {}, { 0.8f, 0.8f });

			if (data[i].m_bConfirm)
				font->DrawXML("B2: Confirm\nB3: Cancel", pt, 0.7f, {});
			else
				font->DrawXML(classNames[data[i].m_numCharacter].c_str(), pt, 1.0f, { 255, 255, 255 });

		}

		//move over bitch
		ptCenter.x += screen.width * 0.23f;
	}
}
void CharacterGameState::RenderKeyboard()
{
	SGD::GraphicsManager* pGraphics = SGD::GraphicsManager::GetInstance();

	//screen size
	SGD::Size screen = { Game::GetInstance()->GetScreenWidth(), Game::GetInstance()->GetScreenHeight() };

	//render at the center
	SGD::Point ptCenter = { (screen.width * 0.255f) - 29.0f, 260 };
	SGD::Point temp = ptCenter;
	font->DrawXML(names[data[0].m_numCharacter].c_str(), { temp.x - 10, temp.y - 60 }, 1.0f, { 255, 255, 255 });

	pGraphics->DrawTexture(icons[data[0].m_numCharacter], ptCenter, 0.0f, {}, {}, { 0.8f, 0.8f });

	if (data[0].m_bConfirm)
	{
		//pGraphics->DrawTexture(X, ptCenter, 0.f, {}, { 255, 255, 255, 255 });
		SGD::Point pt = { ptCenter.x - 10, ptCenter.y + 120 };
		font->DrawXML("Press A To \nConfirm", pt, 0.7f, { 255, 255, 255 });
	}
	else
		font->DrawXML(classNames[data[0].m_numCharacter].c_str(), { temp.x - 20, temp.y + 130 }, 1.0f, { 255, 255, 255 });

}
void CharacterGameState::RenderController()
{
	SGD::GraphicsManager* pGraphics = SGD::GraphicsManager::GetInstance();

	//screen size
	SGD::Size screen = { Game::GetInstance()->GetScreenWidth(), Game::GetInstance()->GetScreenHeight() };

	//render at the center
	SGD::Point ptCenter = { (screen.width * 0.255f) - 29.0f, 260 };
	for (unsigned int i = 0; i < numPlayers; ++i)
	{
		if (data[i].m_bInclude)
		{
			SGD::Point pt = { ptCenter.x - 20, ptCenter.y + 130 };
			SGD::Point topPT = { ptCenter.x - 10, ptCenter.y - 60 };
			font->DrawXML(names[data[i].m_numCharacter].c_str(), topPT, 1.0f, { 255, 255, 255 });
			//render the image
			pGraphics->DrawTexture(icons[data[i].m_numCharacter], ptCenter, 0.0f, {}, {}, { 0.8f, 0.8f });

			if (data[i].m_bConfirm)
				font->DrawXML("B2: Confirm\nB3: Cancel", pt, 0.7f, {});
			else
				font->DrawXML(classNames[data[i].m_numCharacter].c_str(), pt, 1.0f, { 255, 255, 255 });

		}

		//move over bitch
		ptCenter.x += screen.width * 0.23f;
	}
}

CharacterGameState* CharacterGameState::GetInstance(void)
{
	if (pInstance == nullptr)
		pInstance = new CharacterGameState;

	return pInstance;
}
void CharacterGameState::DeleteInstance(void)
{
	delete pInstance;
	pInstance = nullptr;
}

void CharacterGameState::UnloadTextures(void)
{
	SGD::GraphicsManager* pGraphics = SGD::GraphicsManager::GetInstance();
	pGraphics->UnloadTexture(backGround);
	pGraphics->UnloadTexture(X);

	for (unsigned int i = 0; i < NUM_CHARACTERS; ++i)
		pGraphics->UnloadTexture(icons[i]);
}

void CharacterGameState::GetPlayerPacket(std::vector<DataPacket>& vec) const
{
	//local input pointer
	SGD::InputManager* pInput = SGD::InputManager::GetInstance();

	for (unsigned int index = 0; index < 4; ++index)
	{
		if (pInput->IsControllerConnected(index) && data[index].m_bInclude && data[index].m_bConfirm)
			vec.push_back(data[index]);
	}
}

void CharacterGameState::SelectCharacter(int controllerNumber, int characterNumber)
{
	switch (characterNumber)
	{
	case 0:
		data[controllerNumber].m_cName = "mihawk";
		break;
	case 1:
		data[controllerNumber].m_cName = "mystogan";
		break;
	case 2:
		data[controllerNumber].m_cName = "lal";
		break;
	case 3:
		data[controllerNumber].m_cName = "erza";
		break;
	case 4:
		data[controllerNumber].m_cName = "fran";
		break;
	case 5:
		data[controllerNumber].m_cName = "xanxus";
		break;
	default:
		break;
	}

}
