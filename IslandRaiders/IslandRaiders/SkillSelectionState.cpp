#include "SkillSelectionState.h"
#include "BitmapFont.h"
#include "..\Source\Game.h"
#include "Character.h"
#include "..\Source\Game.h"
#include "LevelGameState.h"
#include "PlayGameState.h"
#include "Player.h"
#include "GameInfo.h"
#include "GUI.h"

SkillSelectionState* SkillSelectionState::pInstance = nullptr;


SkillSelectionState* SkillSelectionState::GetInstance()
{
	if (pInstance == nullptr)
		pInstance = new SkillSelectionState;
	return pInstance;
}

void SkillSelectionState::DeleteInstance(void)
{
	delete pInstance;
	pInstance = nullptr;
}
void SkillSelectionState::Enter()
{
	pFont = GUIManager::GetInstance()->GetFont();
	pGraphics = SGD::GraphicsManager::GetInstance();
	pAudio = SGD::AudioManager::GetInstance();
	pInput = SGD::InputManager::GetInstance();
	skillTreeSword = pGraphics->LoadTexture(L"resource/Graphics/placeholder images/SKILLTREESWORD.png");
	skillTreeMage = pGraphics->LoadTexture(L"resource/Graphics/placeholder images/SKILLTREEMAGE.png");
	skillTreeGunner = pGraphics->LoadTexture(L"resource/Graphics/placeholder images/SKILLTREEGUNNER.png");
	backGround = pGraphics->LoadTexture(L"resources/graphics/backgrounds/bg.png");
	icons[0] = pGraphics->LoadTexture(L"resources/graphics/icons/mihawk.png");
	icons[1] = pGraphics->LoadTexture(L"resources/graphics/icons/mystogan.png");
	icons[2] = pGraphics->LoadTexture(L"resources/graphics/icons/lal.png");
	icons[3] = pGraphics->LoadTexture(L"resources/graphics/icons/erza.png");
	icons[4] = pGraphics->LoadTexture(L"resources/graphics/icons/xanxus.png");
	icons[5] = pGraphics->LoadTexture(L"resources/graphics/icons/fran.png");
	redX = pGraphics->LoadTexture(L"resources/graphics/icons/redXSmall.png");
	redBox = pGraphics->LoadTexture(L"resource/Graphics/placeholder images/BETA_REDBOX.png");
	SetWayPoints();

	//	copyParty
	std::vector<PlayerInfo*> playerInfos = GameInfo::GetInstance()->GetPlayers();
	switch (playerInfos.size())
	{
	case 1:
		offSet = .7f;
		break;
	case 2:
		offSet = .5f;
		break;
	case 3:
		offSet = .33f;
		break;
	default:
		break;
	}

	for (unsigned int i = 0; i < playerInfos.size(); i++)
	{
		std::string name = playerInfos[i]->GetCharacterName();
		if (name == "mihawk")
		{
			partyIMG.push_back(icons[0]);
			skillIMG.push_back(skillTreeSword);
			partyType.push_back((unsigned int)PlayerType::swordsman);
		}
		else if (name == "mystogan")
		{
			skillIMG.push_back(skillTreeMage);
			partyIMG.push_back(icons[1]);
			partyType.push_back((unsigned int)PlayerType::alchemist);
		}
		else if (name == "lal")
		{
			skillIMG.push_back(skillTreeGunner);
			partyIMG.push_back(icons[2]);
			partyType.push_back((unsigned int)PlayerType::gunner);
		}
		else if (name == "erza")
		{
			partyIMG.push_back(icons[3]);
			skillIMG.push_back(skillTreeSword);
			partyType.push_back((unsigned int)PlayerType::swordsman);
		}
		else if (name == "xanxus")
		{
			partyIMG.push_back(icons[4]);
			skillIMG.push_back(skillTreeGunner);
			partyType.push_back((unsigned int)PlayerType::gunner);

		}
		else if (name == "fran")
		{
			skillIMG.push_back(skillTreeMage);
			partyIMG.push_back(icons[5]);
			partyType.push_back((unsigned int)PlayerType::alchemist);
		}
	}
	numPlayers = playerInfos.size();
	numPlayersConfirmed = 0;
}
void  SkillSelectionState::SetWayPoints(void)
{
	waypoints[0] = SGD::Point(49, 73);
	waypoints[1] = SGD::Point(49, 73);
	waypointsTierTwoLeft[0] = SGD::Point(19, 131);
	waypointsTierTwoLeft[1] = SGD::Point(77, 131);
	waypointsTierTwoRight[0] = SGD::Point(133, 131);
	waypointsTierTwoRight[1] = SGD::Point(191, 131);
}

void SkillSelectionState::Exit()
{
	UnloadTextures();
	DeleteInstance();
}
bool SkillSelectionState::Update(float dt)
{
	//local game info
	GameInfo* gameInfo = GameInfo::GetInstance();

	if (curPicker == numPlayers)
	{
		while (pInput->IsButtonPressed(0, 1) != true)
		{
			for (int i = 0; i < numPlayers; i++)
			{
				skillSelectConfirm = false;
				//save
				Game::GetInstance()->Save();
				Game::GetInstance()->ChangeState(LevelGameState::GetInstance());
				return true;
			}
		}

	}
	else
	{
		if (Game::GetInstance()->ARCADEMODE)
		{
			ArcadeCursorMovement(dt);
			return ArcadeCharacterSelection(dt);
		}
		else
		{
			PlayerInfo* playerInfo = gameInfo->GetPlayers()[curPicker];

			if ((pInput->IsDPadPressed(curPicker, SGD::DPad::Left) || pInput->IsKeyPressed(SGD::Key::A)) && !skillSelectConfirm)
			{
				skillSelectCursor--;

				if (skillSelectCursor < 0)
					skillSelectCursor = 1;
			}
			if ((pInput->IsDPadPressed(curPicker, SGD::DPad::Right) || pInput->IsKeyPressed(SGD::Key::D)) && !skillSelectConfirm)
			{
				skillSelectCursor++;

				if (skillSelectCursor > 1)
					skillSelectCursor = 0;
			}


			if ((pInput->IsButtonPressed(curPicker, 1) || pInput->IsKeyPressed(SGD::Key::Enter)) && skillSelectConfirm)
			{
				UpdateSkills(curPicker);
				if (curPicker != numPlayers)
					curPicker++;

				skillSelectConfirm = false;

			}
			if (pInput->IsButtonPressed(curPicker, 1) || pInput->IsKeyPressed(SGD::Key::Enter))
			{
				numPlayersConfirmed++;
				skillSelectConfirm = true;
			}
			if (pInput->IsButtonPressed(curPicker, 2) || pInput->IsKeyPressed(SGD::Key::Backspace))
			{
				numPlayersConfirmed--;
				skillSelectConfirm = false;
			}
		}
	}
	return true;
}

void SkillSelectionState::ArcadeCursorMovement(float dt)
{
	SGD::Vector dir = pInput->GetLeftJoystick(0);
	if (dir.x != 0)
	{
		timer += dt;
		if (timer > timerFreq)
		{
			timer = 0.f;
			if (dir.x < 0)
			{
				timer = 0.f;
				skillSelectCursor--;

				if (skillSelectCursor < 0)
					skillSelectCursor = 1;
			}
			else if (dir.x > 0)
			{
				timer = 0.f;
				skillSelectCursor++;

				if (skillSelectCursor > 1)
					skillSelectCursor = 0;

			}

		}
	}
}

bool SkillSelectionState::ArcadeCharacterSelection(float dt)
{

	if (pInput->IsButtonPressed(curPicker, 1))
	{
		numPlayersConfirmed++;
		skillSelectConfirm = true;
	}
	if (pInput->IsButtonPressed(curPicker, 2))
	{
		numPlayersConfirmed--;
		skillSelectConfirm = false;
	}
	if (pInput->IsButtonPressed(curPicker, 1) && skillSelectConfirm)
	{
		UpdateSkills(curPicker);
		if (curPicker != numPlayers)
			curPicker++;

		skillSelectConfirm = false;

	}
	return true;
}


void SkillSelectionState::UpdateSkills(int who)
{
	PlayerInfo* info = GameInfo::GetInstance()->GetPlayers()[who];
	unsigned int lvl = info->GetPlayerLevel();
	switch (lvl)
	{
	case 1:
		UpdateSkillOne(who);
		break;
	case 3:
		UpdateSkillTwo(who);
		break;
	default:
		break;
	}
	//info->SetWorldLevel(++lvl);
}
void SkillSelectionState::UpdateSkillOne(unsigned int who)
{
	//local player inof
	PlayerInfo* playerInfo = GameInfo::GetInstance()->GetPlayers()[who];

	//set skill
	if (skillSelectCursor == 0) // player skill on the left
	{
		switch (playerInfo->GetCharacterID())
		{
		case 0: //mihawk
		case 3: //erza
			playerInfo->SetCharacterClass("berserker");
			break;
		case 1: //mystogon
		case 4:
			//class name
			playerInfo->SetCharacterClass("summoner");
			break;
		case 2: //lal
		case 5:
			//class name
			playerInfo->SetCharacterClass("marksman");
			break;
		default:
			break;
		}
	}
	else if (skillSelectCursor == 1)// player skill on the right  -- 1 << 1
	{

		switch (playerInfo->GetCharacterID())
		{
		case 0: //mihawk
		case 3: //erza
			//class name
			playerInfo->SetCharacterClass("fencer");
			break;
		case 1: //mystogon
		case 4:
			//class name
			playerInfo->SetCharacterClass("poisonfog");
			break;
		case 2: //lal
		case 5:
			//class name
			playerInfo->SetCharacterClass("claymore");
			break;
		default:
			break;
		}
	}
}
void SkillSelectionState::UpdateSkillTwo(unsigned int who)
{
	PlayerInfo* playerInfo = GameInfo::GetInstance()->GetPlayers()[who];
	unsigned char localSkill = playerInfo->GetSkills();

	if (skillSelectCursor == 0) // player skill on the left
	{

		switch (playerInfo->GetCharacterID())
		{
		case 0: //mihawk
		case 3: //erza
			if (!(strcmp(playerInfo->GetCharacterClass(), "berserker")))
				playerInfo->SetCharacterClass("captain");
			else if (!(strcmp(playerInfo->GetCharacterClass(), "fencer")))
				playerInfo->SetCharacterClass("admiral");
			break;
		case 1: //mystogan
		case 4: //frog girl
			if (!(strcmp(playerInfo->GetCharacterClass(), "summoner")))
				playerInfo->SetCharacterClass("witchdoctor");
			else if (!(strcmp(playerInfo->GetCharacterClass(), "poisonfog")))
				playerInfo->SetCharacterClass("pyromaniac");
		case 2: //lal
		case 5: //XANUS
			if (!(strcmp(playerInfo->GetCharacterClass(), "marksman")))
				playerInfo->SetCharacterClass("weaponexchange");
			else if (!(strcmp(playerInfo->GetCharacterClass(), "claymore")))
				playerInfo->SetCharacterClass("hunter");
			break;
			break;
		default:
			break;
		}
	}
	else if (skillSelectCursor == 1)// player skill on the right
	{
		switch (playerInfo->GetCharacterID())
		{
		case 0: //mihawk
		case 3: //erza
			if (!(strcmp(playerInfo->GetCharacterClass(), "berserker")))
				playerInfo->SetCharacterClass("hammer");
			else if (!(strcmp(playerInfo->GetCharacterClass(), "fencer")))
				playerInfo->SetCharacterClass("seaknight");
			break;
		case 1: //mystogon
			if (!(strcmp(playerInfo->GetCharacterClass(), "summoner")))
				playerInfo->SetCharacterClass("beastmaster");
			else if (!(strcmp(playerInfo->GetCharacterClass(), "poisonfog")))
				playerInfo->SetCharacterClass("omniknight");
		case 2: //lal
			if (!(strcmp(playerInfo->GetCharacterClass(), "marksman")))
				playerInfo->SetCharacterClass("cannonfire");
			else if (!(strcmp(playerInfo->GetCharacterClass(), "claymore")))
				playerInfo->SetCharacterClass("camper");
			break;
			break;
		default:
			break;
		}
	}

}
void SkillSelectionState::UpdateSkillThree(unsigned int level)
{

}

void SkillSelectionState::Render(float dt)
{
	//local Game Info
	GameInfo* pGameInfo = GameInfo::GetInstance();

	//screen size
	SGD::Size screen = { Game::GetInstance()->GetScreenWidth(), Game::GetInstance()->GetScreenHeight() };
	int temp = curPicker;

	//render at the center

	SGD::Point ptCenter = { (screen.width * offSet) - 250.f, 150 };
	SGD::Point position = { 0, 0 };
	pGraphics->DrawTexture(backGround, { 0.0f, 0.0f }, {}, {}, {}, { 1.0f, 1.0f });

	if (curPicker >= numPlayers)
	{
		temp--;
	}

	cursor = skillSelectCursor;
	pGraphics->DrawTexture(partyIMG[temp], { ptCenter.x, ptCenter.y - 150 });
	pGraphics->DrawTexture(skillIMG[temp], { ptCenter.x * 0.5f, ptCenter.y });

	//string 
	std::string className = pGameInfo->GetPlayers()[temp]->GetCharacterClass();

	SGD::Point t = { ptCenter.x * 0.5f, ptCenter.y + 1 };

	if (className == "BasicSword" ||
		className == "BasicAlch" ||
		className == "BasicGunner")
	{
		pGraphics->DrawTexture(redBox, { waypoints[0].x + t.x + 114 * cursor, waypoints[0].y + t.y });
		position = { waypoints[0].x + t.x + 114 * cursor, waypoints[0].y + t.y };
	}
	if (className == "summoner" ||
		className == "berserker" ||
		className == "dualshot")
	{
		pGraphics->DrawTexture(redBox, { waypointsTierTwoLeft[0].x + t.x + 58 * cursor, waypointsTierTwoLeft[0].y + t.y });
		position = { waypointsTierTwoLeft[0].x + t.x + 58 * cursor, waypointsTierTwoLeft[0].y + t.y };
	}
	if (className == "poisonfog" ||
		className == "claymore" ||
		className == "fencer")
	{
		pGraphics->DrawTexture(redBox, { waypointsTierTwoRight[0].x + t.x + 58 * cursor, waypointsTierTwoRight[0].y + t.y });
		position = { waypointsTierTwoRight[0].x + t.x + 58 * cursor, waypointsTierTwoRight[0].y + t.y };
	}
	if (skillSelectConfirm)
		pGraphics->DrawTexture(redX, position);

	DisplayText(temp, cursor, SGD::Point(ptCenter.x * 0.5f, ptCenter.y));


}

void SkillSelectionState::DisplayText(int who, int what, SGD::Point sp)
{
	//player info
	PlayerInfo* playerInfo = GameInfo::GetInstance()->GetPlayers()[who];

	SGD::Point startingPoint = { 283 + sp.x, 89 + sp.y };
	std::string className = playerInfo->GetCharacterClass();

#pragma region lvl 1
	if (className == "BasicSword")
	{
		if (what == 0)//cursor == 0
			pFont->DrawXML("Knock down multiple \nenemies at once", startingPoint, .7f);
		else if (what == 1)// cursor == 1
			pFont->DrawXML("Strike through \nmultiple enemies", startingPoint, .7f);
	}
	else if (className == "BasicGunner")
	{
		if (what == 0)//cursor == 0
			pFont->DrawXML("Increase the amount \nof damage on a \nmarked ntarget.", startingPoint, .7f);
		else if (what == 1)// cursor == 1
			pFont->DrawXML("Set an explosive trap \n for enemies", startingPoint, .7f);
	}
	else if (className == "BasicAlch")
	{
		if (what == 0)//cursor == 0
			pFont->DrawXML("Summonn a Robo-Dog\n familiar to help \nin battle.", startingPoint, .7f);
		else if (what == 1)// cursor == 1
			pFont->DrawXML("Poison all \nsurrounding enemies.", startingPoint, .7f);
	}
#pragma endregion

#pragma region lvl 3
	//alch
	else if (className == "summoner")
	{
		if (what == 0)//cursor == 0
			pFont->DrawXML("Temporarily double \nthe attack power of \nall party members", startingPoint, .7f);
		else if (what == 1)// cursor == 1
			pFont->DrawXML("Summon a Black Dragon familiar \n to aid in battle", startingPoint, .7f);
	}
	else if (className == "poisonfog")
	{
		if (what == 0)//cursor == 0
			pFont->DrawXML("Conjure pillars of \nfire for more damage", startingPoint, .7f);
		else if (what == 1)// cursor == 1
			pFont->DrawXML("Awaken your true \npowers!", startingPoint, .7f);
	}//alch done
	//gunner
	else if (className == "claymore")
	{
		if (what == 0)//cursor == 0
			pFont->DrawXML("Attack with your \nanimal partner!", startingPoint, .7f);
		else if (what == 1)// cursor == 1
			pFont->DrawXML("Call in an \nAir Strike", startingPoint, .7f);
	}
	else if (className == "dualshot")
	{
		if (what == 0)//cursor == 0
			pFont->DrawXML("Temporarily use faster \nand larger ammo", startingPoint, .7f);
		else if (what == 1)// cursor == 1
			pFont->DrawXML("Release a blast \nat full power!", startingPoint, .7f);
	}//gunner done
	//sword
	else if (className == "berserker")
	{
		if (what == 0)//cursor == 0
			pFont->DrawXML("Knockback any foe \nthat stands in your \nway!", startingPoint, .7f);
		else if (what == 1)// cursor == 1K
			pFont->DrawXML("Stun any enemy \nthat stands a threat!", startingPoint, .7f);
	}
	else if (className == "fencer")
	{
		if (what == 0)//cursor == 0//ADMIRAL
			pFont->DrawXML("Deal massive damage \nto a single enemy!", startingPoint, .7f);
		else if (what == 1)// cursor == 1//SEAKNIGHT
			pFont->DrawXML("Deal heavy damage \nto mutliple enemies!", startingPoint, .7f);
	}//sworddone
#pragma endregion

}

void SkillSelectionState::UnloadTextures(void)
{
	SGD::GraphicsManager* pGraphics = SGD::GraphicsManager::GetInstance();
	pGraphics->UnloadTexture(backGround);
	pGraphics->UnloadTexture(skillTreeSword);
	pGraphics->UnloadTexture(skillTreeMage);
	pGraphics->UnloadTexture(skillTreeGunner);

	pGraphics->UnloadTexture(redX);
	pGraphics->UnloadTexture(redBox);

	//for (unsigned int i = 0; i < copyParty.size(); i++)
	//	delete copyParty[i];

	//for (unsigned int i = 0; i < partyIMG.size(); i++)
	//	pGraphics->UnloadTexture(partyIMG[i]);

	partyIMG.clear();

	for (unsigned int i = 0; i < 6; ++i)
		pGraphics->UnloadTexture(icons[i]);
}
