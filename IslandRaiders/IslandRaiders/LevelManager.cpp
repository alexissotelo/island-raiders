#include "LevelManager.h"
#include "EntityManager.h"
#include <iterator>
#include "AnimationSystem.h"
#include "AnimationTimestamp.h"
#include "Enemy.h"
#include "Character.h"
#include "Player.h"
#include "Collision.h"
#include "Boss.h"
#include "AudioQueue.h"
#include "../SGD Wrappers/SGD_AudioManager.h"
#include "LoadingManager.h"

#define WIN32_LEAN_AND_MEAN
#include <Windows.h>

#include "PlayGameState.h"

#include "EnemyAI.h"
#include "PlayGameState.h"

#include "../tinyxml/tinyxml.h" //tinyXml

LevelManager * LevelManager::pInstance = nullptr;
EntityManager* LevelManager::entityManager = nullptr;
std::vector<EnemyData*>				  LevelManager::enemyCollection;
std::map<std::string, EntityData>	  LevelManager::entityCollection;
std::map<std::string, SGD::HTexture>  LevelManager::imageCollection;

LevelManager* LevelManager::GetInstance(void)
{
	if (pInstance == nullptr)
		pInstance = new LevelManager;

	return pInstance;
}
void LevelManager::DeleteInstance(void)
{
	for (unsigned int i = 0; i < enemyCollection.size(); ++i)
	{
		delete enemyCollection[i];
		enemyCollection[i] = nullptr;
	}
	enemyCollection.clear();

	std::map<std::string, SGD::HTexture>::iterator iter = imageCollection.begin();
	for (; iter != imageCollection.end(); ++iter)
	{
		SGD::GraphicsManager::GetInstance()->UnloadTexture(iter->second);
	}
	entityCollection.clear();

	delete pInstance;
	pInstance = nullptr;
}
void LevelManager::Initialize(EntityManager * entManager)
{
	entityManager = entManager;
}
void LevelManager::Terminate(void)
{
	//terminate all level sections
	for (unsigned int i = 0; i < levelSections.size(); ++i)
	{
		if (levelSections[i])
		{
			delete levelSections[i];
			levelSections[i] = nullptr;
		}
	}
	levelSections.clear();

	//null the entity manager ref
	entityManager = nullptr;
}

void LevelManager::LoadLevel(std::string filename)
{
	//set amount of enemies in section
	TiXmlDocument doc; // make the tiny xml doc

	//load this file yo
	std::string filePath = "resources/xml/";
	filePath += filename;
	filePath += ".xml";

	if (doc.LoadFile(filePath.c_str()) == false)
		return;

	TiXmlElement* root = doc.RootElement();

	std::string bgImageFilename = "resources/graphics/levelprops/";
	bgImageFilename += root->Attribute("background");
	PlayGameState::GetInstance()->SetBackgroundImage(
		SGD::GraphicsManager::GetInstance()->LoadTexture(bgImageFilename.c_str()));

	int sectionIndex = 0;
	TiXmlElement* xSection = root->FirstChildElement("Section");
	while (xSection)
	{
		//position
		int x, y;
		xSection->Attribute("X", &x);
		xSection->Attribute("Y", &y);
		LevelSection *section = new LevelSection((float)x, (float)y);

		//amount enemies
		TiXmlElement* xEnemies = xSection->FirstChildElement("Enemies");
		int amount;
		xEnemies->Attribute("amountOfEnemies", &amount);
		section->SetAmountOfEnemies(amount);

		//push enemies
		TiXmlElement* xEnemy = xEnemies->FirstChildElement("enemy");
		while (xEnemy)
		{
			WorldEntity we;
			we.name = xEnemy->Attribute("name");
			xEnemy->Attribute("X", &x);
			xEnemy->Attribute("Y", &y);
			we.x = (float)x;
			we.y = (float)y;

			section->AddEnemy(we);
			xEnemy = xEnemy->NextSiblingElement("enemy");
		}
		//load world entities
#if 1
		int layerCounter = 0;
		TiXmlElement* xLayers = xSection->FirstChildElement("Layers");
		TiXmlElement* xLayer = xLayers->FirstChildElement("Layer");
		while (xLayer)
		{
			TiXmlElement* xEntity = xLayer->FirstChildElement("entity");
			while (xEntity)
			{
				WorldEntity we;
				we.name = xEntity->Attribute("name");
				xEntity->Attribute("X", &x);
				xEntity->Attribute("Y", &y);
				we.x = (float)x;
				we.y = (float)y;
				double w, h;
				xEntity->Attribute("scaleX", &w);
				xEntity->Attribute("scaleY", &h);
				we.scaleX = (float)w;
				we.scaleY = (float)h;

				//copy values based on collection
				EntityData sample = entityCollection.find(we.name)->second;
				we.image = sample.image;
				we.renderRect = sample.renderRect;

				if (layerCounter < 3)
					section->AddBackground(we);
				else
					section->AddForeground(we);

				xEntity = xEntity->NextSiblingElement("entity");
			}

			xLayer = xLayer->NextSiblingElement("Layer");
			layerCounter++;
		}
#endif

		//push collisions
#if 1
		TiXmlElement* xCollisions = xSection->FirstChildElement("Collisions");
		TiXmlElement* xColl = xCollisions->FirstChildElement("collision");
		while (xColl)
		{
			int x, y, w, h;
			xColl->Attribute("X", &x);
			xColl->Attribute("Y", &y);
			xColl->Attribute("Width", &w);
			xColl->Attribute("Height", &h);

			SGD::Rectangle rect = SGD::Rectangle(
				SGD::Point((float)x, (float)y), SGD::Size((float)w, (float)h));

			Collision *ent = new Collision(sectionIndex);
			ent->SetPosition(SGD::Point((float)x, (float)y));
			ent->SetSize(SGD::Size((float)w, (float)h));

			TiXmlElement *xEvent = xColl->FirstChildElement("event");
			if (xEvent != nullptr)
			{
				ent->SetTrigger(xEvent->Attribute("name"));
			}

			entityManager->AddEntity(ent, ent->GetType());
			ent->Release();
			ent = nullptr;

			xColl = xColl->NextSiblingElement("collision");
		}
#endif
	levelSections.push_back(section);

		xSection = xSection->NextSiblingElement("Section");
		sectionIndex++;
	}
	//load enemies
}
//loads groups.xml to populate the entitycollection map
void LevelManager::LoadEntities(std::string filePath)
{
	ThreadData* threadData = ThreadData::GetInstance();

	TiXmlDocument doc;
	if (doc.LoadFile(filePath.c_str()) == false)
		return;

	TiXmlElement* xRoot = doc.RootElement();
	TiXmlElement* xGroup = xRoot->FirstChildElement("Group");
	while (xGroup)
	{


		TiXmlElement* xEntity = xGroup->FirstChildElement("entity");
		while (xEntity)
		{
			int x, y, w, h;
			std::string name = xEntity->Attribute("name");

			if (name == "whitetree1")
				int x = 9;

			std::string imageID = xEntity->Attribute("imageFilename");
			std::string imageFilename = "resources/graphics/levelprops/" + imageID;

			xEntity->Attribute("X", &x);
			xEntity->Attribute("Y", &y);
			xEntity->Attribute("Width", &w);
			xEntity->Attribute("Height", &h);

			EntityData entData;
			if (!imageCollection.count(imageID))
			{
				threadData->graphicsMutex.lock();
				imageCollection[imageID] =
					SGD::GraphicsManager::GetInstance()->LoadTexture(
					imageFilename.c_str());
				threadData->graphicsMutex.unlock();

			}
			entData.image = imageCollection[imageID];
			entData.renderRect = SGD::Rectangle((float)x, (float)y, (float)x + w, (float)y + h);
			entityCollection[name] = entData;

			xEntity = xEntity->NextSiblingElement("entity");
		}
		xGroup = xGroup->NextSiblingElement("Group");
	}

	//unload loading screens
	//for (unsigned int index = 0; index < 6; ++index)
	//	SGD::GraphicsManager::GetInstance()->UnloadTexture(loadingTextures[index]);
	//inform a thread is done

	threadData->countMutex.lock();
	--threadData->numberOfThreads;
	threadData->countCondition.notify_all();
	threadData->countMutex.unlock();

}
//Loads an enemies.xml and populates the enemycollection
//enemies inside enemyCollection serve as sample to create ***CLONES***
//that will be spawn in the level sections
void LevelManager::LoadEnemies(std::string filePath)
{
	//document
	TiXmlDocument doc;

	//does the file open?
	if (doc.LoadFile(filePath.c_str()) == false)
		return;//quit

	TiXmlElement* xRoot = doc.RootElement();//root

	//iterate through the children
	TiXmlElement* xEnemy = xRoot->FirstChildElement("Enemy");
	while (xEnemy)
	{
		EnemyData* enemy = new EnemyData();// = new Enemy(xEnemy->Attribute("name"));
		int tempInt;

		enemy->name = xEnemy->Attribute("name");

		xEnemy->Attribute("damage0", &tempInt);
		enemy->damage[0] = (float)tempInt;
		xEnemy->Attribute("health0", &tempInt);
		enemy->health[0] = (float)tempInt;

		xEnemy->Attribute("damage1", &tempInt);
		enemy->damage[1] = (float)tempInt;
		xEnemy->Attribute("health1", &tempInt);
		enemy->health[1] = (float)tempInt;

		xEnemy->Attribute("damage2", &tempInt);
		enemy->damage[2] = (float)tempInt;
		xEnemy->Attribute("health2", &tempInt);
		enemy->health[2] = (float)tempInt;

		xEnemy->Attribute("moveSpeed", &tempInt);
		enemy->speed = (float)tempInt;

		xEnemy->Attribute("AI", &tempInt);
		enemy->tier = tempInt;

		xEnemy->Attribute("DropRate", &tempInt);
		enemy->droprate = tempInt;

		//size
		int w, h;
		TiXmlElement* xSize = xEnemy->FirstChildElement("size");
		xSize->Attribute("w", &w);
		xSize->Attribute("h", &h);
		enemy->size = SGD::Size((float)w, (float)h);

#pragma region Skill
		//skill
		int index = 0;
		while (xSize->NextSiblingElement("skill") != NULL)
		{
			xSize = xSize->NextSiblingElement("skill");

			const char* skill = xSize->GetText();
			if (skill)
			{
				int size = strlen(skill) + 1;
				enemy->skills[index] = new char[size];
				strcpy_s(enemy->skills[index], size, skill);
			}
			double cooldown;
			xSize->Attribute("cooldown", &cooldown);
			enemy->cooldownTime[index] = (float)cooldown;

			//move on
			++index;
		}
#pragma endregion

#pragma region Fire Rate

		if (xSize->NextSiblingElement("fireRate"))
		{
			xSize = xSize->NextSiblingElement("fireRate");
			double fireRate;
			xSize->Attribute("time", &fireRate);
			enemy->fireRate = (float)fireRate;

		}

#pragma endregion

#pragma region Attack Sounds
		if (xSize->NextSiblingElement("attack"))
		{
			xSize = xSize->NextSiblingElement("attack");
			const char* soundFile = xSize->GetText();
			if (soundFile)
			{
				int size = strlen(soundFile) + 1;
				enemy->attackSoundFile = new char[size];
				strcpy_s(enemy->attackSoundFile, size, soundFile);
			}
		}

#pragma endregion

		xEnemy->Attribute("combatType", &tempInt);//set combattype: melee,range,aoe
		enemy->combatType = (CombatType)tempInt;

		enemyCollection.push_back(enemy);
		xEnemy = xEnemy->NextSiblingElement("Enemy");
	}

	//unload loading screens
	//for (unsigned int index = 0; index < 6; ++index)
	//	SGD::GraphicsManager::GetInstance()->UnloadTexture(loadingTextures[index]);
	ThreadData* threadData = ThreadData::GetInstance();
	threadData->countMutex.lock();
	--threadData->numberOfThreads;
	threadData->countCondition.notify_all();
	threadData->countMutex.unlock();
}
//*******************STATIC FUNCTIONS***********************
//returns an enemy COPY to be added to the entity manager
void LevelManager::CreateEnemy(std::string enemyName, SGD::Point position)
{
	Enemy* copy = nullptr;//<--------COPY
	for (unsigned int i = 0; i < enemyCollection.size(); ++i)
	{
		EnemyData* sample = enemyCollection[i];
		if (enemyCollection[i]->name == enemyName)
		{

			//make a duplicate and place in the position
			if (sample->tier < 2)
				copy = new Enemy(sample->name.c_str());
			else if (sample->tier == 2)
				copy = new Boss(sample->name.c_str());

			//set the drop rate
			copy->SetDropRate(sample->droprate);
			//how many players?
			int amountOfPlayers = PlayGameState::GetInstance()->GetParty().size();
			//decrease for array subscripts
			amountOfPlayers--;


			copy->GetCharacterStats()->InitializeStats(sample->damage[amountOfPlayers], sample->health[amountOfPlayers], sample->speed, 
				sample->fireRate, 0.70f, 1.5f);
			copy->SetDropRate(sample->droprate);
			copy->SetHealth(copy->GetMaxHealth());
			

			//ai stamp
			AIStamp* stamp = copy->GetAIStamp();
			//combat type
			stamp->combatType = sample->combatType;
			//owner
			stamp->owner = copy;
			//tier
			stamp->tier = sample->tier;
			//attack sound
			stamp->SetAttackSound(sample->attackSoundFile);

			if (enemyName == "infernokong")
				AudioQueue::GetInstance()->AddSound(SGD::AudioManager::GetInstance()->LoadAudio(stamp->GetAttackSound()));

			//set the skills
			for (unsigned int i = 0; i < 3; ++i)
			{
				if (sample->skills[i])
				{
					copy->GetAIStamp()->enemySkills[i].first = sample->skills[i];
					copy->GetAIStamp()->enemySkills[i].second = sample->cooldownTime[i];
				}
			}
			copy->SetSize(sample->size);

			//position
			copy->SetPosition(position);

			//add the entity to the manager
			entityManager->AddEntity(copy, EntityType::ENT_ENEMY);
			//release owner
			copy->Release();
			break;
		}
	}
	assert((copy != nullptr) && ("INVALID ENEMY NAME"));
	copy = nullptr;
}

EnemyData::EnemyData()
{
	for (unsigned int i = 0; i < 3; ++i)
		skills[i] = nullptr;
}

EnemyData::~EnemyData()
{
	for (unsigned int i = 0; i < 3; ++i)
	if (skills[i])
	{
		delete[] skills[i];
		skills[i] = nullptr;
	}

	if (attackSoundFile)
	{
		delete [] attackSoundFile;
		attackSoundFile = nullptr;
	}
}


