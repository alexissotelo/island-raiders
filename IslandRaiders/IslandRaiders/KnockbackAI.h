#pragma once
#include "EnemyAI.h"

class KnockbackAI : public EnemyAI
{
public:
	KnockbackAI(void) = default;
	~KnockbackAI(void) = default;

	//update - must override
	virtual void Update(float dt, AIStamp& stamp);
	//AI identifier
	virtual EnemyAIType GetAIType(void) const { return EnemyAIType::Knockback; }

private:
	void FallDown(float dt, AIStamp& stamp);
	void ExtraFlinch(float dt, AIStamp& stamp);
};

