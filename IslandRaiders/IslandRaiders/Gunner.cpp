#include "Gunner.h"

#include "../SGD Wrappers/SGD_InputManager.h"
#include "../SGD Wrappers/SGD_MessageManager.h"
#include "../SGD Wrappers/SGD_EventManager.h"
#include "../SGD Wrappers/SGD_Event.h"
#include "../SGD Wrappers/SGD_AudioManager.h"

#include "PlayerSkill.h"
#include "AnimatedParticle.h"
#include "DestroyEntityMessage.h"
#include "CreateAnimatedParticle.h"
#include "AudioQueue.h"

#include <sstream>

Gunner::Gunner(const char* name) : Player(name)
{
	//set the name
	SetName(name);
	SetSkill(0, &PlayerSkill::Basic_SmokeScreen);
	playerType = PlayerType::swordsman;

	//set icon
	playerHUD->LoadIcon("resources/graphics/icons/gunner/basic.png", 0);

	//register
	RegisterForEvent("BACK");
}

Gunner::Gunner(const char* name, unsigned int controllerNumber) :Player(name)
{
	//set animation owner
	aTimeStamp.m_sOwnerName = GetName();
	aTimeStamp.SwitchAnimation("idle");
	//set the controller number
	controllerIndex = controllerNumber;
	//set skill
	SetSkill(0, &PlayerSkill::Basic_SmokeScreen);
	//set icon
	playerHUD->LoadIcon("resources/graphics/icons/gunner/basic.png", 0);

	//register
	RegisterForEvent("BACK");
}


Gunner::~Gunner()
{
}

void Gunner::Update(float dt)
{
	//local input manager
	SGD::InputManager* pInput = SGD::InputManager::GetInstance();

#if CHEAT_SKILLS
	//set skill
	if (pInput->IsKeyPressed(SGD::Key::NumPad1))
	{
		//set skill
		SetSkill(1, &(PlayerSkill::Claymore));
		this->GetPlayerHUD()->LoadIcon("resources/graphics/icons/mage/apothecary.png", 1);

	}
	//set skill
	if (pInput->IsKeyPressed(SGD::Key::NumPad2))
	{
		//set skill
		SetSkill(2, &(PlayerSkill::CannonFire));
		this->GetPlayerHUD()->LoadIcon("resources/graphics/icons/mage/apothecary.png", 2);
	}
#endif
	Player::Update(dt);
}
bool Gunner::HandleCollision(const IEntity* other)
{
	return Player::HandleCollision(other);
}
bool Gunner::UpdateCombo(float dt)
{
	//combo list
	//	X = N		Y = M
	//update the queue
	if (inputQueue.empty())
	{
		//timer run out?
		if (inputQueue.m_fUpdate > 0.45f)
		{
			//animation done?
			if (aTimeStamp.bLoop == false && aTimeStamp.isDone == false)
				return true;

			//reset combo pos
			currCombo = COMBO_LIST::NONE;
			//reset the input
			inputQueue.m_fUpdate = 0;
			comboX = 1;

		}
		//fail
		return false;
	}
	else
	{
		//animation done?
		if (aTimeStamp.bLoop == false && aTimeStamp.isDone == false)
			return true;

		//key at the front
		SGD::Key topKey = inputQueue.front();
		//remove the top keye
		inputQueue.pop();

		//quick atk
		if (topKey == SGD::Key::J)
			stat.bQuickAttack = true;
		else if (SGD::Key::K == topKey)
			stat.bQuickAttack = false;

		if (comboX == 1)
			stat.projectileSpeed = 1.5f;
		else if (comboX == 2)
			stat.projectileSpeed = 2.0f;
		else if (comboX == 3)
			stat.projectileSpeed = 1.0f;

		stat.fireFreq = 1.0f;
		//update the state
		switch (currCombo)
		{
#pragma region NONE

		case Player::NONE:
		{
			//light atk?
			if (topKey == SGD::Key::J)
				currCombo = COMBO_LIST::X;
			//heavy atk
			else if (SGD::Key::K == topKey)
				currCombo = COMBO_LIST::Y;
		}
			break;
#pragma endregion

#pragma region X
		case Player::X:
		{

			if (topKey == SGD::Key::J)
			{
				if (comboX == 3)
				{
					stat.fireTimer = 0.0f;
					stat.fireFreq = (bSpecialCombo) ? 0.1f :0.25f;
					comboX = 1;
					currCombo = NONE;
					inputQueue.clear();
				}
				else
					currCombo = COMBO_LIST::XX;
			}
			else if (SGD::Key::K == topKey)
				currCombo = COMBO_LIST::Y;
		}
			break;
#pragma endregion 

#pragma region Y
		case Player::Y:
		{
			//heavy combo
			if (SGD::Key::K == topKey)
				currCombo = COMBO_LIST::YY;
			//no other combo extension
			else
				currCombo = COMBO_LIST::NONE;

			comboX = 1;
			inputQueue.clear();
		}
			break;
#pragma endregion

#pragma region XX

		case Player::XX:
		{
			//light atk?
			if (topKey == SGD::Key::J && comboX < 3)
			{
				currCombo = COMBO_LIST::X;
				comboX += 1;
			}
			//heavy atk
			else if (SGD::Key::K == topKey)
				currCombo = COMBO_LIST::Y;
			else
			{
				currCombo = COMBO_LIST::NONE;
				comboX = 1;
			}
			stat.fireTimer = 0.0f;
			inputQueue.clear();
			break;
		}
			break;
#pragma endregion

#pragma region XXX

		case Player::XXX:
		{
			//light atk?
			if (topKey == SGD::Key::J && comboX < 2)
			{
				currCombo = COMBO_LIST::X;
				comboX += 1;
			}
			//heavy atk
			else if (SGD::Key::K == topKey)
				currCombo = COMBO_LIST::Y;
			else
			{
				currCombo = COMBO_LIST::NONE;
				comboX = 1;
			}
			stat.fireTimer = 0.0f;
			inputQueue.clear();
			break;
		}
#pragma endregion

		default:
			currCombo = COMBO_LIST::NONE;
			comboX = 1;
			inputQueue.clear();
			break;
		}

		inputQueue.m_fUpdate = 0.f;
	}

	std::string attackName = (bSpecialCombo) ? "special_" : "";

	std::stringstream depth;
	depth << comboX;

	//change the animation to fit the combo
	switch (currCombo)
	{
	case Player::X:
		attackName += "attack_x";
		attackName += depth.str().c_str();
		//play a sound
		AudioQueue::GetInstance()->AddSound(SGD::AudioManager::GetInstance()->LoadAudio(L"resources/audio/sound effects/silencer.wav"));

		break;
	case Player::Y:
		attackName = "attack_y";
		break;
	case Player::XX:
		attackName += "attack_x_x";
		attackName += depth.str().c_str();
		//play a sound
		AudioQueue::GetInstance()->AddSound(SGD::AudioManager::GetInstance()->LoadAudio(L"resources/audio/sound effects/silencer.wav"));

		break;
	case Player::YY:
		attackName = "attack_y_y";
		break;
	case Player::XXX:
		attackName += "attack_x_x_x";
		attackName += depth.str().c_str();
		//play a sound
		AudioQueue::GetInstance()->AddSound(SGD::AudioManager::GetInstance()->LoadAudio(L"resources/audio/sound effects/silencer.wav"));
		break;
	case Player::NONE:
		attackName = "idle";
	default:
		break;
	}
	aTimeStamp.SwitchAnimation(attackName.c_str());

	//no moving
	SetVelocity({ 0, 0 });

	//success!
	return true;
}
void Gunner::HandleEvent(const SGD::Event* pEvent)
{
	//damaage
	if (pEvent->GetEventID() == "DAMAGE")
	{
		if (aTimeStamp.m_sAction == "marksman" || aTimeStamp.m_sAction == "camper"
			|| aTimeStamp.m_sAction == "cannon" || aTimeStamp.m_sAction == "hunter"
		 || skillStamp[0].m_bStart)
			return;
		else
		{
			Player::HandleEvent(pEvent);
			currCombo = NONE;
			return;
		}
	}

	if (pEvent->GetEventID() == "POKE")
	{
		//destroy the claymore
		AnimatedParticle* particle = (AnimatedParticle*)pEvent->GetSender();
		//data
		IEntity* data = (IEntity*)pEvent->GetData();
#pragma region Torch

		if (particle->GetName() == "torch" && skillStamp[1].m_bStart && dynamic_cast<Character*>(data))
		{
			//destroy the torch
			DestroyEntityMessage* msg = new DestroyEntityMessage(particle);
			//create the explosion
			CreateAnimatedParticle* particleMsg = new CreateAnimatedParticle(particle->GetOwner(), "explosion", 5.0f, { 255, 255, 255, 255 }, particle->GetPosition());
			//queue messages
			SGD::MessageManager::GetInstance()->QueueMessage(msg);
			SGD::MessageManager::GetInstance()->QueueMessage(particleMsg);
			//timer 
			skillStamp[1].m_fSkillTimer = skillStamp[1].m_fSkillCooldown;
			//deactive the skill
			skillStamp[1].m_bStart = false;
			//reset
			skillStamp[1].deltaT = 0.0f;
		}
#pragma endregion

#pragma region Explosion & Spirit Bomb
		else if (particle->GetName() == "redclaw")
		{
			int x = 0;
		}
		else if (particle->GetName() == "explosion")
		{
			//entity colliding with the explosion
			Character* boom = (Character*)pEvent->GetData();

			//damage to deal
			AttackInfo atkinfo;
			atkinfo.damage = 2.5f * stat.damage;
			atkinfo.isCriticalHit = false;

			//no more damage
			particle->GetTimeStamp()->m_CurrentFrame++;
			particle->GetTimeStamp()->m_fFrameTimer = 0.f;


			//event to the colliding entity
			SGD::Event* evt = new SGD::Event("DAMAGE", &atkinfo, this);
			evt->SendEventNow(boom);
			delete evt;
		}
		else if (particle->GetName() == "spiritbomb")
		{
			//entity colliding with the explosion
			Character* boom = (Character*)pEvent->GetData();

			//damage to deal
			AttackInfo atkinfo;
			atkinfo.damage = 2.0f * stat.damage;
			atkinfo.isCriticalHit = false;

			//no more damage
			particle->GetTimeStamp()->m_CurrentFrame++;
			particle->GetTimeStamp()->m_fFrameTimer = 0.f;


			//event to the colliding entity
			SGD::Event* evt = new SGD::Event("DAMAGE", &atkinfo, this);
			evt->SendEventNow(boom);
			delete evt;
		}
#pragma endregion

#pragma region  Flamespread

		else if (particle->GetName() == "flamespread")
		{
			//character colliding with
			Character* other = (Character*)pEvent->GetData();

			//knockback
			SGD::Event pow = { "KNOCKBACK", nullptr, this };
			pow.SendEventNow(other);

			//burn - 3 secs for 1 pt each
			float burn[2] = { 1.f, 3.f };
			SGD::Event burnEm = { "BURN", &burn, this };
			burnEm.SendEventNow(other);

		}
#pragma endregion

#pragma region Black Pellet

		else if (particle->GetName() == "blackpellet")
		{
			//character colliding with
			Character* other = (Character*)pEvent->GetData();

			//create a target set to the opponents position
			CreateAnimatedParticle* msg = new CreateAnimatedParticle(other, "target", 10.f, { 150, 255, 255, 255 });

			//queue a message
			SGD::MessageManager::GetInstance()->QueueMessage(msg);
			msg = nullptr;
		}

#pragma endregion

#pragma region Bug 

		else if (particle->GetName() == "bugburn")
		{
			//entity colliding with the explosion
			Character* boom = (Character*)pEvent->GetData();
			//damage to deal
			float dmg = GetDamage();
			//event to the colliding entity
			SGD::Event* evt = new SGD::Event("DAMAGE", &dmg, this);
			evt->SendEventNow(boom);
			int x = 0;
		}

#pragma endregion

#pragma region Airstrike

		else if (particle->GetName() == "indicator")
		{
			//the entity we collided with
			Entity* other = (Entity*)pEvent->GetData();

			//do so only when the other object is a pink missile
			if (other->GetName() == "pinkmissile")// && other->GetOwner() == this)
			{
				Missile* miss = (Missile*)other;
				if (miss->GetOwner() == this)
				{				//explode
					CreateAnimatedParticle* msg = new CreateAnimatedParticle(this, "explosion", 5.0f, { 255, 255, 255, 255 }, miss->GetPosition());
					SGD::MessageManager::GetInstance()->QueueMessage(msg);

					msg = new CreateAnimatedParticle(this, "explosion", 5.0f, { 255, 255, 255, 255 }, miss->GetPosition());
					SGD::MessageManager::GetInstance()->QueueMessage(msg);
					msg = nullptr;
				}

				DestroyEntityMessage* otherMissile  = new DestroyEntityMessage(other);
				otherMissile->QueueMessage();
			}
		}
#pragma endregion

	}

#pragma region Xanxus Jump Back

	if (pEvent->GetEventID() == "BACK")
	{
		//vector
		SGD::Vector fallBack;
		//fall back direction
		fallBack.y = 0;
		fallBack.x = aTimeStamp.bForwards ? -1.0f : 1.0f;
		//scale
		fallBack.x *= 200.0f;
		//set velocity
		SetVelocity(fallBack);
	}

#pragma endregion

	Player::HandleEvent(pEvent);
}

