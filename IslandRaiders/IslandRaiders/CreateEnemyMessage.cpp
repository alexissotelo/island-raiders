#include "CreateEnemyMessage.h"
#include "Entity.h"
#include "MessageID.h"

ComboAttackMessage::ComboAttackMessage(Entity* owner) 
	: Message(MessageID::MSG_COMBO_ATTACK)
{
	postMan = owner;

	if (postMan != nullptr)
		postMan->AddRef();
}


ComboAttackMessage::~ComboAttackMessage()
{
	postMan->Release();
	postMan = nullptr;
}
