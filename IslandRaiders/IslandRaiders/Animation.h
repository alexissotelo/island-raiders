#pragma once
#include "../SGD Wrappers/SGD_Geometry.h"
#include "../SGD Wrappers/SGD_Handle.h"
#include "../SGD Wrappers/SGD_Color.h"

#include <vector>

class Frame;
class AnimationTimestamp;

class Animation
{
public:
	//ctor
	Animation();
	~Animation();

	//point, image, frame, direction, color, scale
	void Render(SGD::HTexture _texture, unsigned int _frame, SGD::Point _pos, bool _forwards = true, 
		SGD::Color color = {255,255,255,255}, float _scale = 1.0f) const;

	void Update(float _dt, AnimationTimestamp* _timeStamp);
	void DoEvent(AnimationTimestamp* stamp, Frame* frame);

	//accessors
	const char* GetName(void) const { return m_sName.c_str(); }
	const char* GetImageFileName(void) const { return imageFilePath.c_str(); }
	bool IsSheetBackwards(void) const { return m_bSheetInverse; }
	bool Loop() const;

	//mutator
	void SetName(const char* _name);
	void SetLoop(bool _loop);
	void SetOrient(bool _orient);
	void SetImageFilePath(const char* filePath) { imageFilePath = filePath; }

	//functions
	void AddFrame(Frame* _frame);

	Frame* operator[](unsigned int _index) const;
	Frame* operator[](unsigned int _index);


	unsigned int size(void) const { return m_vFrames.size(); }
private:
	std::string imageFilePath; //filename of the image to render
	std::string m_sName; //name of the animation
	std::vector<Frame*> m_vFrames; //vector of frames
	bool m_bLoop; //does the animation loop?
	bool m_bSheetInverse; //is the sheet for the animation backwards?


	//Helper Functions
	bool CreateMissile		(AnimationTimestamp* stamp, Frame* frame);
	bool CreateParticles	(AnimationTimestamp* stamp, Frame* frame);
	bool CreateLaser		(AnimationTimestamp* stamp, Frame* frame);
	bool CreateBeam			(AnimationTimestamp* stamp, Frame* frame);
	
};

