#pragma once
#include "..\SGD Wrappers\SGD_Geometry.h"

class IEntity
{
public:
	
	virtual void Update(float dt) = 0;
	virtual void Render() = 0;

	virtual bool HandleCollision(const IEntity* other) = 0;
	virtual void AddRef() = 0;
	virtual void Release() = 0;

	virtual unsigned int GetType() const = 0;
	virtual SGD::Rectangle GetRect() const  = 0;

protected:
	IEntity()  = default;
	virtual ~IEntity() = default;

};

