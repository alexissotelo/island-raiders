#include "FleeAI.h"
#include "EnemyAI.h"
#include "../SGD Wrappers/SGD_Geometry.h"

/**************************************************************
|	Function:
|	Purpose:
|	Parameter:
|	Return Value:
**************************************************************/
void FleeAI::Update(float dt, AIStamp& stamp)
{
	if (stamp.target == nullptr)
	{
		//pursue someone
		stamp.SwitchAIMode(EnemyAIType::Idle);
		//failed
		return;
	}

	//run away from the player for three seconds
	stamp.fStartTimer += dt;

	//time up?
	if (stamp.fStartTimer >= 1.0f || !stamp.target)
		//pursue someone
		stamp.SwitchAIMode(EnemyAIType::Idle);

	//run away vector
	SGD::Vector awayTarget = stamp.owner->GetPosition() - stamp.target->GetPosition();

	//distance
	float distance = awayTarget.ComputeLength();

	//normalize
	awayTarget.Normalize();

	//far enough?
	if (distance >= 300)
		//pursue someone
		stamp.SwitchAIMode(EnemyAIType::Idle);

	//scale 
	float speed = stamp.owner->GetMoveSpeed();
	awayTarget.x *= speed;
	awayTarget.y *= speed;

	//set the velocity
	stamp.owner->SetVelocity(awayTarget);

	//set the direction for animations
	if (stamp.owner->GetVelocity().x > 0)
		stamp.owner->GetTimeStamp()->bForwards = true;
	else if (stamp.owner->GetVelocity().x < 0)
		stamp.owner->GetTimeStamp()->bForwards = false;



	//set the animation to walk
	stamp.owner->GetTimeStamp()->SwitchAnimation("walk");

}


