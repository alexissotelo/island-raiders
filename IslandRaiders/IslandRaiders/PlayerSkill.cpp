#include "PlayerSkill.h"

#include "PlayerSkillStamp.h"
#include "PlayGameState.h"
#include "EntityManager.h"
#include "Player.h"
#include "Alchemist.h"
#include "Gunner.h"
#include "Swordsman.h"
#include "Enemy.h"
#include "AudioQueue.h"


#include "CreateAnimatedParticle.h"
#include "CreateMissileMessage.h"
#include "CreateFamiliar.h"
#include "../SGD Wrappers/SGD_MessageManager.h"
#include "../SGD Wrappers/SGD_AudioManager.h"

#include "../SGD Wrappers/SGD_Event.h"
#include "../SGD Wrappers/SGD_EventManager.h"


//Basics
/**************************************************************
|	Function:
|	Purpose:
|	Parameter:
|	Return Value:
**************************************************************/
bool PlayerSkill::Basic_AllyHealing(float dt, Player* player, bool bStart)
{
	if (player->GetName() == "mukuro")
		return SnakeTrap(dt, player, bStart);

	const float cooldown = 15.f;

	//current stamp
	PlayerSkillStamp* playerSkill = player->GetSkillStamp(); //skill stamp container
	playerSkill->m_mapSkills[0].m_fSkillCooldown = cooldown; //cooldown max set
	SkillStamp* skillStamp = &(*playerSkill)[0]; //local skill stamp

	//update the timer
	skillStamp->m_fSkillTimer -= dt;//decrement the wait time

	//cap
	if (skillStamp->m_fSkillTimer < 0)
		skillStamp->m_fSkillTimer = 0.0f;

	//can we run the skill?
	if (skillStamp->m_fSkillTimer > 0 || !bStart) //check player input and cooldown time
		//no -- failure
		return false;

	////calculate the healing rate
	if (skillStamp->deltaT == 0.0f)
	{//the current damage of the healing character
		skillStamp->originalDamage = player->GetDamage();
		//play a sound
		AudioQueue::GetInstance()->AddSound(SGD::AudioManager::GetInstance()->LoadAudio(L"resources/audio/sound effects/heal.wav"));
	}



	//check timer - have we reached 5 secs?
	if (skillStamp->deltaT > 3.0f)
	{
		//reset all to zero
		skillStamp->deltaT = 0.0f;
		skillStamp->originalDamage = 0.f;
		skillStamp->deltaDamage = 0.0f;
		//timer 
		skillStamp->m_fSkillTimer = cooldown;

		//deactive the skill
		skillStamp->m_bStart = false;
		//stop updating skills
		return false;
	}

	//local gameplay instance
	PlayGameState* pGame = PlayGameState::GetInstance();

	//healing rate = 5% of the damage he deals
	float healPower = skillStamp->originalDamage * dt;

	//send a heal event to all players
	SGD::Event* healAll = new SGD::Event("HEAL", &healPower);

	for (unsigned int index = 0; index < pGame->GetParty().size(); ++index)
	{
		//reference to the player to send the event to
		Character* otherPlayer = pGame->GetPartyMember(index);

		//send NOW
		healAll->SendEventNow(otherPlayer);

		if (skillStamp->deltaT == 0.0f)
		{
			//Create a animtion so show healing
			CreateAnimatedParticle* msg = new CreateAnimatedParticle(otherPlayer, "heal", 5.0f, { 150, 255, 255, 255 });
			SGD::MessageManager::GetInstance()->QueueMessage(msg);
		}
	}

	//delete event 
	delete healAll;
	healAll = nullptr;


	//update the delta timer
	skillStamp->deltaT += dt;

	//success! --however allow the player to update by return false anyways
	return false;
}

/**************************************************************
|	Function:
|	Purpose:
|	Parameter:
|	Return Value:
**************************************************************/
bool PlayerSkill::Basic_StunEnemy(float dt, Player* player, bool bStart)
{
	const float cooldown = 8.0f;

	//current stamp
	PlayerSkillStamp* playerSkill = player->GetSkillStamp(); //skill container
	playerSkill->m_mapSkills[0].m_fSkillCooldown = cooldown; //cooldown max set
	SkillStamp* skillStamp = &(*playerSkill)[0]; //current skill stamp

	//update the timer
	skillStamp->m_fSkillTimer -= dt; //decrement timer

	//can we run the skill?
	if (skillStamp->m_fSkillTimer > 0 || !bStart)
		//no -- failure
		return false;

	//Are we done?
	if (player->GetTimeStamp()->isDone)
	{
		//give theplaye back his original damage
		//player->SetDamage(skillStamp->originalDamage);

		//reset all to zero
		skillStamp->deltaT = 0.0f;
		//skillStamp->originalDamage = 0.f;
		//skillStamp->deltaDamage = 0.0f;
		//timer 
		skillStamp->m_fSkillTimer = cooldown;

		//deactive the skill
		skillStamp->m_bStart = false;
		//return to idle
		player->GetTimeStamp()->SwitchAnimation("idle");
		//stop updating skills
		return false;
	}
	if (skillStamp->deltaT == 0)
		//	//play a sound
		AudioQueue::GetInstance()->AddSound(SGD::AudioManager::GetInstance()->LoadAudio(L"resources/audio/sound effects/sching.wav"));

	skillStamp->deltaT += dt;
	//turn on animations
	player->GetTimeStamp()->SwitchAnimation("basic");

#if 0	//set the player damage
	if (skillStamp->originalDamage != 0)
	{
		//effect damage
		skillStamp->deltaDamage = skillStamp->originalDamage;
		//update the players damage
		player->SetDamage(skillStamp->deltaDamage);

	}
	else
		//catch original damage
		skillStamp->originalDamage = player->GetDamage();
#endif

	//success!
	return true;
}

/**************************************************************
|	Function:
|	Purpose:
|	Parameter:
|	Return Value:
**************************************************************/
bool PlayerSkill::Basic_SmokeScreen(float dt, Player* player, bool bStart)
{
	//cooldown time
	const float cooldown = 15.f;
	const float skillDuration = 5.0f;

	//current stamp
	PlayerSkillStamp* playerSkill = player->GetSkillStamp();
	playerSkill->m_mapSkills[0].m_fSkillCooldown = cooldown; //cooldown max set
	SkillStamp* skillStamp = &(*playerSkill)[0];

	//update the timer
	skillStamp->m_fSkillTimer -= dt;

	//can we run the skill?
	if (skillStamp->m_fSkillTimer > 0 || !bStart)
		//no -- failure
		return false;

	//at the start of teh ability
	if (skillStamp->deltaT == 0)
	{
		//create the smokescreen
		CreateAnimatedParticle* animParticle = new CreateAnimatedParticle(player, "shield", skillDuration, { 150, 255, 255, 255 });
		SGD::MessageManager::GetInstance()->QueueMessage(animParticle);

		AudioQueue::GetInstance()->AddSound(SGD::AudioManager::GetInstance()->LoadAudio(L"resources/audio/sound effects/shieldon.wav"));

	}

	//increment time
	skillStamp->deltaT += dt;

	//times up
	if (skillStamp->deltaT >= skillDuration)
	{
		//timer 
		skillStamp->m_fSkillTimer = cooldown;
		//deactive the skill
		skillStamp->m_bStart = false;
		//reset
		skillStamp->deltaT = 0.0f;
	}

	//success!! however, allow the player to update  -- so return false anyways
	return false;
}


#pragma region Tier 1
//Tier 1
/**************************************************************
|	Function:
|	Purpose:
|	Parameter:
|	Return Value:
**************************************************************/
bool PlayerSkill::Fencer(float dt, Player* player, bool bStart)
{
	const float cooldown = 13.f;

	//current stamp
	PlayerSkillStamp* playerSkill = player->GetSkillStamp();
	playerSkill->m_mapSkills[1].m_fSkillCooldown = cooldown;
	SkillStamp* skillStamp = &(*playerSkill)[1];

	//update the timer
	skillStamp->m_fSkillTimer -= dt;

	//can we run the skill?
	if (skillStamp->m_fSkillTimer > 0 || !bStart)
		//no -- failure
		return false;

	//Are we done?
	if (player->GetTimeStamp()->isDone)
	{
		//give theplaye back his original damage
		//reset all to zero
		skillStamp->deltaT = 0.0f;
		skillStamp->deltaDamage = 0.0f;
		//timer 
		skillStamp->m_fSkillTimer = cooldown;

		//deactive the skill
		skillStamp->m_bStart = false;

		//stop updating skills
		return false;
	}

	//skill
	player->GetTimeStamp()->SwitchAnimation("skill1");


	//cap to zero
	if (skillStamp->m_fSkillTimer < 0.0f)
		skillStamp->m_fSkillTimer = 0.0f;

	//store the original damage
	if (skillStamp->deltaT == 0.0f)
	{	
		//play sound
		AudioQueue::GetInstance()->AddSound(SGD::AudioManager::GetInstance()->LoadAudio(L"resources/audio/sound effects/swordecho.wav"));
	}

	//increment the time
	skillStamp->deltaT += dt;

	//set the player damage
	//if (skillStamp->originalDamage != 0)
	//{
	//	//effect damage
	//	skillStamp->deltaDamage = skillStamp->originalDamage * 15 * dt;
	//	//update the players damage
	//	player->SetDamage(skillStamp->deltaDamage);
	//}

	//success!
	return true;
}

/**************************************************************
|	Function:
|	Purpose:
|	Parameter:
|	Return Value:
**************************************************************/
bool PlayerSkill::Berserker(float dt, Player* player, bool bStart)
{
	const float cooldown = 13.0f;

	//current stamp
	PlayerSkillStamp* playerSkill = player->GetSkillStamp(); //skill container
	playerSkill->m_mapSkills[1].m_fSkillCooldown = cooldown; //cooldown max set
	SkillStamp* skillStamp = &(*playerSkill)[1]; //current skill stamp

	//update the timer
	skillStamp->m_fSkillTimer -= dt; //decrement timer

	//can we run the skill?
	if (skillStamp->m_fSkillTimer > 0 || !bStart)
		//no -- failure
		return false;

	//Are we done?
	if (player->GetTimeStamp()->isDone)
	{
		//reset all to zero
		skillStamp->deltaT = 0.0f;
		//timer 
		skillStamp->m_fSkillTimer = cooldown;

		//deactive the skill
		skillStamp->m_bStart = false;
		//return to idle
		player->GetTimeStamp()->SwitchAnimation("idle");
		//stop updating skills
		return false;
	}

	//store the original damage
	if (skillStamp->deltaT == 0.0f)
		//play sound
		AudioQueue::GetInstance()->AddSound(SGD::AudioManager::GetInstance()->LoadAudio(L"resources/audio/sound effects/sching.wav"));

	//increment the time
	skillStamp->deltaT += dt;


	//turn on animations
	player->GetTimeStamp()->SwitchAnimation("berserker");

	//success!
	return true;
}

/**************************************************************
|	Function:
|	Purpose:
|	Parameter:
|	Return Value:
**************************************************************/
bool PlayerSkill::PoisonFog(float dt, Player* player, bool bStart)
{
	if (player->GetName() == "mukuro")
		return BlazeFire(dt, player, bStart);

	const float cooldown = 17.f;

	//current stamp
	PlayerSkillStamp* playerSkill = player->GetSkillStamp();
	playerSkill->m_mapSkills[1].m_fSkillCooldown = cooldown;
	SkillStamp* skillStamp = &(*playerSkill)[1];

	//update the timer
	skillStamp->m_fSkillTimer -= dt;

	//can we run the skill?
	if (skillStamp->m_fSkillTimer > 0 || !bStart)
		//no -- failure
		return false;

	//Are we done?
	if (player->GetTimeStamp()->isDone)
	{
		//timer 
		skillStamp->m_fSkillTimer = cooldown;
		//deactive the skill
		skillStamp->m_bStart = false;
		//stop updating skills
		return false;
	}
	//store the original damage
	if (skillStamp->deltaT == 0.0f)
	{
		//play sound
		AudioQueue::GetInstance()->AddSound(SGD::AudioManager::GetInstance()->LoadAudio(L"resources/audio/sound effects/lowfreq.wav"));
	}

	//increment the time
	skillStamp->deltaT += dt;

	//turn on animations
	player->GetTimeStamp()->SwitchAnimation("poisonfog");
	return true;
}

/**************************************************************
|	Function:
|	Purpose:
|	Parameter:
|	Return Value:
**************************************************************/
bool PlayerSkill::Marksman(float dt, Player* player, bool bStart)
{
	const float cooldown = 9.0f;

	//current stamp
	PlayerSkillStamp* playerSkill = player->GetSkillStamp();
	playerSkill->m_mapSkills[1].m_fSkillCooldown = cooldown;
	SkillStamp* skillStamp = &(*playerSkill)[1];

	//update the timer
	skillStamp->m_fSkillTimer -= dt;

	//can we run the skill?
	if (skillStamp->m_fSkillTimer > 0 || !bStart)
		//no -- failure
		return false;

	//Are we done?
	if (player->GetTimeStamp()->isDone)
	{
		//reset all to zero
		skillStamp->deltaT = 0.0f;
		//timer 
		skillStamp->m_fSkillTimer = cooldown;
		//deactive the skill
		skillStamp->m_bStart = false;
		//reset proj speed
		player->SetProjectileSpeed(1.0);

		//stop updating skills
		return false;
	}

	//turn on animations
	player->GetTimeStamp()->SwitchAnimation("marksman");
	player->SetProjectileSpeed(2.0);
	return true;

}

/**************************************************************
|	Function:
|	Purpose:
|	Parameter:
|	Return Value:
**************************************************************/
bool PlayerSkill::Claymore(float dt, Player* player, bool bStart)
{
	const float cooldown = 15.0f;
	const float skillDuration = 10.0f;
	//current stamp
	PlayerSkillStamp* playerSkill = player->GetSkillStamp();
	playerSkill->m_mapSkills[1].m_fSkillCooldown = cooldown;
	SkillStamp* skillStamp = &(*playerSkill)[1];

	//update the timer
	skillStamp->m_fSkillTimer -= dt;

	//can we run the skill?
	if (skillStamp->m_fSkillTimer > 0 || !bStart)
		//no -- failure
		return false;

	if (skillStamp->deltaT == 0)
	{
		//created animation
		CreateAnimatedParticle* msg = new CreateAnimatedParticle(player, "torch", skillDuration, { 255, 255, 255, 255 }, player->GetPosition());
		//queue message
		SGD::MessageManager::GetInstance()->QueueMessage(msg);
	}

	//increment time
	skillStamp->deltaT += dt;

	if (skillStamp->deltaT >= skillDuration)
	{
		//timer 
		skillStamp->m_fSkillTimer = cooldown;
		//deactive the skill
		skillStamp->m_bStart = false;
		//reset
		skillStamp->deltaT = 0.0f;
	}

	//complete
	return false;
}

/**************************************************************
|	Function:
|	Purpose:
|	Parameter:
|	Return Value:
**************************************************************/
bool PlayerSkill::Summoner(float dt, Player* player, bool bStart) //mage
{
	//summon a familiar
	//start the timer to run for 20 seconds

	const float cooldown = 10.0f;

	if (player->GetName() == "mukuro")
		return BlazeFire(dt, player, bStart);

	//current stamp
	PlayerSkillStamp* playerSkill = player->GetSkillStamp();
	playerSkill->m_mapSkills[1].m_fSkillCooldown = cooldown; //cooldown max set
	SkillStamp* skillStamp = &(*playerSkill)[1];

	//update the timer
	skillStamp->m_fSkillTimer -= dt;

	//no other dog exists
	if (skillStamp->deltaT < 0)
		return false;

	//cap
	if (skillStamp->m_fSkillTimer < 0)
		skillStamp->m_fSkillTimer = 0.0f;

	//can we run the skill?
	if (skillStamp->m_fSkillTimer > 0 || !bStart)
		//no -- failure
		return false;

	//position to spawn familiar
	SGD::Point spawn = player->GetPosition();
	spawn.x += 100;
	//create a message to create a familiar with the name robodog
	CreateFamiliar* msg = new CreateFamiliar("robodog", player, spawn);
	SGD::MessageManager::GetInstance()->QueueMessage(msg);
	AudioQueue::GetInstance()->AddSound(SGD::AudioManager::GetInstance()->LoadAudio(L"resources/audio/sound effects/Summon 1.wav"));

	//deactive the skill
	skillStamp->m_bStart = false;
	skillStamp->deltaT = -1.0f;

	return true;
}

#pragma endregion

//Tier 2
#pragma region Swordsman

/**************************************************************
|	Function:
|	Purpose:
|	Parameter:
|	Return Value:
**************************************************************/
bool PlayerSkill::Captain(float dt, Player* player, bool bStart)
{
	const float cooldown = 15.0f;
	const float skillDuration = 10.0f;
	//current stamp
	PlayerSkillStamp* playerSkill = player->GetSkillStamp();
	playerSkill->m_mapSkills[2].m_fSkillCooldown = cooldown;
	SkillStamp* skillStamp = &(*playerSkill)[2];

	Swordsman* sword = (Swordsman*)player;

	//update the timer
	skillStamp->m_fSkillTimer -= dt;
	//can we run the skill?
	if (skillStamp->m_fSkillTimer > 0 || !bStart)
		//no -- failure
		return false;

	if (skillStamp->deltaT == 0.f)
	{
		//play sound
		AudioQueue::GetInstance()->AddSound(SGD::AudioManager::GetInstance()->LoadAudio(L"resources/audio/sound effects/powerup.wav"));

		//all entities target me
		std::vector<IEntity*> enemyVec = PlayGameState::GetInstance()->GetManager()->GetEntityVector(ENT_ENEMY);

		for (unsigned int index = 0; index < enemyVec.size(); ++index)
		{
			//enemy to alter
			Enemy* ptr = dynamic_cast<Enemy*>(enemyVec[index]);

			//useless check
			if (!ptr)
				continue;

			//set the target to myself
			ptr->GetAIStamp()->target = player;
			ptr->GetAIStamp()->SwitchAIMode(EnemyAIType::Pursuit);
		}
		player->GetTimeStamp()->SwitchAnimation("captain");
		sword->SetCaptainOn(true);

	}

	//times up 
	if (skillStamp->deltaT >= skillDuration)
	{
		//start the cooldown timer
		skillStamp->m_fSkillTimer = cooldown;
		skillStamp->deltaT = 0.f;
		skillStamp->deltaDamage = 0.f;
		//all entities target me
		std::vector<IEntity*> enemyVec = PlayGameState::GetInstance()->GetManager()->GetEntityVector(ENT_ENEMY);

		for (unsigned int index = 0; index < enemyVec.size(); ++index)
		{
			//enemy to alter
			Enemy* ptr = dynamic_cast<Enemy*>(enemyVec[index]);

			//useless check
			if (!ptr)
				continue;

			//set the target to myself
			ptr->GetAIStamp()->target = player;
			ptr->GetAIStamp()->SwitchAIMode(EnemyAIType::Idle);
		}

		//weak ass
		sword->SetCaptainOn(false);
		skillStamp->m_bStart = false;

		//failure!
		return false;
	}
	//increment
	skillStamp->deltaT += dt;
	skillStamp->deltaDamage += dt;
	//success! -- allow the player to update anyways
	return false;
}

/**************************************************************
|	Function:
|	Purpose:
|	Parameter:
|	Return Value:
**************************************************************/
bool PlayerSkill::Hammer(float dt, Player* player, bool bStart)
{
	const float cooldown = 13.f;

	//current stamp
	PlayerSkillStamp* playerSkill = player->GetSkillStamp();
	playerSkill->m_mapSkills[2].m_fSkillCooldown = cooldown;
	SkillStamp* skillStamp = &(*playerSkill)[2];

	//update the timer
	skillStamp->m_fSkillTimer -= dt;

	//can we run the skill?
	if (skillStamp->m_fSkillTimer > 0 || !bStart)
		//no -- failure
		return false;

	//Are we done?
	if (player->GetTimeStamp()->isDone)
	{
		//timer 
		skillStamp->m_fSkillTimer = cooldown;

		//deactive the skill
		skillStamp->m_bStart = false;

		//skill
		player->GetTimeStamp()->SwitchAnimation("idle");

		//stop updating skills
		return false;
	}

	if (skillStamp->deltaT == 0.f)
		//play sound
		AudioQueue::GetInstance()->AddSound(SGD::AudioManager::GetInstance()->LoadAudio(L"resources/audio/sound effects/blade.wav"));

	skillStamp->deltaT += dt;
	//skill
	player->GetTimeStamp()->SwitchAnimation("hammer");

	////set the player damage
	//if (skillStamp->originalDamage != 0)
	//{
	//	//effect damage
	//	skillStamp->deltaDamage = skillStamp->originalDamage * dt;
	//	//update the players damage
	//	player->SetDamage(skillStamp->deltaDamage);
	//}

	//success! 
	return true;
}

/**************************************************************
|	Function:
|	Purpose:
|	Parameter:
|	Return Value:
**************************************************************/
bool PlayerSkill::Admiral(float dt, Player* player, bool bStart)
{
	const float cooldown = 13.f;

	//current stamp
	PlayerSkillStamp* playerSkill = player->GetSkillStamp();
	playerSkill->m_mapSkills[2].m_fSkillCooldown = cooldown;
	SkillStamp* skillStamp = &(*playerSkill)[2];

	//update the timer
	skillStamp->m_fSkillTimer -= dt;

	//can we run the skill?
	if (skillStamp->m_fSkillTimer > 0 || !bStart)
		//no -- failure
		return false;

	//Are we done?
	if (player->GetTimeStamp()->isDone)
	{
		//timer 
		skillStamp->m_fSkillTimer = cooldown;

		//deactive the skill
		skillStamp->m_bStart = false;

		//stop updating skills
		return false;
	}
	if (skillStamp->deltaT == 0.f)
		//play sound
		AudioQueue::GetInstance()->AddSound(SGD::AudioManager::GetInstance()->LoadAudio(L"resources/audio/sound effects/bladesharpen.wav"));

	//increment
	skillStamp->deltaT += dt;

	//skill
	player->GetTimeStamp()->SwitchAnimation("admiral");
	//success! 
	return true;

}

/**************************************************************
|	Function:
|	Purpose:
|	Parameter:
|	Return Value:
**************************************************************/
bool PlayerSkill::SeaKnight(float dt, Player* player, bool bStart)
{
	const float cooldown = 13.f;

	//current stamp
	PlayerSkillStamp* playerSkill = player->GetSkillStamp();
	playerSkill->m_mapSkills[2].m_fSkillCooldown = cooldown;
	SkillStamp* skillStamp = &(*playerSkill)[2];

	//update the timer
	skillStamp->m_fSkillTimer -= dt;

	//can we run the skill?
	if (skillStamp->m_fSkillTimer > 0 || !bStart)
		//no -- failure
		return false;

	//Are we done?
	if (player->GetTimeStamp()->isDone)
	{
		//timer 
		skillStamp->m_fSkillTimer = cooldown;

		//deactive the skill
		skillStamp->m_bStart = false;

		//stop updating skills
		return false;
	}

	//skill
	player->GetTimeStamp()->SwitchAnimation("seaknight");
	//success! 
	return true;

}

#pragma endregion

#pragma region Mage

/**************************************************************
|	Function:
|	Purpose:
|	Parameter:
|	Return Value:
**************************************************************/
bool PlayerSkill::WitchDoctor(float dt, Player* player, bool bStart)
{
	const float cooldown = 12.0f;
	const float skillDuration = 10.f;

	//current stamp
	PlayerSkillStamp* playerSkill = player->GetSkillStamp();
	playerSkill->m_mapSkills[2].m_fSkillCooldown = cooldown; //cooldown max set
	SkillStamp* skillStamp = &(*playerSkill)[2];

	//update the timer
	skillStamp->m_fSkillTimer -= dt;

	//cap
	if (skillStamp->m_fSkillTimer < 0)
		skillStamp->m_fSkillTimer = 0.0f;

	//can we run the skill?
	if (skillStamp->m_fSkillTimer > 0 || !bStart)
		//no -- failure
		return false;

	//calculate the healing rate
	if (skillStamp->deltaT == 0.0f)
	{
		AudioQueue::GetInstance()->AddSound(SGD::AudioManager::GetInstance()->LoadAudio(L"resources/audio/sound effects/bitpower.wav"));

		//spawn point
		SGD::Point pt = player->GetPosition();
		pt.y -= 10.0f;

		//create animated particle
		CreateAnimatedParticle* boost = new CreateAnimatedParticle(player, "boost", 3.0f, { 255, 255, 255, 255 }, pt);
		SGD::MessageManager::GetInstance()->QueueMessage(boost);
		boost = nullptr;
	}

	//update the delta timer
	skillStamp->deltaT += dt;

	//local gameplay instance
	PlayGameState* pGame = PlayGameState::GetInstance();
	std::vector<IEntity*> allyPtr = pGame->GetManager()->GetEntityVector(ENT_PLAYER);

	//check timer - have we reached 5 secs?
	if (skillStamp->deltaT >= skillDuration)
	{
		//reset all to zero
		skillStamp->deltaT = 0.0f;
		//timer 
		skillStamp->m_fSkillTimer = skillStamp->m_fSkillCooldown;

		for (unsigned int i = 0; i < allyPtr.size(); i++)
		{
			//reset
			Player* playerPtr = (Player*)allyPtr[i];
			//boost
			playerPtr->SetAttackBoost(1.0f);
		}

		//deactive the skill
		skillStamp->m_bStart = false;
		//stop updating skills
		return false;
	}

	for (unsigned int i = 0; i < allyPtr.size(); i++)
	{
		//player pointer
		Character* playerPtr = (Character*)allyPtr[i];
		//boost
		playerPtr->SetAttackBoost(2.5f);
	}

	//success! --however allow the player to update by return false anyways
	return false;
}

/**************************************************************
|	Function:
|	Purpose:
|	Parameter:
|	Return Value:
**************************************************************/
bool PlayerSkill::BeastMaster(float dt, Player* player, bool bStart)
//mage
{
	//summon a familiar
	//start the timer to run for 20 seconds
	const float cooldown = 15.0f;

	//current stamp
	PlayerSkillStamp* playerSkill = player->GetSkillStamp();
	playerSkill->m_mapSkills[2].m_fSkillCooldown = cooldown; //cooldown max set
	SkillStamp* skillStamp = &(*playerSkill)[2];

	//update the timer
	skillStamp->m_fSkillTimer -= dt;

	//no other dragon exists
	if (skillStamp->deltaT < 0)
		return false;

	//cap
	if (skillStamp->m_fSkillTimer < 0)
		skillStamp->m_fSkillTimer = 0.0f;

	//can we run the skill?
	if (skillStamp->m_fSkillTimer > 0 || !bStart)
		//no -- failure
		return false;

	//position to spawn familiar
	SGD::Point spawn = player->GetPosition();
	spawn.x += 100;
	//create a message to create a familiar with the name robodog
	CreateFamiliar* msg = new CreateFamiliar("blackdragon", player, spawn);
	SGD::MessageManager::GetInstance()->QueueMessage(msg);
	msg = nullptr;

	AudioQueue::GetInstance()->AddSound(SGD::AudioManager::GetInstance()->LoadAudio(L"resources/audio/sound effects/Summon 1.wav"));

	//deactive the skill
	skillStamp->m_bStart = false;
	skillStamp->deltaT = -1.0f;

	//success!
	return true;
}

/**************************************************************
|	Function:
|	Purpose:
|	Parameter:
|	Return Value:
**************************************************************/
bool PlayerSkill::Pyromaniac(float dt, Player* player, bool bStart)
{
	//cooldoqwn
	const float cooldown = 15.f;
	//current stamp
	PlayerSkillStamp* playerSkill = player->GetSkillStamp();
	playerSkill->m_mapSkills[2].m_fSkillCooldown = cooldown; //cooldown max set
	SkillStamp* skillStamp = &(*playerSkill)[2];

	//update the timer
	skillStamp->m_fSkillTimer -= dt;

	//can we run the skill?
	if (skillStamp->m_fSkillTimer > 0 || !bStart)
		//no -- failure
		return false;

	//update timer
	skillStamp->deltaT += dt;

	//no moving
	player->SetVelocity({ 0.f, 0.f });

	if (skillStamp->originalDamage < 5.0f)
	{

		//skill
		player->GetTimeStamp()->SwitchAnimation("ability");
		//delta
		skillStamp->deltaDamage = dt;

		if (skillStamp->deltaT >= 0.5f)
		{

			//position to spawn fire pillars
			SGD::Point ptRight, ptLeft;
			ptRight = ptLeft = player->GetPosition();
			ptRight.x += 100.0f; ptLeft.x -= 100.0f;
			ptRight.x += 100.0f * skillStamp->originalDamage; ptLeft.x -= 100.0f * skillStamp->originalDamage;
			ptRight.y += 1.0f; ptLeft.y += 1;

			//fire
			//	std::string fire = player->GetName() == "mystogan" ? "firepillar" : "blackfire";
			//create fire pillars 
			CreateAnimatedParticle* rightPillar = new CreateAnimatedParticle(player, "firepillar", 5.0f, { 255, 255, 255, 255 }, ptRight);
			CreateAnimatedParticle* leftPillar = new CreateAnimatedParticle(player, "firepillar", 5.0f, { 255, 255, 255, 255 }, ptLeft);


			//queue message
			SGD::MessageManager::GetInstance()->QueueMessage(rightPillar);
			SGD::MessageManager::GetInstance()->QueueMessage(leftPillar);

			//add sound
			AudioQueue::GetInstance()->AddSound(SGD::AudioManager::GetInstance()->LoadAudio(L"resources/audio/sound effects/steamburst.wav"));


			//next pillar
			skillStamp->originalDamage += 1.0f;

			//reset
			skillStamp->deltaT = 0.0f;
		}

	}
	else
	{
		//reset all to zero
		skillStamp->deltaT = 0.0f;
		skillStamp->originalDamage = 0.f;
		skillStamp->deltaDamage = 0.0f;
		//timer 
		skillStamp->m_fSkillTimer = cooldown;

		//deactive the skill
		skillStamp->m_bStart = false;
	}

	//success! 
	return true;
}

/**************************************************************
|	Function:
|	Purpose:
|	Parameter:
|	Return Value:
**************************************************************/
bool PlayerSkill::OmniKnight(float dt, Player* player, bool bStart)
{
	const float cooldown = 23.0f;

	//current stamp
	PlayerSkillStamp* playerSkill = player->GetSkillStamp();
	playerSkill->m_mapSkills[2].m_fSkillCooldown = cooldown; //cooldown max set
	SkillStamp* skillStamp = &(*playerSkill)[2];
	Alchemist* alch = (Alchemist*)player;

	//update the timer
	skillStamp->m_fSkillTimer -= dt;

	//cap
	if (skillStamp->m_fSkillTimer < 0)
		skillStamp->m_fSkillTimer = 0.0f;

	//can we run the skill?
	if (skillStamp->m_fSkillTimer > 0 || !bStart)
		//no -- failure
		return false;

	if (skillStamp->deltaT == 0.0f)
	{

		if (skillStamp->deltaT == 0)
			//	//play a sound
			AudioQueue::GetInstance()->AddSound(SGD::AudioManager::GetInstance()->LoadAudio(L"resources/audio/sound effects/lowfreqboom.wav"));

		skillStamp->deltaT += dt;


		//hold onto this stats
		alch->StoreOriginalStats(*player->GetCharacterStats());
		//create a new stat
		CharacterStats mukuro;
		mukuro.InitializeStats(player->GetDamage() * 1.5f, player->GetMaxHealth(), player->GetMoveSpeed() * 1.5f, player->GetFireFreq(),
			player->GetDefensiveStat(), player->GetProjectileSpeed());

		mukuro.health = player->GetHealth();
		mukuro.quickPercent = player->GetCharacterStats()->quickPercent;
		mukuro.heavyPercent = player->GetCharacterStats()->heavyPercent;


		//skill
		player->GetTimeStamp()->m_sOwnerName = "mukuro";
		player->SetName("mukuro");
		player->GetTimeStamp()->SwitchAnimation("intro");
		player->SetVelocity({ 0.f, 0.f });

		//copy to mukuro
		player->GetCharacterStats()->CopyStats(mukuro);

		//skylight
		CreateAnimatedParticle* msg = new CreateAnimatedParticle(player, "lightbeam", 5.0f, { 255, 255, 255, 255 }, player->GetPosition());
		SGD::MessageManager::GetInstance()->QueueMessage(msg);

	}
	//update the delta timer
	skillStamp->deltaT += dt;

	//check timer - have we reached 5 secs?
	if (skillStamp->deltaT > 20.0f)
	{
		//reset all to zero
		skillStamp->deltaT = 0.0f;
		skillStamp->deltaDamage = 0.0f;
		//timer 
		skillStamp->m_fSkillTimer = cooldown;

		player->SetName(alch->GetOriginalName());
		player->GetTimeStamp()->m_sOwnerName = alch->GetOriginalName();
		player->GetTimeStamp()->SwitchAnimation("intro");
		player->GetCharacterStats()->CopyStats(*alch->GetOriginalStats());
		alch->ClearTempStats();

		player->SetVelocity({ 0.0f, 0.0f });
		//deactive the skill
		skillStamp->m_bStart = false;
	}
	//success! --however allow the player to update by return false anyways
	return false;
}

#pragma endregion

#pragma region Gunner

bool PlayerSkill::Camper(float dt, Player* player, bool bStart)
{
	const float cooldown = 13.0f;
	//current stamp
	PlayerSkillStamp* playerSkill = player->GetSkillStamp();
	playerSkill->m_mapSkills[2].m_fSkillCooldown = cooldown; //cooldown max set
	SkillStamp* skillStamp = &(*playerSkill)[2];

	//update the timer -- timer to use the skill
	skillStamp->m_fSkillTimer -= dt;

	//can we run the skill?
	if (skillStamp->m_fSkillTimer > 0 || !bStart)
		//no -- failure
		return false;

	//run the proper animations
	player->GetTimeStamp()->SwitchAnimation("camper");

	//initialize all damage values
	if (player->GetTimeStamp()->isDone)
	{
		skillStamp->originalDamage = 0.f;
		skillStamp->deltaDamage = 0.f;
		skillStamp->deltaT = 0.f;
		//timer 
		skillStamp->m_fSkillTimer = cooldown;

		//deactive the skill
		skillStamp->m_bStart = false;
	}

	if (skillStamp->deltaT == 0.f)
		AudioQueue::GetInstance()->AddSound(SGD::AudioManager::GetInstance()->LoadAudio(L"resources/audio/sound effects/tunnellaugh.wav"));

	//update timer == timer since the skill has been initialized
	skillStamp->deltaT += dt;

	//no moving
	player->SetVelocity({ 0.f, 0.f });

	//every 0.5 seconds during the animation
	if (skillStamp->deltaT >= 0.40f)
	{
		//position to spawn fire pillars
		SGD::Point ptRight, ptLeft;
		ptRight = ptLeft = player->GetPosition();
		ptRight.x += 100.0f; ptLeft.x -= 100.0f;
		ptRight.x += 100.0f * skillStamp->deltaDamage; ptLeft.x -= 100.0f * skillStamp->deltaDamage;
		ptRight.y += 1.0f; ptLeft.y += 1;

		float movement = 150.0f / player->GetMoveSpeed();


		//create indicators for the missiles
		CreateAnimatedParticle* rightPillar = new CreateAnimatedParticle(player, "indicator", 5.0f, { 255, 255, 255, 255 }, ptRight);
		CreateAnimatedParticle* leftPillar = new CreateAnimatedParticle(player, "indicator", 5.0f, { 255, 255, 255, 255 }, ptLeft);

		//place the missiles off screen
		ptRight.y -= 900;
		ptLeft.y -= 900;

		//camera offset
		ptRight.x -= PlayGameState::GetInstance()->GetCameraPosition().x;
		ptRight.y -= PlayGameState::GetInstance()->GetCameraPosition().y;
		//camera offset
		ptLeft.x -= PlayGameState::GetInstance()->GetCameraPosition().x;
		ptLeft.y -= PlayGameState::GetInstance()->GetCameraPosition().y;


		//create missiles to drop
		CreateMissileMessage* rightMissile = new CreateMissileMessage(player, ptRight, NORMAL, "pinkmissile", movement, 3.50f, { 0, 1 });
		CreateMissileMessage* leftMissile = new CreateMissileMessage(player, ptLeft, NORMAL, "pinkmissile", movement, 3.50f, { 0, 1 });

		//queue message
		SGD::MessageManager::GetInstance()->QueueMessage(rightPillar);
		SGD::MessageManager::GetInstance()->QueueMessage(leftPillar);
		SGD::MessageManager::GetInstance()->QueueMessage(rightMissile);
		SGD::MessageManager::GetInstance()->QueueMessage(leftMissile);

		//next pillar
		skillStamp->deltaDamage += 1.0f;

		//reset
		skillStamp->deltaT = 0.0f;
	}

	//success! 
	return true;
}

bool PlayerSkill::CannonFire(float dt, Player* player, bool bStart)
{
	const float cooldown = 11.f;

	//current stamp
	PlayerSkillStamp* playerSkill = player->GetSkillStamp();
	playerSkill->m_mapSkills[2].m_fSkillCooldown = cooldown; //cooldown max set
	SkillStamp* skillStamp = &(*playerSkill)[2];

	//update the timer
	skillStamp->m_fSkillTimer -= dt;

	//can we run the skill?
	if (skillStamp->m_fSkillTimer > 0 || !bStart)
		//no -- failure
		return false;

	//Are we done?
	if (player->GetTimeStamp()->isDone)
	{
		player->SetDamage(skillStamp->originalDamage);
		//reset all to zero
		skillStamp->deltaT = 0.0f;
		skillStamp->originalDamage = 0.f;
		skillStamp->deltaDamage = 0.0f;
		//timer 
		skillStamp->m_fSkillTimer = cooldown;

		//deactive the skill
		skillStamp->m_bStart = false;

		//stop updating skills
		return false;
	}

	//initialize
	if (skillStamp->deltaT == 0.0f) //bigenergyexplosion
	{
		//hold onto the damage
		skillStamp->originalDamage = player->GetBaseDamage();

		AudioQueue::GetInstance()->AddSound(SGD::AudioManager::GetInstance()->LoadAudio(L"resources/audio/sound effects/cannon.wav"));

		//increase damage
		//player->SetDamage(20.0f * skillStamp->originalDamage);
	}

	skillStamp->deltaT += dt;

	//turn on animations
	player->GetTimeStamp()->SwitchAnimation("cannon");
	return true;
}

bool PlayerSkill::WeaponExchange(float dt, Player* player, bool bStart)
{
	const float cooldown = 15.f;

	//current stamp
	PlayerSkillStamp* playerSkill = player->GetSkillStamp();
	playerSkill->m_mapSkills[2].m_fSkillCooldown = cooldown; //cooldown max set
	SkillStamp* skillStamp = &(*playerSkill)[2];


	//update the timer
	skillStamp->m_fSkillTimer -= dt;

	//can we run the skill?
	if (skillStamp->m_fSkillTimer > 0 || !bStart)
		//no -- failure
		return false;

	if (skillStamp->deltaT == 0.f)
	{
		AudioQueue::GetInstance()->AddSound(SGD::AudioManager::GetInstance()->LoadAudio(L"resources/audio/sound effects/gunCock.wav"));
		skillStamp->originalDamage = player->GetMoveSpeed();
		player->SetMoveSpeed(skillStamp->originalDamage * 1.5f);
	}

	//increment timer
	skillStamp->deltaT += dt;

	//access a gunner
	Gunner* gunner = dynamic_cast<Gunner*>(player);

	//safety check
	if (!gunner)
		return false;

	//Are we done?
	if (skillStamp->deltaT >= 30)
	{
		player->SetMoveSpeed(skillStamp->originalDamage);

		//reset all to zero
		skillStamp->deltaT = 0.0f;
		//timer 
		skillStamp->m_fSkillTimer = cooldown;
		skillStamp->originalDamage = 0.f;
		gunner->SetAttackBoost(1.0f);

		//deactive the skill
		skillStamp->m_bStart = false;
		//turn off skill
		gunner->SwitchSpecialCombo(false);

		//stop updating skills
		return true;
	}

	//turn on skill
	gunner->SwitchSpecialCombo(true);
	gunner->SetAttackBoost(2.5f);

	//success! --allow the player to update regardless
	return false;
}

bool PlayerSkill::Hunter(float dt, Player* player, bool bStart)
{
	const float cooldown = 10.f;

	//current stamp
	PlayerSkillStamp* playerSkill = player->GetSkillStamp();
	playerSkill->m_mapSkills[2].m_fSkillCooldown = cooldown; //cooldown max set
	SkillStamp* skillStamp = &(*playerSkill)[2];

	//update the timer
	skillStamp->m_fSkillTimer -= dt;

	//can we run the skill?
	if (skillStamp->m_fSkillTimer > 0 || !bStart)
		//no -- failure
		return false;

	//Are we done?
	if (player->GetTimeStamp()->isDone)
	{
		player->SetDamage(skillStamp->originalDamage);

		//reset all to zero
		skillStamp->deltaT = 0.0f;
		skillStamp->originalDamage = 0.f;
		skillStamp->deltaDamage = 0.0f;
		//timer 
		skillStamp->m_fSkillTimer = cooldown;

		//deactive the skill
		skillStamp->m_bStart = false;

		//stop updating skills
		return false;
	}

	//initialize
	if (skillStamp->deltaT == 0.0f)
	{
		//hold onto the damage
		skillStamp->originalDamage = player->GetBaseDamage();

		//increase damage
		player->SetDamage(2.0f * skillStamp->originalDamage);

		if (player->GetName() == "xanxus")
			AudioQueue::GetInstance()->AddSound(SGD::AudioManager::GetInstance()->LoadAudio(L"resources/audio/sound effects/bester.wav"));
		else 		
			AudioQueue::GetInstance()->AddSound(SGD::AudioManager::GetInstance()->LoadAudio(L"resources/audio/sound effects/centipede.wav"));


	}

	skillStamp->deltaT += dt;

	//turn on animations
	player->GetTimeStamp()->SwitchAnimation("hunter");
	return true;
}
#pragma endregion

#pragma region Mukuro

bool PlayerSkill::SnakeTrap(float dt, Player* player, bool bStart)
{
	const float cooldown = 15.0f;

	//current stamp
	PlayerSkillStamp* playerSkill = player->GetSkillStamp();
	playerSkill->m_mapSkills[0].m_fSkillCooldown = cooldown;
	SkillStamp* skillStamp = &(*playerSkill)[0];

	//update the timer
	skillStamp->m_fSkillTimer -= dt;

	//can we run the skill?
	if (skillStamp->m_fSkillTimer > 0 || !bStart)
		//no -- failure
		return false;

	//Are we done?
	if (player->GetTimeStamp()->isDone)
	{
		//reset all to zero
		skillStamp->deltaT = 0.0f;
		//timer 
		skillStamp->m_fSkillTimer = cooldown;

		//deactive the skill
		skillStamp->m_bStart = false;

		//stop updating skills
		return false;
	}

	if (skillStamp->deltaT == 0)
		//	//play a sound
		AudioQueue::GetInstance()->AddSound(SGD::AudioManager::GetInstance()->LoadAudio(L"resources/audio/sound effects/snakes.wav"));

	skillStamp->deltaT += dt;

	//skill
	player->GetTimeStamp()->SwitchAnimation("mukuro2");

	//success! 
	return true;

}

bool PlayerSkill::BlazeFire(float dt, Player* player, bool bStart)
{
	const float cooldown = 18.0f;

	//current stamp
	PlayerSkillStamp* playerSkill = player->GetSkillStamp();
	playerSkill->m_mapSkills[1].m_fSkillCooldown = cooldown;
	SkillStamp* skillStamp = &(*playerSkill)[1];

	//update the timer
	skillStamp->m_fSkillTimer -= dt;

	//can we run the skill?
	if (skillStamp->m_fSkillTimer > 0 || !bStart)
		//no -- failure
		return false;

	//Are we done?
	if (player->GetTimeStamp()->isDone)
	{
		//reset all to zero
		skillStamp->deltaT = 0.0f;
		//timer 
		skillStamp->m_fSkillTimer = cooldown;

		//deactive the skill
		skillStamp->m_bStart = false;

		//stop updating skills
		return false;
	}

	if (skillStamp->deltaT == 0)
		//	//play a sound
		AudioQueue::GetInstance()->AddSound(SGD::AudioManager::GetInstance()->LoadAudio(L"resources/audio/sound effects/Summon 2.wav"));

	skillStamp->deltaT += dt;

	//skill
	player->GetTimeStamp()->SwitchAnimation("mukuro3");

	//success! 
	return true;

}

#pragma endregion

