#include "BitmapFont.h"
#include "..\SGD Wrappers\SGD_GraphicsManager.h"
#include <ctype.h>
#include <cassert>
#include "../tinyxml/tinyxml.h" //tinyXml
#include <string>
#include <vector>

//pass in the name of the font.xml, same as font.png
BitmapFont::BitmapFont(const char* filepath)
{
	//image = SGD::GraphicsManager::GetInstance()->LoadTexture(L"resource/graphics/bitmap/FONT.png");
	std::string path = "resource/graphics/bitmap/";
	path += filepath;
	path += ".png";
	xmlIMAGE = SGD::GraphicsManager::GetInstance()->LoadTexture(path.c_str());


	LoadFont(filepath);
}
BitmapFont::~BitmapFont(void)
{
	//SGD::GraphicsManager::GetInstance()->UnloadTexture(image);
	SGD::GraphicsManager::GetInstance()->UnloadTexture(xmlIMAGE);
}

const bool BitmapFont::LoadFont(const char* filepath)
{
	TiXmlDocument doc;
	std::string path = "resources/xml/";
	path += filepath;
	path += ".xml";


	if (doc.LoadFile(path.c_str()) == false)
		return false;

	TiXmlElement* root = doc.RootElement();

	if (!root)
		return false;

	TiXmlElement* xsection = root->FirstChildElement("chars");
	while (xsection)
	{
		int count;
		int x = 0;
		xsection->Attribute("count", &count);
		abc.resize(128);
		TiXmlElement* xLetters = xsection->FirstChildElement("char");
		while (xLetters)
		{
			letter a;
			xLetters->Attribute("id", &a.id);
			xLetters->Attribute("x", &a.x);
			xLetters->Attribute("y", &a.y);
			xLetters->Attribute("width", &a.width);
			xLetters->Attribute("height", &a.height);
			xLetters->Attribute("xoffset", &a.xoffset);
			xLetters->Attribute("yoffset", &a.yoffset);


			abc[a.id - 32] = a;
			xLetters = xLetters->NextSiblingElement("char");
			x++;
			if (x == count)
				break;
		}
		if (x == count)
			break;
	}
	return true;
}

void BitmapFont::DrawXML(const char* output, SGD::Point position, float scale, SGD::Color color) const
{
	float startx = position.x;
	for (int i = 0; output[i]; i++)
	{
		int numchar = output[i];
		// catch their number as an int to use for vector lookup of letter
		char ch = output[i];
		float colStart = position.x;

		if (ch == '\n')
		{
			position.x = startx;
			position.y += 40 * scale;
			continue;
		}

		letter a;
		a = abc[numchar - 32];
		if (a.id == 32)//space
		{
			position.x += abc['A'-32].width * scale;
			continue;
		}
		if (a.x == 0)
			a.x = 1;
		if (a.y == 0)
			a.y = 1;



		SGD::Rectangle r;
		r.left = (float)a.x;
		r.top = (float)a.y;
		r.right = (float)a.width + r.left;
		r.bottom = (float)a.height + r.top;


		SGD::GraphicsManager::GetInstance()->DrawTextureSection(
			xmlIMAGE, { position.x + a.xoffset, position.y + a.yoffset }, r, 0.0f, {}, color, { scale, scale });
		int xdistance = (int)(r.right - r.left);
		int ydistance = (int)(r.bottom - r.top);
		position.x = position.x + xdistance * scale;
	}

}