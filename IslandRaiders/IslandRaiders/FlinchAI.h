#pragma once
#include "EnemyAI.h"

class FlinchAI : public EnemyAI
{
public:
	FlinchAI() = default;
	~FlinchAI() = default;

	//update - must override
	virtual void Update(float dt, AIStamp& stamp);
	//ai identifier
	virtual EnemyAIType GetAIType() const { return EnemyAIType::Flinch; } //enemy action identifier
};

class StunAI : public EnemyAI
{
public:
	StunAI(void) = default;
	~StunAI(void) = default;

	//update - must override
	virtual void Update(float dt, AIStamp& stamp);
	//AI identifier
	virtual EnemyAIType GetAIType(void) const { return EnemyAIType::Stun; }

};

