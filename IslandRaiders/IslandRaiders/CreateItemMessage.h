#pragma once
#include "..\SGD Wrappers\SGD_Message.h"
class Enemy;
class CreateItemMessage : public SGD::Message
{
public:
	CreateItemMessage(Enemy* _e);
	~CreateItemMessage();
	Enemy* GetSender(void) const { return postMan; }
private:
	Enemy* postMan = nullptr;
};

