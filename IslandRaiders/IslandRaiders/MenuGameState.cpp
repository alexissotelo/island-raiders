#include "MenuGameState.h"
#include "..\Source\Game.h"
#include "PlayGameState.h"
#include "OptionsGameState.h"
#include "HowToPlayState.h"
#include "CreditsGameState.h"
#include "CharacterGameState.h"
#include "SaveLoadState.h"
#include "BitmapFont.h"
#include "GUI.h"
#include "GUIGroup.h"
#include "AudioQueue.h"
#include "PlayGameState.h"

MenuGameState* MenuGameState::pInstance = nullptr;

MenuGameState* MenuGameState::GetInstance(void)
{
	if (pInstance == nullptr)
		pInstance = new MenuGameState;

	return pInstance;
}
void MenuGameState::DeleteInstance(void)
{
	delete pInstance;
	pInstance = nullptr;
}
void MenuGameState::Enter()
{
	pGraphics = SGD::GraphicsManager::GetInstance();
	pAudio = SGD::AudioManager::GetInstance();
	pInput = SGD::InputManager::GetInstance();
	xfont = GUIManager::GetInstance()->GetFont();

	song = pAudio->LoadAudio(L"resource/Audio/BackGroundMusic/BACKGROUND_PIRATE.xwm");
	pAudio->PlayAudio(song, true);
	//AudioQueue::GetInstance()->AddSound(pAudio->LoadAudio(L"resource/Audio/BackGroundMusic/BACKGROUND_PIRATE.xwm"));
	backGround = pGraphics->LoadTexture(L"resources/graphics/backgrounds/mainmenubg.png");
	teamlogo = pGraphics->LoadTexture(L"resource/Graphics/placeholder images/stringchesee_logo2.png");

	//skin1->SetTextAlignment(TextAlignment::MiddleLeft);
	std::string buttonNames[5] = { "Play", /*"How To Play",*/ "Options", "Credits", "Exit" };
	menuButtons = new ButtonGroup(0, { 300, 200 }, 1.f);
	for (unsigned int i = 0; i < 4; i++)
	{
		menuButtons->AddButton(buttonNames[i]);
	}
}
void MenuGameState::Exit()
{
	pAudio->StopAudio(song);
	pAudio->UnloadAudio(song);

	delete menuButtons;
	menuButtons = nullptr;

	pGraphics->UnloadTexture(teamlogo);
	pGraphics->UnloadTexture(backGround);
	DeleteInstance();
}
void MenuGameState::ArcadeCursorMovement(float dt)
{
	SGD::Vector dir = pInput->GetLeftJoystick(0);
	if (dir.y != 0)
	{
		timer += dt;
		if (timer > timerFreq)
		{
			timer = 0.f;
			if (dir.y < 0)
			{
				timer = 0.f;
				nCursor--;
				if (nCursor < 0)
					nCursor = 4;
			}
			else if (dir.y > 0)
			{
				timer = 0.f;
				nCursor++;
				if (nCursor > 4)
					nCursor = 0;

			}

		}
	}
}
bool MenuGameState::Update(float dt)
{
	pInput->CheckForNewControllers();

	if (Game::GetInstance()->ARCADEMODE)
	{
		ArcadeCursorMovement(dt);
	}
	nCursor = menuButtons->Update(dt);
	switch (nCursor)
	{
	case 0: // play
		Game::GetInstance()->ChangeState(SaveLoadState::GetInstance());
		return true;
		break;
	//case 1: //how to play
	//	Game::GetInstance()->ChangeState(HowToPlayState::GetInstance());
	//	return true;
	//	break;
	case 1: // options
		Game::GetInstance()->ChangeState(OptionsGameState::GetInstance());
		return true;
		break;
	case 2: // credits
		Game::GetInstance()->ChangeState(CreditsGameState::GetInstance());
		return true;
		break;
	case 3: //exit
		return false;
		break;
	}

	return true;//keep playing
}
void MenuGameState::Render(float dt)
{
	float width = Game::GetInstance()->GetScreenWidth();
	pGraphics->DrawTexture(backGround, { 0.0f, 0.0f }, {}, {}, {}, { 1.0f, 1.0f });
	pGraphics->DrawTexture(teamlogo, { 550.f, 480.f }, {}, {}, {}, { 0.45f, 0.45f });

	menuButtons->DrawGroup();
}
