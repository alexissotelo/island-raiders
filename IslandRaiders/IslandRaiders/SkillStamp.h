#pragma once

//structure used to update the player ability
struct SkillStamp
{
	SkillStamp(void) = default;
	~SkillStamp(void) = default;

	void Reset(void)
	{
			originalDamage = 0.0f;
			deltaDamage = 0.0f;
			deltaT = 0.0f;

			m_fSkillTimer = 1.f;
			m_fSkillCooldown = 1.f;
			m_bStart = false;
	}

	float originalDamage = 0.0f;
	float deltaDamage = 0.0f;
	float deltaT = 0.0f;

	float m_fSkillTimer = 0.f;
	float m_fSkillCooldown = 0.f;
	bool  m_bStart = false;
};


