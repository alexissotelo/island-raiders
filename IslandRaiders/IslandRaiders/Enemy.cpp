#include "Enemy.h"
#include "EnemyAI.h"
#include "MessageID.h"

#include "DestroyEntityMessage.h"
#include "DestroyEntityMessage.h"
#include "EnemyDownMessage.h"
#include "CreateItemMessage.h"

#include "Enemy.h"
#include "EnemyAI.h"
#include "LevelManager.h"
#include "AnimationSystem.h"
#include "PlayGameState.h"
#include "EntityManager.h"
#include "GUI.h"

#include "../Source/Game.h"
#include "../SGD Wrappers/SGD_Message.h"
#include "../SGD Wrappers/SGD_MessageManager.h"
#include "../SGD Wrappers/SGD_Event.h"

#include "CreateAnimatedParticle.h"
#include "AudioQueue.h"
#include "Player.h"

#include "GUI.h"
#include <sstream>

#define DROP_ITEMS 1

Enemy::Enemy(const char* name) : Character(name)
{
	//default AI to be idle
	aiStamp.owner = this;
	aiStamp.SwitchAIMode(EnemyAIType::Idle);
	//default animations to idle
	aTimeStamp.SwitchAnimation("idle");
	aTimeStamp.bForwards = false; //face the player

	RegisterForEvent("SMACK");
	RegisterForEvent("MARKED");

}
Enemy::~Enemy()
{
	_CrtCheckMemory();

	//no dynamic memory
	//UnregisterFromEvent("SMACK");
}
void   Enemy::Update(float dt)
{
#pragma region HP Check
	//am i dead?
	if (stat.health <= 0)
	{
		if (!dead)
		{
			//drop the item
#if DROP_ITEMS
			DropWhatItem();
#endif
			if (killer)
				killer->IncreaseKillCount();

			//send a message that I'm dead
			EnemyDownMessage* down = new EnemyDownMessage(this);
			down->QueueMessage();
			down = nullptr;

			//kill yourself
			DestroyEntityMessage* destroy = new DestroyEntityMessage(this);
			SGD::MessageManager::GetInstance()->QueueMessage(destroy);
			//destroy->QueueMessage();
			destroy = nullptr;
		}
		dead = true;
		return;
	}
#pragma endregion

	//update the fire timer
	if (stat.fireTimer < stat.fireFreq)
		stat.fireTimer += dt;

	//regen stamina
	stat.staminaRegen += dt;
	if (stat.staminaRegen >= 8.0f)
	{
		stat.stamina += 0.20f;
		if (stat.stamina > 1.0f)
			stat.stamina = 1.0f;

		stat.staminaRegen = 0.0f;
	}
	//update animations
	Character::Update(dt);//must happen before aiUpdate

	//STAY IN WORLD
#if 1
	//PROVISIONAL, SHOULD USE RENDER RECT 
	SGD::Point camPos = PlayGameState::GetInstance()->GetCameraPosition();
	SGD::Size  camSize = PlayGameState::GetInstance()->GetCameraSize();
	SGD::Point screenPos = { position.x - camPos.x, position.y - camPos.y };
	//if (screenPos.x - size.width < 0)
	if (screenPos.x - size.width < 0)
		//same as position.x < camPos.x
	{//left
		aiStamp.SwitchAIMode(EnemyAIType::Pursuit);
	}
	if (screenPos.x + size.width > camSize.width)
	{//right
		aiStamp.SwitchAIMode(EnemyAIType::Pursuit);

	}
	if (screenPos.y - size.height *2.5f < 0)
	{//top
		aiStamp.SwitchAIMode(EnemyAIType::Pursuit);
	}
	if (screenPos.y > camSize.height)
		//same as position.x < camPos.x
	{//bot
		aiStamp.SwitchAIMode(EnemyAIType::Pursuit);
	}
#endif

	//stunned
	//	return;
	if (!sEffectStatus.IsEffectOn(Status::STUN))
		AIManager::GetInstance()->Update(dt, aiStamp);

}
void   Enemy::Render()
{
	//render animations
	Character::Render();
}

//use the character collision handling then this
bool Enemy::HandleCollision(const IEntity* ptr)
{
	//safe check
	if (!ptr)
		return false;

	//type
	int eType = ptr->GetType();
	switch (eType)
	{
#pragma region Player

	case EntityType::ENT_PLAYER:
	{
		//convert to a character pointer
		const Character* other = dynamic_cast<const Character*>(ptr);

		SGD::Rectangle attack = other->GetActiveRect();
		SGD::Rectangle enemyCol = this->GetCollisionRect();

		if (attack.IsIntersecting(enemyCol) && aTimeStamp.m_sAction == "throw")
		{
			SGD::Event* toss = new SGD::Event("KNOCKBACK", nullptr, this);
			toss->SendEventNow(other);
			delete toss;
			toss = nullptr;
		}
		if (aiStamp.fPause >= -0.25f && aiStamp.aiType == EnemyAIType::Defense)
		{
			SGD::Event toss = { "COUNTER", nullptr, this };
			GUIManager::GetInstance()->PopUpFade(
				"Counter", position, 0.75f, 1.2f, { 127, 127, 127 });
			toss.SendEventNow(other);
		}

	}
		break;
#pragma endregion

	}

	//damage

	//take or deal damage
	return Character::HandleCollision(ptr);
}
void Enemy::HandleEvent(const SGD::Event* pEvent)
{
	//damage
	Character::HandleEvent(pEvent);

#pragma region Damage

	//hurt
	if (pEvent->GetEventID() == "DAMAGE")
	{
		if (marked)
		{
			//Get the damage from the event
			AttackInfo *atkInfo = (AttackInfo*)pEvent->GetData();
			float atk = ((atkInfo->damage) > 0) ? atkInfo->damage : -(atkInfo->damage);

			//send it to th correct character
			stat.health -= atk;
			//play a sound
			AudioQueue::GetInstance()->AddSound(L"resources/audio/sound effects/OnHit.wav");

			//POP UP DAMAGE
			SGD::Color color = { 255, 255, 0 };
			float scale = 0.75f;
			if (atkInfo->isCriticalHit)
			{
				color = { 255, 0, 0 };
				scale = 1.1f;
			}
			//read in value
			std::stringstream txt;
			txt << (int)atk;
			SGD::Point pos = GetPosition();
			pos.Offset(0, -GetRenderRect().ComputeHeight());
			//pop up
			GUIManager::GetInstance()->PopUpFade(txt.str().c_str(), pos, scale, 1.2f, color);

		}

		AttackInfo *dmg = (AttackInfo*)pEvent->GetData();
		if (aiStamp.aiType != EnemyAIType::Defense && this->aTimeStamp.m_sAction != "defend")
		{
			//float* dmg = (float*)pEvent->GetData();
			//switch to flinch
			if ((aiStamp.aiType == EnemyAIType::Flinch
				|| aiStamp.aiType == EnemyAIType::Stun) && dmg->damage > 0 && aiStamp.tier != 0)
			{
				//character
				Character* sender = (Character*)pEvent->GetSender();
				//send flying
				Knockback(sender);
				//vel
				aiStamp.vKnock = GetVelocity();
				//switch to knockback
				aiStamp.SwitchAIMode(EnemyAIType::Knockback);

				//take extra damage
				if (aiStamp.aiType == EnemyAIType::Stun)
					stat.health -= 0.10f * (dmg->damage);
			}
			else
				aiStamp.SwitchAIMode(EnemyAIType::Flinch);

		}
		else
		{
			//flinch
			aiStamp.SwitchAIMode(EnemyAIType::Flinch);
			//stop abilities
			for (unsigned int i = 0; i < 3; ++i)
			{
				if (aiStamp.enemySkills[i].second < 0.0f)
					aiStamp.enemySkills[i].second = 10.0f;
			}
		}

		//target who hit you
		Entity* target = (Entity*)pEvent->GetSender();

		if (dynamic_cast<Character*>(target))
		{
			Character* charTarget = (Character*)target;
			//target the character
			aiStamp.target = charTarget;
		}
		else if (dynamic_cast<Missile*>(target))
		{
			Missile* miss = (Missile*)target;
			aiStamp.target = miss->GetOwner();
		}
	}


#pragma endregion

#pragma region Smack

	else if (pEvent->GetEventID() == "SMACK")
	{
		//Get the damage from the event
		AttackInfo *atkInfo = (AttackInfo*)pEvent->GetData();
		float atk = ((atkInfo->damage) > 0) ? atkInfo->damage : -(atkInfo->damage);

		//defending?
		if (this->aTimeStamp.m_sAction == "guard")
		{
			atk = atk * (1 - stat.defense);
			stat.health -= atk;
			stat.stamina -= 0.2f;
		}
		else
		{
			//send it to th correct character
			stat.health -= atk;
			//add stamina??
			stat.stamina += 0.05f;

			//cap at 1.0f
			if (stat.stamina > 1.0f)
				stat.stamina = 1.0f;
		}

		//POP UP DAMAGE
		SGD::Color color = { 255, 255, 0 };
		float scale = 0.75f;
		if (atkInfo->isCriticalHit)
		{
			color = { 255, 0, 0 };
			scale = 1.1f;
		}

		std::stringstream txt;
		txt << (int)atk;
		SGD::Point pos = GetPosition();
		pos.Offset(0, -GetRenderRect().ComputeHeight() * 0.75f);

		GUIManager::GetInstance()->PopUpFade(
			txt.str().c_str(), pos, scale, 1.2f, color);

	}


#pragma endregion

#pragma region MARKED

	else if (pEvent->GetEventID() == "MARKED")
	{
		marked = !marked;
	}

#pragma endregion

#pragma region Countering
	//countering
	if (pEvent->GetEventID() == "COUNTER")
	{
		GUIManager::GetInstance()->PopUpFade("Stun",
		{ position.x - 50, position.y - GetRenderRect().ComputeHeight() *0.5f },
		1.f, 1.2f, { 127, 127, 127 });
		///stun
		//aiStamp.SwitchAIMode(EnemyAIType::Stun);
		sEffectStatus.StartEffect(Status::STUN, 3.0f, 0.f);
	}
#pragma endregion

#pragma region Knockback
	//knocked tf out
	if (pEvent->GetEventID() == "KNOCKBACK")
	{
		if (aiStamp.tier != 0)
		{
			//character
			Character* sender = (Character*)pEvent->GetSender();
			//send flying
			Knockback(sender);
			//ai
			aiStamp.SwitchAIMode(EnemyAIType::Knockback);
		}
		else aiStamp.SwitchAIMode(EnemyAIType::Flee);
	}
#pragma endregion

#pragma region DEATH

	//a player died
	if (pEvent->GetEventID() == "PLAYER_DEATH")
	{
		AIStamp* stamp = GetAIStamp();
		if (stamp->target == pEvent->GetSender())
		{
			stamp->target = nullptr;
			stamp->SwitchAIMode(EnemyAIType::Idle);
		}
	}

#pragma endregion

#pragma region Stamina Check

	//stamina check
	if (stat.stamina <= 0.0f)
	{
		///stun
		aiStamp.SwitchAIMode(EnemyAIType::Stun);
		//reset stamina
		stat.stamina = 1.0f;
	}

#pragma endregion

#pragma region HP Check
	//am i dead?
	if (stat.health < 0)
	{
		//cap at zero
		stat.health = 0;
		//player who killed the enemy
		Character* tempPtr = (Character*)pEvent->GetSender();
		Player* sender = dynamic_cast<Player*>(tempPtr);
		if (sender && sender->GetType() == ENT_PLAYER && !dead)
			killer = sender;
	}
#pragma endregion

}
void Enemy::DropWhatItem()
{
	//get the temp drop rate to see if the item will drop
	int r = rand() % 100;
	// if the temp rate is less than the monsters drop rate
	// then drop the item
	if (r < dropRate)
	{
		int x;
		x = rand() % Tier::TIERSIZE;
		//x = Tier::Diamond;
		itemTier = (Tier)x;
		x = rand() % ItemType::ITEMTYPESIZE;
		//x = ItemType::DAMAGE;
		if (x < 4)
			queBuff = (ItemType)x;
		else
			queBuff = ItemType::NOITEM;
		if (queBuff == ItemType::NOITEM)
		{
			x = rand() % StatusEffect::STATUSEFFECTSIZE;
			//x = StatusEffect::ICE;
			status = (StatusEffect)x;
		}
		else
			status = StatusEffect::NOEFFECT;

		CreateItemMessage* msg = new CreateItemMessage(this);
		msg->QueueMessage();
		msg = nullptr;
	}
	else
		return;
}
