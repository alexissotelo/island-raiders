 #pragma once
#include "Player.h"
class Swordsman : public Player
{
public:
	virtual void   Update(float dt)	 override;
	virtual bool   HandleCollision(const IEntity* other)	 override;
	virtual void HandleEvent(const SGD::Event* pEvent);
	virtual unsigned int  GetPlayerType() const override { return (int)PlayerType::swordsman; }

	bool UpdateCombo(float dt);

	Swordsman(const char*);
	Swordsman(const char* name, unsigned int controllerNumber);
	virtual ~Swordsman();

	void SetCaptainOn(bool _on);
private:
	bool CaptainOn = false;
};

