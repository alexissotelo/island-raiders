#pragma once
#include "EnemyAI.h"
/****************************************************
| File:	CombatAI
| Purpose: CombatAI will define the behavior of enemies
|			that are in ataccking a player
****************************************************/

class CombatAI : public EnemyAI
{
public:
	CombatAI() = default;
	~CombatAI() = default;

	virtual void Update(float dt, AIStamp& _timeStamp);
	virtual EnemyAIType GetAIType() const { return EnemyAIType::Combat; }

private:
	//melee chasers
	void MeleeUpdate(float dt, AIStamp& _timeStamp);
	void TierZeroMelee(float dt, AIStamp& _timeStamp);
	void TierOneMelee(float dt, AIStamp& _timeStamp);
	void TierTwoMelee(float dt, AIStamp& stamp);
	//ranged chasers
	void RangeUpdate(float dt, AIStamp& _timeStamp);
	void TierZeroRange(float dt, AIStamp& _timeStamp);
	void TierOneRange(float dt, AIStamp& _timeStamp);

	//support chasers
	void SupportUpdate(float dt, AIStamp& _timeStamp);
	void TierOneSupport(float dt, AIStamp& stamp);

	//familiar 
	void FamiliarUpdate(float dt, AIStamp& stamp);
};

