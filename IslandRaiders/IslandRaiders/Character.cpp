#include "Character.h"
#include "Missile.h"
#include "AnimatedParticle.h"

#include "DestroyEntityMessage.h"
#include "../Source/Game.h"
#include "PlayGameState.h"
#include "Player.h"
#include "AudioQueue.h"

#include "../SGD Wrappers/SGD_Geometry.h"
#include "../SGD Wrappers/SGD_EventManager.h"
#include "../SGD Wrappers/SGD_Event.h"
#include "GUI.h"
#include "Items.h"
#include <sstream>




#define EPSILON 0.1f
namespace
{

	bool Equals(float A, float B)
	{
		float diff = A - B;
		return (diff < EPSILON) && (-diff < EPSILON);
	}
}

Character::Character(const char* _name) : Entity(_name)
{
	//damage
	Listener::RegisterForEvent("DAMAGE");
	Listener::RegisterForEvent("COUNTER");
	Listener::RegisterForEvent("KNOCKBACK");
	Listener::RegisterForEvent("POISON");
	Listener::RegisterForEvent("FREEZE");
	Listener::RegisterForEvent("BURN");
	Listener::RegisterForEvent("POKE");
	Listener::RegisterForEvent("STUN");
}
Character::~Character()
{

	for (unsigned int i = 0; i < utilityBelt.size(); i++)
	{
		utilityBelt[i]->Release();
		utilityBelt[i] = nullptr;
	}
}
void Character::Update(float dt)
{
	prevPosition = position;

	//update incase of status effects
	StatusManager::GetInstance()->Update(dt, this, sEffectStatus);
	//stay in screen space
	//StayInWorld();
	//update items
	if (!utilityBelt.empty())
	{
		for (unsigned int i = 0; i < utilityBelt.size(); i++)
		{
			utilityBelt[i]->UpdateItem(dt);
			if (utilityBelt[i]->GetLifeSpan() < 0.0f)
			{
				//Items* temp = utilityBelt[i];
				utilityBelt[i]->Release();
				utilityBelt[i] = nullptr;
				utilityBelt.erase(utilityBelt.begin() + i);
				//delete temp;
				numItemsHeld--;
			}
		}
	}
	//update position
	Entity::Update(dt);
}
void   Character::Render()
{
	Entity::Render();

	SGD::GraphicsManager *pGraphics = SGD::GraphicsManager::GetInstance();
	SGD::Point camPos = PlayGameState::GetInstance()->GetCameraPosition();

	if (Game::GetInstance()->bDebug)
	{
		//coll box with camera
		SGD::Rectangle rect = GetRect();
		rect.Offset(-camPos.x, -camPos.y);
		pGraphics->DrawRectangle(rect, { 120, 255, 255, 0 });
	}

	//draw hp bar
	SGD::Rectangle hpbar = { position.x - size.width * 0.5f, position.y - 10,
		position.x + size.width * 0.5f, position.y - 2, };
	//camera offset
	hpbar.Offset({ -camPos.x, -camPos.y });
	//draw health
	pGraphics->DrawRectangle(hpbar, { 127, 127, 127 });
	//
	if ((int)stat.health > 0)
	{
		hpbar.right = hpbar.left + (size.width* (stat.health*1.f / stat.maxHealth));
		pGraphics->DrawRectangle(hpbar, { 0, 255, 0 });
	}


}


/**************************************************************
|	Function: Character::HandleCollision
|	Purpose: send an event/message, or switch animation and status
|			depending on the entity it collides with
|	Parameter:
|			const IEntity* ptr - a constant reference to the entity
|					the invoking object collided with
|	Return Value: void
**************************************************************/
bool Character::HandleCollision(const IEntity* ptr)
{
	//safe check
	if (!ptr)
		return false;

	int eType = ptr->GetType();
	switch (eType)
	{
#pragma region Player & Enemy

	case EntityType::ENT_PLAYER:
	case EntityType::ENT_ENEMY:
	{
								  //convert to a character pointer
								  const Character* other = dynamic_cast<const Character*>(ptr);

								  const SGD::Rectangle attack = this->GetActiveRect();
								  const SGD::Rectangle enemyCol = other->GetCollisionRect();
								  //collision between activeRect and collisionRect
								  if (attack.IsIntersecting(enemyCol))
								  {
									  //player's show PopUp for combo chain
									  //crit is calculated based on length of the chain
									  //and if last animation of the combo
									  Attack(other);
								  }

	}
		break;
#pragma endregion 

#pragma region Invulnerable

	case EntityType::ENT_INVULNERABLE:
	{
										 //collision with trees, obstacles, etc
										 SGD::Rectangle collide   = this->GetRect();
										 const SGD::Rectangle otherRect = ptr->GetRect();
										 const Entity* other = dynamic_cast<const Entity*>(ptr);

										 if (collide.IsIntersecting(otherRect))
										 {
											 if (stuck)
											 {//already stuck
												 bool leftside = (collide.left <= otherRect.right
													 && collide.right > otherRect.right);

												 bool rightside = (collide.right >= otherRect.left
													 && collide.left < otherRect.left);

												 if (leftside && rightside)
												 {
													 //if both sides inside, just fix Y
													 position.x = otherRect.right + size.width + 1;
												 }
												 else
												 {
													 if (leftside)
													 {//move to the right
														 position.x = otherRect.right + size.width + 1;
													 }
													 else if (rightside)
													 {//move to the left
														 position.x = otherRect.left - size.width - 1;
													 }
												 }

												 collide = this->GetRect();
												 if (collide.IsIntersecting(otherRect))
												 {//still stuck? fix Y
													 if (other->GetPosition().y + other->GetSize().height > PlayGameState::GetInstance()->GetCameraPosition().y + 520)
														 position.y = other->GetPosition().y - 2;
													 else
														 position.y = other->GetPosition().y + other->GetSize().height + 1;
												 }

												 stuck = false;
												 break;
											 }

											 if (position != prevPosition)
											 {
												 position = prevPosition;
												 stuck = false;
											 }
											 else
											 {
#if 0
												 float halfWidth = size.width;
												 if (position.x + halfWidth >= otherRect.left)
												 {
													 position.x--;
												 }
												 else if (position.x - halfWidth <= otherRect.right)
												 {
													 position.x++;
												 }

												 if (position.y >= otherRect.top)
												 {
													 position.y--;
												 }
												 else if (position.y <= otherRect.bottom)
												 {
													 position.y++;
												 }
												 if (position.x + halfWidth >= other->GetPosition().x &&
													 position.x - halfWidth <= otherRect.right)
												 {
													 //bool stuckinx = true;
													 //unstuck in X
													 //always push to the right
													 position.x = otherRect.right + halfWidth+1;
												 }
#endif
												 stuck = true;
											 }
										 }
	}
		break;
#pragma endregion

#pragma region Missile
	case EntityType::ENT_MISSILE:
	{
									//convert to a character pointer
									//const Missile* other = dynamic_cast<const Missile*>(ptr);

									//if (other->GetOwner() == this)
									//	break;

									//const SGD::Rectangle attack = other->GetActiveRect();
									////character 2 active and collision
									//const SGD::Rectangle enemyCol = this->GetCollisionRect();


									//EnemyDownMessage* msg = new EnemyDownMessage(this);
									//msg->QueueMessage();
									//msg = nullptr;
	}
		break;
#pragma endregion 
	}

	return false;
}
void Character::StayInWorld()
{
	//PROVISIONAL, SHOULD USE RENDER RECT 
	SGD::Point camPos = PlayGameState::GetInstance()->GetCameraPosition();
	SGD::Size  camSize = PlayGameState::GetInstance()->GetCameraSize();
	SGD::Point screenPos = { position.x - camPos.x, position.y - camPos.y };
#if 1
	//if (screenPos.x - size.width < 0)
	if (screenPos.x - size.width < 0)
		//same as position.x < camPos.x
	{//left
		position.x = camPos.x + size.width;
	}
	if (screenPos.x + size.width >  camSize.width)
	{//right
		position.x = camPos.x + camSize.width - size.width;
	}
	if (screenPos.y - size.height *2.5f < 0)
	{//top
		position.y = camPos.y + size.height *2.5f;
	}
	if (screenPos.y  > camSize.height)
		//same as position.x < camPos.x
	{//bot
		position.y = camPos.y + camSize.height;
	}
#else
	if (screenPos.x - GetSize().width - (GetSize().width / 2) < 0) // left
		SetPosition(SGD::Point((GetSize().width / 2) + GetSize().width, GetPosition().y));
	else
	if (GetPosition().x + GetSize().width > Game::GetInstance()->GetScreenWidth())//right
		SetPosition({ Game::GetInstance()->GetScreenWidth() - GetSize().width, GetPosition().y });

	if (GetPosition().y - (GetSize().height / 2) - GetSize().height * 2 < 0.0f)// top
		SetPosition(SGD::Point(GetPosition().x, (GetSize().height / 2) + GetSize().height * 2));
	else if (GetPosition().y > Game::GetInstance()->GetScreenHeight())//bot
		SetPosition({ GetPosition().x, Game::GetInstance()->GetScreenHeight() });
#endif // I'm Batman//No
}

float Character::GetDamage(void) const
{
	float boost = stat.bQuickAttack ? stat.quickPercent : stat.heavyPercent;
	return (stat.damage * boost * stat.atkBoost);

}
/*virtual*/ void Character::Attack(const Character *other)
{
	//skip to the next frame
	aTimeStamp.m_CurrentFrame++;
	aTimeStamp.m_fFrameTimer = 0.f;

	//extra
	AttackInfo atkInfo;
	float boost = stat.bQuickAttack ? stat.quickPercent : stat.heavyPercent;
	atkInfo.isCriticalHit = (rand() % 100 < 10);
	boost *= atkInfo.isCriticalHit ? 1.5f : 1.f;
	atkInfo.damage = stat.damage * boost * stat.atkBoost;

	//create a damage event
	SGD::Event dmg = { "DAMAGE", &atkInfo, this };
	//send it out
	dmg.SendEventNow(other);
}
void Character::HandleEvent(const SGD::Event* pEvent)
{

#pragma region HEAL

	//event 
	if (pEvent->GetEventID() == "HEAL")
	{
		float* percentage = (float*)pEvent->GetData();
		stat.health += *percentage;

		//
		if (stat.health > stat.maxHealth)
			stat.health = stat.maxHealth;
	}

#pragma endregion

#pragma region Damage

	if (pEvent->GetEventID() == "DAMAGE")
	{
		//Get the damage from the event
		AttackInfo *atkInfo = (AttackInfo*)pEvent->GetData();
		float atk = ((atkInfo->damage) > 0) ? atkInfo->damage : -(atkInfo->damage);

		//defending?
		if (this->aTimeStamp.m_sAction == "guard")
		{
			atk = atk * (1 - stat.defense);
			stat.health  -= atk;
          	stat.stamina -= 0.25f;
			//play a sound
			AudioQueue::GetInstance()->AddSound(SGD::AudioManager::GetInstance()->LoadAudio(L"resources/audio/sound effects/OnHit2.wav"));
		}
		else
		{
			//send it to th correct character
			stat.health -= atk;
			//play a sound
			AudioQueue::GetInstance()->AddSound(SGD::AudioManager::GetInstance()->LoadAudio(L"resources/audio/sound effects/OnHit.wav"));

		}

		//POP UP DAMAGE
		SGD::Color color = { 255, 255, 0 };
		float scale = 0.75f;
		if (atkInfo->isCriticalHit)
		{
			color = { 255, 0, 0 };
			scale = 1.1f;
		}

		std::stringstream txt;
		txt << (int)atk;
		SGD::Point pos = GetPosition();
		pos.Offset(0, -GetRenderRect().ComputeHeight());

		GUIManager::GetInstance()->PopUpFade(
			txt.str().c_str(), pos, scale, 1.2f, color);
	}
#pragma endregion

#pragma region Statuses

	if (pEvent->GetEventID() == "POISON")
	{
		float* effDamage = (float*)pEvent->GetData();
		GetCharacterStatus()->StartEffect(Status::POISON, effDamage[1], effDamage[0]);
	}
	if (pEvent->GetEventID() == "BURN")
	{
		float* effDamage = (float*)pEvent->GetData();
		GetCharacterStatus()->StartEffect(Status::BURN, effDamage[1], effDamage[0]);
	}
	if (pEvent->GetEventID() == "FREEZE")
	{
		float* effDamage = (float*)pEvent->GetData();
		GetCharacterStatus()->StartEffect(Status::FREEZE, effDamage[1], effDamage[0]);
	}

	if (pEvent->GetEventID() == "STUN")
	{
		float* effDamage = (float*)pEvent->GetData();
		GetCharacterStatus()->StartEffect(Status::STUN, *effDamage, 0.f);
	}
#pragma endregion

#pragma region MOVE
	if (pEvent->GetEventID() == "MOVE")
	{
		//move 
		SGD::Vector forwards = SGD::Vector(1, 0);
		//directions
		if (!aTimeStamp.bForwards)
			forwards.x *= -1;

		//scale
		forwards *= 400;

		//set velocity
		SetVelocity(forwards);
	}
	else if (pEvent->GetEventID() == "STOP")
	{
		SetVelocity({0.f,0.f});
	}
#pragma endregion

}
void Character::Knockback(Entity* source)
{
	//create the vector
	SGD::Vector away = this->GetPosition() - source->GetPosition();
	away.Normalize();
	//scale 
	away.x *= 500.0f;
	away.y = 0;
	//set the vector
	SetVelocity(away);
	//switch the animation
	aTimeStamp.SwitchAnimation("knockback");
}

void CharacterStats::InitializeStats(float dmg, float maxHP, float moveSpd, float firefreq,
	float def , float projSpeed, float quickBoost , float heavyBoost )
{
	damage = dmg;
	maxHealth = health = maxHP;
	moveSpeed = moveSpd;
	fireFreq = firefreq;
	defense = def;
	projectileSpeed = projSpeed;
	quickPercent = quickBoost;
	heavyPercent = heavyBoost;
	stamina = 1.0f;
	fireTimer = 0.0f; 
	staminaRegen = 0.0f;
	atkBoost = 1.0f;

}

void CharacterStats::CopyStats(const CharacterStats& stat)
{
	this->damage = stat.damage;
	this->moveSpeed = stat.moveSpeed;
	this->fireFreq = stat.fireFreq;
	this->defense = stat.defense;
	this->projectileSpeed = stat.projectileSpeed;
	this->quickPercent = stat.heavyPercent;
	this->heavyPercent = stat.heavyPercent;
	this->stamina			= stat.stamina;
	this->fireTimer			= stat.fireTimer;
	this->staminaRegen		= stat.staminaRegen;
	this->atkBoost			= stat.atkBoost;

}

SGD::Rectangle	Character::GetRect() const
{
	return SGD::Rectangle{ SGD::Point(position.x - size.width, position.y),
		SGD::Size(size.width  *2.f, size.height) };
}