#include "GUIGroup.h"
#include "GUI.h"
#include "GUISkin.h"
#include "../SGD Wrappers/SGD_InputManager.h"
#include "../SGD Wrappers/SGD_GraphicsManager.h"
#include "AudioQueue.h"

#include "../Source/Game.h"

ButtonGroup::ButtonGroup(int skinIndex, SGD::Point _position, float fontScale)
{
	skin = GUIManager::GetInstance()->GetSkin(skinIndex);
	position = _position;
	this->fontScale = fontScale;

	cursorIcon = SGD::GraphicsManager::GetInstance()->LoadTexture(
		L"resource/Graphics/placeholder images/ARROW.png");
}
ButtonGroup::~ButtonGroup()
{
	SGD::GraphicsManager::GetInstance()->UnloadTexture(cursorIcon);
	for (size_t i = 0; i < group.size(); i++)
	{
		delete group[i];
	}
}

void ButtonGroup::AddButton(std::string text)
{
	Button *b = new Button(text, fontScale);
	b->SetMiddleRect(position.y + 50.f * fontScale * group.size());
	group.push_back(b);
}

//returns the index of the button pressed
int ButtonGroup::Update(float dt)
{
	SGD::InputManager* pInput = SGD::InputManager::GetInstance();
	
	//move cursor
	if (pInput->IsKeyPressed(SGD::Key::Down) == true ||
		pInput->IsKeyPressed(SGD::Key::S) == true ||
		pInput->IsDPadPressed(0, SGD::DPad::Down))
	{
		if (++nCursor >= (int)group.size())
			nCursor = 0;
		//play sound 
		AudioQueue::GetInstance()->AddSound("resources/audio/sound effects/menuPress.wav");

	}
	if (pInput->IsKeyPressed(SGD::Key::Up) == true ||
		pInput->IsKeyPressed(SGD::Key::W) == true ||
		pInput->IsDPadPressed(0, SGD::DPad::Up))
	{
		//play sound 
		AudioQueue::GetInstance()->AddSound("resources/audio/sound effects/menuPress.wav");

		if (--nCursor < 0)
			nCursor = group.size() - 1;
	}

	if (pInput->IsKeyPressed(SGD::Key::Enter) == true || pInput->IsButtonPressed(0, 1))
	{
		//play sound 
		AudioQueue::GetInstance()->AddSound("resources/audio/sound effects/menuPress.wav");
		return nCursor;
	}

	for (unsigned int i = 0; i < group.size(); i++)
	{
		if (group[i]->OnClick(dt))
		{
			nCursor = i;
			return i;
		}
	}
	if (nCursor >= 0 && nCursor < (int)group.size())
		group[nCursor]->isHover = true;

	return -1;//no button was pressed
}
void ButtonGroup::DrawGroup()
{
	for (unsigned int i = 0; i < group.size(); i++)
	{
		group[i]->Draw();
	}

	float width = Game::GetInstance()->GetScreenWidth();
	if (nCursor >= 0 && nCursor < (int)group.size())
		SGD::GraphicsManager::GetInstance()->DrawTexture(
		cursorIcon, { position.x - 50, group[nCursor]->rect.top }, 0.0f, {}, {}, { .7f, .7f });
}
