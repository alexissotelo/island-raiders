#include "LevelSection.h"
#include "LevelManager.h"
#include "PlayGameState.h"
#include "../SGD Wrappers/SGD_Event.h"
#include "../SGD Wrappers/SGD_GraphicsManager.h"

LevelSection::LevelSection(float x, float y) : Listener(this)
{
	position.x = x;
	position.y = y;

}
LevelSection::~LevelSection()
{
	background.clear();
	foreground.clear();
	enemies.clear();
}
void LevelSection::SpawnEnemies()
{
	PlayGameState::GetInstance()->LockCamera();

	for (unsigned int i = 0; i < enemies.size(); i++)
	{
		LevelManager::CreateEnemy(enemies[i].name, { enemies[i].x, enemies[i].y });
	}
}
void LevelSection::RenderBackground()
{
	SGD::Point camPos = PlayGameState::GetInstance()->GetCameraPosition();
	for (unsigned int i = 0; i < background.size(); i++)
	{
		SGD::GraphicsManager::GetInstance()->DrawTextureSection(
			background[i].image,
			{ background[i].x - camPos.x, background[i].y - camPos.y },
			background[i].renderRect, 0, { 0, 0 }, {},
			{ background[i].scaleX, background[i].scaleY });
	}
}
void LevelSection::RenderForeground()
{
	SGD::Point camPos = PlayGameState::GetInstance()->GetCameraPosition();
	for (unsigned int i = 0; i < foreground.size(); i++)
	{
		SGD::GraphicsManager::GetInstance()->DrawTextureSection(
			foreground[i].image,
			{ foreground[i].x - camPos.x, foreground[i].y - camPos.y },
			foreground[i].renderRect, 0, { 0, 0 }, {},
			{ foreground[i].scaleX, foreground[i].scaleY });
	}
}
/*virtual*/ void LevelSection::HandleEvent(const SGD::Event* pEvent)
{
	if (pEvent->GetEventID() == "SPAWNENEMIES")
	{
		SpawnEnemies();
	}
}

