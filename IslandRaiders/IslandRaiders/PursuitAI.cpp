#include "PursuitAI.h"
#include "AISkills.h"
#include "EntityManager.h"
#include "PlayGameState.h"
#include "../SGD Wrappers/SGD_Geometry.h"

/**************************************************************
|	Function:
|	Purpose:
|	Parameter:
|	Return Value:
**************************************************************/
void PursuitAI::Update(float dt, AIStamp& stamp)
{
	//no target? 
	if (!stamp.target)
	{
		//find one
		stamp.SwitchAIMode(EnemyAIType::Idle);
		//leave -- you'll get em next time
		return;
	}

	//update timer
	stamp.fStartTimer += dt;

	//wait based on the enemy's tier
	switch (stamp.combatType)
	{
	case CombatType::Melee:
		MeleeUpdate(dt, stamp);
		break;
	case CombatType::Range:
		RangeUpdate(dt, stamp);
		break;
	case CombatType::Support:
		SupportUpdate(dt, stamp);
		break;
	case CombatType::Familiar:
		FamiliarUpdate(dt, stamp);
	default:
		break;
	}

	//set the direction for animations
	if (stamp.owner->GetVelocity().x > 0)
		stamp.owner->GetTimeStamp()->bForwards = true;
	else if (stamp.owner->GetVelocity().x < 0)
		stamp.owner->GetTimeStamp()->bForwards = false;

}

//melee chasers
/**************************************************************
|	Function:
|	Purpose:
|	Parameter:
|	Return Value:
**************************************************************/
void PursuitAI::MeleeUpdate(float dt, AIStamp& stamp)
{
	//wait based on the enemy's tier
	switch (stamp.tier)
	{
	case 0:
		TierZeroMelee(dt, stamp);
		break;
	case 1:
		TierOneMelee(dt, stamp);
		break;
	case 2:
		TierTwoMelee(dt, stamp);
		break;
	case 3:
		break;
	default:
		break;
	}
}

/**************************************************************
|	Function:
|	Purpose:
|	Parameter:
|	Return Value:
**************************************************************/
void PursuitAI::TierZeroMelee(float dt, AIStamp& stamp)
{
	//retarget
	//if (stamp.fStartTimer >= 15.0f)
	//{
	//	stamp.SwitchAIMode(EnemyAIType::Idle);
	//	return;
	//}
	//local pointer to teh owner
	Character* ptr = stamp.owner;

	//create  a vector to the player
	SGD::Vector toTarget = stamp.target->GetPosition() - ptr->GetPosition();

	//calculate the distance
	float distance = toTarget.ComputeLength();

	//50 pixels - attack if the target is close
	if (distance < 75 && fabs(toTarget.y) < 20)
	{
		//normalize the vector
		toTarget.Normalize();
		//stop moving
		ptr->SetVelocity(toTarget);
		//set the animation to walk
		stamp.SwitchAIMode(EnemyAIType::Combat);
	}
	else //walk to the target
	{
		//normalize the vector
		toTarget.Normalize();

		//set the velocity
		float speed = ptr->GetMoveSpeed();
		toTarget.x *= speed;
		toTarget.y *= speed;
		ptr->SetVelocity(toTarget);

		//set the animation to walk
		ptr->GetTimeStamp()->SwitchAnimation("walk");
	}

	//set the direction for animations
	//if (ptr->GetVelocity().x > 0)
	//	ptr->GetTimeStamp()->bForwards = true;
	//else if (ptr->GetVelocity().x < 0)
	//	ptr->GetTimeStamp()->bForwards = false;

}

/**************************************************************
|	Function:
|	Purpose:
|	Parameter:
|	Return Value:
**************************************************************/
void PursuitAI::TierOneMelee(float dt, AIStamp& stamp)
{
	//retarget
	if (stamp.fStartTimer >= 15.0f)
	{
		stamp.SwitchAIMode(EnemyAIType::Idle);
		return;
	}

	//velocity to move at
	SGD::Vector toMove;

	//dodge projectile
	//search for projectiles
#pragma region Missile Dodging / Blocking

	//local entity manager
	EntityManager* entManager = PlayGameState::GetInstance()->GetManager();

	//vector of entities
	std::vector<IEntity*> missileVector = entManager->GetEntityVector(EntityType::ENT_MISSILE);
	//search for projectiles
	for (unsigned int index = 0; index < missileVector.size(); ++index)
	{
		//local missile pointer
		Missile* missile = (Missile*)missileVector[index];
		//same kind?
		if (stamp.owner->GetType() == missile->GetOwner()->GetType())
			continue;

		//colliding rectangels
		SGD::Rectangle missileActive = missile->GetActiveRect();
		SGD::Rectangle enemyCollision = stamp.owner->GetCollisionRect();

		//vector from me to bullet
		SGD::Vector toMissile = missile->GetPosition() - stamp.owner->GetPosition();
		int enemyDirection = (stamp.owner->GetTimeStamp()->bForwards) ? 1 : -1;

		//can it hit me?
		if ((enemyDirection > 0 && toMissile.x > 0 && missile->GetVelocity().x < 0)
			|| (enemyDirection < 0 && toMissile.x < 0 && missile->GetVelocity().x > 0))
		{
			//dodge vector
			SGD::Vector dodge = SGD::Vector(0.f, 0.f);

			//in range?
			if ((enemyCollision.bottom > missileActive.top && enemyCollision.bottom <= missileActive.bottom)
				|| (enemyCollision.top > missileActive.top && enemyCollision.top <= missileActive.bottom))
			{//block

				//distance 
				float distance = toMissile.ComputeLength();

				//not enough time?
				if (distance < 200.0f)
				{
					stamp.SwitchAIMode(EnemyAIType::Defense);
					return;
				}

				//dodge
				if (enemyCollision.bottom > missileActive.top && enemyCollision.bottom < missileActive.bottom)
					dodge.y = -1.0f;
				else if (enemyCollision.top > missileActive.top && enemyCollision.top < missileActive.bottom)
					dodge.y = 1.0f;

				//set the enemy to dodge
				toMove = dodge;
				////normalize moving vector
				toMove.Normalize();
				//scale the movement
				float speed = stamp.owner->GetMoveSpeed() * 1.25f;
				toMove.x *= speed;
				toMove.y *= speed;

				//set the animation to walk
				stamp.owner->GetTimeStamp()->SwitchAnimation("walk");

				//set the direction for animations
				//if (toMove.x > 0)
				//	stamp.owner->GetTimeStamp()->bForwards = true;
				//else if (toMove.x < 0)
				//	stamp.owner->GetTimeStamp()->bForwards = false;

				//set the velocity to move
				stamp.owner->SetVelocity(toMove);
				//success!
				return;
			}
		}
		//safe to move freely

		//set the velocity to move
		stamp.owner->SetVelocity({ 0, 0 });

	}

#pragma endregion

#pragma region Skill

	for (unsigned int i = 0; i < 3; ++i)
	{
		if (AISkill::GetInstance()->Update(dt, (Enemy*)stamp.owner, &stamp.enemySkills[i]))
			return;
	}

#pragma endregion


	//check for danger
#pragma region Defend Physical Attacks
	//local pointer to teh owner
	Character* ptr = stamp.owner;

	//create  a vector to the player
	SGD::Vector toTarget = stamp.target->GetPosition() - ptr->GetPosition();

	//calculate the distance
	float distance = toTarget.ComputeLength();

	//50 pixels - attack if the target is close
	if (distance < 75 && fabs(toTarget.y) < 20)
	{
		std::string targetAction = stamp.target->GetTimeStamp()->m_sAction;
		if (targetAction != "idle" && targetAction != "guard" && targetAction != "flinch" /*&& targetAction != "knockback"*/
			&& targetAction != "stun" && targetAction != "walk")
		{
			if (rand() % 2)
				stamp.SwitchAIMode(EnemyAIType::Defense);
			else
			{
				//normalize the vector
				toTarget.Normalize();
				//stop moving
				ptr->SetVelocity(toTarget);
				//set the animation to walk
				stamp.SwitchAIMode(EnemyAIType::Combat);

				return;
			}
		}
		else
		{
			//normalize the vector
			toTarget.Normalize();
			//stop moving
			ptr->SetVelocity(toTarget);
			//set the animation to walk
			stamp.SwitchAIMode(EnemyAIType::Combat);

			return;
		}

	}

	//normalize the vector
	toTarget.Normalize();

	//set the velocity
	float speed = ptr->GetMoveSpeed();
	toTarget.x *= speed;
	toTarget.y *= speed;
	ptr->SetVelocity(toTarget);

	//set the animation to walk
	ptr->GetTimeStamp()->SwitchAnimation("walk");


	//set the direction for animations
	//if (ptr->GetVelocity().x > 0)
	//	ptr->GetTimeStamp()->bForwards = true;
	//else if (ptr->GetVelocity().x < 0)
	//	ptr->GetTimeStamp()->bForwards = false;

#pragma endregion

}

/**************************************************************
|	Function:
|	Purpose:
|	Parameter:
|	Return Value:
**************************************************************/
void PursuitAI::TierTwoMelee(float dt, AIStamp& stamp)
{
	//retarget
	if (stamp.fStartTimer >= 15.0f)
	{
		stamp.SwitchAIMode(EnemyAIType::Idle);
		return;
	}

	//velocity to move at
	SGD::Vector toMove;

#pragma region Skill

	for (unsigned int i = 0; i < 3; ++i)
	{
		if (AISkill::GetInstance()->Update(dt, (Enemy*)stamp.owner, &stamp.enemySkills[i]))
			return;
	}

#pragma endregion

	//dodge projectile
	//search for projectiles
#pragma region Missile Dodging / Blocking

	//local entity manager
	EntityManager* entManager = PlayGameState::GetInstance()->GetManager();

	//vector of entities
	std::vector<IEntity*> missileVector = entManager->GetEntityVector(EntityType::ENT_MISSILE);
	//search for projectiles
	for (unsigned int index = 0; index < missileVector.size(); ++index)
	{
		//local missile pointer
		Missile* missile = (Missile*)missileVector[index];
		//same kind?
		if (stamp.owner->GetType() == missile->GetOwner()->GetType())
			continue;

		//colliding rectangels
		SGD::Rectangle missileActive = missile->GetActiveRect();
		SGD::Rectangle enemyCollision = stamp.owner->GetCollisionRect();

		//vector from me to bullet
		SGD::Vector toMissile = missile->GetPosition() - stamp.owner->GetPosition();
		int enemyDirection = (stamp.owner->GetTimeStamp()->bForwards) ? 1 : -1;

		//can it hit me?
		if ((enemyDirection > 0 && toMissile.x > 0 && missile->GetVelocity().x < 0)
			|| (enemyDirection < 0 && toMissile.x < 0 && missile->GetVelocity().x > 0))
		{
			//dodge vector
			SGD::Vector dodge = SGD::Vector(0.f, 0.f);

			//in range?
			if ((enemyCollision.bottom > missileActive.top && enemyCollision.bottom <= missileActive.bottom)
				|| (enemyCollision.top > missileActive.top && enemyCollision.top <= missileActive.bottom))
			{//block

				//distance 
				float distance = toMissile.ComputeLength();

				//not enough time?
				if (distance < 200.0f)
				{
					stamp.SwitchAIMode(EnemyAIType::Defense);
					return;
				}

				//dodge
				if (enemyCollision.bottom > missileActive.top && enemyCollision.bottom < missileActive.bottom)
					dodge.y = -1.0f;
				else if (enemyCollision.top > missileActive.top && enemyCollision.top < missileActive.bottom)
					dodge.y = 1.0f;

				//set the enemy to dodge
				toMove = dodge;
				////normalize moving vector
				toMove.Normalize();
				//scale the movement
				float speed = stamp.owner->GetMoveSpeed() * 1.25f;
				toMove.x *= speed;
				toMove.y *= speed;

				//set the animation to walk
				stamp.owner->GetTimeStamp()->SwitchAnimation("walk");

				//set the velocity to move
				stamp.owner->SetVelocity(toMove);
				//success!
				return;
			}
		}
		//safe to move freely

		//set the velocity to move
		stamp.owner->SetVelocity({ 0, 0 });

	}

#pragma endregion

	//check for danger
#pragma region Defend Physical Attacks
	//local pointer to teh owner
	Character* ptr = stamp.owner;

	//create  a vector to the player
	SGD::Vector toTarget = stamp.target->GetPosition() - ptr->GetPosition();

	//calculate the distance
	float distance = toTarget.ComputeLength();

	//50 pixels - attack if the target is close
	if (distance < 100 && fabs(toTarget.y) < 20)
	{

			//normalize the vector
			toTarget.Normalize();
			//stop moving
			ptr->SetVelocity(toTarget);
			//set the animation to walk
			stamp.SwitchAIMode(EnemyAIType::Combat);

			return;
		
	}

	//normalize the vector
	toTarget.Normalize();

	//set the velocity
	float speed = ptr->GetMoveSpeed();
	toTarget.x *= speed;
	toTarget.y *= speed;
	ptr->SetVelocity(toTarget);

	//set the animation to walk
	ptr->GetTimeStamp()->SwitchAnimation("walk");


	//set the direction for animations
	//if (ptr->GetVelocity().x > 0)
	//	ptr->GetTimeStamp()->bForwards = true;
	//else if (ptr->GetVelocity().x < 0)
	//	ptr->GetTimeStamp()->bForwards = false;

#pragma endregion

}

//ranged chasers
/**************************************************************
|	Function:
|	Purpose:
|	Parameter:
|	Return Value:
**************************************************************/
void PursuitAI::RangeUpdate(float dt, AIStamp& stamp)
{

	//wait based on the enemy's tier
	switch (stamp.tier)
	{
	case 0:
		TierZeroRange(dt, stamp);
		break;
	case 1:
		TierOneRange(dt, stamp);
		break;
	case 2:
		break;
	case 3:
		break;
	default:
		break;
	}
}

/**************************************************************
|	Function:
|	Purpose:
|	Parameter:
|	Return Value:
**************************************************************/
void PursuitAI::TierZeroRange(float dt, AIStamp& stamp)
{
	//create  a vector to the player
	SGD::Vector toTarget = stamp.target->GetPosition() - stamp.owner->GetPosition();
	//facing
	float direction = toTarget.x;
	//we dont care about the x-value
	toTarget.x = 0.0f;

	//move up until the y position is reached
	//calculate the distance
	float distance = toTarget.ComputeLength();
	//normalize the vector
	toTarget.Normalize();


	//100 pixels - shoot if the target is close enough on the yAxis
	if (distance <= 10)
	{
		//stop moving
		stamp.owner->SetVelocity({ 0, 0 });
		//attack
		stamp.SwitchAIMode(EnemyAIType::Combat);
	}
	else
	{
		//set the velocity
		float speed = stamp.owner->GetMoveSpeed();
		toTarget.x *= speed;
		toTarget.y *= speed;
		stamp.owner->SetVelocity(toTarget);

		//set the animation to walk
		stamp.owner->GetTimeStamp()->SwitchAnimation("walk");
	}

	//set the direction for animations
	if (direction > 0)
		stamp.owner->GetTimeStamp()->bForwards = true;
	else if (direction < 0)
		stamp.owner->GetTimeStamp()->bForwards = false;

}

/**************************************************************
|	Function:
|	Purpose:
|	Parameter:
|	Return Value:
**************************************************************/
void PursuitAI::TierOneRange(float dt, AIStamp& stamp)
{
	//velocity to move at
	SGD::Vector toMove;

#pragma region Missile Dodging / Blocking

	//local entity manager
	EntityManager* entManager = PlayGameState::GetInstance()->GetManager();

	//vector of entities
	std::vector<IEntity*> missileVector = entManager->GetEntityVector(EntityType::ENT_MISSILE);
	//search for projectiles
	for (unsigned int index = 0; index < missileVector.size(); ++index)
	{
		//local missile pointer
		Missile* missile = (Missile*)missileVector[index];
		//same kind?
		if (stamp.owner->GetType() == missile->GetOwner()->GetType())
			continue;

		//colliding rectangels
		SGD::Rectangle missileActive = missile->GetActiveRect();
		SGD::Rectangle enemyCollision = stamp.owner->GetCollisionRect();

		//vector from me to bullet
		SGD::Vector toMissile = missile->GetPosition() - stamp.owner->GetPosition();
		int enemyDirection = (stamp.owner->GetTimeStamp()->bForwards) ? 1 : -1;

		//can it hit me?
		if ((enemyDirection > 0 && toMissile.x > 0 && missile->GetVelocity().x < 0)
			|| (enemyDirection < 0 && toMissile.x < 0 && missile->GetVelocity().x > 0))
		{
			//dodge vector
			SGD::Vector dodge = SGD::Vector(0.f, 0.f);

			//in range?
			if ((enemyCollision.bottom > missileActive.top && enemyCollision.bottom <= missileActive.bottom)
				|| (enemyCollision.top > missileActive.top && enemyCollision.top <= missileActive.bottom))
			{//block

				//distance 
				float distance = toMissile.ComputeLength();

				//not enough time?
				if (distance < 200.0f)
				{
					stamp.SwitchAIMode(EnemyAIType::Defense);
					return;
				}

				//dodge
				if (enemyCollision.bottom > missileActive.top && enemyCollision.bottom < missileActive.bottom)
					dodge.y = -1.0f;
				else if (enemyCollision.top > missileActive.top && enemyCollision.top < missileActive.bottom)
					dodge.y = 1.0f;

				//set the enemy to dodge
				toMove = dodge;
				////normalize moving vector
				toMove.Normalize();
				//scale the movement
				float speed = stamp.owner->GetMoveSpeed() * 1.25f;
				toMove.x *= speed;
				toMove.y *= speed;

				//set the animation to walk
				stamp.owner->GetTimeStamp()->SwitchAnimation("walk");

				//set the velocity to move
				stamp.owner->SetVelocity(toMove);
				//success!
				return;
			}
		}
		//safe to move freely

		//set the velocity to move
		stamp.owner->SetVelocity({ 0, 0 });

	}

#pragma endregion

	//check for danger
#pragma region Defend Physical Attacks

	//local pointer to teh owner
	Character* ptr = stamp.owner;

	//create  a vector to the player
	SGD::Vector toTarget = stamp.target->GetPosition() - ptr->GetPosition();

	//calculate the distance
	float distance = toTarget.ComputeLength();
	//facing
	float direction = toTarget.x;


	//closer when the target is 180 pixels or greater
	if (distance > 400.0f)
	{
		//normalize the vector
		toTarget.Normalize();

		//set the velocity
		float speed = ptr->GetMoveSpeed();
		toTarget.x *= speed;
		toTarget.y *= speed;
		ptr->SetVelocity(toTarget);

		//set the animation to walk
		ptr->GetTimeStamp()->SwitchAnimation("walk");
	}
	//once he's in range match his Y
	else if (distance < 400.0f /*&& distance > 350 */&& fabs(toTarget.y) > 20.f)
	{
		//we dont care about the x-value
		toTarget.x = toTarget.x > 0 ? 10.0f : -10.0f;

		//normalize the vector
		toTarget.Normalize();

		//set the velocity
		float speed = ptr->GetMoveSpeed();
		toTarget.x *= speed;
		toTarget.y *= speed;
		ptr->SetVelocity(toTarget);

		//set the animation to walk
		ptr->GetTimeStamp()->SwitchAnimation("walk");
	}
	else if (distance < 350.0f && distance > 120.0f && fabs(toTarget.y) < 20)
	{
		//normalize the vector+
		toTarget.Normalize();
		//stop moving
		ptr->SetVelocity(toTarget);
		//set the animation to walk
		stamp.SwitchAIMode(EnemyAIType::Combat);
	}
	//50 pixels - attack if the target is close
	else if (distance < 120 && fabs(toTarget.y) < 30)
	{
		std::string targetAction = stamp.target->GetTimeStamp()->m_sAction;
		if (targetAction != "idle" && targetAction != "guard" && targetAction != "flinch" && targetAction != "knockback"
			&& targetAction != "stun" && targetAction != "walk")
		{
			stamp.SwitchAIMode(EnemyAIType::Defense);
			return;
		}
		else
		{
			//normalize the vector
			toTarget.Normalize();
			//stop moving
			ptr->SetVelocity(toTarget);
			//set the animation to walk
			stamp.SwitchAIMode(EnemyAIType::Combat);

			return;
		}
	}

#pragma endregion

	//set the direction for animations
	if (direction > 0)
		stamp.owner->GetTimeStamp()->bForwards = true;
	else if (direction < 0)
		stamp.owner->GetTimeStamp()->bForwards = false;

}

//support pursuit
/**************************************************************
|	Function:
|	Purpose:
|	Parameter:
|	Return Value:
**************************************************************/
void PursuitAI::SupportUpdate(float dt, AIStamp& stamp)
{
	//wait based on the enemy's tier
	switch (stamp.tier)
	{
	case 1:
		TierOneSupport(dt, stamp);
		break;
	case 2:
		TierTwoSupport(dt, stamp);
		break;
	}
}
//similar to ranged type
//should request help once life is low -- handle that inside of enemy cpp

/**************************************************************
|	Function:
|	Purpose:
|	Parameter:
|	Return Value:
**************************************************************/
void PursuitAI::TierOneSupport(float dt, AIStamp& stamp)
{
	//velocity to move at
	SGD::Vector toMove;

#pragma region  Skills
	//this could potentially break....
	//	Entity* oldTarget = stamp.target;
	//	stamp.target = nullptr;

	if (AISkill::GetInstance()->Update(dt, (Enemy*)stamp.owner, &stamp.enemySkills[0]))
		return;

	//	stamp.target = oldTarget;
#pragma endregion

#pragma region Missile Dodging / Blocking

	//local entity manager
	EntityManager* entManager = PlayGameState::GetInstance()->GetManager();

	//vector of entities
	std::vector<IEntity*> missileVector = entManager->GetEntityVector(EntityType::ENT_MISSILE);
	//search for projectiles
	for (unsigned int index = 0; index < missileVector.size(); ++index)
	{
		//local missile pointer
		Missile* missile = (Missile*)missileVector[index];
		//same kind?
		if (stamp.owner->GetType() == missile->GetOwner()->GetType())
			continue;

		//colliding rectangels
		SGD::Rectangle missileActive = missile->GetActiveRect();
		SGD::Rectangle enemyCollision = stamp.owner->GetCollisionRect();

		//vector from me to bullet
		SGD::Vector toMissile = missile->GetPosition() - stamp.owner->GetPosition();
		int enemyDirection = (stamp.owner->GetTimeStamp()->bForwards) ? 1 : -1;

		//can it hit me?
		if ((enemyDirection > 0 && toMissile.x > 0 && missile->GetVelocity().x < 0)
			|| (enemyDirection < 0 && toMissile.x < 0 && missile->GetVelocity().x > 0))
		{
			//dodge vector
			SGD::Vector dodge = SGD::Vector(0.f, 0.f);

			//in range?
			if ((enemyCollision.bottom > missileActive.top && enemyCollision.bottom <= missileActive.bottom)
				|| (enemyCollision.top > missileActive.top && enemyCollision.top <= missileActive.bottom))
			{//block

				//distance 
				float distance = toMissile.ComputeLength();

				//not enough time?
				if (distance < 200.0f)
				{
					stamp.SwitchAIMode(EnemyAIType::Defense);
					return;
				}

				//dodge
				if (enemyCollision.bottom > missileActive.top && enemyCollision.bottom < missileActive.bottom)
					dodge.y = -1.0f;
				else if (enemyCollision.top > missileActive.top && enemyCollision.top < missileActive.bottom)
					dodge.y = 1.0f;

				//set the enemy to dodge
				toMove = dodge;
				////normalize moving vector
				toMove.Normalize();
				//scale the movement
				float speed = stamp.owner->GetMoveSpeed() * 1.25f;
				toMove.x *= speed;
				toMove.y *= speed;

				//set the animation to walk
				stamp.owner->GetTimeStamp()->SwitchAnimation("walk");

				//set the velocity to move
				stamp.owner->SetVelocity(toMove);
				//success!
				return;
			}
		}
		//set the velocity to move
		stamp.owner->SetVelocity({ 0, 0 });
	}

#pragma endregion

	//check for danger
#pragma region Defend Physical Attacks

	//local pointer to teh owner
	Character* ptr = stamp.owner;

	//create  a vector to the player
	SGD::Vector toTarget = stamp.target->GetPosition() - ptr->GetPosition();

	//calculate the distance
	float distance = toTarget.ComputeLength();
	//facing
	float direction = toTarget.x;


	//closer when the target is 180 pixels or greater
	if (distance > 400.0f)
	{
		//normalize the vector
		toTarget.Normalize();

		//set the velocity
		float speed = ptr->GetMoveSpeed();
		toTarget.x *= speed;
		toTarget.y *= speed;
		ptr->SetVelocity(toTarget);

		//set the animation to walk
		ptr->GetTimeStamp()->SwitchAnimation("walk");
	}
	//once he's in range match his Y
	else if (distance < 400.0f /*&& distance > 350 */&& fabs(toTarget.y) > 20.f)
	{
		//we dont care about the x-value
		toTarget.x = toTarget.x > 0 ? 10.0f : -10.0f;

		//normalize the vector
		toTarget.Normalize();

		//set the velocity
		float speed = ptr->GetMoveSpeed();
		toTarget.x *= speed;
		toTarget.y *= speed;
		ptr->SetVelocity(toTarget);

		//set the animation to walk
		ptr->GetTimeStamp()->SwitchAnimation("walk");
	}
	else if (distance < 350.0f && distance > 120.0f && fabs(toTarget.y) < 20)
	{
		//normalize the vector+
		toTarget.Normalize();
		//stop moving
		ptr->SetVelocity(toTarget);
		//set the animation to walk
		stamp.SwitchAIMode(EnemyAIType::Combat);
	}
	//50 pixels - attack if the target is close
	else if (distance < 120 && fabs(toTarget.y) < 30)
	{
		std::string targetAction = stamp.target->GetTimeStamp()->m_sAction;
		if (targetAction != "idle" && targetAction != "guard" && targetAction != "flinch" && targetAction != "knockback"
			&& targetAction != "stun" && targetAction != "walk")
		{
			stamp.SwitchAIMode(EnemyAIType::Defense);
			return;
		}
		else
		{
			//normalize the vector
			toTarget.Normalize();
			//stop moving
			ptr->SetVelocity(toTarget);
			//set the animation to walk
			stamp.SwitchAIMode(EnemyAIType::Combat);

			return;
		}
	}

#pragma endregion

	//set the direction for animations
	if (direction > 0)
		stamp.owner->GetTimeStamp()->bForwards = true;
	else if (direction < 0)
		stamp.owner->GetTimeStamp()->bForwards = false;

}

/**************************************************************
|	Function:
|	Purpose:
|	Parameter:
|	Return Value:
**************************************************************/

void PursuitAI::TierTwoSupport(float dt, AIStamp& timeStamp)
{}

void PursuitAI::FamiliarUpdate(float dt, AIStamp& stamp)
{
	//retarget
	if (stamp.fStartTimer >= 15.0f)
	{
		stamp.SwitchAIMode(EnemyAIType::Idle);
		return;
	}

	//velocity to move at
	SGD::Vector toMove;

	//dodge projectile
	//search for projectiles
#pragma region Missile Dodging / Blocking

	//local entity manager
	EntityManager* entManager = PlayGameState::GetInstance()->GetManager();

	//vector of entities
	std::vector<IEntity*> missileVector = entManager->GetEntityVector(EntityType::ENT_MISSILE);
	//search for projectiles
	for (unsigned int index = 0; index < missileVector.size(); ++index)
	{
		//local missile pointer
		Missile* missile = (Missile*)missileVector[index];
		//same kind?
		if (stamp.owner->GetType() == missile->GetOwner()->GetType())
			continue;

		//colliding rectangels
		SGD::Rectangle missileActive = missile->GetActiveRect();
		SGD::Rectangle enemyCollision = stamp.owner->GetCollisionRect();

		//vector from me to bullet
		SGD::Vector toMissile = missile->GetPosition() - stamp.owner->GetPosition();
		int enemyDirection = (stamp.owner->GetTimeStamp()->bForwards) ? 1 : -1;

		//can it hit me?
		if ((enemyDirection > 0 && toMissile.x > 0 && missile->GetVelocity().x < 0)
			|| (enemyDirection < 0 && toMissile.x < 0 && missile->GetVelocity().x > 0))
		{
			//dodge vector
			SGD::Vector dodge = SGD::Vector(0.f, 0.f);

			//in range?
			if ((enemyCollision.bottom > missileActive.top && enemyCollision.bottom <= missileActive.bottom)
				|| (enemyCollision.top > missileActive.top && enemyCollision.top <= missileActive.bottom))
			{//block

				//distance 
				float distance = toMissile.ComputeLength();

				//not enough time?
				if (distance < 200.0f)
				{
					stamp.SwitchAIMode(EnemyAIType::Defense);
					return;
				}

				//dodge
				if (enemyCollision.bottom > missileActive.top && enemyCollision.bottom < missileActive.bottom)
					dodge.y = -1.0f;
				else if (enemyCollision.top > missileActive.top && enemyCollision.top < missileActive.bottom)
					dodge.y = 1.0f;

				//set the enemy to dodge
				toMove = dodge;
				////normalize moving vector
				toMove.Normalize();
				//scale the movement
				float speed = stamp.owner->GetMoveSpeed() * 1.25f;
				toMove.x *= speed;
				toMove.y *= speed;

				//set the animation to walk
				stamp.owner->GetTimeStamp()->SwitchAnimation("walk");

				//set the direction for animations
				//if (toMove.x > 0)
				//	stamp.owner->GetTimeStamp()->bForwards = true;
				//else if (toMove.x < 0)
				//	stamp.owner->GetTimeStamp()->bForwards = false;

				//set the velocity to move
				stamp.owner->SetVelocity(toMove);
				//success!
				return;
			}
		}
		//safe to move freely

		//set the velocity to move
		stamp.owner->SetVelocity({ 0, 0 });

	}

#pragma endregion

#pragma region Specials

	//update skills
	if (AISkill::GetInstance()->Update(dt, (Enemy*)stamp.owner, &stamp.enemySkills[0]))
		return;

#pragma endregion
	//check for danger
#pragma region Defend Physical Attacks
	//local pointer to teh owner
	Character* ptr = stamp.owner;

	//create  a vector to the player
	SGD::Vector toTarget = stamp.target->GetPosition() - ptr->GetPosition();

	//calculate the distance
	float distance = toTarget.ComputeLength();

	//50 pixels - attack if the target is close
	if (distance < 80 && fabs(toTarget.y) < 25)
	{
		std::string targetAction = stamp.target->GetTimeStamp()->m_sAction;
		if (targetAction != "idle" && targetAction != "guard" && targetAction != "flinch" && targetAction != "knockback"
			&& targetAction != "stun" && targetAction != "walk")
		{
			stamp.SwitchAIMode(EnemyAIType::Defense);
			return;
		}
		else
		{
			//normalize the vector
			toTarget.Normalize();
			//stop moving
			ptr->SetVelocity(toTarget);
			//set the animation to walk
			stamp.SwitchAIMode(EnemyAIType::Combat);

			return;
		}
	}

	//normalize the vector
	toTarget.Normalize();

	//set the velocity
	float speed = ptr->GetMoveSpeed();
	toTarget.x *= speed;
	toTarget.y *= speed;
	ptr->SetVelocity(toTarget);

	//set the animation to walk
	ptr->GetTimeStamp()->SwitchAnimation("walk");


	//set the direction for animations
	//if (ptr->GetVelocity().x > 0)
	//	ptr->GetTimeStamp()->bForwards = true;
	//else if (ptr->GetVelocity().x < 0)
	//	ptr->GetTimeStamp()->bForwards = false;

#pragma endregion

}

