#include "Collision.h"
#include "../Source/Game.h"
#include "../SGD Wrappers/SGD_EventManager.h"
#include "../SGD Wrappers/SGD_Event.h"
#include "../SGD Wrappers/SGD_MessageManager.h"
#include "../IslandRaiders/CreateAnimatedParticle.h"
#include "DestroyEntityMessage.h"

#include "LevelManager.h"
#include "ParticleManager.h"

#include "PlayGameState.h"

#include "GUI.h"

#define EFFECT_AREA 1

namespace{

	float RandomFloat(float min, float max)
	{
		return min + static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / (max - min)));;
	}
}

Collision::Collision(int _section)
{
	SetType(EntityType::ENT_INVULNERABLE);
	aTimeStamp.SetOwner(nullptr);
	section = _section;
}
Collision::~Collision()
{
}
/*virtual*/ void Collision::Render() //override
{

	Entity::Render();
}
/*virtual*/ bool Collision::HandleCollision(const IEntity* other) //override;
{
	if (eventID.size() > 0)
	{
		if (destroyAfterCollision)
		{
			//prevent skiping a section with fencer skill
			if (eventID == "CAM2SECTION" && LevelManager::GetInstance()->GetEnemiesLeft() > 0)
				return true;

#if 1
			if (eventID == "SPAWNENEMIES" && 
				section == PlayGameState::GetInstance()->GetCurrentSectionIndex())
				return true;
#endif

			SGD::Event* evnt = new SGD::Event(eventID.c_str(), &section, this);
			SGD::EventManager::GetInstance()->QueueEvent(evnt);

			//destroy after collision
			DestroyEntityMessage* destroyMsg = new DestroyEntityMessage(this);
			SGD::MessageManager::GetInstance()->QueueMessage(destroyMsg);
		}
		else
		{
			//means trigger must have an overtime effect
			if (++effectCounter > effectMax)
			{
				if (eventID == "LAVAPIT")
				{
					float damage = 3;
					const SGD::Event dmg = { "DAMAGE", &damage, this };
					dmg.SendEventNow(other);
				}
				else if (eventID == "SLOWAREA")
				{
					float freezeStat[2] = { 0.5f, 2.5f };
					const SGD::Event dmg = { "FREEZE", &freezeStat, this };
					dmg.SendEventNow(other);

					const Entity* player = dynamic_cast<const Entity*>(other);
					SGD::Point pos = player->GetPosition();
					pos.Offset(0, -player->GetSize().height);

					GUIManager::GetInstance()->PopUpFade(
						"freeze", pos, 0.8f, 1.f, { 160, 228, 255 });
				}
				else if (eventID == "THUNDERAREA")
				{
					const Character* player = dynamic_cast<const Character*>(other);
					if (player)
					{
						SGD::Point pos = {
							RandomFloat(position.x, position.x + size.width),
							RandomFloat(position.y, position.y + size.height) - 970.f };
						CreateAnimatedParticle *msg = new CreateAnimatedParticle(
							(Character*)player, "lighting", 150, {}, pos, {});
						SGD::MessageManager::GetInstance()->QueueMessage(msg);
					}
				}
				effectCounter = 0;
			}//end trigger effect
		}
	}
	return true;
}
void Collision::SetTrigger(const char *eventName)
{
	eventID = eventName;
	SetType(EntityType::ENT_BASE);//trigger

	destroyAfterCollision = true;//by default

#if (EFFECT_AREA)
	if (eventID == "LAVAPIT")
	{
		destroyAfterCollision = false;

		ParticleEmitter *emitter = new ParticleEmitter(2, 50, 0.001f, size);
		ParticleManager::GetInstance()->AddEmitter(emitter, true);
		emitter->SetPosition(position);
	}
	if (eventID == "SLOWAREA")
	{
		destroyAfterCollision = false;
		effectMax = 60;
		ParticleEmitter *emitter = new ParticleEmitter(4, 50, 0.001f, size);
		ParticleManager::GetInstance()->AddEmitter(emitter, true);
		emitter->SetPosition(position);
	}
	if (eventID == "THUNDERAREA")
	{
		destroyAfterCollision = false;
		effectMax = 120;
	}
#endif
}
