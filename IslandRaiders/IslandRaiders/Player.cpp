#include "Player.h"
#include "../SGD Wrappers/SGD_AudioManager.h"
#include "../SGD Wrappers/SGD_GraphicsManager.h"
#include "../SGD Wrappers/SGD_InputManager.h"
#include "CreateAnimatedParticle.h"
#include "CreateMissileMessage.h"
#include "PlayerDownMessage.h"
#include "PlayGameState.h"
#include "AnimationSystem.h"
#include "CharacterGameState.h"
#include "PlayGameState.h"
#include "../SGD Wrappers/SGD_GraphicsManager.h"
#include "../SGD Wrappers/SGD_EventManager.h"
#include "../SGD Wrappers/SGD_Event.h"
#include "../SGD Wrappers/SGD_MessageManager.h"

#include "GUI.h"
#include "ParticleManager.h"
#include "../Source/Game.h"
#include <assert.h>
#include <sstream>

namespace{
	float FloatLerp(float a, float b, float t){
		if (t > 1.f) t = 1.f;
		return  ((1.0f - t)*a + t*b);
	}
}

//Player::Player() : Character()
Player::Player(const char* _name, int index) : Character(_name)
{
	SGD::AudioManager*	  pAudio = SGD::AudioManager::GetInstance();

	//HUD
	playerHUD = new PlayerHUD(this);

	//controller
	controllerIndex = index;

	//register
	Listener::RegisterForEvent("ATTACK");
	Listener::RegisterForEvent("HEAL");

	ParticleFlyWeight *flyblood = ParticleManager::GetInstance()->GetFlyWeight(1);
	blood = new ParticleEmitter(flyblood, 50, 0.001f, { 16, 16 });
	ParticleManager::GetInstance()->AddEmitter(blood, false);
	spreeSound[0] = pAudio->LoadAudio(L"resources/audio/sound effects/kssound/doublekill.wav");
	spreeSound[1] = pAudio->LoadAudio(L"resources/audio/sound effects/kssound/triplekill.wav");
	spreeSound[2] = pAudio->LoadAudio(L"resources/audio/sound effects/kssound/fourkill.wav");
}
Player::~Player()
{
	blood = nullptr;

	//delete all dynamic memory
	delete playerHUD;
	playerHUD = nullptr;

	SGD::AudioManager*	  pAudio = SGD::AudioManager::GetInstance();
	for (int i = 0; i < 3; i++)
		pAudio->UnloadAudio(spreeSound[i]);
}

void Player::Update(float dt)
{

	SGD::AudioManager*	  pAudio = SGD::AudioManager::GetInstance();

#pragma region Stamina
	//regen stamina
	stat.staminaRegen += dt;
	if (stat.staminaRegen >= 4.0f)
	{
		stat.stamina += 0.20f;
		if (stat.stamina > 1.0f)
			stat.stamina = 1.0f;

		stat.staminaRegen = 0.0f;
	}
#pragma endregion

	//skip all if stunned
	if (sEffectStatus.IsEffectOn(Status::STUN) || (aTimeStamp.m_sAction == "intro" && !aTimeStamp.isDone))
	{
		//Update(dt);
		Character::Update(dt);
		return;
	}

#pragma region Regen

	//if (health < maxHealth)
	//	//one point of health a sec
	//	health += dt * 0.5f;
	//else if (health > maxHealth)
	//	health = maxHealth;

#pragma endregion

#pragma region DEAD 
	//dead?
	if (fResetTimer > 0)
	{
		//decrease the time
		fResetTimer -= dt;

		fBlickChange += dt;
		if (fBlickChange >= 0.25f)
		{
			bBlink = !bBlink;
			fBlickChange = 0.0f;
		}

		//reset to zero
		if (fResetTimer <= 0.0f)
		{
			fResetTimer = 0;
			bDead = false;
			bBlink = false;
		}

	}
#pragma endregion

#pragma region Knockback
	if (aTimeStamp.m_sAction == "knockback")
	{
		//scale the velocity
		velocity.x *= 0.90f;

		//check
		if (fabsf(velocity.x) <= 3.0f)
			velocity.x = 0.0f;

		//flip
		if (velocity.x > 0)
			aTimeStamp.bForwards = false;
		else if (velocity.x < 0)
			aTimeStamp.bForwards = true;
	}


#pragma endregion

#pragma region Skills

	//skills
	if (skillBasic)
	{
		if (skillBasic(dt, this, skillStamp[0].m_bStart))
		{
			Character::Update(dt);
			return;
		}
	}

	if (skillOne)
	{		//skill (time, object, player wants to use it
		if (skillOne(dt, this, skillStamp[1].m_bStart))
		{
			Character::Update(dt);
			return;
		}
	}
	if (skillTwo)
	{
		if (skillTwo(dt, this, skillStamp[2].m_bStart))
		{
			Character::Update(dt);
			return;
		}
	}

#pragma endregion

#pragma region Combos
	//update the fire rate
	stat.fireTimer += dt;

	if (stat.fireTimer >= stat.fireFreq)
		//update the combos
		Input(dt);
	else
		int x = 0;

	if (aTimeStamp.m_sAction != "guard" && UpdateCombo(dt))
	{
		Character::Update(dt);
		return;
	}

#pragma endregion

#pragma region Input Boolean
	//local iput instance
	SGD::InputManager* pInput = SGD::InputManager::GetInstance();
	bool moveUp = false;
	bool moveDown = false;
	bool moveLeft = false;
	bool moveRight = false;
	bool basicSpecial = false;
	bool specialOne = false;
	bool specialTwo = false;
	bool defend = false;
	//input booleans
	if (!(Game::GetInstance()->ARCADEMODE))
	{
		moveUp = pInput->IsKeyDown(SGD::Key::W) || pInput->IsDPadDown(controllerIndex, SGD::DPad::Up);
		moveDown = pInput->IsKeyDown(SGD::Key::S) || pInput->IsDPadDown(controllerIndex, SGD::DPad::Down);
		moveLeft = pInput->IsKeyDown(SGD::Key::A) || pInput->IsDPadDown(controllerIndex, SGD::DPad::Left);
		moveRight = pInput->IsKeyDown(SGD::Key::D) || pInput->IsDPadDown(controllerIndex, SGD::DPad::Right);
		basicSpecial = pInput->IsKeyPressed(SGD::Key::U) || pInput->IsButtonPressed(controllerIndex, 4);
		specialOne = pInput->IsKeyPressed(SGD::Key::I) || pInput->IsButtonPressed(controllerIndex, 6);
		specialTwo = pInput->IsKeyPressed(SGD::Key::O) || pInput->IsButtonPressed(controllerIndex, 7);
		defend = pInput->IsKeyDown(SGD::Key::L) || pInput->IsButtonDown(controllerIndex, 5);
	}
	else
	{
		if (pInput->GetLeftJoystick(controllerIndex).y < 0.0f)
			moveUp = true;
		else
			moveUp = false;

		if (pInput->GetLeftJoystick(controllerIndex).y > 0.0f)
			moveDown = true;
		else
			moveDown = false;

		if (pInput->GetLeftJoystick(controllerIndex).x < 0.0f)
			moveLeft = true;
		else
			moveLeft = false;

		if (pInput->GetLeftJoystick(controllerIndex).x > 0.0f)
			moveRight = true;
		else
			moveRight = false;

		basicSpecial = pInput->IsButtonPressed(controllerIndex, 3);
		specialOne = pInput->IsButtonPressed(controllerIndex, 4);
		specialTwo = pInput->IsButtonPressed(controllerIndex, 5);
		defend = pInput->IsButtonDown(controllerIndex, 2);
	}

	bool stand = !(moveUp || moveDown || moveLeft || moveRight
		|| basicSpecial || specialOne || specialTwo || defend);

	//animation done?
	if (aTimeStamp.bLoop == false && aTimeStamp.isDone == false)
	{
		//update animations only
		Character::Update(dt);

		//return to idle when complete with animations --avoid crash
		if (aTimeStamp.isDone)
			aTimeStamp.SwitchAnimation("idle");
		//skip everything else
		return;
	}

	//countering
	if (fCounter > 0.0f)
	{
		fCounter -= dt;
		if (fCounter <= 0.0f)
			fCounter = 0.0f;
	}

	//if (pInput->IsKeyUp()) pInput->IsButtonDown(controllerIndex, 5)
	if (!Game::GetInstance()->ARCADEMODE)
	{
		if (pInput->IsKeyReleased(SGD::Key::Spacebar) || pInput->IsButtonReleased(controllerIndex, 5))
			fCounter = 0.0f;
		else if (pInput->IsKeyPressed(SGD::Key::Spacebar) || pInput->IsButtonPressed(controllerIndex, 5))
			fCounter = 0.20f; //break - lower threshold 
	}
	else
	{
		if (pInput->IsButtonReleased(controllerIndex, 2))
			fCounter = 0.0f;
		else if (pInput->IsButtonPressed(controllerIndex, 2))
			fCounter = 0.20f;
	}

	//no input
	velocity.x = velocity.y = 0;

	//NO INPUT
	if (stand)
		//stand still
		this->aTimeStamp.SwitchAnimation("idle");
	else if (defend)
	{
		//guard yourself
		aTimeStamp.SwitchAnimation("guard");
		currCombo = NONE;
		inputQueue.clear();
	}
	else if (basicSpecial)
		//activate ONLY when the pointer actually exists
		skillStamp[0].m_bStart = (skillBasic != nullptr && skillStamp[0].m_fSkillTimer <= 0.0f);
	else if (specialOne)
		//activate ONLY when the pointer actually exists
		skillStamp[1].m_bStart = (skillOne != nullptr && skillStamp[1].m_fSkillTimer <= 0.0f);
	else if (specialTwo)
		//activate ONLY when the pointer actually exists
		skillStamp[2].m_bStart = (skillTwo != nullptr && skillStamp[2].m_fSkillTimer <= 0.0f);
	//walking
	else
	{
		//animate 
		aTimeStamp.SwitchAnimation("walk");
		//moving left
		if (moveLeft)
		{
			velocity.x = -1; //left direction
			aTimeStamp.bForwards = false; //walking back
		}
		//moving right
		else if (moveRight)
		{
			velocity.x = 1; //right direction
			aTimeStamp.bForwards = true; //walking forwards
		}
		else velocity.x = 0; //neither left nor right

		if (moveUp) //up
			velocity.y = -1; //up direction
		else if (moveDown) //down
			velocity.y = 1; //down direction
		else velocity.y = 0; //neither up nor down

		//normalize the vector
		velocity.Normalize();
		//scale normal vector
		velocity *= stat.moveSpeed;
	}

#pragma endregion

	//correct color 
	aTimeStamp.m_clrRender = (defend) ? SGD::Color(255, 255, (unsigned char)(stat.stamina * 255), 0) : SGD::Color(255, 255, 255, 255);
	//update the frame

#pragma region FX
	blood->SetPosition({ position.x, position.y - 20 });

	//MULTIKILL POPUPS
	if (killCount > 0)
	{
		killTimer += dt;
		if (killTimer > killDuration)
		{
			if (killCount > 1) // between 1 enemy killed but less than 4 : 2 or 3 enemies were killed
			{
				std::string txt = "OVER-KILL";
				SGD::Color colorA = { 255, 0, 0 };//red,pink
				SGD::Color colorB = { 255, 0, 100 };
				SGD::HAudio sfx = spreeSound[2];
				switch (killCount)
				{
				case 2:
					txt = "DOUBLE KILL";
					colorA = { 0, 0, 255 };//blue,cyan
					colorB = { 0, 255, 255 };
					sfx = spreeSound[0];
					break;
				case 3:
					txt = "TRIPLE KILL";
					colorA = { 0, 255, 0 };//green,yellow
					colorB = { 255, 255, 0 };
					sfx = spreeSound[1];
					break;
				}
				SGD::AudioManager::GetInstance()->PlayAudio(sfx);
				SGD::Point pos = position;
				pos.Offset(-50, -size.height);
				GUIManager::GetInstance()->PopUpFlash(
					txt, pos, 0.9f, 0.8f, colorA, colorB, { 10, 5 });

			}
			killTimer = 0.f;
			killCount = 0;
		}
	}
#pragma endregion

	Character::Update(dt);
	StayInWorld();
}
void Player::Render()
{
	if (bBlink)
		return;

	Character::Render();
}
void Player::RenderHUD()
{
	//render the HUD
	if (playerHUD != nullptr)
		playerHUD->DrawHUD();
}

SGD::Rectangle Player::OffSetRect(void)
{
	SGD::Size offset = GetSize() / 2;

	SGD::Rectangle rekt;
	rekt.top = GetPosition().y - offset.height;
	rekt.left = GetPosition().x - offset.width;
	rekt.right = GetPosition().x + GetSize().width;
	rekt.bottom = GetPosition().y + GetSize().height;

	return rekt;
}
bool Player::HandleCollision(const IEntity* ptr)
{
	//safe check
	if (!ptr)
		return true;

	//type
	int eType = ptr->GetType();

	switch (eType)
	{
	case EntityType::ENT_MISSILE:
	{
									//convert to a character pointer
									const Missile* other = dynamic_cast<const Missile*>(ptr);

									const SGD::Rectangle attack = other->GetActiveRect();
									const SGD::Rectangle enemyCol = this->GetCollisionRect();

									if (attack.IsIntersecting(enemyCol) && fCounter > 0 && GetName() != "lal" && GetName() != "xanxus")
									{
										//counter pop up
										GUIManager::GetInstance()->PopUpFade(
											"Counter", position, 0.75f, 1.2f, { 127, 127, 127 });

										//create a damage event
										SGD::Event counter = { "COUNTER", nullptr, this };
										//send it out
										counter.SendEventNow(other);
										//cast
										Missile* missile = (Missile*)other;

										//reflect
										if (GetName() == "mihawk" || GetName() == "erza")
										{
											//take ownership
											missile->SetOwner(this);
											//reflect
											missile->SetVelocity(missile->GetVelocity() * -1.0f);
											//turn around 
											missile->GetTimeStamp()->bForwards = !missile->GetTimeStamp()->bForwards;
											//counter all
											return true;
										}
										else if (GetName() == "fran" || GetName() == "mystogan")
										{
											//heal
											stat.health += missile->GetDamage() * 0.5f;
											//healign animations
											CreateAnimatedParticle* cAP = new CreateAnimatedParticle(this, "heal", 2.0f, { 255, 255, 255, 255 });
											SGD::MessageManager::GetInstance()->QueueMessage(cAP);
											//leave
											return false;
										}
									}
	}
		break;
	case EntityType::ENT_ENEMY:
	{
								  //convert to a character pointer
								  const Character* other = dynamic_cast<const Character*>(ptr);

								  const SGD::Rectangle attack = other->GetActiveRect();
								  const SGD::Rectangle enemyCol = this->GetCollisionRect();

								  if (fCounter > 0 /*&& attack.IsIntersecting(enemyCol)*/)
								  {
									  //counter pop up
									  GUIManager::GetInstance()->PopUpFade("Counter", position, 0.75f, 1.2f, { 127, 127, 127 });

									  //create a damage event
									  SGD::Event counter = { "COUNTER", nullptr, this };
									  //send it out
									  counter.SendEventNow(other);
									  //counter all
									  return true;
								  }

	}
		break;
	}
	return Character::HandleCollision(ptr);
}

//events
void Player::HandleEvent(const SGD::Event* pEvent)
{
	//nothing
	if (bDead)
		return;

	//damage
	if (fCounter == 0)
		Character::HandleEvent(pEvent);


#pragma region Damage

	if (pEvent->GetEventID() == "DAMAGE")
	{
		//knockbacked?
		if (aTimeStamp.m_sAction == "knockback")
		return;
		

		if (aTimeStamp.m_sAction != "guard")
		{
			chain = 0;//combo chain breaks
			blood->Emit(0.5f, false);

			//switch to flinch
			if (aTimeStamp.m_sAction == "flinch")
			{
				//character
				Character* sender = (Character*)pEvent->GetSender();
				//send flying
				Knockback(sender);
			}
			else if (sEffectStatus.IsEffectOn(Status::STUN))
			{
				//character
				Character* sender = (Character*)pEvent->GetSender();
				//send flying
				Knockback(sender);
				//turn off stun
				sEffectStatus.ToggleEffect(Status::STUN);
			}
			else
			{
				//flinch
				aTimeStamp.SwitchAnimation("flinch");
				//cant move
				SetVelocity({ 0, 0 });
			}
		}
		//reset the combo
		currCombo = COMBO_LIST::NONE;
		inputQueue.clear();
	}
#pragma endregion

#pragma region Knockback

	if (pEvent->GetEventID() == "KNOCKBACK")
	{
		if (aTimeStamp.m_sAction != "guard")
		{
			//character
			Character* sender = (Character*)pEvent->GetSender();
			//send flying
			Knockback(sender);
		}
		else stat.stamina -= 0.4f;

		inputQueue.clear();
		currCombo = NONE;
	}
#pragma endregion

#pragma region Counter

	if (pEvent->GetEventID() == "COUNTER")
	{
		sEffectStatus.StartEffect(Status::STUN, 3.0f, 0.0f);
		GUIManager::GetInstance()->PopUpFade("Stun",
		{ position.x - 50, position.y - GetRenderRect().ComputeHeight() *0.5f },
		1.f, 1.2f, { 127, 127, 127 });
	}

#pragma endregion

#pragma region STOP

	if (pEvent->GetEventID() == "STOP")
	{
		//stop moving
		SetVelocity({ 0.f, 0.f });
	}

#pragma endregion

#pragma region HP Check
	//am i dead?
	if (stat.health <= 0)
	{	//health
		stat.health = 0;
		//spawn the player at the top - via event and message play game
		const SGD::Event* dead = new SGD::Event("PLAYER_DEATH", nullptr, this);
		//send to all
		SGD::EventManager::GetInstance()->QueueEvent(dead);

		//message for the gamestate
		PlayerDownMessage* down = new PlayerDownMessage(this);
		SGD::MessageManager::GetInstance()->QueueMessage(down);

		//reset animations
		aTimeStamp.SwitchAnimation("idle");
	}
#pragma endregion

#pragma region Stamina Check

	//stamina check
	if (stat.stamina <= 0.0f)
	{
		//stun the player
		sEffectStatus.StartEffect(Status::STUN, 5.0f, 0.0f);
		//reset
		stat.stamina = 1.0f;
		aTimeStamp.m_clrRender = { 255, 255, 255, 255 };
	}

#pragma endregion

}
void Player::Reset(SGD::Point _respawn)
{
	killCount = chain = 0;

	//timer
	fResetTimer = 3.0f;
	bDead = true;
	//reset position
	SetPosition(_respawn);
	//reset health
	stat.health = GetMaxHealth();
}
void Player::Input(float dt)
{
	//local input manager pointer
	SGD::InputManager* pInput = SGD::InputManager::GetInstance();

	//update the timer
	inputQueue.m_fUpdate += dt;

	//local time for the max input
	float maxTime = 1.00f;
	//input timer done?
	if (inputQueue.m_fUpdate < maxTime)
	{
		//take in key input as long as the timer is not done
		if (!Game::GetInstance()->ARCADEMODE)
		{
			if (pInput->IsKeyPressed(SGD::Key::J) || pInput->IsButtonPressed(controllerIndex, 1))
				inputQueue.AddKey(SGD::Key::J);
			else if (pInput->IsKeyPressed(SGD::Key::K) || pInput->IsButtonPressed(controllerIndex, 2))
				inputQueue.AddKey(SGD::Key::K);
		}
		else
		{
			//PC
			if (pInput->IsButtonPressed(controllerIndex, 0))
				inputQueue.AddKey(SGD::Key::J);
			else if (pInput->IsButtonPressed(controllerIndex, 1))
				inputQueue.AddKey(SGD::Key::K);
		}
	}
}
bool Player::UpdateCombo(float dt)
{
	//empty
	return false;
}
void Player::SetSkill(unsigned int skill, bool(*functionPtr)(float, Player*, bool))
{	//skill 0, 2, or 3 only
	if (skill == 1)
		skillOne = functionPtr;
	else if (skill == 2)
		skillTwo = functionPtr;
	else if (skill == 0)
		skillBasic = functionPtr;
}
///////////////////////////////////////////////////
//		INPUT QUEUE
//////////////////////////////////////////////////

void InputQueue::AddKey(SGD::Key key)
{
	//push the key into the queue
	m_qInput.push(key);
}

void InputQueue::clear(void)
{
	//clear the queue
	for (unsigned int i = 0; i < m_qInput.size(); ++i)
		m_qInput.pop();
}

SGD::Key InputQueue::front(void) const
{
	return m_qInput.front();
}

void InputQueue::pop(void)
{
	//remove the front item
	m_qInput.pop();
}

bool InputQueue::empty(void) const
{
	//is the queue empty?
	return m_qInput.empty();
}

unsigned int InputQueue::size(void) const
{
	//return the size of the queue
	return m_qInput.size();
}

/*virtual*/ void Player::Attack(const Character *other) //override;
{
	++chain;
	//skip to the next frame
	aTimeStamp.m_CurrentFrame++;
	aTimeStamp.m_fFrameTimer = 0.f;

	//extra
	AttackInfo atkInfo;
	float boost = stat.bQuickAttack ? stat.quickPercent : stat.heavyPercent;
	atkInfo.isCriticalHit = (rand() % 100 < 10 + (10 * chain / 5));
	boost *= atkInfo.isCriticalHit ? 1.5f : 1.f;
	atkInfo.damage = stat.damage * boost * stat.atkBoost;

	//create a damage event
	SGD::Event dmg = { "DAMAGE", &atkInfo, this };
	//send it out
	dmg.SendEventNow(other);

	//FX - POP UP combo chain
	SGD::Color color = SGD::Color::Lerp({}, { 255, 0, 0 }, chain / 25.f);
	float scale = FloatLerp(0.60f, .9f, chain / 10.f);

	if (chain > 0)
	{
		//popup
		std::stringstream txt;
		txt << "x" << chain;
		SGD::Point pos = position;
		pos.Offset(0, -GetRenderRect().ComputeHeight()* 0.5f);

		GUIManager::GetInstance()->PopUpFade(
			txt.str().c_str(), pos, scale, 1.2f, color);
	}
}
