#include "CreateFamiliar.h"
#include "Character.h"
#include "MessageID.h"

CreateFamiliar::CreateFamiliar(const char* name, Character* owner, SGD::Point position) : Message(MessageID::MSG_CREATE_FAMILIAR)
{
	//point
	m_ptPosition = position;

	//set owner pointer
	m_pOwner = owner;

	//add ref 
	if (m_pOwner != nullptr)
		m_pOwner->AddRef();

	//set the name
	int size = strlen(name) + 1;
	m_cName = new char[size];
	strcpy_s(m_cName, size, name);

}


CreateFamiliar::~CreateFamiliar()
{
	//release the owner
	m_pOwner->Release();
	m_pOwner = nullptr;
	//delete name
	delete m_cName;
	m_cName = nullptr;
}
