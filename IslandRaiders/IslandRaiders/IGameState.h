#pragma once
class IGameState
{
public:
	IGameState() = default;
	virtual ~IGameState() = default;

	virtual void Enter()			= 0;
	virtual void Exit()				= 0;
	virtual bool Update(float dt)	= 0;
	virtual void Render(float dt)	= 0;
};

