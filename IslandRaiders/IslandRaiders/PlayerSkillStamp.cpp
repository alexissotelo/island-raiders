#include "PlayerSkillStamp.h"
#include <map>

/**************************************************************
|	Function:
|	Purpose:
|	Parameter:
|	Return Value:
**************************************************************/
SkillStamp& PlayerSkillStamp::operator[](unsigned int index)
{
	//return the item at the index
	return m_mapSkills[index];
}

