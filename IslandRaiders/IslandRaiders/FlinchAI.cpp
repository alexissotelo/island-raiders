#include "FlinchAI.h"
#include "Enemy.h"
#include "AnimationTimestamp.h"



/**************************************************************
|	Function: FlinchAI::Update [virtual]
|	Purpose: stop the enemy from moving while running the animation
|			for a specific time span
|	Parameter:
|			float dt - the elapsed time since the last update
|			AIStamp& stamp - the current state of the enemy
|	Return Value: void
**************************************************************/
void FlinchAI::Update(float dt, AIStamp& stamp)
{

	//access the timestamp
	Enemy* owner = dynamic_cast<Enemy*>(stamp.owner);
	AnimationTimestamp* timeStamp = owner->GetTimeStamp();

	//switch to flinching animations
	timeStamp->SwitchAnimation("flinch");

	//don't let them move
	stamp.fStartTimer += dt;

	//switch after a second
	if (stamp.fStartTimer >= 0.80f)
	{
		timeStamp->SwitchAnimation("idle");
		stamp.SwitchAIMode(EnemyAIType::Pursuit);
	}

	//no moving
	owner->SetVelocity({ 0.0f, 0.0f });
}

/**************************************************************
|	Function:
|	Purpose:
|	Parameter:
|	Return Value:
**************************************************************/
void StunAI::Update(float dt, AIStamp& stamp)
{

	//stun animations
	stamp.owner->GetTimeStamp()->SwitchAnimation("stun");

	//timer
	stamp.fStartTimer += dt;

	//time up?
	if (stamp.fStartTimer >= 5.0f)
		stamp.SwitchAIMode(EnemyAIType::Pursuit);

	//set velocity
	stamp.owner->SetVelocity({ 0.f, 0.f });
}
