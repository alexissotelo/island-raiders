#pragma once
#include "Enemy.h"


class Boss : public Enemy
{
public:

	virtual bool   HandleCollision(const IEntity* other);
	virtual void   HandleEvent(const SGD::Event* pEvent);

	Boss(const char* = nullptr);
	virtual ~Boss();

};

