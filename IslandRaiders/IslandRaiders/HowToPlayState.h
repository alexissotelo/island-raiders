#pragma once
#include "IGameState.h"
#include <vector>
#include <string>
#include "../SGD Wrappers/SGD_AudioManager.h"
#include "../SGD Wrappers/SGD_GraphicsManager.h"
#include "../SGD Wrappers/SGD_InputManager.h"

class BitmapFont;
class Button;

class HowToPlayState : public IGameState
{
public:
	static HowToPlayState* GetInstance(void);
	static void DeleteInstance(void);

	virtual void Enter();
	virtual void Exit();
	virtual bool Update(float dt);
	virtual void Render(float dt);

private:
	std::vector<Button*> menuButtons;

	static HowToPlayState* pInstance;
	const BitmapFont* pFont = nullptr;

	SGD::GraphicsManager* pGraphics = nullptr;
	SGD::AudioManager* pAudio = nullptr;
	SGD::InputManager* pInput = nullptr;
	SGD::HTexture backGround = SGD::INVALID_HANDLE;

	HowToPlayState() = default;
	~HowToPlayState() = default;
	HowToPlayState(const HowToPlayState&) = delete;
	HowToPlayState operator=(const HowToPlayState&) = delete;
};

