#pragma once
#include "../SGD Wrappers/SGD_Message.h"
class Player;
class PlayerDownMessage : public SGD::Message
{
public:
	PlayerDownMessage(Player* _p);
	~PlayerDownMessage(void);

	Player* GetSender(void) const;
private:
	Player* player = nullptr;
};

