#include "Swordsman.h"
#include "../SGD Wrappers/SGD_InputManager.h"
#include "../SGD Wrappers/SGD_AudioManager.h"
#include "../SGD Wrappers/SGD_Event.h"
#include "GameInfo.h"
#include "PlayerSkill.h"
#include "AudioQueue.h"
#include "PlayGameState.h"
#include "EntityManager.h"



Swordsman::Swordsman(const char* name) : Player(name)
{
	aTimeStamp.m_sOwnerName = GetName();
	SetSkill(0, &(PlayerSkill::Basic_StunEnemy));

	//useles??
	playerType = PlayerType::swordsman;

	//load image
	playerHUD->LoadIcon("resources/graphics/icons/swordsman/basic.png", 0);
}

Swordsman::Swordsman(const char* name, unsigned int controllerNumber) : Player(name)
{
	//set animation owner
	aTimeStamp.m_sOwnerName = GetName();
	aTimeStamp.SwitchAnimation("idle");
	//set the controller number
	controllerIndex = controllerNumber;
	//set skill
	SetSkill(0, &(PlayerSkill::Basic_StunEnemy));
	//load image
	playerHUD->LoadIcon("resources/graphics/icons/swordsman/basic.png", 0);

}

Swordsman::~Swordsman()
{

}

void Swordsman::Update(float dt)
{
	//local input manager
	SGD::InputManager* pInput = SGD::InputManager::GetInstance();

#if CHEAT_SKILLS

	//set skill
	if (pInput->IsKeyPressed(SGD::Key::NumPad1))
		//set skill
	{
		SetSkill(1, &(PlayerSkill::Berserker));
		this->GetPlayerHUD()->LoadIcon("resources/graphics/icons/swordsman/fencer.png", 1);
	}

	//set skill
	if (pInput->IsKeyPressed(SGD::Key::NumPad2))
		//set skill
	{
		SetSkill(2, &(PlayerSkill::Admiral));
		this->GetPlayerHUD()->LoadIcon("resources/graphics/icons/swordsman/admiral.png", 2);
	}
#endif

	//movement
	Player::Update(dt);
}

bool Swordsman::HandleCollision(const IEntity* ptr)
{
	//convert to a character pointer
	const Character* other = dynamic_cast<const Character*>(ptr);

	if (!other)
		return Player::HandleCollision(ptr);


	const SGD::Rectangle attack = this->GetActiveRect();
	const SGD::Rectangle enemyCol = other->GetCollisionRect();

#pragma region Fencer
	if (aTimeStamp.m_sAction == "skill1")
	{
		//do no increment the frame
		if (attack.IsIntersecting(enemyCol))
		{
			AttackInfo atkinfo;
			atkinfo.damage = -stat.damage * 1.750f;
			//create a damage event
			SGD::Event dmg = { "DAMAGE", &atkinfo, this };
			//send it out
			dmg.SendEventNow(ptr);

			std::vector<IEntity*> allEnemies = (std::vector<IEntity*>)PlayGameState::GetInstance()->GetManager()->GetEntityVector(ENT_ENEMY);
			for each (IEntity* current in allEnemies)
			{
				Character* cur = dynamic_cast<Character*>(current);
				//within range?
				//if (cur == other)
				//	continue;
				//collision box for other
				const SGD::Rectangle collisionBox = cur->GetCollisionRect();

				//in contact?
				if (collisionBox.IsIntersecting(attack))
				{
					//damage event
					SGD::Event dmg = { "DAMAGE", &atkinfo, this };
					//send it out
					dmg.SendEventNow(cur);
				}
			}

			aTimeStamp.m_CurrentFrame++;
			aTimeStamp.m_fFrameTimer = 0.f;
			return false;
		}
	}
#pragma endregion

#pragma region Hammer

	else if (aTimeStamp.m_sAction == "hammer")
	{

		std::vector<IEntity*> allEnemies = PlayGameState::GetInstance()->GetManager()->GetEntityVector(ENT_ENEMY);
		//for each (IEntity* current in allEnemies)
		for (unsigned int i = 0; i < allEnemies.size(); ++i)
		{
			Character* cur = dynamic_cast<Character*>(allEnemies[i]);
			//within range?
			//collision box for other
			const SGD::Rectangle collisionBox = cur->GetCollisionRect();
			if (!cur)
				continue;

			if (collisionBox.IsIntersecting(attack))
			{
				Status* status = (Status*)cur->GetCharacterStatus();
				//turn on effect
				if (status->StartEffect(Status::STUN, 7.0f, -stat.damage))
				{
					//apply damage once
					AttackInfo atkinfo;
					atkinfo.damage = -stat.damage * 2.0f;
					atkinfo.isCriticalHit = false;

					//create a damage event
					SGD::Event dmg = { "DAMAGE", &atkinfo, this };
					//send it out
					dmg.SendEventNow(cur);
				}
			}
		}

		aTimeStamp.m_CurrentFrame++;
		aTimeStamp.m_fFrameTimer = 0.f;

		return true;
	}
#pragma endregion

#pragma region Basic & Berserker

	else if (aTimeStamp.m_sAction == "basic")
	{
		if (attack.IsIntersecting(enemyCol))
		{
			AttackInfo atkinfo;
			//skill 0 or 1 ?
			atkinfo.damage = stat.damage;
			//increase based on tier
			if (skillStamp[0].m_bStart) atkinfo.damage *= 1.5f;
			else if (skillStamp[1].m_bStart) atkinfo.damage *= 2.5f;
			//critical? = no
			atkinfo.isCriticalHit = false;
			//create a damage event
			SGD::Event dmg = { "DAMAGE", &atkinfo, this };
			//send it out
			dmg.SendEventNow(ptr);
			//create a damage event
			SGD::Event KO = { "KNOCKBACK", nullptr, this };
			//send it out
			KO.SendEventNow(ptr);
		}

		//next frame preeze
		aTimeStamp.m_CurrentFrame += 1;
		aTimeStamp.m_fFrameTimer = 0.0f;

		return skillStamp[1].m_bStart;
	}

	else if (aTimeStamp.m_sAction == "berserker")
	{

		AttackInfo atkinfo;
		atkinfo.damage = -stat.damage * 2.00f;
		//create a damage event
		SGD::Event dmg = { "DAMAGE", &atkinfo, this };
		//send it out
		dmg.SendEventNow(ptr);

		std::vector<IEntity*> allEnemies = PlayGameState::GetInstance()->GetManager()->GetEntityVector(ENT_ENEMY);
		//for each (IEntity* current in allEnemies)
		for (unsigned int i = 0; i < allEnemies.size(); ++i)
		{
			Character* cur = dynamic_cast<Character*>(allEnemies[i]);
			//within range?
			//collision box for other
			const SGD::Rectangle collisionBox = cur->GetCollisionRect();
			if (!cur)
				continue;

			//in contact?
			if (collisionBox.IsIntersecting(attack))
			{
				//damage event
				SGD::Event dmg = { "DAMAGE", &atkinfo, this };
				//send it out
				dmg.SendEventNow(cur);
				//create a damage event
				SGD::Event KO = { "KNOCKBACK", nullptr, this };
				//send it out
				KO.SendEventNow(cur);
			}
		}

		aTimeStamp.m_CurrentFrame++;
		aTimeStamp.m_fFrameTimer = 0.f;
		return false;
	}
#pragma endregion

#pragma region Admiral and SeaKnight

	else if (aTimeStamp.m_sAction == "admiral")
	{
		//do no increment the frame
		if (attack.IsIntersecting(enemyCol))
		{
			AttackInfo atkinfo;
			atkinfo.damage = -stat.damage * 2.0f;
			//create a damage event
			SGD::Event dmg = { "DAMAGE", &atkinfo, this };
			//send it out
			dmg.SendEventNow(ptr);

			std::vector<IEntity*> allEnemies = (std::vector<IEntity*>)PlayGameState::GetInstance()->GetManager()->GetEntityVector(ENT_ENEMY);
			for each (IEntity* current in allEnemies)
			{
				Character* cur = dynamic_cast<Character*>(current);
				const SGD::Rectangle collisionBox = cur->GetCollisionRect();

				//in contact?
				if (collisionBox.IsIntersecting(attack))
				{
					//damage event
					SGD::Event dmg = { "DAMAGE", &atkinfo, this };
					//send it out
					dmg.SendEventNow(cur);
				}
			}

			aTimeStamp.m_CurrentFrame++;
			aTimeStamp.m_fFrameTimer = 0.f;
			return false;
		}

		return false;
	}
	else if (aTimeStamp.m_sAction == "seaknight")
	{
		AttackInfo atkinfo;
		atkinfo.damage = -stat.damage * 2.0f;
		atkinfo.isCriticalHit = true;

		//create a damage event
		SGD::Event dmg = { "DAMAGE", &atkinfo, this };
		//send it out
		dmg.SendEventNow(ptr);

		aTimeStamp.m_CurrentFrame++;
		aTimeStamp.m_fFrameTimer = 0.f;

		return false;
	}
#pragma endregion

	else return Player::HandleCollision(ptr);

	return false;
}

void  Swordsman::HandleEvent(const SGD::Event* pEvent)
{

	if (pEvent->GetEventID() == "DAMAGE")
	{
		if (CaptainOn || aTimeStamp.m_sAction == "skill1" || aTimeStamp.m_sAction == "admiral"
			|| aTimeStamp.m_sAction == "hammer" || aTimeStamp.m_sAction == "basic"
			|| aTimeStamp.m_sAction == "seaknight")
			return;
		else
		{
			Player::HandleEvent(pEvent);
			currCombo = NONE;
			return;
		}
	}

	Player::HandleEvent(pEvent);

	if (pEvent->GetEventID() == "MOVE")
	{
		//move 
		SGD::Vector forwards = SGD::Vector(1, 0);
		//directions
		if (!aTimeStamp.bForwards)
			forwards.x *= -1;

		//scale
		forwards *= 400;

		//set velocity
		SetVelocity(forwards);
	}

	if (pEvent->GetEventID() == "STOP")
	{
		SetVelocity({ 0, 0 });
	}


}

bool Swordsman::UpdateCombo(float dt)
{
	//combo list
	//	X = N		Y = M
	//update the queue
	if (inputQueue.empty())
	{
		//timer run out?
		if (inputQueue.m_fUpdate > 0.20f)
		{
			//animation done?
			if (aTimeStamp.bLoop == false && aTimeStamp.isDone == false)
				return true;

			//reset combo pos
			currCombo = COMBO_LIST::NONE;
			//reset the input
			inputQueue.m_fUpdate = 0;
		}
		//fail
		return false;
	}
	else
	{
		//animation done?
		if (aTimeStamp.bLoop == false && aTimeStamp.isDone == false)
			return true;

		//key at the front
		SGD::Key topKey = inputQueue.front();
		//remove the top keye
		inputQueue.pop();

		//quick atk
		if (topKey == SGD::Key::J)
			stat.bQuickAttack = true;
		else if (SGD::Key::K == topKey)
			stat.bQuickAttack = false;

		//reset for the next key input
		inputQueue.m_fUpdate = 0;

		//update the state
		switch (currCombo)
		{
		case Player::NONE:
			if (topKey == SGD::Key::J)
				currCombo = COMBO_LIST::X;
			else if (SGD::Key::K == topKey)
				currCombo = COMBO_LIST::Y;
			break;
		case Player::X:
			if (topKey == SGD::Key::J)
				currCombo = COMBO_LIST::XX;
			else if (SGD::Key::K == topKey)
			{
				currCombo = COMBO_LIST::XY;
				inputQueue.clear();
			}
			break;
		case Player::XX:
			if (topKey == SGD::Key::J)
				currCombo = COMBO_LIST::XXX;
			else if (SGD::Key::K == topKey)
				currCombo = COMBO_LIST::XXY;
			inputQueue.clear();
			break;
		case Player::Y:
			if (topKey == SGD::Key::K)
			{
				stat.projectileSpeed = 0.0f;
				currCombo = COMBO_LIST::YY;
			}
			else
				//reset combo pos
				currCombo = COMBO_LIST::NONE;
			inputQueue.clear();
			break;
		default:
			currCombo = COMBO_LIST::NONE;
			inputQueue.clear();
		}
	}	//chain = 0;
	//change the animation to fit the combo
	switch (currCombo)
	{
	case Player::X:
		aTimeStamp.SwitchAnimation("attack_x");
		break;
	case Player::Y:
		aTimeStamp.SwitchAnimation("attack_y");
		break;
	case Player::XX:
		aTimeStamp.SwitchAnimation("attack_x_x");
		break;
	case Player::XY:
		aTimeStamp.SwitchAnimation("attack_x_y");
		break;
	case Player::YY:
		aTimeStamp.SwitchAnimation("attack_y_y");
		break;
	case Player::XXX:
		aTimeStamp.SwitchAnimation("attack_x_x_x");
		//chain = 3;
		break;
	case Player::XXY:
		aTimeStamp.SwitchAnimation("attack_x_x_y");
		break;
	default:
		break;
	}
	//no moving
	SetVelocity({ 0, 0 });
	//play a sound
	AudioQueue::GetInstance()->AddSound(SGD::AudioManager::GetInstance()->LoadAudio(L"resources/audio/sound effects/swordsmanAttack.wav"));

	//success!
	return true;
}

//----------------SKILLLS-----------------------//
void Swordsman::SetCaptainOn(bool _on)
{
	CaptainOn = _on;
}
