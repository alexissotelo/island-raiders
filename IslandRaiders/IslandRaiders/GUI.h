#pragma once
#include "..\SGD Wrappers\SGD_Geometry.h"
#include "GUISkin.h"
#include <string>
#include <vector>
//#include <list>

class BitmapFont;
class PopUp;
enum PopUpEffect
{
	UIFX_FADE, UIFX_FLASH, UIFX_BLINK, UIFX_DIALOG
};

class GUIManager
{
	// Singleton Object:
	static GUIManager*	s_pInstance;
	GUIManager(void)	= default;		// default constructor
	~GUIManager(void)	= default;		// destructor

	GUIManager(const GUIManager&) = delete;	// copy constructor
	GUIManager& operator= (const GUIManager&) = delete;	// assignment operator
	
	//
	std::vector<BitmapFont*> fonts;
	std::vector<GUISkin*> skins;

	//
	std::vector<PopUp*> popups;

public:
	static GUIManager*	GetInstance(void);
	static void		DeleteInstance(void);
	static void Initialize();
	void Terminate();

	void AddSkin(GUISkin *skin){ skins.push_back(skin); }
	//void SwitchSkin(int index){ currSkin = skins[index]; }
	
	//pop up messages
	void PopUpFade(std::string _text, SGD::Point _pos, float scale, float time,
		SGD::Color startColor = {});
	void PopUpFlash(std::string _text, SGD::Point _pos, float scale, float time,
		SGD::Color aColor, SGD::Color bColor, SGD::Vector offsetVector);
	void PopUpBlinker(std::string _text, SGD::Point _pos, float scale, float blinkFreq,
		SGD::Color aColor, SGD::Color bColor = {});
	void PopUpDialog(std::string _text, SGD::Point _pos, float scale, float letterTime,
		SGD::Color color = {});

	void ClearPopUpMessages();
	
	void Update(float dt);

	//const GUISkin*		GetSkin(void) const	{ return currSkin; }
	const GUISkin*		GetSkin(int index) const	{ return skins[index]; }
	const BitmapFont*	GetFont(void) const	{ return fonts[0]; }
};

class Label
{
	friend class GUISkin;
public:
	Label(std::string _text, float fontScale = 1.f);
	Label(std::string _text, SGD::Point _pos, float fontScale = 1.f);
	Label(std::string _text, SGD::Rectangle _rect, float fontScale = 1.f);

	void SetMiddleRect(float posY);

	virtual bool Update(float dt);
	virtual ~Label() = default;
	virtual void Draw();

	TextAlignment alignment = MiddleLeft;
protected:
	bool isHover = false;

	SGD::Rectangle rect;
	std::string text;

	SGD::Size fontSize;
	int textLength;
	float scale;

	const GUISkin *skin;
};

class PopUp : public Label
{
	friend class GUISkin;
private:
	float timer = 0;
	float yieldTime = 0.5f;
	float fadeTime;
	SGD::Color startColor;
	SGD::Color endColor;
	SGD::Color currColor;

	SGD::Point startPosition;
	SGD::Point endPosition;
	SGD::Point currPosition;

	unsigned int effect;//type
	int effectCounter = 0;

	SGD::Vector utilVector;
	std::string currText;

public:
	PopUp(const GUISkin* _skin, std::string _text, SGD::Point _pos, float fontScale, float time, 
		SGD::Color startColor, SGD::Color endColor, SGD::Vector utilVector, unsigned int fx);
	~PopUp() = default;

	bool Update(float dt);
	virtual void Draw();
};

class Button : public Label
{
	friend class ButtonGroup;

public:
	Button(std::string text, float fontScale);
	Button(std::string _text, SGD::Point _pos, float fontScale = 1.f);
	Button(std::string _text, SGD::Rectangle _rect, float fontScale = 1.f);

	~Button() = default;
	bool OnClick(float dt);
};



