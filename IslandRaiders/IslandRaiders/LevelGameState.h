#pragma once
#include "IGameState.h"
#include <string>
#include "../SGD Wrappers/SGD_AudioManager.h"
#include "../SGD Wrappers/SGD_GraphicsManager.h"
#include "../SGD Wrappers/SGD_InputManager.h"

class BitmapFont;
#define UNLOCK_ALL_LEVELS 0

class LevelGameState : public IGameState
{

public:
	static LevelGameState* GetInstance(void);
	static void DeleteInstance(void);

	virtual void Enter();
	virtual void Exit();
	virtual bool Update(float dt);
	virtual void Render(float dt);

	int GetLevelPicked() const 
	{ 
		return lvlPicked;
	}
	void ArcadeCursorMovement(float dt);

private:
	float timer = 0.0f;
	float timerFreq = 0.4f;
	SGD::Point waypoints[6];
	SGD::Point cursorPoints[6];
	SGD::Point position;
	unsigned int currPoint = 0;
	bool reachWaypoint = false;
	bool startMovement = false;
	void MoveBird(float dt);

	const BitmapFont* pFont = nullptr;
	
	SGD::GraphicsManager* pGraphics = nullptr;
	SGD::AudioManager* pAudio = nullptr;
	SGD::InputManager* pInput = nullptr;
	SGD::HTexture backGround = SGD::INVALID_HANDLE;
	SGD::HTexture bird = SGD::INVALID_HANDLE;
	int cursorIndex;
	bool confirm;

	LevelGameState() = default;
	~LevelGameState() = default;
	LevelGameState(const LevelGameState&) = delete;
	LevelGameState operator=(const LevelGameState&) = delete;
	SGD::Point lerp(float t, SGD::Point a, SGD::Point b);
	static LevelGameState* pInstance;

	int startingPoint;

	//the lvl the player wants to pick
	int lvlPicked;
	// the highest lvl the player can pick
	int maxLvlCanPick = 0;
	int noPlayLvl ;
	SGD::HTexture shipIcon = SGD::INVALID_HANDLE;
	SGD::HTexture grayShipIcon = SGD::INVALID_HANDLE;
	SGD::HTexture CursorIcon = SGD::INVALID_HANDLE;
	bool changeState = false;
	int cursor = 0;
};