#pragma once
#include "LevelSection.h"
#include <map>
#include <string>
#include <vector>
#include "..\SGD Wrappers\SGD_Geometry.h"
#include "../SGD Wrappers/SGD_Handle.h"
#include "../SGD Wrappers/SGD_GraphicsManager.h"


//typedef std::pair<std::string, Enemy*> eKey;
class EntityManager;
class Enemy;

struct EntityData
{
	SGD::HTexture image = SGD::INVALID_HANDLE;
	SGD::Rectangle renderRect;
};
struct EnemyData
{
	EnemyData();
	~EnemyData();

	std::string name;

	float damage[3];
	float health[3];
	float speed;
	char* skills[3];
	char* attackSoundFile = nullptr;
	int tier;
	int droprate;
	float fireRate;
	float cooldownTime[3];
	SGD::Size size;

	CombatType combatType;
};

class LevelManager
{
private:
	static std::vector<EnemyData*>				enemyCollection;
	static std::map<std::string, EntityData>	entityCollection;
	static std::map<std::string, SGD::HTexture>	imageCollection;
	static EntityManager *entityManager;

	std::vector<std::string> vImageFilePath;

	int partyLives = 0;
	int enemiesLeft = 0;
	std::vector<LevelSection*> levelSections;

	//SINGLETON
	static LevelManager* pInstance;
	LevelManager() = default;
	~LevelManager() = default;
	LevelManager(const LevelManager&) = delete;
	LevelManager operator=(const LevelManager&) = delete;
public:
	static LevelManager* GetInstance(void);
	static void DeleteInstance(void);

	void Initialize(EntityManager * entManager);
	void Terminate(void);
	static void LoadEnemies(std::string  filename);
	static void LoadEntities(std::string filename);
	void LoadLevel(std::string filename);

	static void CreateEnemy(std::string enemyName, SGD::Point position);

	//accessors
	int GetPartyLives() const	{ return partyLives; }
	int GetEnemiesLeft() const	{ return enemiesLeft; }
	//SGD::HTexture GetLevelBackground() const { return bgImage; }

	//mutattors
	void SetPartyLives(int pl)	{ partyLives = pl; }
	void SetEnemiesLeft(int el) { enemiesLeft = el; }

	int GetNumberOfSections(){ return levelSections.size(); }
	LevelSection* GetLevelSection(int index) { return levelSections[index]; }
};

