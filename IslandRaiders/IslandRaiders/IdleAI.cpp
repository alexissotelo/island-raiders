#include "IdleAI.h"
#include "Enemy.h"
#include "Familiar.h"
#include "AnimationTimestamp.h"

/**************************************************************
|	Function:
|	Purpose:
|	Parameter:
|	Return Value:
**************************************************************/
void IdleAI::Update(float dt, AIStamp& _timeStamp)
{
	//no moving allowed
	_timeStamp.owner->SetVelocity({ 0, 0 });

	if (_timeStamp.combatType == CombatType::Familiar)
	{
		FamiliarIdle(dt, _timeStamp);
		return;
	}

	//wait based on the enemy's tier
	switch (_timeStamp.tier)
	{
	case 0:
		TierZeroWait(dt, _timeStamp);
		break;
	case 1:
		TierOneWait(dt, _timeStamp);
		break;
	case 2:
		TierTwoWait(dt, _timeStamp);
		break;
	case 3:
		BossWait(dt, _timeStamp);
		break;
	default:
		break;
	}

}

/**************************************************************
|	Function:
|	Purpose:
|	Parameter:
|	Return Value:
**************************************************************/
void IdleAI::TierZeroWait(float dt, AIStamp& stamp)
{
	//If an enemy were to find their prey, then switch their 
	//status to TARGET.Tier 0 will target based on position.

	//wait in idle
	stamp.owner->GetTimeStamp()->SwitchAnimation("idle");
	//after 3 seconds of waititng search for a player]
	stamp.fStartTimer += dt;
	if (stamp.fStartTimer >= 0.50f)
	{
		//search
		if (AIManager::GetInstance()->FindClosestTarget(stamp))
		{
			//once a target is found switch states
			stamp.SwitchAIMode(EnemyAIType::Pursuit);
		}
	}


}

/**************************************************************
|	Function:
|	Purpose:
|	Parameter:
|	Return Value:
**************************************************************/
void IdleAI::TierOneWait(float dt, AIStamp& stamp)
{
	//wait in idle
	stamp.owner->GetTimeStamp()->SwitchAnimation("idle");
	//after 3 seconds of waititng search for a player]
	stamp.fStartTimer += dt;
	if (stamp.fStartTimer >= 0.75f)
	{
		//search
		if (AIManager::FindWeakestTarget(stamp))
			//once a target is found switch states
			stamp.SwitchAIMode(EnemyAIType::Pursuit);
		else if (AIManager::FindClosestTarget(stamp))
			//once a target is found switch states
			stamp.SwitchAIMode(EnemyAIType::Pursuit);
	}

}

/**************************************************************
|	Function:
|	Purpose:
|	Parameter:
|	Return Value:
**************************************************************/
void IdleAI::TierTwoWait(float dt, AIStamp& stamp)
{
	if (stamp.owner->GetTimeStamp()->isDone)
	//wait in idle
	stamp.owner->GetTimeStamp()->SwitchAnimation("idle");

	//after  some tiem of waititng search for a player
	stamp.fStartTimer += dt;

	//search for player based on priorities
	if (stamp.fStartTimer >= 1.f)
	{
		//search
		if (AIManager::FindPreferredTarget(stamp))
			//once a target is found switch states
			stamp.SwitchAIMode(EnemyAIType::Pursuit);
		else if (AIManager::FindWeakestTarget(stamp))
			//once a target is found switch states
			stamp.SwitchAIMode(EnemyAIType::Pursuit);
		else if (AIManager::FindClosestTarget(stamp))
			//once a target is found switch states
			stamp.SwitchAIMode(EnemyAIType::Pursuit);
	}
}

/**************************************************************
|	Function:
|	Purpose:
|	Parameter:
|	Return Value:
**************************************************************/
void IdleAI::BossWait(float dt, AIStamp& stamp)
{
	//wait in idle
	stamp.owner->GetTimeStamp()->SwitchAnimation("idle");
	//after  some tiem of waititng search for a player
	stamp.fStartTimer += dt;

	//search for player based on priorities
	if (stamp.fStartTimer >= 2.5f)
	{
		//search
		/*if (AIManager::FindPreferredTarget(stamp))
			//once a target is found switch states
			stamp.SwitchAIMode(EnemyAIType::Chaser);
			else*/

		if (AIManager::FindWeakestTarget(stamp))
			//once a target is found switch states
			stamp.SwitchAIMode(EnemyAIType::Pursuit);
		else if (AIManager::FindClosestTarget(stamp))
			//once a target is found switch states
			stamp.SwitchAIMode(EnemyAIType::Pursuit);
	}


}

void IdleAI::FamiliarIdle(float dt, AIStamp& stamp)
{

	//after 3 seconds of waititng search for a player]
	stamp.fStartTimer += dt;
	if (stamp.fStartTimer >= 0.75f)
	{
		//search
		if (stamp.target || AIManager::FindWeakestTarget(stamp) || AIManager::FindClosestTarget(stamp))
		{
			stamp.owner->GetTimeStamp()->SwitchAnimation("transform");

			if (stamp.owner->GetTimeStamp()->isDone)
			//once a target is found switch states
			stamp.SwitchAIMode(EnemyAIType::Pursuit);
		}
		else
		{
			//run to the owners position
			Familiar* pet = (Familiar*)stamp.owner;

			//vector to the owner of the familiar
			SGD::Vector toMaster = pet->GetOwner()->GetPosition() - pet->GetPosition();

			//distance
			float distance = toMaster.ComputeLength();

			if (distance > 50.0f)
			{
				//normalize
				toMaster.ComputeNormalized();

				//scale
				toMaster *-pet->GetMoveSpeed();

				//move
				pet->SetVelocity(toMaster);

				//animations
				pet->GetTimeStamp()->SwitchAnimation("follow");

				//direction
				if (toMaster.x > 0)
					pet->GetTimeStamp()->bForwards = true;
				else if (toMaster.x < 0)
					pet->GetTimeStamp()->bForwards = false;
				return;
			}
		}
	}

	if (stamp.owner->GetTimeStamp()->m_sAction != "transform")
		stamp.owner->GetTimeStamp()->SwitchAnimation("idle");

}
