#pragma once
#include "Character.h"
#include "Animation.h"
#include "../SGD Wrappers/SGD_InputManager.h"
#include "../SGD Wrappers/SGD_Key.h"
#include "PlayerHUD.h"
#include "PlayerSkillStamp.h"
#include <queue>

#define CHEAT_SKILLS 0
//forward declarations
class InputQueue;
class ParticleEmitter;

enum class PlayerType
{
	swordsman, gunner, alchemist, none
};

class InputQueue
{
	std::queue<SGD::Key> m_qInput;

public:
	//ctor and dtor
	InputQueue() = default;
	~InputQueue(void) = default;
	//push a key
	void AddKey(SGD::Key key);
	//clear the queue
	void clear(void);
	//accessor for the front key only
	SGD::Key front(void) const;
	//remove the front key only
	void pop(void);
	//empty check
	bool empty(void) const;
	//size
	unsigned int size(void) const;
	//memeber for updating
	float m_fUpdate = 0;


};


class Player : public Character
{
public:
	void RenderHUD();

	//COMBAT FUNCTIONS 
	virtual void Attack(const Character *other) override;

	virtual void   Update(float dt) override;
	virtual void   Render() override;
	virtual bool   HandleCollision(const IEntity* other) override;
	virtual unsigned int  GetType() const override { return EntityType::ENT_PLAYER; }

	virtual void Input(float elapsedTime);
	virtual bool UpdateCombo(float elapsedTime);

	//CTOR & DTOR
	Player(const char*, int index = -1);
	virtual ~Player();

	//accessor
	virtual	unsigned int	GetPlayerType()					const = 0;
	//float					GetAttackBonus(void)			const			{ return attackBonus; }
	unsigned int			GetPlayerLevel(void)			const			{ return playerLevel; }
	std::string				GetClassName(void)				const			{ return className; }
	int						GetComboChain()					const			{ return chain; }
	PlayerHUD*				GetPlayerHUD(void)				const			{ return playerHUD; }
	int						GetControllerNumber(void)		const			{ return controllerIndex; }
	int						GetWorldLevel(void)				const			{ return worldUnlocked; }
	//mutators
	//void SetAttackBonus(float f)			{ attackBonus = f; }
	void SetControllerNumber(int num)		{ controllerIndex = num; }
	void SetPlayerLevel(unsigned int i)		{ playerLevel = i; }
	void SetClassName(std::string s)		{ className = s; }
	void SetWorldLevel(int lvl)				{ worldUnlocked = lvl; }
	//events
	virtual void HandleEvent(const SGD::Event* pEvent);

	//player dying
	void Reset(SGD::Point _respawn);

	//skills
	PlayerSkillStamp* GetSkillStamp(void) { return &skillStamp; }
	void SetSkill(unsigned int skill, bool(*functionPtr)(float, Player*, bool));

	void IncreaseKillCount(){ ++killCount; }
protected:
	//sound effect for killing spree
	SGD::HAudio spreeSound[3];
	//controller
	int controllerIndex;


	//enum of combo keys
	enum COMBO_LIST{ NONE, X, Y, XX, XY, YY, YX, XXX, XXY, YXX, YXY, YYX };

	//skills
	bool(*skillBasic)(float, Player*, bool) = nullptr;
	bool(*skillOne)(float, Player*, bool) = nullptr;
	bool(*skillTwo)(float, Player*, bool) = nullptr;
	std::string className;

	//enum object
	InputQueue inputQueue;
	PlayerType playerType;
	PlayerSkillStamp skillStamp;
	COMBO_LIST currCombo = COMBO_LIST::NONE;

	//player stuff
	PlayerHUD* playerHUD;
	SGD::Rectangle OffSetRect(void);

	unsigned int playerLevel = 1;

	//reset the player
	//make in invisible for 3 seconds
	//have him blinking
	//dead bool, invinsible bool
	float fResetTimer = 0.0f;
	float fBlickChange = 0.f;
	bool bDead = false;
	bool bBlink = false;

	//countering
	float fCounter = 0.0f;

	//skill bits
	int killCount = 0;
	float killTimer = 0;
	float killDuration = 0.1f;
	int chain = 0;
	unsigned int critChance = 10;//chance to make a critical hit

	//particle fx
	ParticleEmitter *blood;

	//current world unlocked
	int worldUnlocked = -1;
};
