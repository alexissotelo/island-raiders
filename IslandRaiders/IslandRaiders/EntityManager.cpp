/***************************************************************
|	File:		EntityManager.cpp
|	Author:		David Mejia
|	Course:		SGD
|	Purpose:	EntityManager class stores & maintains all game entities
***************************************************************/

#include "EntityManager.h"
#include "IEntity.h"
#include "Entity.h"
#include "Character.h"
#include "Missile.h"
#include <cassert>
#include <queue>

#include "../Source/Game.h"

//structure necessary for comparing entites (ZSorting)
struct compare
{
	bool operator()(const IEntity* ptrOne, const IEntity* ptrTwo)
	{
		const Entity* ptOne = dynamic_cast<const Entity*>(ptrOne);
		const Entity* ptTwo = dynamic_cast<const Entity*>(ptrTwo);

		return ptOne->GetPosition().y > ptTwo->GetPosition().y;
	}
};

/**************************************************************/
// AddEntity
//	- store the entity into the specified bucket
//	- the Entity Manager holds a reference to the entity
void EntityManager::AddEntity(IEntity* pEntity, unsigned int bucket)
{
	// Validate the parameter
	assert(pEntity != nullptr && "EntityManager::AddEntity - parameter cannot be null");

	if (m_tEntities.size() == 0)
		m_tEntities.resize(EntityType::ENT_COUNT);

	// Expand the table?
	if (bucket >= m_tEntities.size())
		m_tEntities.resize(bucket + 1);


	// Append the entity into the specified vector
	m_tEntities[bucket].push_back(pEntity);

	// Hold a reference to keep the entity in memory
	pEntity->AddRef();
}


/**************************************************************/
// RemoveEntity
//	- remove the entity from the specified bucket
//	- release the reference to the entity
void EntityManager::RemoveEntity(IEntity* pEntity, unsigned int bucket)
{
	// Validate the iteration state
	assert(m_bIterating == false && "EntityManager::RemoveEntity - cannot remove while iterating");

	// Validate the parameters
	assert(pEntity != nullptr && "EntityManager::RemoveEntity - cannot remove NULL");
	assert(bucket < m_tEntities.size() && "EntityManager::RemoveEntity - invalid bucket");


	// Try to find the entity
	EntityVector& vec = m_tEntities[bucket];
	for (unsigned int i = 0; i < vec.size(); i++)
	{
		if (vec[i] == pEntity)
		{
			// Remove the entity
			vec.erase(vec.begin() + i);
			pEntity->Release();
			pEntity = nullptr;
			break;
		}
	}

}


/**************************************************************/
// RemoveEntity
//	- remove & release the entity from any bucket
void EntityManager::RemoveEntity(IEntity* pEntity)
{
	// Validate the iteration state
	assert(m_bIterating == false && "EntityManager::RemoveEntity - cannot remove while iterating");

	// Validate the parameters
	assert(pEntity != nullptr && "EntityManager::RemoveEntity - pointer cannot be null");


	// Try to find the entity in any buckect
	for (unsigned int bucket = 0; bucket < m_tEntities.size(); bucket++)
	{
		EntityVector& vec = m_tEntities[bucket];
		for (unsigned int i = 0; i < vec.size(); i++)
		{
			if (vec[i] == pEntity)
			{
				// Remove the entity
				vec.erase(vec.begin() + i);
				pEntity->Release();
				pEntity = nullptr;
				return;
			}
		}
	}
}


/**************************************************************/
// RemoveAll
//	- remove all entities from a specific bucket
void EntityManager::RemoveAll(unsigned int unBucket)
{
	// Validate the iteration state
	assert(m_bIterating == false && "EntityManager::RemoveAll - cannot remove while iterating");

	// Validate the parameter
	assert(unBucket < m_tEntities.size() && "EntityManager::RemoveAll - invalid bucket");


	// Lock the iterator
	m_bIterating = true;
	{
		// Release the reference to EVERY entity
		EntityVector& vec = m_tEntities[unBucket];
		for (unsigned int i = 0; i < vec.size(); i++)
		{
			vec[i]->Release();
			vec[i] = nullptr;
		}

		vec.clear();
	}
	// Unlock the iterator
	m_bIterating = false;
}



/**************************************************************/
// RemoveAll
//	- release each entity in the table
void EntityManager::RemoveAll(void)
{

	// Validate the iteration state
	assert(m_bIterating == false && "EntityManager::RemoveAll - cannot remove while iterating");

	// Lock the iterator
	m_bIterating = true;
	{
		// Release every entity
		for (unsigned int bucket = 0; bucket < m_tEntities.size(); bucket++)
		{

			EntityVector& vec = m_tEntities[bucket];
			for (unsigned int i = 0; i < vec.size(); i++)
			{
				vec[i]->Release();
				vec[i] = nullptr;
			}

		}
	}

	// Unlock the iterator
	m_bIterating = false;


	// Collapse the table
	//m_tEntities.clear();
}


/**************************************************************/
// UpdateAll
//	- update each entity in the table
void EntityManager::UpdateAll(float elapsedTime)
{
	// Validate the iteration state
	assert(m_bIterating == false && "EntityManager::UpdateAll - cannot update while iterating");

	// Lock the iterator
	m_bIterating = true;
	{
		// Update every entity
		for (unsigned int bucket = 0; bucket < m_tEntities.size(); bucket++)
		{
			for (unsigned int i = 0; i < m_tEntities[bucket].size(); i++)
				m_tEntities[bucket][i]->Update(elapsedTime);
		}
	}
	// Unlock the iterator
	m_bIterating = false;
}


/**************************************************************/
// RenderAll
//	- render each entity in the table
void EntityManager::RenderAll(void)
{
	// Validate the iteration state
	assert(m_bIterating == false && "EntityManager::RenderAll - cannot render while iterating");

	//std::priority_queue<IEntity*, std::vector<IEntity*>, IEntity::compare> renderQ;

	//vector for all entities
	std::vector<IEntity*> Objects;

#if 0
	//store all entities into the bucket
	for (unsigned int bucket = 0; bucket < m_tEntities.size(); ++bucket)
	{
		//local entity vector
		EntityVector& vec = m_tEntities[bucket];
		//push all into the queue
		for (unsigned int i = 0; i < m_tEntities[bucket].size(); ++i)
		{
			Entity* ptr = dynamic_cast<Entity*>(m_tEntities[bucket][i]);

			if (ptr)
				Objects.push_back(ptr);
		}
	}
#endif
	//store all entities into the bucket
	for (unsigned int bucket = 0; bucket < m_tEntities.size(); ++bucket)
	{
		if ((bucket == ENT_INVULNERABLE || bucket == ENT_BASE) && !Game::GetInstance()->bDebug)
			continue;
		//push all into the queue
		IEntity *ent;
		for (unsigned int i = 0; i < m_tEntities[bucket].size(); ++i)
		{
			ent = m_tEntities[bucket][i];
			Objects.push_back(ent);
		}
	}

	//store all entities into a priority queue
	std::priority_queue<IEntity*, std::vector<IEntity*>, compare> renderQ;

	//put everything into the queue
	for (unsigned int i = 0; i < Objects.size(); ++i)
		renderQ.push(Objects[i]);

	Objects.clear();

	//render
	while (renderQ.size() > 0)
	{
		//the iEntity at the top
		//Entity* ptr = 
		renderQ.top()->Render();
		//render the object
		//ptr->Render();
		//remove the top
		renderQ.pop();
	}
}


/**************************************************************/
// CheckCollisions
//	- check collision between the entities within the two buckets
void EntityManager::CheckCollisions(unsigned int bucket1, unsigned int bucket2)
{
	// Validate the iteration state
	assert(m_bIterating == false && "EntityManager::CheckCollisions - cannot collide while iterating");

	// Quietly validate the parameters
	if (bucket1 >= m_tEntities.size()
		|| bucket2 >= m_tEntities.size()
		|| m_tEntities[bucket1].size() == 0
		|| m_tEntities[bucket2].size() == 0)
		return;


	// Lock the iterator
	m_bIterating = true;
	{
		// Are they different buckets?
		if (bucket1 != bucket2)
		{
			// Which bucket is smaller?
			//	should be the outer loop for less checks (n0)*(n1+1) + 1
			EntityVector* pVec1 = &m_tEntities[bucket1];
			EntityVector* pVec2 = &m_tEntities[bucket2];

			//swap the vectors
			if (pVec2->size() < pVec1->size())
			{
				EntityVector* pTemp = pVec1;
				pVec1 = pVec2;
				pVec2 = pTemp;
			}

			//vector references
			EntityVector& vec1 = *pVec1;
			EntityVector& vec2 = *pVec2;


			// Iterate through the smaller bucket
			for (unsigned int i = 0; i < vec1.size(); i++)
			{
				// Iterate through the larger bucket
				for (unsigned int j = 0; j < vec2.size(); j++)
				{
					// Ignore self-collision
					if (vec1[i] == vec2[j])
						continue;

					// Local variables help with debugging
					SGD::Rectangle rEntity1 = vec1[i]->GetRect();
					SGD::Rectangle rEntity2 = vec2[j]->GetRect();

					// Check for collision between the entities
					if (rEntity1.IsIntersecting(rEntity2) == true)
					{
						// Both objects handle collision
						vec1[i]->HandleCollision(vec2[j]);
						vec2[j]->HandleCollision(vec1[i]);
					}
				}
			}
		}
		else // bucket1 == bucket2
		{
			EntityVector& vec = m_tEntities[bucket1];

			// Optimized loop to ensure objects do not collide with
			// each other twice
			for (unsigned int i = 0; i < vec.size() - 1; i++)
			{
				for (unsigned int j = i + 1; j < vec.size(); j++)
				{
					// Ignore self-collision
					if (vec[i] == vec[j])
						continue;

					// Local variables help with debugging
					SGD::Rectangle rEntity1 = vec[i]->GetRect();
					SGD::Rectangle rEntity2 = vec[j]->GetRect();

					// Check for collision between the entities
					if (rEntity1.IsIntersecting(rEntity2) == true)
					{
						// Both objects handle collision
						vec[i]->HandleCollision(vec[j]);
						vec[j]->HandleCollision(vec[i]);
					}
				}
			}
		}
	}
	// Unlock the iterator
	m_bIterating = false;
}
void EntityManager::CheckActiveCollision(unsigned int _attacking, unsigned int _colliding)
{
	//cycle through all buckets
	EntityVector& tableOne = m_tEntities[_attacking]; //bucket 1
	EntityVector& tableTwo = m_tEntities[_colliding]; //bucket 2

	//for each item in table 1
	for (unsigned int index1 = 0; index1 < tableOne.size(); ++index1)
	{
		//character1
		Entity* characterOne = dynamic_cast<Entity*>(tableOne[index1]);
		if (!characterOne) break;
		//for each item in table 2
		for (unsigned int index2 = 0; index2 < tableTwo.size(); ++index2)
		{
			//character2
			Entity* characterTwo = dynamic_cast<Entity*>(tableTwo[index2]);
			if (!characterTwo)
				break;

			if (characterOne == characterTwo)
				continue;

			//character 1 active and collision
			const SGD::Rectangle active1 = characterOne->GetActiveRect();
			const SGD::Rectangle collision1 = characterOne->GetCollisionRect();
			//character 2 active and collision
			const SGD::Rectangle active2 = characterTwo->GetActiveRect();
			const SGD::Rectangle collision2 = characterTwo->GetCollisionRect();

			if (active1.IsIntersecting(collision2) || active2.IsIntersecting(collision1))
			{
				bool two = characterTwo->HandleCollision(characterOne);
				bool one = characterOne->HandleCollision(characterTwo);

				if (one || two)  continue; else break;
			}
		}
	}
}

void EntityManager::CheckProjectileCollision(unsigned int _projectile, unsigned int _entity)
{
	//MUST BE A PROJECTILE
	if (_projectile != (unsigned int)EntityType::ENT_MISSILE)
		return;

	//cycle through all buckets
	EntityVector& tableOne = m_tEntities[_projectile]; //bucket 1
	EntityVector& tableTwo = m_tEntities[_entity]; //bucket 2

	//for each item in table 1
	for (unsigned int index1 = 0; index1 < tableOne.size(); ++index1)
	{
		//character1
		Missile* characterOne = dynamic_cast<Missile*>(tableOne[index1]);
		if (!characterOne) break;
		//for each item in table 2
		for (unsigned int index2 = 0; index2 < tableTwo.size(); ++index2)
		{
			//character2
			Character* characterTwo = dynamic_cast<Character*>(tableTwo[index2]);
			if (!characterTwo)
				break;

			//character 1 active and collision
			const SGD::Rectangle active1 = characterOne->GetActiveRect();
			const SGD::Rectangle collision1 = characterOne->GetCollisionRect();
			//character 2 active and collision
			const SGD::Rectangle active2 = characterTwo->GetActiveRect();
			const SGD::Rectangle collision2 = characterTwo->GetCollisionRect();

			if (active1.IsIntersecting(collision2) || active2.IsIntersecting(collision1))
			{
				characterOne->HandleCollision(characterTwo);
				characterTwo->HandleCollision(characterOne);
				break;
			}
		}
	}
}

void EntityManager::CheckEntityCharacterCollision(unsigned int _entity, unsigned int _character)
{
	//cycle through all buckets
	EntityVector& tableOne = m_tEntities[_entity]; //bucket 1
	EntityVector& tableTwo = m_tEntities[_character]; //bucket 2

	//for each item in table 1
	for (unsigned int index1 = 0; index1 < tableOne.size(); ++index1)
	{
		//character1
		Entity* characterOne = dynamic_cast<Entity*>(tableOne[index1]);
		if (!characterOne) break;
		//for each item in table 2
		for (unsigned int index2 = 0; index2 < tableTwo.size(); ++index2)
		{
			//character2
			Character* characterTwo = dynamic_cast<Character*>(tableTwo[index2]);
			if (!characterTwo)
				break;

			//character 1 active and collision
			const SGD::Rectangle rect1 = characterOne->GetRect();
			//character 2 active and collision
			const SGD::Rectangle collision2 = characterTwo->GetCollisionRect();

			if (rect1.IsIntersecting(collision2))
			{
				characterOne->HandleCollision(characterTwo);
				characterTwo->HandleCollision(characterOne);
			}
		}
	}
}

void EntityManager::CheckProjectiles(void)
{

	//cycle through all buckets
	EntityVector& tableOne = m_tEntities[(unsigned int)EntityType::ENT_MISSILE]; //bucket 1
	EntityVector& tableTwo = m_tEntities[(unsigned int)EntityType::ENT_MISSILE]; //bucket 2

	//for each item in table 1
	for (unsigned int index1 = 0; index1 < tableOne.size(); ++index1)
	{
		//character1
		Missile* characterOne = dynamic_cast<Missile*>(tableOne[index1]);
		if (!characterOne) break;
		//for each item in table 2
		for (unsigned int index2 = 0; index2 < tableTwo.size(); ++index2)
		{
			//character2
			Missile* characterTwo = dynamic_cast<Missile*>(tableTwo[index2]);
			if (!characterTwo || characterTwo == characterOne)
				break;

			//character 1 active and collision
			const SGD::Rectangle active1 = characterOne->GetActiveRect();
			const SGD::Rectangle collision1 = characterOne->GetCollisionRect();
			//character 2 active and collision
			const SGD::Rectangle active2 = characterTwo->GetActiveRect();
			const SGD::Rectangle collision2 = characterTwo->GetCollisionRect();

			if (active1.IsIntersecting(collision2) || active2.IsIntersecting(collision1))
			{
				characterOne->HandleCollision(characterTwo);
				characterTwo->HandleCollision(characterOne);
				break;
			}
		}
	}
}
