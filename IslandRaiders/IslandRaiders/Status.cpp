#include "Status.h"
#include "Character.h" 

#include "GUI.h"
#include <sstream>

/**************************************************************
|	Function:
|	Purpose:
|	Parameter:
|	Return Value:
**************************************************************/
Status::Status(void)
{}

/**************************************************************
|	Function:
|	Purpose:
|	Parameter:
|	Return Value:
**************************************************************/
Status::~Status(void)
{
	//empty
}

/**************************************************************
|	Function:
|	Purpose:
|	Parameter:
|	Return Value:
**************************************************************/
void Status::ToggleEffect(unsigned int _status) { m_cEffect ^= (1 << _status); }

/**************************************************************
|	Function:
|	Purpose:
|	Parameter:
|	Return Value:
**************************************************************/
void Status::TurnEffectOn(unsigned int _status) { m_cEffect |= (1 << _status); }

/**************************************************************
|	Function:
|	Purpose:
|	Parameter:
|	Return Value:
**************************************************************/
void Status::TurnEffectOff(unsigned int _status) { m_cEffect &= ~(1 << _status); }

/**************************************************************
|	Function:
|	Purpose:
|	Parameter:
|	Return Value:
**************************************************************/
void Status::TurnAllBitsOff(void){ m_cEffect = 0; }


/**********************************************************************************/
/**********************           STATUS       EFFECT      ************************/
/**********************************************************************************/

/**************************************************************
|	Member:
|	Purpose:
**************************************************************/
StatusManager* StatusManager::pInstance = nullptr;

/**************************************************************
|	Function:
|	Purpose:
|	Parameter:
|	Return Value:
**************************************************************/
StatusManager* StatusManager::GetInstance(void)
{
	if (!pInstance)
		pInstance = new StatusManager();

	return pInstance;
}

/**************************************************************
|	Function:
|	Purpose:
|	Parameter:
|	Return Value:
**************************************************************/
void StatusManager::DeleteInstance(void)
{
	delete pInstance;
	pInstance = nullptr;
}

/**************************************************************
|	Function:
|	Purpose:
|	Parameter:
|	Return Value:
**************************************************************/
bool Status::StartEffect(STATUS eff, float time, float damage)
{
	if (m_cEffect & (1 << eff))
		return false;

	TurnEffectOn(eff);
	m_fDurations[eff] = time;
	m_fDamages[eff] = damage;

	return true;
}

/**************************************************************
|	Function:
|	Purpose:
|	Parameter:
|	Return Value:
**************************************************************/
void Status::ResetEffect(STATUS eff)
{
	TurnEffectOff(eff);
	m_fDurations[eff] = 0.0f;
	m_fDurations[eff] = 0.0f;

}

/**************************************************************
|	Function:
|	Purpose:
|	Parameter:
|	Return Value:
**************************************************************/
bool Status::IsEffectOn(STATUS eff) const
{
	return (m_cEffect & (1 << eff)) ? true : false;

}

/**************************************************************
|	Function:
|	Purpose:
|	Parameter:
|	Return Value:
**************************************************************/
void StatusManager::Update(float dt, Character* player, Status& status)
{
	//status character
	unsigned char stat = status.GetEffect();

	if (stat == 0)
		return;

	if (stat & (1 << Status::BURN))
		Burn(dt, player);

	if (stat & (1 << Status::POISON))
		Posion(dt, player);

	if (stat & (1 << Status::FREEZE))
		Freeze(dt, player);

	if (stat & (1 << Status::STUN))
		Stun(dt, player);

}

//helper functions 
/**************************************************************
|	Function:
|	Purpose:
|	Parameter:
|	Return Value:
**************************************************************/
void StatusManager::Burn(float dt, Character* player)
{
	static float timeSec = 0.f;

	Status* stats = player->GetCharacterStatus();
	Status::STATUS status = Status::BURN;

	//decrease the timer on it
	float time = stats->GetDuration(status);
	time -= dt;
	timeSec += dt;
	//time up?
	if (time <= 0.0f)
		stats->ResetEffect(status);

	//burn damage
	float playerHP = player->GetHealth();
	playerHP -= stats->GetDamage(status) * dt;

	//set the necessary variables
	player->SetHealth(playerHP);
	stats->SetDuration(status, time);

	if (timeSec >= 1.0f)
	{
		std::stringstream txt;
		txt << stats->GetDamage(status);
		GUIManager::GetInstance()->PopUpFade(
			txt.str(), player->GetPosition(), 1.f, 1.f, { 255, 130, 0 });

		timeSec = 0.0f;
	}

}

/**************************************************************
|	Function:
|	Purpose:
|	Parameter:
|	Return Value:
**************************************************************/
void StatusManager::Posion(float dt, Character* player)
{
	static float timeSec = 0.f;

	Status* stats = player->GetCharacterStatus();
	Status::STATUS status = Status::POISON;

	//decrease the timer on it
	float time = stats->GetDuration(status);
	time -= dt;

	//time up?
	if (time <= 0.0f)
		stats->ResetEffect(status);



	//burn damage
	float playerHP = player->GetHealth();
	playerHP -= stats->GetDamage(status) * dt;

	//set the necessary variables
	player->SetHealth(playerHP);
	stats->SetDuration(status, time);


	if (timeSec >= 1.0f)
	{
		std::stringstream txt;
		txt << stats->GetDamage(status);
		GUIManager::GetInstance()->PopUpFade(
			txt.str(), player->GetPosition(), 1.f, 1.f, { 128, 0, 128 });

		timeSec = 0.0f;
	}

}

/**************************************************************
|	Function:
|	Purpose:
|	Parameter:
|	Return Value:
**************************************************************/
void StatusManager::Freeze(float dt, Character* player)
{
	Status* stats = player->GetCharacterStatus();
	Status::STATUS status = Status::FREEZE;

	//decrease the timer on it
	float time = stats->GetDuration(status);
	time -= dt;

	//time up?
	if (time <= 0.0f)
		stats->ResetEffect(status);


	//slow down the character
	SGD::Vector speed = player->GetVelocity();
	speed *= stats->GetDamage(status);

	//set needed variables
	player->SetVelocity(speed);
	stats->SetDuration(status, time);

	//std::stringstream txt;
	//txt << stats->GetDamage(status);
	//GUIManager::GetInstance()->AddPopUpMessage(
	//	txt.str(), player->GetPosition(), 1.f, 1.f, { 0, 255, 255 });
}

/**************************************************************
|	Function:
|	Purpose:
|	Parameter:
|	Return Value:
**************************************************************/
void StatusManager::Stun(float dt, Character* player)
{
	Status* stats = player->GetCharacterStatus();
	Status::STATUS status = Status::STUN;

	//decrease the timer on it
	float time = stats->GetDuration(status);
	time -= dt;

	//time up?
	if (time <= 0.0f)
		stats->ResetEffect(status);

	//set timer
	stats->SetDuration(status, time);

	//stun animations
	player->GetTimeStamp()->SwitchAnimation("stun");
	//set velocity

	player->SetVelocity({ 0.f, 0.f });

}
