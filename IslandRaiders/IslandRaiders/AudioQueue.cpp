#include "AudioQueue.h"
#include "../SGD Wrappers/SGD_AudioManager.h"

AudioQueue* AudioQueue::pInstance = nullptr;

AudioQueue* AudioQueue::GetInstance(void)
{
	if (!pInstance)
		pInstance = new AudioQueue();

	return pInstance;
}

void AudioQueue::DeleteInstance(void)
{
	delete pInstance;
	pInstance = nullptr;
}

void AudioQueue::Update(void)
{
	//local audio pointer
	SGD::AudioManager* pAudio = SGD::AudioManager::GetInstance();

	//unload any that are done
	for (unsigned int i = 0; i < soundQueue.size(); ++i)
	{
		//done playing?
		if (pAudio->IsAudioPlaying(soundQueue[i]) == false)
		{
			//unload
			pAudio->UnloadAudio(soundQueue[i]);

			//remove
			soundQueue.erase(soundQueue.begin() + i);
		}
	}
}

void AudioQueue::clear(void)
{
	for (unsigned int i = 0; i < soundQueue.size(); ++i)
		SGD::AudioManager::GetInstance()->UnloadAudio(soundQueue[i]);

	soundQueue.clear();
}
unsigned int AudioQueue::size(void) const { return soundQueue.size(); }
void AudioQueue::AddSound(SGD::HAudio _sound, bool _loop)
{
	//add song into the queue
	soundQueue.push_back(_sound);
	//play the sound
	SGD::AudioManager::GetInstance()->PlayAudio(_sound, _loop);
}

void AudioQueue::AddSound(const char* filePath, bool _loop)
{
	//load handle
	SGD::HAudio handle = SGD::AudioManager::GetInstance()->LoadAudio(filePath);
	//add song into the queue
	soundQueue.push_back(handle);
	//play sound
	SGD::AudioManager::GetInstance()->PlayAudio(handle, _loop);
}

void AudioQueue::AddSound(const wchar_t* filePath, bool _loop)
{
	//load handle
	SGD::HAudio handle = SGD::AudioManager::GetInstance()->LoadAudio(filePath);
	//add song into the queue
	soundQueue.push_back(handle);
	//play sound
	SGD::AudioManager::GetInstance()->PlayAudio(handle, _loop);
}

