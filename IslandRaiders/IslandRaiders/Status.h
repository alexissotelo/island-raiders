#pragma once
#include <map>

class Character;

class Status
{

public:
	enum STATUS {BURN, POISON, FREEZE, STUN};

	//overloaded constructor
	Status(void);
	//destructor
	~Status();

	void SetDuration(STATUS eff, float _duration) { m_fDurations[eff] = _duration; }
	void SetDamage(STATUS eff, float _damage) { m_fDamages[eff] = _damage; }
	void ToggleEffect(unsigned int _status);
	void TurnEffectOn(unsigned int _status);
	void TurnEffectOff(unsigned int _status);
	void TurnAllBitsOff(void);

	unsigned char GetEffect(void) const { return m_cEffect; }
	float GetDuration(STATUS eff) { return m_fDurations[eff]; }
	float GetDamage(STATUS eff) { return m_fDamages[eff]; }

	bool StartEffect(STATUS eff, float time, float damage = 0.0f);
	void ResetEffect(STATUS eff);

	bool IsEffectOn(STATUS eff) const;
private:
	unsigned char m_cEffect = 0;
	std::map<STATUS, float> m_fDurations;
	std::map<STATUS, float> m_fDamages;
};

//singleton
class StatusManager
{
	//singleton -- none of these should be accessible
	StatusManager(void) = default;
	~StatusManager(void) = default;
	StatusManager(const StatusManager&) = delete;
	StatusManager& operator=(const StatusManager&) = delete;

	//singleton instance
	static StatusManager* pInstance;
public:
	//static functions
	static StatusManager* GetInstance(void);
	static void DeleteInstance(void);

	void Update(float dt, Character* effected, Status& status);

private:
	//helper functions 
	void Burn(float dt, Character* player);
	void Posion(float dt, Character* player);
	void Freeze(float dt, Character* player);
	void Stun(float dt, Character* player);
};
