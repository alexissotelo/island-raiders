#pragma once
#include "..\SGD Wrappers\SGD_Color.h"
#include "..\SGD Wrappers\SGD_Geometry.h"
#include "..\SGD Wrappers\SGD_Handle.h"
#include <vector>
//#include <string>

class BitmapFont
{
public:
	BitmapFont(const char* filepath);
	~BitmapFont();

	struct letter
	{
		int id;
		int x;
		int y;
		int width;
		int height;
		int xoffset;
		int yoffset;
	};

	std::vector<letter> abc;

	//void Draw(const char* output, SGD::Point position, float scale, SGD::Color color) const;
	void DrawXML(const char* output, SGD::Point position, float scale = 1.f, SGD::Color color = {0,0,0}) const;

	
private:
	const bool LoadFont(const char* filepath);
	//SGD::HTexture image = SGD::INVALID_HANDLE;
	SGD::HTexture xmlIMAGE = SGD::INVALID_HANDLE;
	//cell data 
	int charWidth = 0;
	int charHeight = 0;
	int numRows = 0;
	int numCols = 0;

	//font info
	char firstChar = '\0';
};

