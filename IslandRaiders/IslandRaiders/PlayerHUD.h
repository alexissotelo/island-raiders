#pragma once
#include <string>
#include "..\SGD Wrappers\SGD_GraphicsManager.h"

#define NUMBER_OF_SKILLS 3
class Player;

class PlayerHUD
{
public:
	friend class Player;
	PlayerHUD(Player* p);
	~PlayerHUD();

	void LoadIcon(std::string fileName, int skillIndex);
	void DrawHUD();

private:
	Player* player;
	SGD::Point HUDposition;

	SGD::HTexture portrait = SGD::INVALID_HANDLE;
	SGD::HTexture skillIcons[NUMBER_OF_SKILLS];// = SGD::INVALID_HANDLE;
};
