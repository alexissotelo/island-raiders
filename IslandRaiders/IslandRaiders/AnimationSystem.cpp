#include "AnimationSystem.h"
#include "AnimationTimestamp.h"
#include "Animation.h"
#include "Frame.h"
#include "LoadingManager.h"

#define WIN32_LEAN_AND_MEAN
#include <Windows.h>

#include "../tinyxml/tinyxml.h" //tinyXml
#include "../SGD Wrappers/SGD_GraphicsManager.h"
#include <map>
#include <string>
#include <vector>

using namespace SGD;
using namespace std;

//static singleton pointer
AnimationSystem* AnimationSystem::pInstance = nullptr;

/**************************************************************
|	Function: AnimationSystem::GetInstance [static]
|	Purpose: returns the only instanc of the class and creates one
|		if one does not already exist
|	Parameter: void
|	Return Value: AnimationSystem* - a pointer to the singleton instance
**************************************************************/
AnimationSystem* AnimationSystem::GetInstance(void)
{
	if (pInstance == nullptr)
		pInstance = new AnimationSystem();

	return pInstance;
}

/**************************************************************
|	Function: AnimationSystem::DeleteInstance [static]
|	Purpose: deletes and resets the singleton instance
|	Parameter: void
|	Return Value: void
**************************************************************/
void AnimationSystem::DeleteInstance(void)
{
	delete pInstance;
	pInstance = nullptr;
}

/**************************************************************
|	Function: AnimationSystem::Initialize
|	Purpose: generates the animations and images of all characters in the
|			game and stores them inside a container by name
|	Parameter: void
|	Return Value: void
**************************************************************/
void AnimationSystem::Initialize(void)
{
	//create all the aniamtions
	SGD::GraphicsManager* pGraphics = SGD::GraphicsManager::GetInstance();
	ThreadData* threadData = ThreadData::GetInstance();

	pInstance->GenerateAllAnimations();

	//populate the names for all vectors
	pInstance->PopulateName("resources/xml/missileList.xml", &pInstance->m_vMissiles);
	pInstance->PopulateName("resources/xml/laserList.xml", &pInstance->m_vLasers);
	pInstance->PopulateName("resources/xml/beamList.xml", &pInstance->m_vBeams);
	pInstance->PopulateName("resources/xml/animatedParticleList.xml", &pInstance->m_vParticles);


	//load images
	for (unsigned int index = 0; index < pInstance->m_vImageFilePaths.size(); ++index)
	{
		threadData->graphicsMutex.lock();
		SGD::HTexture texture = pGraphics->LoadTexture(pInstance->m_vImageFilePaths[index].c_str());
		threadData->graphicsMutex.unlock();
		pInstance->m_mSpriteSheet[pInstance->m_vImageFilePaths[index]] = texture;
	}
	//clear
	pInstance->m_vImageFilePaths.clear();

//inform a thread is done
	threadData->countMutex.lock();
	--threadData->numberOfThreads;
	threadData->countCondition.notify_all();
	threadData->countMutex.unlock();

}

/**************************************************************
|	Function: AnimationSystem::Terminate
|	Purpose: delete all animations and all images used for character
|			animations
|	Parameter: void
|	Return Value: void
**************************************************************/
void AnimationSystem::Terminate(void)
{
	//delete animations using an iterator
	std::map<std::string, Animation*>::iterator animIter;
	std::map<std::string, HTexture>::iterator hIter;

	//delete the animatiosn
	for (animIter = m_mAnimation.begin(); animIter != m_mAnimation.end(); ++animIter)
	{
		delete (*animIter).second;
		(*animIter).second = nullptr;
	}

	//unload the textures
	for (hIter = m_mSpriteSheet.begin(); hIter != m_mSpriteSheet.end(); ++hIter)
		SGD::GraphicsManager::GetInstance()->UnloadTexture((*hIter).second);

	//clear the containers
	m_mAnimation.clear();
	m_mSpriteSheet.clear();
}

/**************************************************************
|	Function: Render
|	Purpose: use the animation frame usung the time stamp
|	Parameter:
|			AnimationTimeStamp* _timeStamp - the current timeStamp
|			Point _pos - position at the anchor
|	Return Value: void
**************************************************************/
void AnimationSystem::Render(AnimationTimestamp* _timeStamp, SGD::Point _pos)
{
	//identifying string
	//animation ID key = name_action
	std::string animID = _timeStamp->m_sOwnerName;
	animID += "_";
	animID += _timeStamp->m_sAction;

	//Animation
	Animation* currAnim = m_mAnimation[animID];
	//Texture
	HTexture spriteSheet = m_mSpriteSheet[currAnim->GetImageFileName()];


	bool forwards = (currAnim->IsSheetBackwards()) ? !_timeStamp->bForwards : _timeStamp->bForwards;
	//render the animation
	currAnim->Render(spriteSheet, _timeStamp->m_CurrentFrame, _pos, forwards, _timeStamp->m_clrRender);

}

/**************************************************************
|	Function: AnimationSystem::Update
|	Purpose: update the current animations frame and update all
|			values of the given timestamp
|	Parameter:
|				float dt - the amount of time that has elapsed
|							since the last update
|				AnimationTimestamp* timeStamp - the current
status of the entity to update
|	Return Value: void
**************************************************************/
bool AnimationSystem::Update(float dt, AnimationTimestamp* timeStamp)
{
	//identifying string
	std::string animID = timeStamp->m_sOwnerName;
	animID += "_";
	animID += timeStamp->m_sAction;

	//update the animation
	Animation* currAnim = m_mAnimation[animID];

	if (!currAnim)
		return false;

	//update
	m_mAnimation[animID]->Update(dt, timeStamp);

	return true;
}

/**************************************************************
|	Function: AnimationSystem::LoadAnimation
|	Purpose: to read from an xml file information for the animation
|			of a given character
|	Parameter:
|				const char* playerName - the name of a character
|										to read in animations for
|	Return Value: returns false only when the file fails to load,
|					otherwise, returns true
**************************************************************/
bool AnimationSystem::LoadAnimation(const char* playerName)
{
	//document
	TiXmlDocument doc;

	//load the file
	std::string filePath = "resources/xml/";
	filePath += playerName;
	filePath += ".xml";

	//does the file open?
	if (doc.LoadFile(filePath.c_str()) == false)
		//quit
		return false;

	//access the root element
	TiXmlElement* pRoot = doc.RootElement();

	//empty file
	if (!pRoot)
		return false;

	//image used
	//TiXmlElement* xImage = pRoot->FirstChildElement("image");
	const char* imageName = pRoot->Attribute("image");

	//load the image --path
	std::string imagePath = "resources/graphics/sprites/";
	imagePath += imageName;

	bool unique = false;
	for (unsigned int i = 0; i < m_vImageFilePaths.size(); ++i)
	{
		if (imagePath == m_vImageFilePaths[i])
			unique = true;
	}

	if (!unique)
		m_vImageFilePaths.push_back(imagePath);

	//sheet orientation
	int orient;
	pRoot->Attribute("sheet", &orient);

	//set the orient bool in the animatin
	bool orientMe = (orient == 1);

	//texture
	//HTexture texture = SGD::GraphicsManager::GetInstance()->LoadTexture(imagePath.c_str());
	//m_mSpriteSheet[playerName] = texture;

	//iterate through the children
	TiXmlElement* animation = pRoot->FirstChildElement("animation");

	while (animation)
	{
		//create a new animation
		Animation* animate = new Animation();
		//new element

		//image name
		animate->SetImageFilePath(imagePath.c_str());

		//animation name
		const char* animName = animation->Attribute("name");
		animate->SetName(animName);

		//animation loop
		int _bool;
		animation->Attribute("loop", &_bool);

		//sheet orientation
		animate->SetOrient(orientMe);

		//set the loop bool in the animatin
		bool _b = _bool == 0 ? false : true;
		animate->SetLoop(_b);

		//frames
		TiXmlElement* frameInfo = animation->FirstChildElement("frame");
		while (frameInfo)
		{
			//new frame
			Frame* frame = new Frame();
			//local double
			double doub;
			//duration 
			frameInfo->Attribute("duration", &doub);
			frame->m_fDuration = (float)doub;

			//anchor
			TiXmlElement* xInfo = frameInfo->FirstChildElement("anchor");
			xInfo->Attribute("x", &doub);
			frame->m_ptAnchor.x = (float)doub;
			xInfo->Attribute("y", &doub);
			frame->m_ptAnchor.y = (float)doub;

			//spawn
			xInfo = xInfo->NextSiblingElement("spawn");
			xInfo->Attribute("x", &doub);
			frame->m_ptSpawnOffset.x = (float)doub;
			xInfo->Attribute("y", &doub);
			frame->m_ptSpawnOffset.y = (float)doub;

			//render
			xInfo = xInfo->NextSiblingElement("render");
			xInfo->Attribute("x", &doub);
			frame->m_rRender.left = (float)doub;
			xInfo->Attribute("y", &doub);
			frame->m_rRender.top = (float)doub;
			xInfo->Attribute("width", &doub);
			frame->m_rRender.right = (float)doub + frame->m_rRender.left;
			xInfo->Attribute("height", &doub);
			frame->m_rRender.bottom = (float)doub + frame->m_rRender.top;
			xInfo->Attribute("offsetX", &doub);
			frame->m_ptRenderOffset.x = (float)doub;
			xInfo->Attribute("offsetY", &doub);
			frame->m_ptRenderOffset.y = (float)doub;
			//collision
			xInfo = xInfo->NextSiblingElement("collision");
			xInfo->Attribute("x", &doub);
			frame->m_rCollision.left = (float)doub;
			xInfo->Attribute("y", &doub);
			frame->m_rCollision.top = (float)doub;
			xInfo->Attribute("width", &doub);
			frame->m_rCollision.right = (float)doub;
			xInfo->Attribute("height", &doub);
			frame->m_rCollision.bottom = (float)doub;
			xInfo->Attribute("offsetX", &doub);
			frame->m_ptCollisionOffset.x = (float)doub;
			xInfo->Attribute("offsetY", &doub);
			frame->m_ptCollisionOffset.y = (float)doub;

			//active
			xInfo = xInfo->NextSiblingElement("active");
			xInfo->Attribute("x", &doub);
			frame->m_rActive.left = (float)doub;
			xInfo->Attribute("y", &doub);
			frame->m_rActive.top = (float)doub;
			xInfo->Attribute("width", &doub);
			frame->m_rActive.right = (float)doub;
			xInfo->Attribute("height", &doub);
			frame->m_rActive.bottom = (float)doub;
			xInfo->Attribute("offsetX", &doub);
			frame->m_ptActiveOffset.x = (float)doub;
			xInfo->Attribute("offsetY", &doub);
			frame->m_ptActiveOffset.y = (float)doub;

			//event
			const char* events = frameInfo->Attribute("event");
			frame->m_sEvent = events;

			//add the frame to the animation
			animate->AddFrame(frame);
			//update 
			frameInfo = frameInfo->NextSiblingElement("frame");
		}

		std::string animationID = playerName;

		//ID 
		if (animationID != "reset")
		{
			animationID += "_";
			animationID += animName;
		}
		else
			animationID = animName;


		//add animation
		m_mAnimation[animationID] = animate;

		//update the thingy
		animation = animation->NextSiblingElement("animation");

	}

	//success
	return true;
}

/**************************************************************
|	Function: AnimationSystem::GenerateAllAnimations
|	Purpose: read in the names of all characters from an xml file
|			and load in the animations for each character
|	Parameter: void
|	Return Value: void
**************************************************************/
void AnimationSystem::GenerateAllAnimations()
{

	SGD::GraphicsManager* pGraphics = SGD::GraphicsManager::GetInstance();

	//document
	TiXmlDocument doc;

	//does the file open?
	if (doc.LoadFile("resources/xml/namelist.xml") == false)
		//quit
		return;

	//root
	TiXmlElement* root = doc.RootElement();

	//iterate for all names
	TiXmlElement* xNames = root->FirstChildElement("name");

	while (xNames)
	{
		//name to load in
		const char* currName = xNames->GetText();
		//load the animations
		LoadAnimation(currName); //display error on failure
		//next name
		xNames = xNames->NextSiblingElement("name");
	}
}

void AnimationSystem::PopulateName(const char* fileName, NameTable* table)
{
	SGD::GraphicsManager* pGraphics = SGD::GraphicsManager::GetInstance();

	//document
	TiXmlDocument doc;

	//does the file open?
	if (doc.LoadFile(fileName) == false)
		//quit
		return;

	//root
	TiXmlElement* root = doc.RootElement();

	//iterate for all names
	TiXmlElement* xNames = root->FirstChildElement("name");

	while (xNames)
	{
		//name to load in
		const char* currName = xNames->GetText();
		//load the animations
		table->push_back(currName);
		//next name
		xNames = xNames->NextSiblingElement("name");
	}
}

