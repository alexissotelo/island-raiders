#pragma once
#include <vector>
#include <string>
#include "..\SGD Wrappers\SGD_Geometry.h"
#include "..\SGD Wrappers\SGD_Handle.h"


//class Button;
//class GUISkin;

class ButtonGroup
{
	friend class GUISkin;
	friend class Button;

	std::vector<Button*> group;
	const GUISkin *skin;

	int nCursor = 0;
	SGD::HTexture cursorIcon = SGD::INVALID_HANDLE;

	SGD::Point position;
	float fontScale;
public:
	ButtonGroup(int skinIndex, SGD::Point _position, float fontScale);
	~ButtonGroup();

	int Update(float dt);
	void DrawGroup();

	void AddButton(std::string text);
};

