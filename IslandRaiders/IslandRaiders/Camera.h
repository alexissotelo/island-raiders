#pragma once
#include "Entity.h"
#include "PlayGameState.h"

class LevelManager;
class LevelSection;

class Camera : public Entity
{
	friend class PlayGameState;
	PlayGameState *pPlayState = nullptr;
	LevelManager  *levelManager = nullptr;

	SGD::Point targetPos;
public:
	bool lock = false;

	Camera();
	~Camera();

	LevelSection *currentSection;
	LevelSection *nextSection;
	int currSectionIndex = 0;
	int nextSectionIndex = 0;

	virtual void Update(float dt) final;
	virtual void Render() final;
	void RenderForeground();
	virtual void HandleEvent(const SGD::Event* pEvent) final;

	void EnterLevelSection();
	void Reset();

	//static SGD::Point GetScreenPosition(SGD::Point worldPosition);
};