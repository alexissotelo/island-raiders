#pragma once
#include "..\SGD Wrappers\SGD_Handle.h"
#include "..\SGD Wrappers\SGD_Color.h"
#include "..\SGD Wrappers\SGD_Geometry.h"

class ParticleFlyWeight
{
	friend class Particle;
	friend class ParticleEmitter;

public:
	ParticleFlyWeight(const char* imgPath);
	~ParticleFlyWeight();

	float GetRandomLifeTime();
	float GetRandomRotation();
	float GetRandomScale();
	float GetRandomSpeed();
	SGD::Vector GetRandomVelocity();

//public:
	SGD::HTexture image = SGD::INVALID_HANDLE;

	float minLife = 0.f;
	float maxLife = 1.f;
	SGD::Color startColor;
	SGD::Color targetColor = { 0, 0, 0, 0 };

	float minScale = 1.f;
	float maxScale = 1.f;
	float deltaScale = 0.f;
	
	float minRotation = 0.f;
	float maxRotation = 0.f;
	float deltaRotation = 0.f;
	SGD::Vector rotationOffset;// = {};
	
	float minSpeed = 0.f;
	float maxSpeed = 1.f;

	SGD::Vector velocity;
	SGD::Vector randomVelocity;//gravity
};

