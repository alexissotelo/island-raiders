#pragma once
#include "EnemyAI.h"
#include "Entity.h"
#include "Player.h"
#include "Enemy.h"

class BossAI : public EnemyAI
{
public:
	void FindClosestTarget();
	BossAI(/*Enemy* e*/);
	~BossAI();

	virtual void Update(float dt, AIStamp& _timeStamp);
	virtual EnemyAIType GetAIType() const { return EnemyAIType::Boss; }

private:
	Entity* previousTarget;
	Player* party[50];

};

