#include "Items.h"
#include "Character.h"
#include "..\SGD Wrappers\SGD_GraphicsManager.h"
#include "PlayGameState.h"
#include "EntityManager.h"
#include "DeleteItemFromEntityManager.h"
#include "..\SGD Wrappers\SGD_Event.h"
#include "GUI.h"

//need to set all the important item information when making an item
Items::Items(Tier level, float time, SGD::HTexture pic, StatusEffect effect,
	ItemType it, bool isPickedUp) : Entity()
{
	tier = level;
	timer = time;
	Entity::SetImage(pic);

	sEffect = effect;
	queItem = it;
	switch (tier)
	{
	case Gold:
		lifeSpan = 5.0f;
		break;
	case Plat:
		lifeSpan = 8.0f;
		break;
	case Diamond:
		lifeSpan = 10.0f;
		break;
	default:
		break;
	}

	//no aniamtions
	aTimeStamp.SetOwner(nullptr);
}


Items::~Items()
{

}

//this update function will update the item to do its current effect
void Items::UpdateItem(float dt)
{

	if (owner == nullptr)
		return;


	//each item has a lifespan, so items don't stay in the world forever
	lifeSpan -= dt;
	if (lifeSpan > 0.1f)
	{
		switch (tier)
		{
		case Tier::Gold:
			TierOne();
			if (sEffect != NOEFFECT) // if no status effect the read in value will be NOEFFECT
				ApplyStatusEffect(owner, dt);
			break;
		case Tier::Plat:
			TierTwo();
			if (sEffect != NOEFFECT) // if no status effect the read in value will be NOEFFECT
				ApplyStatusEffect(owner, dt);
			break;
		case Tier::Diamond:
			TierThree();
			if (sEffect != NOEFFECT) // if no status effect the read in value will be NOEFFECT
				ApplyStatusEffect(owner, dt);
			break;
		default:
			break;
		}
	}
	else
		ResetMoveSpeed();

}
void Items::ResetMoveSpeed()
{
	owner->SetMoveSpeed(ms2);
	this->owner->SetAttackBoost(1.f);
	owner->SetDefense(OGBlock);
}

//Draw the Item if and only if the item is on the gound on the map
// if there is an owner no need to draw it, hokay
void Items::Render(void)
{
	//if item is not picked up then draw the item drop on the map
	//if the item is pickced up no need to draw the item
	if (!isPickedUp)
	{
		Entity::Render();
	}
}
//Tier::Gold items 10% baby
void Items::TierOne(void)
{
	float temp;
	float h = owner->GetHealth();
	float d = owner->GetDamage();
	float m = owner->GetMoveSpeed();
	float def;
	switch (queItem)
	{
	case BLOCK:
		if (once)
		{
			def = owner->GetDefensiveStat();
			blockIncrease = .2f;
			def += blockIncrease;
			owner->SetDefense(def);
		}
		if (owner->GetHealth() > OGHP)
			owner->SetHealth(OGHP);
		once = false;
		break;
	case DAMAGE:
		if (once)
			owner->SetAttackBoost(1.2f);
		once = false;
		break;
	case HP:
		temp = h;
		temp *= 0.5f;
		hpIncrease = temp;
		if (once)
			h += hpIncrease;
		if (h >= owner->GetMaxHealth())
			h = owner->GetMaxHealth();
		owner->SetHealth(h);
		once = false;
		break;
	case MOVESPEED:
		temp = m;
		temp *= 0.5f;
		msIncrease = temp;
		if (once)
			m += msIncrease;
		owner->SetMoveSpeed(m);
		once = false;
		break;

	default:
		break;
	}

}
//Tier::Plat items 20% v^.^v
void Items::TierTwo(void)
{
	float temp;
	float d = owner->GetDamage();
	float h = owner->GetHealth();
	float m = owner->GetMoveSpeed();
	float def;
	switch (queItem)
	{
	case BLOCK:
		if (once)
		{
			def = owner->GetDefensiveStat();
			blockIncrease = .30f;
			def += blockIncrease;
			owner->SetDefense(def);
		}
		if (owner->GetHealth() > OGHP)
			owner->SetHealth(OGHP);
		once = false;
		break;
	case DAMAGE:
		if (once)
			owner->SetAttackBoost(1.5f);
		once = false;
		break;
	case HP:
		temp = h;
		temp *= 1.0f;
		hpIncrease = temp;
		if (once)
			h += hpIncrease;
		if (h >= owner->GetMaxHealth())
			h = owner->GetMaxHealth();
		owner->SetHealth(h);
		once = false;
		break;
	case MOVESPEED:
		temp = m;
		temp *= 0.5f;
		msIncrease = temp;
		if (once)
			m += msIncrease;
		owner->SetMoveSpeed(m);
		once = false;
		break;
	default:
		break;
	}
}
//Tier::Diamond items 30%!! SuperSayian, just sayin
void Items::TierThree(void)
{
	float temp;
	float d = owner->GetDamage();
	float h = owner->GetHealth();
	float m = owner->GetMoveSpeed();
	float def;
	switch (queItem)
	{
	case BLOCK:
		if (once)
		{
			def = owner->GetDefensiveStat();
			blockIncrease = .5f;
			def += blockIncrease;
			owner->SetDefense(def);
		}
		if (owner->GetHealth() > OGHP)
			owner->SetHealth(OGHP);
		once = false;
		break;
	case DAMAGE:
		if (once)
			owner->SetAttackBoost(2.3f);
		once = false;
		break;
	case HP:
		temp = h;
		temp *= 1.5f;
		hpIncrease = temp;
		if (once)
			h += hpIncrease;
		if (h >= owner->GetMaxHealth())
			h = owner->GetMaxHealth();
		owner->SetHealth(h);
		once = false;
		break;
	case MOVESPEED:
		temp = m;
		temp *= 0.8f;
		msIncrease = temp;
		if (once)
			m += msIncrease;
		owner->SetMoveSpeed(m);
		once = false;
		break;
	default:
		break;
	}
}

void Items::ApplyStatusEffect(Character* whom, float delta)
{
	if (hasStatus)
	{
		switch (sEffect)
		{
		case StatusEffect::FIRE:
			FireEffect(delta);
			break;
		case StatusEffect::ICE:
			IceEffect(delta);
			break;
		case StatusEffect::POISON:
			PoisonEffect(delta);
			break;
		default:
			break;
		}
	}
}

void Items::FireEffect(float delta)
{
	float temp;
	float h = owner->GetHealth();
	if (fireBurn)
	{
		switch (tier)
		{
		case Gold:
			maxTime = 0.3f;
			maxTime -= delta;
			if (maxTime > 0.0f)
			{
				temp = h;
				temp *= 0.1f;
				h = h - temp;
				owner->SetHealth(h);
			}
			break;
		case Plat:
			maxTime = 0.6f;
			maxTime -= delta;
			if (maxTime > 0.0f)
			{
				temp = h;
				temp *= 0.2f;
				h = h - temp;
				owner->SetHealth(h);
			}
			break;
		case Diamond:
			maxTime = 1.0f;
			maxTime -= delta;
			if (maxTime > 0.0f)
			{
				temp = h;
				temp *= 0.3f;
				h = h - temp;
				owner->SetHealth(h);
			}
			break;
		default:
			break;
		}
	}
	fireBurn = false;
}

void Items::IceEffect(float delta)
{
	float temp;
	float ms = owner->GetMoveSpeed();
	float maxSpeed = 0.0f;

	switch (tier)
	{
	case Gold:
		if (fireBurn)
			maxTime = 2.0f;
		maxTime -= delta;
		if (maxTime > 0.0f)
		{
			temp = ms;
			temp *= 0.4f;
			ms = ms - temp;
			if (fireBurn)
			{
				maxSpeed = owner->GetMoveSpeed();
				owner->SetMoveSpeed(ms);
			}
		}

		break;
	case Plat:
		if (fireBurn)
			maxTime = 3.0f;
		maxTime -= delta;
		if (maxTime > 0.0f)
		{
			temp = ms;
			temp *= 0.5f;
			ms = ms - temp;
			if (fireBurn)
			{
				maxSpeed = owner->GetMoveSpeed();
				owner->SetMoveSpeed(ms);
			}
		}
		break;
	case Diamond:
		if (fireBurn)
			maxTime = 4.0f;
		maxTime -= delta;
		if (maxTime > 0.0f)
		{
			temp = ms;
			temp *= 0.6f;
			ms = ms - temp;
			if (fireBurn)
			{
				maxSpeed = owner->GetMoveSpeed();
				owner->SetMoveSpeed(ms);
			}
		}

		break;
	default:
		break;
	}

	fireBurn = false;

}

void Items::PoisonEffect(float delta)
{
	float temp;
	float h = owner->GetHealth();
	switch (tier)
	{
	case Gold:
		if (fireBurn)
			maxTime = 2.0f;
		maxTime -= delta;
		if (maxTime > 0.0f)
		{
			temp = h;
			temp *= 0.0003f;
			h = h - temp;
			owner->SetHealth(h);
		}
		break;
	case Plat:
		if (fireBurn)
			maxTime = 3.0f;
		maxTime -= delta;
		if (maxTime > 0.0f)
		{
			temp = h;
			temp *= 0.00045f;
			h = h - temp;
			owner->SetHealth(h);
		}
		break;
	case Diamond:
		if (fireBurn)
			maxTime = 4.0f;
		maxTime -= delta;
		if (maxTime > 0.0f)
		{
			temp = h;
			temp *= 0.0005f;
			h = h - temp;
			owner->SetHealth(h);
		}
		break;
	default:
		break;
	}
	fireBurn = false;
}

bool Items::HandleCollision(const IEntity* other)
{
	int type = other->GetType();
	switch (type)
	{
	case EntityType::ENT_PLAYER:
		owner = (Character*)other;
		this->AddRef();
		for (unsigned int i = 0; i <= owner->utilityBelt.size() && owner->utilityBelt.size() != 2; i++)
		{
			if (owner->utilityBelt.size() < 2)
			{
				isPickedUp = true;
				owner->utilityBelt.push_back(this);
				ms2 = owner->GetMoveSpeed();
				OGDamage = owner->GetDamage();
				OGBlock = owner->GetDefensiveStat();
				OGHP = owner->GetMaxHealth();
				ItemFlashText(queItem, sEffect, owner->GetPosition());
				DeleteItemFromEntityManager* msg = new DeleteItemFromEntityManager(this);
				msg->QueueMessage();
				msg = nullptr;
				break;
			}
		}
		break;
	default:
		break;
	}
	return false;
}

void Items::ItemFlashText(ItemType it, StatusEffect sEffect, SGD::Point pos)
{
	SGD::Color colorA;
	std::string txt;
	if (it != ItemType::NOITEM)
	{
		switch (it)
		{
		case ItemType::BLOCK:
			txt = "+Stamina";
			colorA = { 146, 227, 37 };//green bar
			break;
		case ItemType::DAMAGE:
			txt = "+Damage";
			colorA = { 146, 227, 37 };//green bar
			break;
		case ItemType::HP:
			txt = "+Health";
			colorA = { 146, 227, 37 };//green bar
			break;
		case ItemType::MOVESPEED:
			txt = "+Speed";
			colorA = { 146, 227, 37 };//green bar
			break;
		default:
			break;
		}
		GUIManager::GetInstance()->PopUpFade(txt.c_str(), pos, 1.f, 1.2f, colorA);
	}
	else// if (it == ItemType::NOITEM)
	{
		switch (sEffect)
		{
		case StatusEffect::FIRE:
			txt = "Burn";
			colorA = { 255, 0, 0 };//red,pink
			break;
		case StatusEffect::ICE:
			txt = "Frozen";
			colorA = { 255, 0, 0 };//red,pink
			break;
		case StatusEffect::POISON:
			txt = "Poisoned";
			colorA = { 255, 0, 0 };//red,pink
			break;
		}
		GUIManager::GetInstance()->PopUpFade(txt.c_str(), pos, 0.75f, 1.2f, colorA);
	}
}

