#include "Alchemist.h"
#include "Familiar.h"
#include "PlayerSkill.h"
#include "AnimatedParticle.h"

#include "CreateAnimatedParticle.h"
#include "../SGD Wrappers/SGD_MessageManager.h"
#include "../SGD Wrappers/SGD_InputManager.h"
#include "../SGD Wrappers/SGD_EventManager.h"
#include "../SGD Wrappers/SGD_Event.h"
#include "../SGD Wrappers/SGD_AudioManager.h"

#include "PlayGameState.h"
#include "EntityManager.h"



Alchemist::Alchemist(const char* name) : Player(name)
{

	SetSkill(0, &PlayerSkill::Basic_AllyHealing);
	playerType = PlayerType::swordsman;

	playerHUD->LoadIcon("resources/graphics/icons/mage/basic.png", 0);

	RegisterForEvent("RIP");
	RegisterForEvent("TRANSFORM");
	RegisterForEvent("PARTNER");

	//set original name
	int size = strlen(name) + 1;
	originalName = new char[size];
	strcpy_s(originalName, size, name);
}

Alchemist::Alchemist(const char* name, unsigned int controllerNumber) : Player(name)
{
	//set animation owner
	aTimeStamp.m_sOwnerName = GetName();
	aTimeStamp.SwitchAnimation("idle");
	//set the controller number
	controllerIndex = controllerNumber;
	//set skill
	SetSkill(0, &PlayerSkill::Basic_AllyHealing);
	//set icon
	playerHUD->LoadIcon("resources/graphics/icons/mage/basic.png", 0);

	//set original name
	int size = strlen(name) + 1;
	originalName = new char[size];
	strcpy_s(originalName, size, name);

	RegisterForEvent("RIP");
	RegisterForEvent("TRANSFORM");
	RegisterForEvent("PARTNER");

}

Alchemist::~Alchemist()
{
	ClearOriginalName();
	if (tempStats)
	{
		delete tempStats;
		tempStats = nullptr;
	}
}

void Alchemist::Update(float dt)
{
	//local input manager
	SGD::InputManager* pInput = SGD::InputManager::GetInstance();

#if CHEAT_SKILLS

	//set skill
	if (pInput->IsKeyPressed(SGD::Key::NumPad1))
		//set skill
	{
		SetSkill(1, &(PlayerSkill::Summoner));
		this->GetPlayerHUD()->LoadIcon("resources/graphics/icons/mage/apothecary.png", 1);
	}

	//set skill
	if (pInput->IsKeyPressed(SGD::Key::NumPad2))
		//set skill
	{
		SetSkill(2, &(PlayerSkill::OmniKnight));
		this->GetPlayerHUD()->LoadIcon("resources/graphics/icons/mage/omniknight.png", 2);
	}
#endif

	//player update
	Player::Update(dt);
}

bool Alchemist::HandleCollision(const IEntity* ptr)
{

#pragma region Poison
	if (aTimeStamp.m_sAction == "poisonfog")
	{
		//convert to a character pointer
		const Character* other = dynamic_cast<const Character*>(ptr);

		if (other && other->GetType() != this->GetType())
		{
			const SGD::Rectangle attack = this->GetActiveRect();
			const SGD::Rectangle enemyCol = other->GetCollisionRect();

			if (attack.IsIntersecting(enemyCol))
			{
				//float poison = damage * 0.5f;
				float* poisonStat = new float[2];
				poisonStat[0] = stat.damage * 0.20f;
				poisonStat[1] = 10.0f;

				//create a damage event
				SGD::Event dmg = { "POISON", poisonStat, this };
				//send it out
				dmg.SendEventNow(ptr);

				//create a damage event
				SGD::Event* stun = new SGD::Event("STUN", &poisonStat[1], this);
				//send it out
				stun->SendEventNow(ptr);

				delete poisonStat;
				delete stun;
				stun = nullptr;
			}
		}
		return true;
	}
#pragma endregion 

#pragma region
	else if (aTimeStamp.m_sAction == "mukuro2" && dynamic_cast<const Character*>(ptr)
		&& !dynamic_cast<const Player*>(ptr))
	{
		Character* enemy = (Character*)ptr;

		if (!enemy->GetCharacterStatus()->IsEffectOn(Status::STUN))
		{
			//stun the enemy
			float stunTime = 5.0f;
			//startup the snakes animations
			SGD::Event stunEvent = { "STUN", &stunTime };
			//stun the enemy
			stunEvent.SendEventNow(ptr);

			//create the snakes
			CreateAnimatedParticle* snake = new CreateAnimatedParticle((Character*)ptr, "snakes", 5.0f, { 255, 255, 255, 255 });
			SGD::MessageManager::GetInstance()->QueueMessage(snake);
		}
	}

#pragma endregion
	else return Player::HandleCollision(ptr);

	return false;
}

void Alchemist::HandleEvent(const SGD::Event* pEvent)
{
	//damaage
	if (pEvent->GetEventID() == "DAMAGE")
	{
		if (aTimeStamp.m_sAction == "poisonfog" || aTimeStamp.m_sAction == "ability"
			 || aTimeStamp.m_sAction == "basic"|| aTimeStamp.m_sAction == "mukuro3"
			|| aTimeStamp.m_sAction == "mukuro2")
			return;
		//else if (skillStamp[2].m_fSkillTimer > 0 && skillStamp[2].deltaT > 0) //captain
		//	return;

		else
		{
			Player::HandleEvent(pEvent);
			currCombo = NONE;
			return;
		}
	}

	Player::HandleEvent(pEvent);

	//familiar down
	if (pEvent->GetEventID() == "RIP")
	{
		Familiar* buddy = (Familiar*)(pEvent->GetSender());
		if (buddy->GetName() == "robodog")
		{
			//turn on skill
			skillStamp[1].deltaT = 1.0f;
			skillStamp[1].m_fSkillTimer = skillStamp[1].m_fSkillCooldown;
		}
		else if (buddy->GetName() == "blackdragon")
		{	//turn on skill
			skillStamp[2].deltaT = 1.0f;
			skillStamp[1].m_fSkillTimer = skillStamp[1].m_fSkillCooldown;
		}
	}
	//mukuro rokudo
	else if (pEvent->GetEventID() == "TRANSFORM")
	{
		int length = GetName().length() + 1;
		originalName = new char[length];
		strcpy_s(originalName, length, GetName().c_str());

		GetTimeStamp()->m_sOwnerName = "mukuro";
		GetTimeStamp()->SwitchAnimation("intro");
		SetName("mukuro");

		skillStamp[0].Reset();
		skillStamp[1].Reset();
	}
	//GO!
	else if (pEvent->GetEventID() == "MOVE")
	{
		//move 
		SGD::Vector forwards = SGD::Vector(1, 0);
		//directions
		if (!aTimeStamp.bForwards)
			forwards.x *= -1;

		//scale
		forwards *= 600;

		//set velocity
		SetVelocity(forwards);
	}

	else if (pEvent->GetEventID() == "STOP")
	{
		SetVelocity({ 0, 0 });
	}
	//animated particles
	else if (pEvent->GetEventID() == "POKE")
	{
		AnimatedParticle* particle = (AnimatedParticle*)pEvent->GetSender();
		//fire pillar
#pragma region FirePillar

		if (particle->GetName() == "firepillar" || particle->GetName() == "pokefire")
		{
			//hurt the other
			AttackInfo atkinfo;
			atkinfo.damage = GetDamage() * 3.5f /* -5.0f * skillStamp[2].deltaDamage*/;
			atkinfo.isCriticalHit = false;

			std::vector<IEntity*> allEnemies = (std::vector<IEntity*>)PlayGameState::GetInstance()->GetManager()->GetEntityVector(ENT_ENEMY);
			for each (IEntity* current in allEnemies)
			{
				Character* cur = dynamic_cast<Character*>(current);

				//collision box for other
				const SGD::Rectangle attack = particle->GetActiveRect();
				const SGD::Rectangle collisionBox = cur->GetCollisionRect();

				//in contact?
				if (collisionBox.IsIntersecting(attack))
				{
					//damage event
					SGD::Event dmg = { "DAMAGE", &atkinfo, this };
					//send it out
					dmg.SendEventNow(cur);
				}
			}

			particle->GetTimeStamp()->m_CurrentFrame++;
			particle->GetTimeStamp()->m_fFrameTimer = 0.f;
		}
#pragma endregion

#pragma region Poison Ball
		if (particle->GetName() == "poisonball")
		{
			//float poison = damage * 0.5f;
			float* poisonStat = new float[2];
			poisonStat[0] = 4.0;
			poisonStat[1] = 10.0f;

			//create a damage event
			SGD::Event dmg = { "POISON", poisonStat, this };
			//send it out
			dmg.SendEventNow(pEvent->GetData());

			//create a damage event
			SGD::Event stun = { "STUN", &poisonStat[1], this };
			//send it out
			stun.SendEventNow(pEvent->GetData());
			delete poisonStat;

		}
#pragma endregion

#pragma region Combo Attacks

		//combo attacks
		else if (particle->GetName() == "icemonkey" || particle->GetName() == "icedragon" || particle->GetName() == "whirlwind"
			|| particle->GetName() == "darkclaw" || particle->GetName() == "shadowstrike" || particle->GetName() == "darkswirl"
			|| particle->GetName() == "ravens" || particle->GetName() == "stonehand")
		{
			//hurt the other
			AttackInfo atkinfo;
			atkinfo.damage = GetDamage();
			atkinfo.isCriticalHit = false;

			SGD::Event* msg = new SGD::Event("DAMAGE", &atkinfo, this);
			msg->SendEventNow(pEvent->GetData());
			delete msg;
			msg = nullptr;

			particle->GetTimeStamp()->m_CurrentFrame++;
			particle->GetTimeStamp()->m_fFrameTimer = 0;
		}
		else if (particle->GetName() == "rah" )
		{
			//hurt the other
			AttackInfo atkinfo;
			atkinfo.damage = GetDamage() * 1.5f;
			atkinfo.isCriticalHit = false;

			SGD::Event* msg = new SGD::Event("DAMAGE", &atkinfo, this);
			msg->SendEventNow(pEvent->GetData());
			delete msg;
			msg = nullptr;

			particle->GetTimeStamp()->m_CurrentFrame++;
			particle->GetTimeStamp()->m_fFrameTimer = 0;
		}



#pragma endregion
	}


#pragma region Familiars?
	else if (pEvent->GetEventID() == "PARTNER")
	{
		AttackInfo atkinfo;
		atkinfo.damage = 100;

		SGD::Event* msg = new SGD::Event("DAMAGE", &atkinfo, this);
		msg->SendEventNow(pEvent->GetData());
		delete msg;
		msg = nullptr;

	}
#pragma endregion
}

bool Alchemist::UpdateCombo(float dt)
{
	//combo list
	//	X = N		Y = M

	if (aTimeStamp.isDone && currCombo == XY && GetName() == "mukuro")
		aTimeStamp.bForwards = !aTimeStamp.bForwards;
	//update the queue
	if (inputQueue.empty())
	{
		//timer run out?
		if (inputQueue.m_fUpdate > 0.750f)
		{
			//animation done?
			if (aTimeStamp.bLoop == false && aTimeStamp.isDone == false)
				return true;

			//reset combo pos
			currCombo = COMBO_LIST::NONE;
			//reset the input
			inputQueue.m_fUpdate = 0;
		}
		//fail
		return false;
	}
	else
	{
		//animation done?
		if (aTimeStamp.bLoop == false && aTimeStamp.isDone == false)
			return true;

		//key at the front
		SGD::Key topKey = inputQueue.front();
		//remove the top keye
		inputQueue.pop();

		//quick atk
		if (topKey == SGD::Key::J)
			stat.bQuickAttack = true;
		else if (SGD::Key::K == topKey)
			stat.bQuickAttack = false;

		//reset for the next key input
		inputQueue.m_fUpdate = 0;

		//update the state
		switch (currCombo)
		{
		case Player::NONE:
			if (topKey == SGD::Key::J)
				currCombo = COMBO_LIST::X;
			else if (SGD::Key::K == topKey)
				currCombo = COMBO_LIST::Y;
			break;
		case Player::X:
			if (topKey == SGD::Key::J)
				currCombo = COMBO_LIST::XX;
			else if (SGD::Key::K == topKey)
			{
				currCombo = COMBO_LIST::XY;
				inputQueue.clear();
			}
			break;
		case Player::XX:
			if (topKey == SGD::Key::J)
				currCombo = COMBO_LIST::XXX;
			else if (SGD::Key::K == topKey)
				currCombo = COMBO_LIST::XXY;
			inputQueue.clear();
			break;
		case Player::Y:
			if (topKey == SGD::Key::K)
			{
				stat.projectileSpeed = 0.0f;
				currCombo = COMBO_LIST::YY;
				inputQueue.clear();
			}
			else
			{
				//reset combo pos
				currCombo = COMBO_LIST::NONE;
			inputQueue.clear();
		}

			break;
		default:
			currCombo = COMBO_LIST::NONE;
			inputQueue.clear();
		}
	}

	//change the animation to fit the combo
	switch (currCombo)
	{
	case Player::X:
		aTimeStamp.SwitchAnimation("attack_x");
		break;
	case Player::XX:
		aTimeStamp.SwitchAnimation("attack_x_x");
		break;
	case Player::XXX:
		aTimeStamp.SwitchAnimation("attack_x_x_x");
		break;
	case Player::XY:
		aTimeStamp.SwitchAnimation("attack_x_y");
		break;
	case Player::XXY:
		aTimeStamp.SwitchAnimation("attack_x_x_y");
		break;
	case Player::YY:
		aTimeStamp.SwitchAnimation("attack_y_y");
		break;
	case Player::Y:
		aTimeStamp.SwitchAnimation("attack_y");
		break;
	case Player::NONE:
		aTimeStamp.SwitchAnimation("idle");
	default:
		break;
	}
	//no moving
	SetVelocity({ 0, 0 });
	//play a sound
	//AudioQueue::GetInstance()->AddSound(SGD::AudioManager::GetInstance()->LoadAudio(L"resources/audio/sound effects/Summon 2.wav"));

	//success!
	return true;
}

void Alchemist::StoreOriginalStats(CharacterStats& stats)
{
	//hold onto the current stats for now
	tempStats = new CharacterStats();
	//copy everything inside
	tempStats->CopyStats(stats);

}

void Alchemist::ClearTempStats(void)
{
	delete tempStats;
	tempStats = nullptr;
}
