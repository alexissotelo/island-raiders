#pragma once
#include <vector>
#include <string>
#include "Enemy.h"
#include "../SGD Wrappers/SGD_Listener.h"

//this struct is use to store the
//data needed to spawn entities/enemies
struct WorldEntity
{
	std::string name;
	float x, y, scaleX, scaleY;
	SGD::Rectangle renderRect;
	SGD::HTexture image = SGD::INVALID_HANDLE;
};

class LevelSection : public SGD::Listener
{
private:
	std::vector<WorldEntity> enemies;
	std::vector<WorldEntity> background;
	std::vector<WorldEntity> foreground;

	int amountOfEnemies;
	SGD::Point position;

	void SpawnEnemies();
public:
	LevelSection(float x, float y);
	~LevelSection();

	void RenderBackground();
	void RenderForeground();
	
	virtual void	HandleEvent(const SGD::Event* pEvent);	// Callback function to process events

	void AddEnemy(WorldEntity we){ enemies.push_back(we); }
	void AddBackground(WorldEntity we){ background.push_back(we); }
	void AddForeground(WorldEntity we){ foreground.push_back(we); }

	//acc and mut
	void SetAmountOfEnemies(int amount){ amountOfEnemies = amount; }
	int GetAmountOfEnemies()const { return amountOfEnemies; }

	SGD::Point GetPosition() const { return position; }
};

