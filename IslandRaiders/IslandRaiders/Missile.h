#pragma once
#include "Entity.h"
#include "Character.h"
#include "AnimationTimestamp.h"
#include "..\SGD Wrappers\SGD_GraphicsManager.h"

enum ProjectileType {NORMAL, LASER, BEAM};

class ParticleEmitter;

class Missile : public Entity
{
	
public:
	virtual void   Update(float dt);
	virtual void   Render();
	virtual bool   HandleCollision(const IEntity* other);
	virtual void HandleEvent(const SGD::Event* pEvent);
	virtual unsigned int GetType()	const override	{ return ENT_MISSILE; }


	Missile(const char* _name = "missile", unsigned int _projType = ProjectileType::NORMAL, const char* _initAction = "animate");
	virtual ~Missile(void);

	//accessors
	float			GetDamage(void)				const { return damage; }
	float			GetSpeed(void)				const { return speed; }
	Character*		GetOwner(void)				const { return owner; }
	std::string		GetName(void)				const { return name; }
	SGD::Rectangle	GetCollisionRect(void)		const;
	SGD::Rectangle	GetActiveRect(void)			const;

	//const AnimationTimestamp* GetTimeStamp(void) const { return &animStamp; }
	//AnimationTimestamp* GetTimeStamp(void) { return &animStamp; }

	//mutators
	void SetDamage(float d)			{ damage = d; }
	void SetSpeed(float s)			{ speed = s; }
	void SetOwner(Character* c);

	void SetActiveRect(SGD::Rectangle);
	void SetCollisionRect(SGD::Rectangle);

	void SetTrail(ParticleEmitter *emitter, int _trailID){ trail = emitter; trailID = _trailID; }

protected:

	//active and collision rect
	SGD::Rectangle collision;
	SGD::Point ptOffset;

	float damage;
	float speed;
	Character* owner = nullptr;
	bool OutOfWorld();
	unsigned int projType;

	ParticleEmitter *trail = nullptr;
	int trailID = -1;

};

class Laser : public Missile
{
public:

	Laser(const char* _name, float destruct = 3.5f, unsigned int _projType = ProjectileType::LASER, const char* _initAction = "create");
	virtual ~Laser(void) = default;


	virtual void   Update(float dt);
	virtual bool   HandleCollision(const IEntity* other);

private:
	float fSelfDestruct = 0.f;
	float fDeltaDamage = 0.f;
};
