#pragma once
#include "EnemyAI.h"
/****************************************************
| File:	PursuitAI
| Purpose: Pursiut will define the behavior of enemies
|			that are in pursuit of a player
****************************************************/

class PursuitAI : public EnemyAI
{
public:
	PursuitAI() = default;
	~PursuitAI() = default;

	virtual void Update(float dt, AIStamp& _timeStamp);
	virtual EnemyAIType GetAIType() const { return EnemyAIType::Pursuit; }

private:
	//melee chasers
	void MeleeUpdate(float dt, AIStamp& _timeStamp);
	void TierZeroMelee(float dt, AIStamp& _timeStamp);
	void TierOneMelee(float dt, AIStamp& _timeStamp);
	void TierTwoMelee(float dt, AIStamp& _timeStamp);

	//ranged chasers
	void RangeUpdate(float dt, AIStamp& _timeStamp);
	void TierZeroRange(float dt, AIStamp& _timeStamp);
	void TierOneRange(float dt, AIStamp& _timeStamp);

	//support chasers
	void SupportUpdate(float dt, AIStamp& _timeStamp);
	void TierOneSupport(float dt, AIStamp& timeStamp);
	void TierTwoSupport(float dt, AIStamp& timeStamp);

	//familiars
	void FamiliarUpdate(float dt, AIStamp& _timeStamp);
};

