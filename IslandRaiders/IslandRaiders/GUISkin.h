#pragma once
#pragma once
#include "..\SGD Wrappers\SGD_Color.h"
#include "BitmapFont.h"

enum TextAlignment
{
	MiddleCenter, MiddleLeft
};

class GUISkin
{
	friend class Label;
	friend class PopUp;

public:
	~GUISkin();

	GUISkin(const BitmapFont *font, SGD::Color textColor,
		SGD::Color backColor);
	GUISkin(const BitmapFont *font, SGD::Color textColor,
		SGD::Color backColor, SGD::Color hoverColor);

	void SetTextAlignment(TextAlignment align) { alignment = align; }
private:
	//GUISkin() = default;

	TextAlignment alignment = MiddleCenter;
	SGD::Color textColor;
	SGD::Color backColor;
	SGD::Color hoverColor;
	const BitmapFont *font;//protect font
	
	bool hoverEnabled;
};

