#pragma once
#include "EnemyAI.h"

class FollowAI : public EnemyAI
{
public:

	//ctor and dtor
	FollowAI();
	virtual ~FollowAI();

	//update -- MUST OVERRIDE
	virtual void Update(float dt, AIStamp& stamp);
	//AI identifier
	virtual EnemyAIType GetAIType() const { return EnemyAIType::Follow; }
};



