#include "..\Source\Game.h"
#include "..\tinyxml\tinyxml.h"
#include "BitmapFont.h"
#include "CharacterGameState.h"
#include "MenuGameState.h"
#include "PlayGameState.h"
#include "LevelGameState.h"
#include "SaveLoadState.h"
#include <sstream>
#include "GameInfo.h"

#include "GUI.h"
#include "GUIGroup.h"

SaveLoadState* SaveLoadState::pInstance = nullptr;
SaveLoadState* SaveLoadState::GetInstance(void)
{
	if (pInstance == nullptr)
		pInstance = new SaveLoadState;

	return pInstance;
}
void SaveLoadState::DeleteInstance(void)
{
	delete pInstance;
	pInstance = nullptr;
}

void SaveLoadState::Enter()
{
	GameInfo::GetInstance()->Terminate();

	//GUI Font
	font = GUIManager::GetInstance()->GetFont();
	//local SGD Instances
	pGraphics = SGD::GraphicsManager::GetInstance();
	pAudio = SGD::AudioManager::GetInstance();
	pInput = SGD::InputManager::GetInstance();
	//background image
	backGround = pGraphics->LoadTexture(L"resource/Graphics/placeholder images/map.png");
	currstate = states::game;

	gameButtons = new ButtonGroup(0, { 270, 200 }, 2.f);
	gameButtons->AddButton("New Game");
	gameButtons->AddButton("Load Game");

	slotone = GameInfo::GetInstance()->CheckProFile("slotone", 0);
	slottwo = GameInfo::GetInstance()->CheckProFile("slottwo", 1);
	slotthree = GameInfo::GetInstance()->CheckProFile("slotthree", 2);

}
void SaveLoadState::Exit()
{
	Game::GetInstance()->SetMasterSlot(slotPick);
	pGraphics->UnloadTexture(backGround);

	delete gameButtons;
	gameButtons = nullptr;

	for (size_t x = 0; x < SavedInfo.size(); x++)
	{
		for (size_t m = 0; m < SavedInfo[x].size(); m++)
		{
			delete SavedInfo[x][m];
		}
	}
	DeleteInstance();
}

void SaveLoadState::ArcadeCursorMovement(float dt)
{
	SGD::Vector dir = pInput->GetLeftJoystick(0);
	if (dir.x != 0.0f)
	{
		timer += dt;
		if (currstate == states::game)
		{
			if (timer > timerFreq)
			{
				timer = 0.f;
				if (dir.x < 0.0f)
				{
					timer = 0.0f;
					cursor--;
					if (cursor < 0)
						cursor = 1;
				}
				else if (dir.x > 0.0f)
				{
					timer = 0.f;
					cursor++;
					if (cursor > 1)
						cursor = 0;
				}

			}
		}
		else if (currstate == states::whatSlot)
		{
			if (timer > timerFreq)
			{
				timer = 0.f;
				if (dir.x < 0.0f)
				{
					timer = 0.0f;
					cursor--;
					if (cursor < 0)
						cursor = 2;
				}
				else if (dir.x > 0.0f)
				{
					timer = 0.f;
					cursor++;
					if (cursor > 2)
						cursor = 0;
				}
			}
		}
	}
}

bool SaveLoadState::Update(float dt)
{
	if (pInput->IsKeyPressed(SGD::Key::Escape) || pInput->IsButtonPressed(0, 2))
	{
		Game::GetInstance()->ChangeState(MenuGameState::GetInstance());
		return true;
	}
	if (Game::GetInstance()->ARCADEMODE)
	{
		ArcadeCursorMovement(dt);
	}
	else if (!Game::GetInstance()->ARCADEMODE)
	{
		if (currstate == states::game)
		{
			cursor = gameButtons->Update(dt);
			if (cursor != -1)
			{
				currstate = states::whatSlot;
				loading = (cursor == 1);
				cursor = 0;
				return true;
			}
		}
		else if (currstate == states::whatSlot)
		{
			if (pInput->IsKeyPressed(SGD::Key::LeftArrow) == true || pInput->IsDPadPressed(0, SGD::DPad::Left))
			{
				cursor--;
				if (cursor < 0)
					cursor = 2;
			}
			if (pInput->IsKeyPressed(SGD::Key::RightArrow) == true || pInput->IsDPadPressed(0, SGD::DPad::Right))
			{
				cursor++;

				if (cursor > 2)
					cursor = 0;
			}
		}
	}

	if (pInput->IsKeyPressed(SGD::Key::Enter) == true || pInput->IsButtonPressed(0, 1))
	{
		if (currstate == states::game)
		{
		}
		else
		{
			slotPick = cursor;
			if (loading)
			{
				switch (slotPick)
				{
				case 0:
					if (slotone)
					{
						LoadProfile("slotone");
						Game::GetInstance()->ChangeState(LevelGameState::GetInstance());
						return true;
					}
					break;
				case 1:
					if (slottwo)
					{
						LoadProfile("slottwo");
						Game::GetInstance()->ChangeState(LevelGameState::GetInstance());
						return true;
					}
					break;
				case 2:
					if (slotthree)
					{
						LoadProfile("slotthree");
						Game::GetInstance()->ChangeState(LevelGameState::GetInstance());
						return true;
					}
					break;
				default:
					break;
				}
			}
			else
				Game::GetInstance()->ChangeState(CharacterGameState::GetInstance());
		}

	}

	return true;
}
void SaveLoadState::Render(float dt)
{
	pGraphics->DrawTexture(backGround, { 0.0f, 0.0f }, {}, {}, {}, { 4.0f, 4.0f });

	float width = Game::GetInstance()->GetScreenWidth();

	switch (currstate)
	{
	case SaveLoadState::game:
		gameButtons->DrawGroup();
		break;
	case SaveLoadState::whatSlot:
		if (loading)
			font->DrawXML("What slot would you like to load?", { 200.0f, 10.0f }, 1.0f, SGD::Color{ 0, 0, 0 });
		else
			font->DrawXML("Pick a slot you would like to save to", { 200.0f, 10.0f }, 1.0f, SGD::Color{ 0, 0, 0 });
		
		if (slotone)
			font->DrawXML("Slot One", { 50, 200 }, 1.0, SGD::Color{ 0, 0, 0 });
		else
			font->DrawXML("EMPTY", { 50, 200 }, 1.0, SGD::Color{ 0, 0, 0 });

		if (slottwo)
			font->DrawXML("Slot Two", { 300, 200 }, 1.0, SGD::Color{ 0, 0, 0 });
		else
			font->DrawXML("EMPTY", { 300, 200 }, 1.0, SGD::Color{ 0, 0, 0 });

		if (slotthree)
			font->DrawXML("Slot Three", { 550, 200 }, 1.0, SGD::Color{ 0, 0, 0 });
		else
			font->DrawXML("EMPTY", { 550, 200 }, 1.0, SGD::Color{ 0, 0, 0 });

		font->DrawXML("X", { 245.0f * cursor + 25, 200 }, 1.0f,
		{ 0, 0, 0 });

		DisplaySavedInfo(slotone, slottwo, slotthree);

		break;
	case SaveLoadState::none:
		break;
	default:
		break;
	}
}

bool SaveLoadState::LoadProfile(const char* profile)
{
	return GameInfo::GetInstance()->LoadProfile(profile);
}

void SaveLoadState::WhichProfile(void)
{
	if (pInput->IsKeyPressed(SGD::Key::RightArrow) == true || pInput->IsDPadPressed(0, SGD::DPad::Right))
	{
		slotPick++;
		if (slotPick > 2)
			slotPick = 0;
	}
	if (pInput->IsKeyPressed(SGD::Key::LeftArrow) == true || pInput->IsDPadPressed(0, SGD::DPad::Left))
	{
		slotPick--;
		if (slotPick < 0)
			slotPick = 2;
	}
	if (pInput->IsKeyPressed(SGD::Key::Enter) == true || pInput->IsButtonPressed(0, 1))
	{

		if (slotPick == 0 && slotone)
		{
			Game::GetInstance()->ChangeState(CharacterGameState::GetInstance());
			falsePositive = true;
		}
		else if (slotPick == 1 && !slottwo)
		{
			Game::GetInstance()->ChangeState(CharacterGameState::GetInstance());
			falsePositive = true;
		}
		else if (slotPick == 2 && slotthree)
		{
			Game::GetInstance()->ChangeState(CharacterGameState::GetInstance());
			falsePositive = true;
		}


	}
}

void SaveLoadState::DisplaySavedInfo(bool slot1, bool slot2, bool slot3)
{
	if (slot1)
	{
		int size = SavedInfo[0].size();
		std::string names[3];
		int lvl[3];
		for (int x = 0; x < 3; x++) // null out the info the check against later
		{
			names[x] = "dummy";
			lvl[x] = -1;
		}
		for (int x = 0; x < size; x++)//grab all the info saved
		{
			names[x] = SavedInfo[0][x]->GetCharacterName();
			lvl[x] = SavedInfo[0][x]->GetPlayerLevel();
		}
		std::stringstream stream;
		//check to see if the info is nulled out
		if (names[0] != "dummy")
			stream << names[0] << " Level : " << (lvl[0]) << "\n";
		if (names[1] != "dummy")
			stream << names[1] << " Level : " << (lvl[1]) << "\n";
		if (names[2] != "dummy")
			stream << names[2] << " Level : " << (lvl[2]);
		//show that shit to the screen
		if (names[0] != "dummy")
			font->DrawXML(stream.str().c_str(), SGD::Point{ 10, 250 }, 1.0f, SGD::Color{ 0, 0, 0 });

	}
	if (slot2)
	{
		int size = SavedInfo[1].size();
		std::string names[3];
		int lvl[3];
		for (int x = 0; x < 3; x++) // null out the info the check against later
		{
			names[x] = "dummy";
			lvl[x] = -1;
		}
		for (int x = 0; x < size; x++)
		{
			names[x] = SavedInfo[1][x]->GetCharacterName();
			lvl[x] = SavedInfo[1][x]->GetPlayerLevel();
		}
		std::stringstream stream;

		if (names[0] != "dummy")
			stream << names[0] << " Level : " << (lvl[0]) << "\n";
		if (names[1] != "dummy")
			stream << names[1] << " Level : " << (lvl[1]) << "\n";
		if (names[2] != "dummy")
			stream << names[2] << " Level : " << (lvl[2]);
		if (names[0] != "dummy")
			font->DrawXML(stream.str().c_str(), SGD::Point{ 270, 250 }, 1.0f, SGD::Color{ 0, 0, 0 });

	}
	if (slot3)
	{
		int size = SavedInfo[2].size();
		std::string names[3];
		int lvl[3];
		for (int x = 0; x < 3; x++) // null out the info the check against later
		{
			names[x] = "dummy";
			lvl[x] = -1;
		}
		for (int x = 0; x < size; x++)
		{
			names[x] = "dummy";
			names[x] = SavedInfo[2][x]->GetCharacterName();
			lvl[x] = -1;
			lvl[x] = SavedInfo[2][x]->GetPlayerLevel();

		}
		std::stringstream stream;

		if (names[0] != "dummy")
			stream << names[0] << " Level : " << (lvl[0]) << "\n";
		if (names[1] != "dummy")
			stream << names[1] << " Level : " << (lvl[1]) << "\n";
		if (names[2] != "dummy")
			stream << names[2] << " Level : " << (lvl[2]);
		if (names[0] != "dummy")
			font->DrawXML(stream.str().c_str(), SGD::Point{ 515, 250 }, 1.0f, SGD::Color{ 0, 0, 0 });

	}
}


