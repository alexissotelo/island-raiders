#pragma once
#include "Player.h"
#include "Missile.h"
#include "Entity.h"

class Alchemist : public Player
{
public:
	virtual void   Update(float dt)	 override;
	virtual bool HandleCollision(const IEntity* other)	 override;
	virtual void HandleEvent(const SGD::Event* pEvent);

	virtual unsigned int  GetPlayerType() const override { return (int)PlayerType::alchemist; }
	bool UpdateCombo(float dt);

	Alchemist(const char*);
	Alchemist(const char* name, unsigned int controllerNumber);

	virtual ~Alchemist();

	const CharacterStats* GetOriginalStats(void) const { return tempStats; }
	void StoreOriginalStats(CharacterStats& stats);
	void ClearTempStats(void);
	const char* GetOriginalName(void) const { return originalName; }
	void ClearOriginalName(void) { delete [] originalName; originalName = nullptr; }

private:

	float healPercent = 0.2f;
	CharacterStats* tempStats = nullptr;
	char* originalName = nullptr;
};

