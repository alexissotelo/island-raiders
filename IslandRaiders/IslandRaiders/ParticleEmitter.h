#pragma once
#include "Particle.h"
#include "ParticleFlyWeight.h"
#include <vector>

class ParticleEmitter
{
	friend class Particle;
private:
	ParticleFlyWeight *particleFlyWeight;
	bool emit = false;
	unsigned int enabledCount = 0;

	float spawnRate;
	float spawnTimer = 0.f;
	int maxParticles;
	std::vector<Particle*> pool;

	SGD::Size size;
	SGD::Point position;
	SGD::Vector velocity;

	//animation
	float emitTimer = 0.f;
	float emitTime = -1;//-1 == infinite

	float deathTimer = 0.f;
	bool  playOnce;//destroys after emit time is over

public:
	ParticleEmitter(ParticleFlyWeight* flyweight, int _maxParticles, float _spawnRate, SGD::Size siz = {8,8});
	ParticleEmitter(int FlyweightIndex, int _maxParticles, float _spawnRate, SGD::Size siz = { 8, 8 });
	~ParticleEmitter();

	bool Update(float dt);
	void Render();

	//acc
	void SetPosition(SGD::Point pos) { position = pos; }

	//mutater
	void Emit(bool _emit){ emit = _emit; }
	//emit for a period of time
	void Emit(float time = 1.f, bool dieAfter = true){ 
		emit = true;
		emitTime = time;
		playOnce = dieAfter;
	}
};

