#pragma once
#include "EnemyAI.h"
//store and define all abilities for the AI
//allocate based on either the name of the charctaer 
//or the name of the skill**
//should have an update function as well as a
//function to allocate function pointers serving as a look uptable

class Enemy;
class AISkill
{
	//defaults
	AISkill() = default;
	~AISkill() = default;

	//deletes
	AISkill(const AISkill&) = delete;
	AISkill& operator=(const AISkill&) = delete;

	//instance
	static AISkill* pInstance;

public:

	//singleton instance
	static AISkill* GetInstance(void);
	static void DeleteInstance(void);

	//update
	bool Update(float dt, Enemy* enemy, EnemySkill* enemySkill);

	//return a function pointer
	void FindSkill(const char* actionName, bool(**funcPtr)(float, Enemy*, EnemySkill* enemySkill));

private:
	//abilities
	static bool PlaceHolder(float dt, Enemy* ptr, EnemySkill* enemySkill);
	static bool HealParty(float dt, Enemy* ptr, EnemySkill* enemySkill);
	static bool HealMember(float dt, Enemy* ptr, EnemySkill* enemySkill);
	static bool Charge(float dt, Enemy* ptr, EnemySkill* enemySkill);
	static bool SandWarp(float dt, Enemy* ptr, EnemySkill* enemySkill);
	static bool GenericSkillOne(float dt, Enemy* ptr, EnemySkill* enemySkill);
	static bool GenericSkillTwo(float dt, Enemy* ptr, EnemySkill* enemySkill);
	static bool GenericSkillThree(float dt, Enemy* ptr, EnemySkill* enemySkill);
};

