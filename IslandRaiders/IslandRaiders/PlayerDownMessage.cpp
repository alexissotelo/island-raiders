#include "PlayerDownMessage.h"
#include "Player.h"
#include "MessageID.h"

PlayerDownMessage::PlayerDownMessage(Player* p) 
: SGD::Message(MessageID::MSG_PLAYER_DOWN)
{
	player = p;
	player->AddRef();
}


PlayerDownMessage::~PlayerDownMessage()
{
	player->Release();
}

Player* PlayerDownMessage::GetSender(void) const { return player; }

