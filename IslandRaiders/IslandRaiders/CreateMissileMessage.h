#pragma once
#include "../SGD Wrappers/SGD_Geometry.h"
#include "..\SGD Wrappers\SGD_Message.h"
#include "Missile.h"
#include <string>


class Character;

class CreateMissileMessage : public SGD::Message
{
public:
	//overloaded constructor
	CreateMissileMessage(Character* owner, SGD::Point spawn, ProjectileType _type = ProjectileType::NORMAL, std::string _name = "missile",
		float _speedPercentage = 2.0f, float _timer = 3.5f, SGD::Vector _vel = SGD::Vector(0,0));
	//destructor
	~CreateMissileMessage();

	Character* GetOwner(void) const { return postMan; }
	SGD::Point GetSpawnPoint(void) const { return ptSpawn; }
	ProjectileType GetType(void)const { return type; }
	std::string GetName(void) const { return name; }
	float GetTime(void) const { return timer; }
	float GetMovementAmount(void) const { return moveSpeed; }
	SGD::Vector GetVelocity(void) const { return m_vVelocity; }
	 
private:
	Character* postMan;
	SGD::Point ptSpawn;
	SGD::Vector m_vVelocity;
	ProjectileType type;
	std::string name; 
	float timer;
	float moveSpeed;
};

