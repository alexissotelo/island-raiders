#pragma once
#include "IGameState.h"
#include <vector>
#include <string>
#include "../SGD Wrappers/SGD_AudioManager.h"
#include "../SGD Wrappers/SGD_GraphicsManager.h"
#include "../SGD Wrappers/SGD_InputManager.h"

class BitmapFont;
class ButtonGroup;
struct DisplaySave
{
	std::string name;
	int lvl;

};

class SaveLoadState : public IGameState
{
public:
	static SaveLoadState* GetInstance(void);
	static void DeleteInstance(void);

	virtual void Enter();
	virtual void Exit();
	virtual bool Update(float dt);
	virtual void Render(float dt);
	int GetSlotPick(void)		const { return slotPick; }
	bool GetLoading(void)		const { return loading; }
	bool GetFalsePositive(void) const { return falsePositive; }
	void  DisplaySavedInfo(bool slot1, bool slot2, bool slot3) ;
	std::vector<std::vector<PlayerInfo*>> SavedInfo;
	void ArcadeCursorMovement(float dt);
	float timer = 0.0f;
	float timerFreq = 0.3f;
private:

	enum states
	{
		game, whatSlot, none
	};
	ButtonGroup* gameButtons;

	SaveLoadState() = default;
	~SaveLoadState() = default;
	SaveLoadState(const SaveLoadState&) = delete;
	SaveLoadState operator=(const SaveLoadState&) = delete;

	static SaveLoadState* pInstance;

	int slotPick = 0;
	int cursor = 0;
	bool loading = false;
	bool bNewGame = false;
	//function used to load in profile 
	bool LoadProfile(const char* profile);
	void WhichProfile(void);
	bool slotone = false;
	bool slottwo = false;
	bool slotthree = false;
	bool falsePositive = false;
	states currstate;
	SGD::GraphicsManager* pGraphics = nullptr;
	SGD::AudioManager* pAudio = nullptr;
	SGD::InputManager* pInput = nullptr;
	SGD::HTexture backGround = SGD::INVALID_HANDLE;
	const BitmapFont* font = nullptr;

	SGD::HAudio BackgroundMusic = SGD::INVALID_HANDLE;

	int nCursor = 0;
};

