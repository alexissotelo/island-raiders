#pragma once
#include "IGameState.h"
#include <vector>
#include "Character.h"
#include "../SGD Wrappers/SGD_Declarations.h"
#include "Missile.h"
#include "Camera.h"

class LevelManager;
class LevelSection;
class EntityManager;
class BitmapFont;
class Entity;
class Player;
class AnimatedParticle;
class Familiar;
class PlayerInfo;

class ParticleEmitter;
#define MAX_LIVES 3
#define CHEAT_WIN 0

class PlayGameState : public IGameState
{
	ParticleEmitter *weather = nullptr;
	friend class Camera;

public:
	static PlayGameState* GetInstance(void);
	static void DeleteInstance(void);

	virtual void Enter();
	virtual void Exit();
	virtual bool Update(float dt);
	virtual void Render(float dt);

	const std::vector<Player*>& GetParty() const { return party; }
	Player* GetPartyMember(int index) { return party[index]; }
	void ArcadeCursorMovement(float dt);

	//accessor
	EntityManager* GetManager() const { return entityManager; }// no mutator only one
	/**********************************************************/
	// Message Callback Function:
	static void MessageProc(const SGD::Message* pMsg);
	void UnloadTexture(void);

	SGD::Point GetScreenPosition(SGD::Point worldPosition);

	void SetBackgroundImage(SGD::HTexture img){ backgroundImage = img; }
	void ClearCharacterTargets(Character*);

private:
	float timer = 0.0f;
	float timerFreq = 0.3f;
	//if u leave the game this bool cleans up memory
	bool leavingGame = false;
	//Current level counter
	unsigned int CurrentLevel;
	// bool to control if we are going to skill selection state or not
	bool nextLevelPlox = false;

	//SGD::Point pCam;
	SGD::Vector camDistance;
	Camera *camera;
	SGD::Vector playerDistance;

	bool gameIsSaved = false;
	float saveTimer = 0.0f;

	const BitmapFont* font = nullptr;
	SGD::GraphicsManager* pGraphics = nullptr;
	SGD::AudioManager* pAudio = nullptr;
	SGD::InputManager* pInput = nullptr;
	LevelManager* levelManager = nullptr;
	EntityManager*	entityManager = nullptr;

	PlayGameState() = default;
	~PlayGameState() = default;
	PlayGameState(const PlayGameState&) = delete;
	PlayGameState operator=(const PlayGameState&) = delete;

	static PlayGameState* pInstance;

	//party variables
	std::vector<Player*> party;
	unsigned int partyLives = MAX_LIVES;
	SGD::HTexture hearts;

	//GUI//end of game condition
	short EOG = 0; //+1 = win, -1 = lose, 0 = gameplay in session
	float eogTimer;
	int	  nCursor = 0;
	int	  cursorMax;

	//pause
	int pausedCursor = 0;

	//party functions
	void AddPlayer(Player* _player);
	void UpdateSkills(Player* player, const char* className);

	//Character functions and variables
	Player* CreateSwordsman	(const char* name, PlayerInfo* playerInfo);
	Player* CreateGunner	(const char* name, PlayerInfo* playerInfo) const;
	Player* CreateAlch		(const char* name, PlayerInfo* playerInfo) const;

	//projectile factory methods
	Familiar* CreateFamiliarPartner(const char* name, Character* owner, SGD::Point point);
	Missile* CreateMissile(Character* c, SGD::Point pos, ProjectileType type = ProjectileType::NORMAL, 
		std::string n = "missile", float move = 2.0f, float time = 3.5f, SGD::Vector vec = SGD::Vector(0,0));
	//animated particles
	AnimatedParticle* CreateAnimateParticle(Character* _owner, std::string _name, float _span, SGD::Color clr, SGD::Point pt, SGD::Vector vec);

	SGD::Point lerp(float t, SGD::Point a, SGD::Point b);

	SGD::HTexture backgroundImage = SGD::INVALID_HANDLE;
//	SGD::HTexture starIMG = SGD::INVALID_HANDLE;
	SGD::HTexture itemIMG = SGD::INVALID_HANDLE;
	
	//level sections
	int enemiesLeft = 0;

	void ResetLevel();
	bool loadWasNotGood = false;

public:
	//Camera Functions
	SGD::Size  GetCameraSize()	  const;
	SGD::Point GetCameraPosition()const;
	//LevelSection* GetCurrentSection()const;
	int GetCurrentSectionIndex()const;
	bool IsCameraLocked();

	void LockCamera(); 

	void	ClearPlayerParty(void);

	private:
		//load into audio queue
		bool LoadBackgroundMusic(unsigned int _level);
};

