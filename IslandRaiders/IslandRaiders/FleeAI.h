#pragma once
#include "EnemyAI.h"

class FleeAI : public EnemyAI
{
public:
	FleeAI() = default;
	~FleeAI() = default;

	void Update(float dt, AIStamp& _timeStamp) ;
	EnemyAIType GetAIType() const { return EnemyAIType::Flee; }

};

