
#include "Missile.h"
#include "..\Source\Game.h"
#include "DestroyEntityMessage.h"
#include "AnimationSystem.h"
#include "PlayGameState.h" //remove --for debug purposes only
#include "../SGD Wrappers/SGD_Event.h"
#include "../SGD Wrappers/SGD_EventManager.h"
#include "../SGD Wrappers/SGD_MessageManager.h"
#include "AnimatedParticle.h"
#include "ParticleManager.h"
#include "CreateAnimatedParticle.h"

Missile::Missile(const char* _name, unsigned int _projType, const char* _initAction) : Entity(_name)
{
	Listener::RegisterForEvent("MOVE");

	//pGraphics = SGD::GraphicsManager::GetInstance();
	//SetImage(picture);
	owner = nullptr;
	aTimeStamp.m_sOwnerName = _name;

	aTimeStamp.SwitchAnimation(_initAction);
	projType = _projType;
}


Missile::~Missile()
{
	if (trail)
	{
		ParticleManager::GetInstance()->AddEmitter(trail);//so the particle manager updates
		trail->Emit(0.5f, true);
		trail = nullptr;
	}

	SetOwner(nullptr);

}

void Missile::Update(float dt)
{
	if (trail)
	{
		trail->SetPosition({ position.x, position.y - size.height *0.5f });
		trail->Update(dt);
	}

	//update position & animation
	Entity::Update(dt);

	//die?
	if (OutOfWorld() || aTimeStamp.isDone || !owner)
	{
		DestroyEntityMessage* Msg = new DestroyEntityMessage(this);
		Msg->QueueMessage();
		Msg = nullptr;
	}

	//
	SGD::Point camPos = PlayGameState::GetInstance()->GetCameraPosition();
	float width = collision.right - collision.left;

	//collision relative to the anchor point
	collision.left = (aTimeStamp.bForwards) ? GetPosition().x : GetPosition().x - width;
	collision.right = collision.left + width;
}
void   Missile::Render()
{
	//aniamtions
	Entity::Render();
	if (trail)
	{
		trail->Render();
	}

	SGD::Point camPos = PlayGameState::GetInstance()->GetCameraPosition();
	if (Game::GetInstance()->bDebug)
	{
		float  width = collision.right - collision.left;
		float  height = collision.bottom - collision.top;

		if (width > 0 && height > 0)
		{
			SGD::Rectangle dbg;
			dbg.left = collision.left - camPos.x;
			dbg.top = collision.top - camPos.y;
			dbg.right = dbg.left + width;
			dbg.bottom = dbg.top + height;

			SGD::GraphicsManager::GetInstance()->DrawRectangle(dbg, SGD::Color(0, 0, 0, 0), { 255, 0, 255, 0 }, 2);

		}
	}

}
bool   Missile::HandleCollision(const IEntity* other)
{
	//do nothig to the owner
	if (GetOwner() == other || GetOwner()->GetType() == other->GetType()  || this == other || !GetOwner())
		return true;

	const Missile* missilePtr = dynamic_cast<const Missile*>(other);
	if (missilePtr && missilePtr->GetOwner() &&missilePtr->GetOwner()->GetType() == this->GetOwner()->GetType())
		return true;


	const AnimatedParticle* animPtr = dynamic_cast<const AnimatedParticle*>(other);

	if (animPtr && animPtr->GetOwner() && animPtr->GetOwner()->GetType() == this->GetOwner()->GetType())
	if (animPtr->GetName() != "indicator" && animPtr->GetName() != "target" && animPtr->GetName() != "torch")
		return true;

	if (other->GetType() == ENT_INVULNERABLE)
		return true;

	if (other->GetType() != EntityType::ENT_MISSILE)
	{
		//pink missile explode
		if (GetName() == "pinkmissile")
		{
			const Entity* ptr = dynamic_cast<const Entity*>(other);
			if (ptr->GetName() != "indicator")
				return true;
		}

		//break? - fix for beams
		float totalDamage = /*(this->projType == ProjectileType::BEAM) ? -damage :*/ damage;

		//create a damage event
		const SGD::Event dmg = { "DAMAGE", &damage, this };
		//send it out
		dmg.SendEventNow(other);

		//marksman
		if (GetName() == "blackpellet")
		{
			//create a target set to the opponents position
			CreateAnimatedParticle* msg = new CreateAnimatedParticle((Character*)other, "target", 10.f, { 150, 255, 255, 255 });
			//queue a message
			SGD::MessageManager::GetInstance()->QueueMessage(msg);
		}

		else if (GetName() == "bugspike")
		{
			//create a target set to the opponents position
			CreateAnimatedParticle* msg = new CreateAnimatedParticle((Character*)other, "bugburn", 10.f, { 255, 255, 255, 255 });
			//queue a message
			SGD::MessageManager::GetInstance()->QueueMessage(msg);

			//stun the player
			float stunTime = 5.0f;///stun player
			SGD::Event evnt = { "STUN", &stunTime, this };
			evnt.SendEventNow(other);

		}
	}

		//self destruct
		DestroyEntityMessage* Msg = new DestroyEntityMessage(this);
		//queue message
		Msg->QueueMessage();
		Msg = nullptr;

		return false;
}
void   Missile::SetOwner(Character* c)
{
	if (owner)
	{
		owner->Release();
		owner = nullptr;
}
	//set pointer
	owner = c;

	if (owner)
		owner->AddRef();
}

bool Missile::OutOfWorld()
{
	SGD::Point camPos = PlayGameState::GetInstance()->GetCameraPosition();

	if (position.x - camPos.x < 0 ||
		position.x - camPos.x > Game::GetInstance()->GetScreenWidth() ||
		/*position.y - camPos.y < 0 ||*/
		position.y - camPos.y > Game::GetInstance()->GetScreenHeight())
		return true;
	else
		return false;
}

SGD::Rectangle Missile::GetCollisionRect(void) const
{
	if (collision.top != collision.bottom || collision.left == collision.right)
	return collision;
	else return Entity::GetCollisionRect();
}

SGD::Rectangle Missile::GetActiveRect(void) const
{
	if (collision.top != collision.bottom && collision.left != collision.right)
	return collision;
	else return Entity::GetActiveRect();
}

void Missile::SetActiveRect(SGD::Rectangle rect) //collision rectangle of the sender
{
	SGD::Point camPos = PlayGameState::GetInstance()->GetCameraPosition();

	//size
	float width = rect.right - rect.left;
	float height = rect.bottom - rect.top;

	//collision relative to the anchor point
	collision.left = (aTimeStamp.bForwards) ? GetPosition().x : GetPosition().x - width;
	collision.top = rect.top;
	collision.right = collision.left + width;
	collision.bottom = collision.top + height;

	//offset
	ptOffset.x = collision.left - GetPosition().x;
	ptOffset.y = collision.top - GetPosition().y;

	SGD::Rectangle dbg;
	dbg.left = GetPosition().x + ptOffset.x;
	dbg.top = GetPosition().y + ptOffset.y;
	dbg.right = dbg.left + width;
	dbg.bottom = dbg.top + height;

	//
	collision = dbg;

}

void Missile::HandleEvent(const SGD::Event* pEvent)
{
	if (pEvent->GetEventID() == "MOVE")
	{
		//move 
		SGD::Vector forwards = SGD::Vector(1, 0);
		//directions
		if (!aTimeStamp.bForwards)
			forwards.x *= -1;

		//scale
		forwards *= 600;

		//set velocity
		SetVelocity(forwards);

	}
}

/***********************************************************************/
/************************** LASER  CLASS *******************************/
/***********************************************************************/
Laser::Laser(const char* _name, float destruct, unsigned int _proj, const char* _initAction) : Missile(_name, _proj, _initAction)
{
	//default
	fSelfDestruct = 1.5f;// destruct;
}

void Laser::Update(float dt)
{
	//self destruct timer
	fSelfDestruct -= dt;

	//damage counter
	fDeltaDamage = damage * dt;


	//update animations
	AnimationSystem::GetInstance()->Update(dt, &aTimeStamp);

	if (aTimeStamp.isDone && aTimeStamp.m_sAction == "create")
		aTimeStamp.SwitchAnimation("animate");


	//update position
	Entity::Update(dt);



	if (OutOfWorld() || fSelfDestruct <= 0.0f)
	{
		DestroyEntityMessage* Msg = new DestroyEntityMessage(this);
		Msg->QueueMessage();
		Msg = nullptr;
	}

	SGD::Point camPos = PlayGameState::GetInstance()->GetCameraPosition();
	float width = collision.right - collision.left;
	//collision relative to the anchor point
	collision.left = (aTimeStamp.bForwards) ? GetPosition().x : GetPosition().x - width;
	collision.right = collision.left + width;
}

bool Laser::HandleCollision(const IEntity* other)
{
	//Missile::HandleCollision(other);
	//do nothig to the owner
	if (GetOwner() == other || GetOwner()->GetType() == other->GetType() || other->GetType() == this->GetType())
		return true;

	//if (other->GetType() == EntityType::ENT_MISSILE )
	//{
	//	Missile* otr = (Missile*)other;
	//	if (otr->GetOwner() == this->GetOwner())
	//		return;

	//	//self destruct
	//	DestroyEntityMessage* Msg = new DestroyEntityMessage(this);
	//	//queue message
	//	Msg->QueueMessage();
	//	Msg = nullptr;

	//}

	if (other->GetType() != EntityType::ENT_MISSILE)
	{
		float stunDamage = -fDeltaDamage;

		if (GetOwner()->GetTimeStamp()->m_sAction == "hammer")
		{

			Character* ptr = (Character*)other;
			ptr->GetCharacterStatus()->StartEffect(Status::STUN, 7.0f, damage);
			return true;
		}
		else
		{
			//create a damage event
			const SGD::Event dmg = { "DAMAGE", &stunDamage, this };
			//send it out
			dmg.SendEventNow(other);

		}
	}
	return true;
}


