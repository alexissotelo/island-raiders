#include "GUI.h"
#include "../Source/Game.h"
#include "../SGD Wrappers/SGD_InputManager.h"
#include "../SGD Wrappers/SGD_GraphicsManager.h"
#include "LoadingManager.h"
#include "PlayGameState.h"

namespace {

	float RandomFloat(float min, float max)
	{
		return min + static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / (max - min)));;
	}

	SGD::Point PointLerp(SGD::Point a, SGD::Point b, float t)
	{
		//return (1 - t)* a + t*b;
		float x = 1 - t;
		a.x *= x;
		a.y *= x;
		b.x *= t;
		b.y *= t;
		SGD::Point p = { a.x + b.x, a.y + b.y };
		return p;
	}

	SGD::Vector GetRandomVector(SGD::Vector range)
	{
		return{
			RandomFloat(-range.x, range.x),
			RandomFloat(-range.y, range.y) };
	}
}

/***************GUI MANAGER********************/
GUIManager* GUIManager::s_pInstance = nullptr;
GUIManager* GUIManager::GetInstance(void)
{
	if (s_pInstance == nullptr)
		s_pInstance = new GUIManager;
	return s_pInstance;
}
void GUIManager::DeleteInstance(void)
{
	delete s_pInstance;
	s_pInstance = nullptr;
}
//By default currSkin: index = 0
void GUIManager::Initialize(void)
{
	s_pInstance->fonts.push_back(new BitmapFont("pirateXML"));
	s_pInstance->fonts.push_back(new BitmapFont("fontPopUp"));
	s_pInstance->fonts.push_back(new BitmapFont("fontBoom"));

	s_pInstance->AddSkin(new GUISkin(
		s_pInstance->fonts[0], { 0, 0, 0 }, { 0, 255, 255, 255 }, { 255, 255, 0 }));//menu
	s_pInstance->AddSkin(new GUISkin(
		s_pInstance->fonts[1], { 255, 255, 255 }, { 0, 0, 0, 0 }, { 255, 255, 0 }));//hits
	s_pInstance->AddSkin(new GUISkin(
		s_pInstance->fonts[2], { 255, 255, 255 }, { 0, 0, 0, 0 }, { 255, 255, 0 }));//double kill

	//inform a thread is done
	ThreadData* threadData = ThreadData::GetInstance();
	threadData->countMutex.lock();
	--threadData->numberOfThreads;
	threadData->countCondition.notify_all();
	threadData->countMutex.unlock();
}
void GUIManager::Terminate()
{
	ClearPopUpMessages();

	for (unsigned int i = 0; i < fonts.size(); i++)
	{
		delete fonts[i];
		fonts[i] = nullptr;
	}
	fonts.clear();

	for (unsigned int i = 0; i < skins.size(); i++)
	{
		delete skins[i];
		skins[i] = nullptr;
	}
	skins.clear();
}

//pop up messages
#pragma region POP_UPS
void GUIManager::ClearPopUpMessages()
{
	std::vector<PopUp*>::iterator iter = popups.begin();
	for (; iter != popups.end(); ++iter)
	{
		delete *iter;
		*iter = nullptr;
	}
	popups.clear();
}
void GUIManager::PopUpFade(
	std::string _text, SGD::Point _pos, float scale, float time, SGD::Color startColor)
{
	popups.push_back(new PopUp(GetSkin(1), _text, _pos, scale, time, startColor, { 0, 0, 0, 0 }, {}, PopUpEffect::UIFX_FADE));
}
void GUIManager::PopUpFlash(
	std::string _text, SGD::Point _pos, float scale, float time,
	SGD::Color aColor, SGD::Color bColor, SGD::Vector offsetVector)
{
	popups.push_back(new PopUp(GetSkin(2), _text, _pos, scale, time, aColor, bColor, offsetVector, PopUpEffect::UIFX_FLASH));
}
void GUIManager::PopUpBlinker(
	std::string _text, SGD::Point _pos, float scale, float blinkFreq,
	SGD::Color aColor, SGD::Color bColor)
{
	popups.push_back(new PopUp(GetSkin(1), _text, _pos, scale, blinkFreq,
		aColor, bColor, {}, PopUpEffect::UIFX_BLINK));
}
void GUIManager::PopUpDialog(
	std::string _text, SGD::Point _pos, float scale, float idleTime,
	SGD::Color color)
{
	PopUp *p = new PopUp(GetSkin(1), _text, _pos, scale, idleTime * _text.size(),
		color, { 0, 255, 255, 255 }, {}, PopUpEffect::UIFX_DIALOG);
	p->alignment = MiddleCenter;

	popups.push_back(p);
}
#pragma endregion
void GUIManager::Update(float dt)
{
	//std::vector<PopUp*>::iterator iter = popups.begin();
	//for (auto iter = popups.begin(); iter != popups.end(); /*empty*/)

	for (unsigned int i = 0; i < popups.size();)
	{
		//update popups
		PopUp* popup = popups[i];// *iter;
		if (popup->Update(dt))
		{
			delete popup;
			popup = nullptr;
			popups.erase(popups.begin() + i);
			continue;
		}

		popup->Draw();
		++i;
	}
}

//***************GUI FUNCTIONS****************/
//LABEL
//witdh and height will get calculated for you
Label::Label(std::string _text, SGD::Point _pos, float fontScale)
{
	skin = GUIManager::GetInstance()->GetSkin(0);

	text = _text;
	scale = fontScale;
	textLength = _text.size();
	fontSize.height = fontScale * skin->font->abc[32].height;//root fontsize * scale
	fontSize.width = fontScale * skin->font->abc[32].width;//root fontsize * scale

	rect.left = _pos.x;
	rect.right = rect.left + (textLength * fontSize.width);
	rect.top = _pos.y - 10;
	rect.bottom = rect.top + fontSize.height + 10;
}
Label::Label(std::string _text, SGD::Rectangle _rect, float fontScale)
{
	rect = _rect;
	skin = GUIManager::GetInstance()->GetSkin(0);

	text = _text;
	scale = fontScale;
	textLength = _text.size();
	fontSize.height = fontScale * skin->font->abc[32].height;//root fontsize * scale
	fontSize.width = fontScale * skin->font->abc[32].width;//root fontsize * scale
}
Label::Label(std::string _text, float fontScale/*opts*/)
{
	//calculate rect based on opts//rect = {l,t,r,b}
	skin = GUIManager::GetInstance()->GetSkin(0);

	text = _text;
	scale = fontScale;
	textLength = _text.size();
	fontSize.height = fontScale * skin->font->abc[32].height;//root fontsize * scale
	fontSize.width = fontScale * skin->font->abc[32].width;//root fontsize * scale
}
/*virtual*/ bool Label::Update(float dt)
{
	isHover = false;
	if (skin->hoverEnabled)
	{
		isHover = rect.IsPointInRectangle(
			SGD::InputManager::GetInstance()->GetCursorPosition());
	}
	return false;
}
void Label::Draw()
{
	//button
	SGD::Color backColor = skin->backColor;
	if (Game::GetInstance()->bDebug)
	{
		backColor.alpha = 255;
	}
	SGD::GraphicsManager::GetInstance()->DrawRectangle(rect, backColor);

	SGD::Point position;
	switch (alignment)
	{
	case MiddleCenter:
		position =
			//{ ((rect.left + rect.right) *0.5f) - (textLength * fontSize.width * 0.4f) +5,
			//(rect.top + rect.bottom) * 0.5f /*numberOfRows*/ - fontSize.height +5 };// middle center
		{ ((rect.left + rect.right) *0.5f) - (textLength * fontSize.width * 0.4f) + 5,
		(rect.top + rect.bottom) * 0.5f /*numberOfRows*/ - fontSize.height + 5 };// middle center
		break;
	case MiddleLeft:
		position =
		{ rect.left,
		(rect.top + rect.bottom) * 0.5f /*numberOfRows*/ - fontSize.height + 5 };
		break;
	}

	skin->font->DrawXML(text.c_str(), position, scale,
		(!isHover) ? skin->textColor : skin->hoverColor);
}

//HELPER FUNCTIONS
//will return a rect on the center <X axis> of the screen
void Label::SetMiddleRect(float posY)
{
	float screenWidth = Game::GetInstance()->GetScreenWidth();
	rect.left = (screenWidth - (textLength * fontSize.width)) / 2;
	rect.right = rect.left + (textLength * fontSize.width);
	rect.top = posY - 10;
	rect.bottom = rect.top + fontSize.height + 10;
}
//BUTTON
Button::Button(std::string text, float _scale)
	: Label(text, _scale){}
Button::Button(std::string _text, SGD::Rectangle _rect, float fontScale)
	: Label(_text, _rect, fontScale){}
Button::Button(std::string _text, SGD::Point _pos, float fontScale)
	: Label(_text, _pos, fontScale){}
bool Button::OnClick(float dt)
{
	Label::Update(dt);
	SGD::InputManager * pInput = SGD::InputManager::GetInstance();

	if (pInput->IsKeyPressed(SGD::Key::MouseLeft))
		if (rect.IsPointInRectangle(pInput->GetCursorPosition()))
		{
		return true;
		}
	return false;
}
//POP UP
PopUp::PopUp(const GUISkin* _skin, std::string _text, SGD::Point _pos, float fontScale, float time,
	SGD::Color _startColor, SGD::Color _endColor, SGD::Vector _utilVector, unsigned int fx)
	: Label(_text, _pos, fontScale)
{
	skin = _skin;
	effect = fx;
	if (effect != PopUpEffect::UIFX_DIALOG)
		currText = _text;

	utilVector = _utilVector;

	fadeTime = time;
	currColor = startColor = _startColor;
	endColor = _endColor;

	currPosition = startPosition = _pos;
	endPosition = { startPosition.x, startPosition.y - 20 };
}
/*virtual*/ void PopUp::Draw()
{
	float t = 0;
	if (timer > yieldTime)
	{
		t = (timer - yieldTime) / fadeTime;
	}

	SGD::Point camPos = PlayGameState::GetInstance()->GetCameraPosition();
	switch (effect)
	{
	case PopUpEffect::UIFX_FADE:
		currPosition = PointLerp(startPosition, endPosition, t);//left alligment
		currPosition.Offset(-camPos.x, -camPos.y);
		currColor = SGD::Color::Lerp(startColor, endColor, t);
		break;
	case PopUpEffect::UIFX_FLASH:
		if (timer > 0.15f * effectCounter)
		{
			effectCounter++;
			if (effectCounter % 2 == 0)
			{
				currColor = startColor;
			}
			else
			{
				currColor = endColor;
			}
			currPosition = startPosition;
			currPosition.Offset(GetRandomVector(utilVector));
			currPosition.Offset(-camPos.x, -camPos.y);
		}
		break;
	case PopUpEffect::UIFX_BLINK:
		if (timer > fadeTime)
		{
			yieldTime = 0.f;
			timer = 0.f;
			//swap colors
			SGD::Color aux = startColor;
			startColor = endColor;
			endColor = aux;
		}
		currPosition = PlayGameState::GetInstance()->GetScreenPosition(startPosition);
		currColor = SGD::Color::Lerp(startColor, endColor, t);
		break;
	case PopUpEffect::UIFX_DIALOG:
		currPosition = PlayGameState::GetInstance()->GetScreenPosition(startPosition);
		if ((unsigned int)effectCounter < text.size())
		{
			if (timer > fadeTime / text.size())
			{
				timer = 0.f;
				currText += text[effectCounter];
				++effectCounter;
			}
		}
		else
		{
			effect = PopUpEffect::UIFX_FADE;
			timer  = 0.f;
			fadeTime = 1.5f;
		}
		break;
	}

	if (alignment == MiddleCenter)
	{
		currPosition =
		{ currPosition.x - (textLength * fontSize.width * 0.4f) + 5,
		 currPosition.y};// middle center
	}

	skin->font->DrawXML(currText.c_str(), currPosition, scale, currColor);
}

bool PopUp::Update(float dt)
{
	//increase timer
	timer += dt;

	if (effect == UIFX_BLINK)
		return false;
	//pass time
	if (timer > fadeTime + yieldTime)
	{
		return true;//destroy
	}
	return false;
}


