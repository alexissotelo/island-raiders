#pragma once
#include "../SGD Wrappers/SGD_Color.h"
#include "../SGD Wrappers/SGD_Geometry.h"
#include "../SGD Wrappers/SGD_Message.h"
#include <string>

class Character;

class CreateAnimatedParticle : public SGD::Message
{
	Character* m_pOwner;
	std::string m_sName;
	SGD::Color m_clrRender;
	SGD::Point m_ptPosition;
	SGD::Vector m_vVelocity;
	float m_fLifeSpan;

public:
	CreateAnimatedParticle(Character* _owner, std::string _name, float _span, SGD::Color _clr, SGD::Point _pos = SGD::Point(0,0), SGD::Vector _vel = SGD::Vector(0,0));
	~CreateAnimatedParticle();

	std::string GetName(void) const		{ return m_sName; }
	Character* GetOwner(void) const		{ return m_pOwner; }
	float GetLifeSpan(void) const		{ return m_fLifeSpan; }
	SGD::Color GetColor(void) const		{ return m_clrRender; }
	SGD::Point GetPosition(void) const	{ return m_ptPosition; }
	SGD::Vector GetVelocity(void) const	{ return m_vVelocity; }
};
