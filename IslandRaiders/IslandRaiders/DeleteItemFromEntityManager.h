#pragma once
#include "..\SGD Wrappers\SGD_Message.h"

class Items;

class DeleteItemFromEntityManager : public SGD::Message
{
public:
	DeleteItemFromEntityManager(Items* item);
	virtual ~DeleteItemFromEntityManager();

	Items* GetOwner(void) const  { return postMan; }

private:
	Items* postMan;
};

