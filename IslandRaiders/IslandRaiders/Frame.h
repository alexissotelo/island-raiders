#pragma once
#include "..\SGD Wrappers\SGD_Geometry.h"
#include <string>

class Frame
{
public:

	std::string m_sEvent; //identifying event

	SGD::Point m_ptRenderOffset; //offest values from the anchor point for the render rectangle
	SGD::Point m_ptCollisionOffset; //offset valuse from the anchor point for the collision rectangle
	SGD::Point m_ptActiveOffset; //offset values from the anchor point for the active rectangle
	SGD::Rectangle m_rRender; //render rectangle
	SGD::Rectangle m_rCollision; //collision rectangle
	SGD::Rectangle m_rActive; //active rect
	SGD::Point m_ptAnchor;//anchor point
	SGD::Point m_ptSpawnOffset; //an offset from the anchor point on where to spawn something

	float m_fDuration; //duration of each frame
	
	
};

