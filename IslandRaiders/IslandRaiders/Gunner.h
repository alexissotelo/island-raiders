#pragma once
#include "Player.h"
#include "Missile.h"
class Gunner :
	public Player
{
public:
	virtual void   Update(float dt)	 override;
	virtual bool   HandleCollision(const IEntity* other)	 override;
	virtual void HandleEvent(const SGD::Event* pEvent);
	virtual unsigned int    GetPlayerType() const override { return (int)PlayerType::gunner; }

	//handle input
	bool UpdateCombo(float dt);

	Gunner(const char*);
	Gunner(const char* name, unsigned int controllerNumber);

	virtual ~Gunner();

	void SwitchSpecialCombo(bool special) { bSpecialCombo = special; }
private:
	short comboX = 1;
	bool bSpecialCombo = false;
};

