#include "DeleteItemFromEntityManager.h"
#include "MessageID.h"
#include "Items.h"

DeleteItemFromEntityManager::DeleteItemFromEntityManager(Items* item) : Message(MessageID::MSG_DELETE_ITEM_MANAGER)
{
	postMan = item;
	if (postMan != nullptr)
		postMan->AddRef();
}


DeleteItemFromEntityManager::~DeleteItemFromEntityManager()
{
	postMan->Release();
	postMan = nullptr;
}
