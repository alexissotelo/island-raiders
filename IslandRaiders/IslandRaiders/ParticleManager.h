#pragma once
#include "ParticleEmitter.h"
#include <list>
#include <vector>
#include <string>

enum ParticleFX
{
	PAR_BLIZZARD,PAR_BLOOD,PAR_FIRE,PAR_TRAIL1,PAR_SNOW,PAR_RAIN
};

class ParticleManager
{
	static ParticleManager*	s_pInstance;
	ParticleManager(void) = default;		// default constructor
	~ParticleManager(void) = default;		// destructor

	ParticleManager(const ParticleManager&) = delete;	// copy constructor
	ParticleManager& operator= (const ParticleManager&) = delete;	// assignment operator
	//SINGLETON-----------

	std::list<ParticleEmitter*> emitters;
	std::vector<ParticleFlyWeight*> flyweights;//stores all flyweights


public:
	static ParticleManager*	GetInstance(void);
	static void		DeleteInstance(void);
	static void Initialize();
	void Terminate();
	//SINGLETON------------

	void AddFlyWeight(ParticleFlyWeight *fly){ flyweights.push_back(fly); }
	ParticleFlyWeight* GetFlyWeight(int index) const { return flyweights[index]; }

	void ClearParticles();

	int AddEmitter( ParticleEmitter *emitter, bool emit = false);

	void UpdateEmitters(float dt);
	void RenderEmitters();
};

