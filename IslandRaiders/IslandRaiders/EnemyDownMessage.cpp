#include "EnemyDownMessage.h"
#include "Enemy.h"
#include "MessageID.h"

EnemyDownMessage::EnemyDownMessage(Enemy* owner) 
: Message(MessageID::MSG_ENEMY_DOWN)
{
	postMan = owner;

	if (postMan != nullptr)
		postMan->AddRef();
}


EnemyDownMessage::~EnemyDownMessage()
{
	postMan->Release();
	postMan = nullptr;
}
