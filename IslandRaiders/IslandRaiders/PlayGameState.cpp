#include "..\Source\Game.h"
#include "..\SGD Wrappers\SGD_Event.h"
#include "..\IslandRaiders\MessageID.h"
#include "..\tinyxml\tinyxml.h"

#include "..\SGD Wrappers\SGD_EventManager.h"
#include "..\SGD Wrappers\SGD_MessageManager.h"
#include "..\SGD Wrappers\SGD_AudioManager.h"
#include "GUI.h"

#include "../IslandRaiders/CreateEnemyMessage.h"
#include "..\IslandRaiders\MessageID.h"
#include "BitmapFont.h"

#include "GameInfo.h"
#include "CreateEnemyMessage.h"
#include "CreateItemMessage.h"
#include "CreateMissileMessage.h"
#include "DestroyEntityMessage.h"
#include "EnemyDownMessage.h"
#include "PlayerDownMessage.h"
#include "CreateAnimatedParticle.h"
#include "DeleteItemFromEntityManager.h"
#include "CreateFamiliar.h"

#include "Player.h"
#include "Enemy.h"
#include "Alchemist.h"
#include "Gunner.h"
#include "Swordsman.h"
#include "Missile.h"
#include "AnimatedParticle.h"
#include "AISkills.h"
#include "Items.h"
#include "Familiar.h"
#include "PlayerSkill.h"

#include "EntityManager.h"
#include "LevelManager.h"
#include "Status.h"

#include "CharacterGameState.h"
#include "MenuGameState.h"
#include "PlayGameState.h"
#include "SaveLoadState.h"
#include "SkillSelectionState.h"
#include "LevelGameState.h"
#include "CreditsGameState.h"

#include "ParticleEmitter.h"
#include "ParticleManager.h"
#include "AudioQueue.h"

#include <cassert>
#include <string>
#include <sstream>

#pragma comment (lib, "User32.lib")

#define DEFAULT_SPAWNPOSITION {250.0f, 350.0f}

namespace{
	float Clamp(float x, float min, float max)
	{
		if (x < min) x = min;
		else if (x > max) x = max;
		return x;
	}
	int Clamp(int x, int min, int max)
	{
		if (x < min) x = min;
		else if (x > max) x = max;
		return x;
	}
}

//HELPER FUNCTIONS
SGD::Point PlayGameState::GetScreenPosition(SGD::Point worldPosition)
{
	SGD::Point camPos = GetCameraPosition();
	return{ worldPosition.x - camPos.x, worldPosition.y - camPos.y };
}
SGD::Point PlayGameState::lerp(float t, SGD::Point a, SGD::Point b)
{
	//return (1 - t)* a + t*b;
	float x = 1 - t;
	a.x *= x;
	a.y *= x;
	b.x *= t;
	b.y *= t;
	SGD::Point p = { a.x + b.x, a.y + b.y };
	return p;

}

#pragma region INSTANCE_MANAGEMENT
//*************SINGLETON*******************
PlayGameState* PlayGameState::pInstance = nullptr;
PlayGameState* PlayGameState::GetInstance(void)
{
	if (pInstance == nullptr)
		pInstance = new PlayGameState;

	return pInstance;
}
void PlayGameState::DeleteInstance(void)
{

	delete pInstance;
	pInstance = nullptr;

}

//****************GAME STATE*************
void PlayGameState::Enter()
{
	//local manager pointers
	pGraphics = SGD::GraphicsManager::GetInstance();
	pAudio = SGD::AudioManager::GetInstance();
	pInput = SGD::InputManager::GetInstance();
	font = GUIManager::GetInstance()->GetFont();

	//instantiate a new manager -- stores all objects
	entityManager = new EntityManager;

	//instantiate a new level manager -- instantiates all world obj
	levelManager = LevelManager::GetInstance();
	levelManager->Initialize(entityManager);

	//instatntiate a status manager -- updates all statuses on characters
	StatusManager::GetInstance();
	//instaitate the enemy skills
	AISkill::GetInstance();

	//lives
	hearts = pGraphics->LoadTexture(L"resources/graphics/icons/heart.png");
	itemIMG = pGraphics->LoadTexture(L"resource/Graphics/placeholder images/BETA_ITEMDROP.png");



#pragma region Character Creation
	//create characters
	GameInfo* gameInfo = GameInfo::GetInstance(); //aame info
	std::vector<PlayerInfo*> playerInfo = gameInfo->GetPlayers();

	std::vector<DataPacket> vPackets;
	CharacterGameState::GetInstance()->GetPlayerPacket(vPackets);

	//did i load a part game
	if (gameInfo->GetPlayers().size() != 0)
	{
		//local pointer used to initialize player(s)
		Player* humanPlayer = nullptr;

		//cycle through the packets
		for (unsigned int index = 0; index < playerInfo.size(); ++index)
		{

			//player number
			switch (playerInfo[index]->GetCharacterID())
			{
			case 0: //swordsman
			case 3:
				humanPlayer = CreateSwordsman(playerInfo[index]->GetCharacterName(), playerInfo[index]);
				break;
			case 1: //alchemist
			case 4:
				humanPlayer = CreateAlch(playerInfo[index]->GetCharacterName(), playerInfo[index]);
				break;
			case 2: //gunner
			case 5:
				humanPlayer = CreateGunner(playerInfo[index]->GetCharacterName(), playerInfo[index]);
				break;
			}

			//set the rank of the character
			playerInfo[index]->SetCharacterClass(humanPlayer->GetClassName().c_str());
			//controller number
			humanPlayer->SetControllerNumber(playerInfo[index]->GetContollerNumber());

			//set player skills
			UpdateSkills(humanPlayer, playerInfo[index]->GetCharacterClass());
			//add the player into the entity manager
			entityManager->AddEntity(humanPlayer, EntityType::ENT_PLAYER);
			//player array
			AddPlayer(humanPlayer);

			//release
			humanPlayer->Release();
			humanPlayer = nullptr;
		}
	}
	else
	{
		int size = 0;
		int numControllers = 0;
		for (int i = 0; i < 4; i++)
		{
			if (pInput->IsControllerConnected(i))
				numControllers++;
		}
		switch (Game::GetInstance()->GetMasterSlot())
		{
		case 0:
			// check number of players in xml
			size = GameInfo::GetInstance()->GetNumPlayersXML("slotone");
			// if no controllers : keyboad player  and size of party in xml is one load it up
			if (numControllers == 0 && size == 1)
				GameInfo::GetInstance()->LoadProfile("slotone");
			// if there are controlers connected and the size in xml doesn't match the number of controllers : can't load
			if (numControllers != 0 && size != numControllers)
				loadWasNotGood = true;
			else
				GameInfo::GetInstance()->LoadProfile("slotone");
			break;
		case 1:
			size = GameInfo::GetInstance()->GetNumPlayersXML("slottwo");
			if (numControllers == 0 && size == 1)
				GameInfo::GetInstance()->LoadProfile("slottwo");
			if (numControllers != 0 && size != numControllers)
				loadWasNotGood = true;
			else
				GameInfo::GetInstance()->LoadProfile("slottwo");

			break;
		case 2:
			size = GameInfo::GetInstance()->GetNumPlayersXML("slotthree");
			if (numControllers == 0 && size == 1)
				GameInfo::GetInstance()->LoadProfile("slotthree");
			if (numControllers != 0 && size != numControllers)
				loadWasNotGood = true;
			else
				GameInfo::GetInstance()->LoadProfile("slotthree");
			break;
		default:
			break;
		}
		playerInfo = gameInfo->GetPlayers();
		Player* humanPlayer = nullptr;

		for (size_t x = 0; x < playerInfo.size(); x++)
		{
			//player number
			switch (playerInfo[x]->GetCharacterID())
			{
			case 0: //swordsman
			case 3:
				humanPlayer = CreateSwordsman(playerInfo[x]->GetCharacterName(), playerInfo[x]);
				break;
			case 1: //alchemist
			case 4:
				humanPlayer = CreateAlch(playerInfo[x]->GetCharacterName(), playerInfo[x]);
				break;
			case 2: //gunner
			case 5:
				humanPlayer = CreateGunner(playerInfo[x]->GetCharacterName(), playerInfo[x]);
				break;
			}
			//set the rank of the character
			playerInfo[x]->SetCharacterClass(humanPlayer->GetClassName().c_str());
			//controller number
			humanPlayer->SetControllerNumber(playerInfo[x]->GetContollerNumber());

			//set player skills
			UpdateSkills(humanPlayer, playerInfo[x]->GetCharacterClass());
			//add the player into the entity manager
			entityManager->AddEntity(humanPlayer, EntityType::ENT_PLAYER);
			//player array
			AddPlayer(humanPlayer);

			//release
			humanPlayer->Release();
			humanPlayer = nullptr;
		}

	}
#pragma endregion


	//load the levels xml based on what your current level is 
	CurrentLevel = gameInfo->GetCurrentLevel();
	std::stringstream lvl;
	lvl << "level" << CurrentLevel;
	levelManager->LoadLevel(lvl.str().c_str());

	//level manager
	LevelGameState::GetInstance()->DeleteInstance();
	//Initialize the Camera
	camera = new Camera();
	camera->SetSize({ Game::GetInstance()->GetScreenWidth(), 520 });
	camera->EnterLevelSection();

	char *levelNames[] = { "Tortuga", "Inferno Island", "Crystal Caves", "Temple Of Offerings", "The Abyss" };
	GUIManager::GetInstance()->PopUpDialog(levelNames[CurrentLevel], { 400, 100 }, 1.75f, 0.15f);

	switch (CurrentLevel)
	{
	case 0:
		GUIManager::GetInstance()->PopUpBlinker("B2/A: Quick Attack", { 1150, 440 }, 0.8f, 1.f, { 255, 255, 0 });
		GUIManager::GetInstance()->PopUpBlinker("B3/B: Heavy Attack", { 1150, 470 }, 0.8f, 1.f, { 255, 255, 0 });

		GUIManager::GetInstance()->PopUpBlinker("B6/RB: Block-Counter", { 1850, 450 }, 0.8f, 1.f, { 255, 255, 0 });
		
		GUIManager::GetInstance()->PopUpBlinker("B5/LB: Skill1", { 2700, 440 }, 0.8f, 1.f, { 255, 255, 0 });
		GUIManager::GetInstance()->PopUpBlinker("B7/LT: Skill2", { 2700, 470 }, 0.8f, 1.f, { 255, 255, 0 });
		break;
	case 2:
		//blizzard
		weather = new ParticleEmitter(0, 500, 1, { camera->GetSize().width * 2, 100 });
		ParticleManager::GetInstance()->AddEmitter(weather, true);
		break;
	case 4:
		//rain
		weather = new ParticleEmitter(PAR_RAIN, 1200, 3, { camera->GetSize().width * 2, 100 });
		ParticleManager::GetInstance()->AddEmitter(weather, true);
		break;
	}

	//initialize the audio queue
	AudioQueue::GetInstance();

	//play game
	EOG = 0;
	eogTimer = 0.f;
	if (loadWasNotGood)
		Game::GetInstance()->ChangeState(SaveLoadState::GetInstance());

	LoadBackgroundMusic(CurrentLevel);

	Game::GetInstance()->PAUSED = false;
}
void PlayGameState::ResetLevel()
{
	EOG = 0;
	partyLives = MAX_LIVES;

	for (size_t i = 0; i < party.size(); i++)
	{
		if (party[i]->GetType() == ENT_PLAYER)
			party[i]->AddRef();
	}
	entityManager->RemoveAll();

	//there should only be players 
	for (size_t i = 0; i < party.size(); i++)
	{
		party[i]->SetPosition({ 250, 400 });
		party[i]->SetHealth(party[i]->GetMaxHealth());

		entityManager->AddEntity(party[i], EntityType::ENT_PLAYER);
		party[i]->Release();//avoid deleting user
		party[i] = nullptr;
	}

	levelManager->Terminate();
	std::stringstream lvl;
	lvl << "level" << CurrentLevel;
	levelManager->LoadLevel(lvl.str().c_str());

	camera->Reset();
}
void PlayGameState::Exit()
{

	//update player info
	for (unsigned int index = 0; index < GameInfo::GetInstance()->GetPlayers().size(); ++index)
	{
		//local pointer
		PlayerInfo* ptr = GameInfo::GetInstance()->GetPlayers()[index];
		for (unsigned int person = 0; person < GetParty().size(); ++person)
		{
			//local player pointer
			Player* player = GetParty()[index];

			//matching?
			if (ptr->GetContollerNumber() == player->GetControllerNumber())
			{
				//set the levels
				ptr->SetPlayerLevel(player->GetPlayerLevel());
				if (nextLevelPlox)
				{
					int x = CurrentLevel + 1;
					if (x > 4)
						x = 4;
					ptr->SetPlayerLevel(x);
				}
				//break -- success!
				break;
			}

		}
	}

	//delete camera
	delete camera;
	camera = nullptr;

	//if (leavingGame)
	ClearPlayerParty();

	ParticleManager::GetInstance()->ClearParticles();
	GUIManager::GetInstance()->ClearPopUpMessages();

	//character gamestates
	CharacterGameState::GetInstance()->UnloadTextures();
	CharacterGameState::GetInstance()->DeleteInstance();

	//delete effect singleton
	StatusManager::DeleteInstance();
	AISkill::DeleteInstance();

	//clean up sound
	AudioQueue::GetInstance()->clear();
	AudioQueue::DeleteInstance();

	UnloadTexture();

	levelManager->Terminate();
	levelManager = nullptr;

	//broke on remove all
	entityManager->RemoveAll();	// remove everything in the entity manager 
	delete entityManager;	//delete the manager itself
	entityManager = nullptr; //set to null

	DeleteInstance();
}
void PlayGameState::UnloadTexture(void)
{
	pGraphics->UnloadTexture(backgroundImage);
	//pGraphics->UnloadTexture(starIMG);
	pGraphics->UnloadTexture(hearts);
	pGraphics->UnloadTexture(itemIMG);
}
#pragma endregion

void PlayGameState::ArcadeCursorMovement(float dt)
{
	SGD::Vector dir = pInput->GetLeftJoystick(0);
	if (dir.y != 0)
	{
		timer += dt;
		if (timer > timerFreq)
		{
			timer = 0.f;
			if (dir.y < 0)
			{
				timer = 0.f;

				if (Game::GetInstance()->PAUSED)
				{
					if (--pausedCursor < 0)
						pausedCursor = 2;
				}

				if (--nCursor < 0)
					nCursor = 1;
			}
			else if (dir.y > 0)
			{
				timer = 0.f;

				if (Game::GetInstance()->PAUSED)
				{
					if (++pausedCursor > 2)
						pausedCursor = 0;
				}

				nCursor++;
				if (nCursor > 1)
					nCursor = 0;
			}
		}
	}
}

bool PlayGameState::Update(float dt)
{
	SGD::InputManager* pInput = SGD::InputManager::GetInstance();
	AudioQueue::GetInstance()->Update();

#pragma region EOG
	if (EOG != 0) //game over
	{
		eogTimer += dt;
		if (eogTimer < 1.5f)
			return true;

		if (Game::GetInstance()->ARCADEMODE)
		{
			ArcadeCursorMovement(dt);
		}
		else
		{
			//move cursor
			if (pInput->IsKeyPressed(SGD::Key::Down) == true
				|| pInput->IsDPadPressed(0, SGD::DPad::Down))
				if (pInput->IsKeyPressed(SGD::Key::Down) == true || pInput->IsDPadPressed(0, SGD::DPad::Down))
				{
				if (++nCursor > 1)
					nCursor = 0;
				}
			if (pInput->IsKeyPressed(SGD::Key::Up) == true
				|| pInput->IsDPadPressed(0, SGD::DPad::Up))
			{
				if (--nCursor < 0)
					nCursor = 1;
			}
		}
		if (pInput->IsKeyPressed(SGD::Key::Enter) == true || pInput->IsButtonPressed(0, 1))
		{
			if (EOG == -1)//LOSE
			{
				Game::GetInstance()->Save();
				switch (nCursor)
				{
				case 3:
					ResetLevel();
					break;
				case 0:
					Game::GetInstance()->ChangeState(LevelGameState::GetInstance());
					return true;
					break;
				case 1:
					Game::GetInstance()->ChangeState(MenuGameState::GetInstance());
					return true;
					break;
				}
			}
			else//WIN
			{
				bool notGoing = false;
				if (CurrentLevel == GameInfo::GetInstance()->GetUnlockLevels())
				{
					if (CurrentLevel == 0 || CurrentLevel == 2)
						notGoing = true;

					GameInfo::GetInstance()->IncreaseUnlockLevels();
				}
				Game::GetInstance()->Save();
				switch (nCursor)
				{
				case 0://continue

					if (CurrentLevel == 4)
					{
						nextLevelPlox = true;
						Game::GetInstance()->ChangeState(CreditsGameState::GetInstance());
						return true;
					}

					if (notGoing)
					{
						nextLevelPlox = true;
						Game::GetInstance()->ChangeState(SkillSelectionState::GetInstance());
						return true;
					}
					else
					{
						for (size_t i = 0; i < party.size(); i++)
						{
							unsigned int x = party[i]->GetPlayerLevel();
							x++;
							party[i]->SetPlayerLevel(x);
						}
						nextLevelPlox = true;
						Game::GetInstance()->ChangeState(LevelGameState::GetInstance());
						return true;
					}

					break;
				case 1://exit
					PlayGameState::GetInstance()->leavingGame = true;
					Game::GetInstance()->ChangeState(MenuGameState::GetInstance());
					return true;
					break;
				}
			}
		}
		return true;
	}
#pragma endregion

#pragma region PAUSE
	if (!Game::GetInstance()->ARCADEMODE)
	{
		if (pInput->IsKeyPressed(SGD::Key::Escape) || pInput->IsButtonPressed(0, 8))
			Game::GetInstance()->PAUSED = !(Game::GetInstance()->PAUSED);

		//PC MODE
		if (Game::GetInstance()->PAUSED)
		{
			if (pInput->IsKeyPressed(SGD::Key::Down) == true || pInput->IsDPadPressed(0, SGD::DPad::Down))
			{
				if (++pausedCursor > 1)
					pausedCursor = 0;
			}
			if (pInput->IsKeyPressed(SGD::Key::Up) == true || pInput->IsDPadPressed(0, SGD::DPad::Up))
			{
				if (--pausedCursor < 0)
					pausedCursor = 1;
			}

			if (pInput->IsKeyPressed(SGD::Key::Enter) == true || pInput->IsButtonPressed(0, 1))
			{
				switch (pausedCursor)
				{
				case 0://RESUME
					Game::GetInstance()->PAUSED = false;
					break;
				case 1://SAVE
					Game::GetInstance()->Save();
					Game::GetInstance()->ChangeState(MenuGameState::GetInstance());
				}
			}
			return true;
		}
#if CHEAT_WIN
		//go to win menu
		if (pInput->IsKeyDown(SGD::Key::F7))
		{
			EOG = 1;
			nextLevelPlox = true;
			Game::GetInstance()->ChangeState(SkillSelectionState::GetInstance());
			return true;
		}
		//go to levelselection state
		if (pInput->IsKeyDown(SGD::Key::F8))
		{
			Game::GetInstance()->ChangeState(LevelGameState::GetInstance());
			nextLevelPlox = true;
		}

#endif
	}
	else//ARCADE MODE
	{
		if (pInput->IsButtonPressed(0, 6))
			Game::GetInstance()->PAUSED = !(Game::GetInstance()->PAUSED);
	}
#pragma endregion

	camera->Update(dt);
	if (weather)
		weather->SetPosition({ camera->position.x - 400, camera->position.y - 100 });
	//update the message manager
	entityManager->UpdateAll(dt);
	entityManager->CheckActiveCollision(EntityType::ENT_PLAYER, EntityType::ENT_ENEMY);
	entityManager->CheckActiveCollision(EntityType::ENT_MISSILE, EntityType::ENT_PLAYER);
	entityManager->CheckActiveCollision(EntityType::ENT_MISSILE, EntityType::ENT_ENEMY);
	entityManager->CheckActiveCollision(EntityType::ENT_MISSILE, EntityType::ENT_MISSILE);
	entityManager->CheckActiveCollision(EntityType::ENT_MISSILE, EntityType::ENT_ANIM_PARTICLE);
	entityManager->CheckActiveCollision(EntityType::ENT_ANIM_PARTICLE, EntityType::ENT_ENEMY);
	entityManager->CheckActiveCollision(EntityType::ENT_ANIM_PARTICLE, EntityType::ENT_PLAYER);
	entityManager->CheckEntityCharacterCollision(EntityType::ENT_BASE, EntityType::ENT_PLAYER);//check against triggers
	entityManager->CheckEntityCharacterCollision(EntityType::ENT_INVULNERABLE, EntityType::ENT_PLAYER);//check against obstacles
	entityManager->CheckEntityCharacterCollision(EntityType::ENT_INVULNERABLE, EntityType::ENT_ENEMY);//check against obstacles
	entityManager->CheckCollisions(EntityType::ENT_PLAYER, EntityType::ENT_ITEMS);

	SGD::MessageManager::GetInstance()->Update();
	//saveSlot
	if (pInput->IsKeyPressed(SGD::Key::F12) == true)
	{
		gameIsSaved = true;
	}
	ParticleManager::GetInstance()->UpdateEmitters(dt);
	//keep playing
	return true;
}
//RENDERING ORDER
//BACKGROUND>ENTITIES>FOREGROUND>GUI
void PlayGameState::Render(float dt)
{
	// BACKGROUND
	pGraphics->DrawTexture(backgroundImage, { 0, 0 });
	//render all the level
	camera->Render();

	//ENTITIES
	entityManager->RenderAll();
	ParticleManager::GetInstance()->RenderEmitters();

	//FOREGROUND
	camera->RenderForeground();

	//====================================HUD/GUI===================================
	SGD::Point pt;//render point
	pt.x = Game::GetInstance()->GetScreenWidth();
	pt.x -= partyLives * 32;
	pt.y = 0;
	//render party lives
	for (unsigned int life = 0; life < partyLives; life++)
	{
		pGraphics->DrawTexture(hearts, pt);
		pt.x += 32;
	}
	//PLAYER HUD
	pGraphics->DrawRectangle({ 0, 520, 800, 600 }, { 127, 127, 127 });
	for (unsigned int i = 0; i < party.size(); i++)
	{
		party[i]->RenderHUD();
	}

	if (EOG == -1)//LOSE
	{
		float fontSize = 50;
		pGraphics->DrawRectangle({ 0, 0, 800, 600 }, { 100, 0, 0, 0 });
		font->DrawXML("You Lose", { 240, 130 }, 3.f, {});
		font->DrawXML("Select Level", { 300, 200 + fontSize }, 1.5f, {});
		font->DrawXML("Main Menu", { 300, 200 + fontSize * 2 }, 1.5f, {});
		font->DrawXML("X", { 270, 250 + fontSize * nCursor }, 1.5f, {});
		return;
	}
	if (EOG == 1)//WIN
	{
		float fontSize = 50;
		pGraphics->DrawRectangle({ 0, 0, 800, 600 }, { 100, 0, 0, 0 });
		font->DrawXML("You Win", { 240, 130 }, 3.f, {});
		font->DrawXML("Continue", { 300, 200 + fontSize }, 1.5f, {});
		font->DrawXML("Main Menu", { 300, 200 + fontSize * 2 }, 1.5f, {});
		font->DrawXML("X", { 270, 250 + fontSize * nCursor }, 1.5f, {});
		return;
	}

	if (Game::GetInstance()->PAUSED)
	{
		pGraphics->DrawRectangle({ 0, 0, 800, 600 }, { 100, 0, 0, 0 });

		font->DrawXML("GAME IS PAUSED", SGD::Point{ 100, 50 }, 2.0, {});
		font->DrawXML("Resume", SGD::Point(200, 150), 1.0f, {});
		font->DrawXML("Exit", SGD::Point(200, 200), 1.0f, {});
		font->DrawXML("X", SGD::Point{ 180, 150 + 50.0f * pausedCursor }, 1.0f, {});
	}
}
/**************************************************************/
// MessageProc
//	- process messages queued in the MessageManager
//	- STATIC METHOD
//		- does NOT have invoking object!!!
//		- must use singleton to access members
/*static*/ void PlayGameState::MessageProc(const SGD::Message* pMsg)
{
	switch (pMsg->GetMessageID())
	{
	default:
		break;
	case MessageID::MSG_COMBO_ATTACK:
	{
		const ComboAttackMessage* msg = (ComboAttackMessage*)pMsg;
		assert(msg != nullptr && "MSG_COMBO_ATTACK threw aproblem");
	}
		break;
	case MessageID::MSG_CREATE_MISSILE:
	{
		//cast to the correct message type
		const CreateMissileMessage* msg = (CreateMissileMessage*)pMsg;
		///assert check
		assert(msg != nullptr && "MSG_CREATE_MISSILE threw a problem");

		//create an projectile
		SGD::Point pt = msg->GetSpawnPoint();
		pt.x += PlayGameState::GetInstance()->GetCameraPosition().x;
		pt.y += PlayGameState::GetInstance()->GetCameraPosition().y;
		Entity* m = PlayGameState::GetInstance()->CreateMissile(msg->GetOwner(), pt, msg->GetType(), msg->GetName(),
			msg->GetMovementAmount(), msg->GetTime(), msg->GetVelocity());
		//add into the entity manager
		PlayGameState::GetInstance()->entityManager->AddEntity(m, m->GetType());
		m->Release();
		m = nullptr;
	}
		break;
	case MessageID::MSG_CREATE_ANIM_PARTICLE:
	{

		//cast to the correct message type
		const CreateAnimatedParticle* msg = (CreateAnimatedParticle*)pMsg;
		//local play game instance
		PlayGameState* pGame = PlayGameState::GetInstance();
		///assert check
		assert(msg != nullptr && "MSG_CREATE_ANIM_PARTICLE threw a problem");
		//create an animated particle
		AnimatedParticle* particle = pGame->CreateAnimateParticle(msg->GetOwner(),
			msg->GetName(), msg->GetLifeSpan(), msg->GetColor(), msg->GetPosition(), msg->GetVelocity());
		//add into the entity
		pGame->entityManager->AddEntity(particle, particle->GetType());
		particle->Release();
		particle = nullptr;
	}
		break;
	case MessageID::MSG_DESTROY_ENTITY:
	{
		const DestroyEntityMessage* msg = dynamic_cast<const DestroyEntityMessage*>(pMsg);
		assert(msg != nullptr && "GamePlayState::Destroy Entity failed");
		Entity* m = msg->GetEntity();
		//remove from all AI
		PlayGameState::GetInstance()->ClearCharacterTargets(dynamic_cast<Character*>(m));
		PlayGameState::GetInstance()->entityManager->RemoveEntity(m);

		m = nullptr;
	}
		break;
	case MessageID::MSG_ENEMY_DOWN:
	{
		const EnemyDownMessage* msg = (EnemyDownMessage*)pMsg;
		assert(msg != nullptr && "MSG_ENEMY_DOWN threw a problem");
		int amount = LevelManager::GetInstance()->GetEnemiesLeft();
		LevelManager::GetInstance()->SetEnemiesLeft(--amount);

		int curr = GetInstance()->camera->currSectionIndex;
		if (amount <= 0)
		{
			if (curr == LevelManager::GetInstance()->GetNumberOfSections())
			{
				//WIN
				GetInstance()->EOG = 1;
				GetInstance()->cursorMax = 1;
			}
			else
			{
				//unlock camera
				GetInstance()->camera->lock = false;
			}
		}

		//remove from all AI
		if (msg->GetOwner())
			PlayGameState::GetInstance()->ClearCharacterTargets(dynamic_cast<Character*>(msg->GetOwner()));

	}
		break;
	case MessageID::MSG_PLAYER_DOWN:
	{
		//player down
		const PlayerDownMessage* msg = dynamic_cast<const PlayerDownMessage*>(pMsg);
		//decrease life ppol
		--GetInstance()->partyLives;
		if (GetInstance()->partyLives == 0)
		{
			GetInstance()->partyLives = 0;
			GetInstance()->EOG = -1;
		}
		GetInstance()->cursorMax = 2;

		Player* player = msg->GetSender();
		//POP UP
		GUIManager::GetInstance()->PopUpFade(
			"DEAD", player->GetPosition(), 1.f, 1.2f, {});

		//reset the player
		SGD::Point respawn = GetInstance()->GetCameraPosition();
		respawn.Offset({ player->GetSize().width + 15, 325 });
		player->Reset(respawn);

		//remove from all AI
		PlayGameState::GetInstance()->ClearCharacterTargets(player);
	}
		break;

	case MessageID::MSG_CREATE_ITEM:
	{
		const CreateItemMessage* msg = (CreateItemMessage*)(pMsg);
		assert(msg != nullptr && "CreateItemMessage threw a problem");
		Tier tier = msg->GetSender()->GetItemTier();
		ItemType buff = (ItemType)msg->GetSender()->GetQueBuff();
		StatusEffect status = msg->GetSender()->GetStatus();
		Items* item = new Items(tier, 0.0f, GetInstance()->itemIMG, status, buff, false);
		item->SetPosition(msg->GetSender()->GetPosition());
		item->SetSize(SGD::Size(42, 45));
		PlayGameState::GetInstance()->entityManager->AddEntity(item, EntityType::ENT_ITEMS);
		item->Release();
		item = nullptr;
	}
		break;
	case MessageID::MSG_DELETE_ITEM_MANAGER:
	{
		const DeleteItemFromEntityManager* msg = (DeleteItemFromEntityManager*)(pMsg);
		Items* item = msg->GetOwner();
		PlayGameState::GetInstance()->entityManager->RemoveEntity(item, EntityType::ENT_ITEMS);
	}
		break;

	case MessageID::MSG_CREATE_FAMILIAR:
	{
		const CreateFamiliar* msg = (CreateFamiliar*)(pMsg);
		Familiar* pet = PlayGameState::GetInstance()->CreateFamiliarPartner(msg->GetName(), msg->GetOwner(), msg->GetPosition());
		PlayGameState::GetInstance()->entityManager->AddEntity(pet, pet->GetType());
		pet->Release(); pet = nullptr;
	}
		break;
	}
}
//********************************
#pragma region FACTORY METHODS

void	PlayGameState::AddPlayer(Player* _player)
{
	//reference counting
	_player->AddRef();

	//add the player into the party
	party.push_back(_player);
}
void	PlayGameState::ClearPlayerParty(void)
{
	//release the party table
	for (unsigned int i = 0; i < party.size(); ++i)
	{
		party[i]->Release();
		party[i] = nullptr;
	}

	//clear
	party.clear();
}

Player* PlayGameState::CreateSwordsman(const char* name, PlayerInfo* playerInfo)
{
	//Player pointer 
	Swordsman* player = new Swordsman(name, playerInfo->GetContollerNumber());
	//Initialize Player values
	//player->GetCharacterStats()->InitializeStats(10.f, 100.f, 150.f, 0.80f, 1.0f, 2.0f, 0.75f, 1.25f);
	player->GetCharacterStats()->InitializeStats(12.f, 120.f, 150, 0.0f, 0.85f, 2.0f, 0.80f, 1.30f);
	player->SetClassName(playerInfo->GetCharacterClass());
	player->SetSize(SGD::Size(40.0f, 31.0f));
	player->SetPosition(DEFAULT_SPAWNPOSITION);
	player->SetPlayerLevel(playerInfo->GetPlayerLevel());
	//return newly created player
	return player;
}
Player* PlayGameState::CreateGunner(const char* name, PlayerInfo* playerInfo) const
{
	//Player pointer 
	Gunner* player = new Gunner(name, playerInfo->GetContollerNumber());
	//Initialize Player values
	player->GetCharacterStats()->InitializeStats(10.f, 100.f, 165.f, 1.0f, .80f, 1.5f, 0.8f, 1.20f);
	player->SetClassName(playerInfo->GetCharacterClass());
	player->SetSize(SGD::Size(40.0f, 31.0f));
	player->SetPosition(DEFAULT_SPAWNPOSITION);
	player->SetPlayerLevel(playerInfo->GetPlayerLevel());

	//return newly created player
	return player;
}
Player* PlayGameState::CreateAlch(const char* name, PlayerInfo* playerInfo) const
{
	//Player pointer 
	Alchemist* player = new Alchemist(name, playerInfo->GetContollerNumber());
	//Initialize Player values
	player->GetCharacterStats()->InitializeStats(10.f, 80.0f, 140.f, 1.0f, 0.80f, 1.0f, 0.6f, 1.4f);
	player->SetClassName(playerInfo->GetCharacterClass());
	player->SetSize(SGD::Size(40.0f, 31.0f));
	player->SetPosition(DEFAULT_SPAWNPOSITION);
	player->SetPlayerLevel(playerInfo->GetPlayerLevel());

	//return newly created player
	return player;
}


Missile* PlayGameState::CreateMissile(Character* c, SGD::Point pos, ProjectileType type, std::string n, float move, float timer, SGD::Vector vec)
{
	//new missile
	Missile* m = nullptr;
	SGD::Vector vel = vec;

	//new missile
	switch (type)
	{
	case NORMAL:
		m = new Missile(n.c_str(), type);
		if (vec.x == 0 && vec.y == 0)
			vel.x = c->GetTimeStamp()->bForwards ? 1.0f : -1.0f;
		break;
	case LASER:
		m = new Laser(n.c_str(), timer, type);
		if (vec.x == 0 && vec.y == 0)
			vel.x = 0;
		break;
	case BEAM:
		m = new Laser(n.c_str(), timer, type, "animate");
		break;
	default:
		break;
	}

	//name of the projectiles
	m->SetOwner(c);

	//position
	m->SetPosition(pos);

	//set the active rectangle
	m->SetActiveRect(c->GetCollisionRect());

	//size
	m->SetSize({ 28, 28 });
	m->SetSpeed(c->GetMoveSpeed()* move);
	m->GetTimeStamp()->bForwards = c->GetTimeStamp()->bForwards;
	m->SetDamage(c->GetDamage());

	if (n == "redclaw")
		m->SetDamage(c->GetDamage() * 3.0f);

	//up by the speed
	vel.x *= m->GetSpeed()* 2.0f;
	vel.y *= m->GetSpeed()* 2.0f;

	//set the veloct=ity
	m->SetVelocity(vel);

#if 1
	ParticleFlyWeight *flylava = ParticleManager::GetInstance()->GetFlyWeight(3);
	ParticleEmitter *emitter = new ParticleEmitter(flylava, 50, 0.001f, m->GetSize()*0.5f);
	//int id = ParticleManager::GetInstance()->AddEmitter(emitter, true);
	emitter->Emit(true);
	m->SetTrail(emitter, 0);
#endif

	return m;
}
AnimatedParticle* PlayGameState::CreateAnimateParticle(Character* _owner, std::string _name, float _span, SGD::Color clr, SGD::Point pt, SGD::Vector vec)
{
	AnimatedParticle* ptr = new AnimatedParticle(_owner, _name.c_str(), _span, pt, vec);
	ptr->GetTimeStamp()->m_clrRender = clr;
	ptr->GetTimeStamp()->bForwards = _owner->GetTimeStamp()->bForwards;


	return ptr;
}
Familiar* PlayGameState::CreateFamiliarPartner(const char* name, Character* owner, SGD::Point point)
{
	//create the familiar
	Familiar* fam = new Familiar(name, owner);

	//set stats
	fam->GetCharacterStats()->InitializeStats(owner->GetDamage() * 0.75f, owner->GetMaxHealth() * 0.5f, owner->GetMoveSpeed(), owner->GetFireFreq(), 0.0f);
	//(owner->GetMaxHealth() * 0.750f, owner->GetDamage() * 0.75f, owner->GetMoveSpeed(), owner->GetFireFreq(), 1.0f, owner->GetProjectileSpeed());
	fam->SetPosition(point);
	//success!
	return fam;
}

#pragma endregion

SGD::Size  PlayGameState::GetCameraSize() const
{
	return (camera != nullptr) ? camera->GetSize() : SGD::Size();
}
SGD::Point PlayGameState::GetCameraPosition()const
{
	return (camera != nullptr) ? camera->GetPosition() : SGD::Point();
}
#if 0
LevelSection* PlayGameState::GetCurrentSection()const{
	return camera->currentSection;
}
#endif
int PlayGameState::GetCurrentSectionIndex() const
{
	return camera->currSectionIndex;
}
bool PlayGameState::IsCameraLocked(){ return camera->lock; }

void PlayGameState::LockCamera() { camera->lock = true; }

void PlayGameState::ClearCharacterTargets(Character* ptr)
{
	if (!ptr)
		return;

	//entity vector
	std::vector<IEntity*> vec = entityManager->GetEntityVector(ENT_PLAYER);
	for (unsigned int i = 0; i < vec.size(); ++i)
	{
		_CrtCheckMemory();

		//local familiar
		Familiar* pt = dynamic_cast<Familiar*>(vec[i]);

		//skip at null
		if (!pt)
			continue;
		//disown
		if (pt->GetAIStamp()->target == ptr)
			pt->GetAIStamp()->target = nullptr;

		_CrtCheckMemory();

	}

	//clear and search enemies
	vec.clear();
	vec = entityManager->GetEntityVector(ENT_ENEMY);
	for (unsigned int i = 0; i < vec.size(); ++i)
	{
		//pointer
		Enemy* pt = dynamic_cast<Enemy*>(vec[i]);

		//skip at null
		if (!pt)
			continue;
		//disown
		if (pt->GetAIStamp()->target == ptr)
			pt->GetAIStamp()->target = nullptr;

	}

	//delete all animations
	//clear and search enemies
	vec.clear();
	vec = entityManager->GetEntityVector(ENT_ANIM_PARTICLE);
	for (unsigned int i = 0; i < vec.size(); ++i)
	{
		//pointer
		AnimatedParticle* pt = dynamic_cast<AnimatedParticle*>(vec[i]);

		//skip at null
		if (!pt)
			continue;

		//disown
		if (pt->GetOwner() == ptr)
			pt->SetOwner(nullptr);
	}

}

void PlayGameState::UpdateSkills(Player* player, const char* className)
{
	std::string skillName = className;

	switch ((PlayerType)player->GetPlayerType())
	{
	case PlayerType::swordsman:
		if (skillName == "berserker")
		{
			player->SetSkill(1, &(PlayerSkill::Berserker));
			player->GetPlayerHUD()->LoadIcon("resources/graphics/icons/swordsman/berserker.png", 1);
		}
		else if (skillName == "fencer")
		{
			player->SetSkill(1, &(PlayerSkill::Fencer));
			player->GetPlayerHUD()->LoadIcon("resources/graphics/icons/swordsman/fencer.png", 1);
		}
		else if (skillName == "captain")
		{
			player->SetSkill(1, &(PlayerSkill::Berserker));
			player->GetPlayerHUD()->LoadIcon("resources/graphics/icons/swordsman/berserker.png", 1);
			player->SetSkill(2, &(PlayerSkill::Captain));
			player->GetPlayerHUD()->LoadIcon("resources/graphics/icons/swordsman/moustache.png", 2);
		}
		else if (skillName == "admiral")
		{
			player->SetSkill(1, &(PlayerSkill::Fencer));
			player->GetPlayerHUD()->LoadIcon("resources/graphics/icons/swordsman/fencer.png", 1);
			player->SetSkill(2, &(PlayerSkill::Admiral));
			player->GetPlayerHUD()->LoadIcon("resources/graphics/icons/swordsman/admiral.png", 2);
		}
		else if (skillName == "hammer")
		{
			player->SetSkill(1, &(PlayerSkill::Berserker));
			player->GetPlayerHUD()->LoadIcon("resources/graphics/icons/swordsman/berserker.png", 1);
			player->SetSkill(2, &(PlayerSkill::Hammer));
			player->GetPlayerHUD()->LoadIcon("resources/graphics/icons/swordsman/hammer.png", 2);
		}
		else if (skillName == "seaknight")
		{
			player->SetSkill(1, &(PlayerSkill::Fencer));
			player->GetPlayerHUD()->LoadIcon("resources/graphics/icons/swordsman/fencer.png", 1);
			player->SetSkill(2, &(PlayerSkill::SeaKnight));
			player->GetPlayerHUD()->LoadIcon("resources/graphics/icons/swordsman/seaknight.png", 2);
		}
		break;
	case PlayerType::alchemist:
		if (skillName == "summoner")
		{
			player->SetSkill(1, &(PlayerSkill::Summoner));
			player->GetPlayerHUD()->LoadIcon("resources/graphics/icons/mage/summoner.png", 1);
		}
		else if (skillName == "poisonfog")
		{
			player->SetSkill(1, &(PlayerSkill::PoisonFog));
			player->GetPlayerHUD()->LoadIcon("resources/graphics/icons/mage/apothecary.png", 1);
		}
		else if (skillName == "witchdoctor")
		{
			player->SetSkill(1, &(PlayerSkill::Summoner));
			player->GetPlayerHUD()->LoadIcon("resources/graphics/icons/mage/summoner.png", 1);
			player->SetSkill(2, &(PlayerSkill::WitchDoctor));
			player->GetPlayerHUD()->LoadIcon("resources/graphics/icons/mage/witchdoctor.png", 2);
		}
		else if (skillName == "pyromaniac")
		{
			player->SetSkill(1, &(PlayerSkill::PoisonFog));
			player->GetPlayerHUD()->LoadIcon("resources/graphics/icons/mage/apothecary.png", 1);
			player->SetSkill(2, &(PlayerSkill::Pyromaniac));
			player->GetPlayerHUD()->LoadIcon("resources/graphics/icons/mage/pyromaniac.png", 2);
		}
		else if (skillName == "beastmaster")
		{
			player->SetSkill(1, &(PlayerSkill::Summoner));
			player->GetPlayerHUD()->LoadIcon("resources/graphics/icons/mage/summoner.png", 1);
			player->SetSkill(2, &(PlayerSkill::BeastMaster));
			player->GetPlayerHUD()->LoadIcon("resources/graphics/icons/mage/beastmaster.png", 2);
		}
		else if (skillName == "omniknight")
		{
			player->SetSkill(1, &(PlayerSkill::PoisonFog));
			player->GetPlayerHUD()->LoadIcon("resources/graphics/icons/mage/apothecary.png", 1);
			player->SetSkill(2, &(PlayerSkill::OmniKnight));
			player->GetPlayerHUD()->LoadIcon("resources/graphics/icons/mage/omniknight.png", 2);
		}
		break;
	case PlayerType::gunner:
		if (skillName == "marksman")
		{
			player->SetSkill(1, &(PlayerSkill::Marksman));
			player->GetPlayerHUD()->LoadIcon("resources/graphics/icons/gunner/dualshot.png", 1);
		}
		else if (skillName == "claymore")
		{
			player->SetSkill(1, &(PlayerSkill::Claymore));
			player->GetPlayerHUD()->LoadIcon("resources/graphics/icons/gunner/scout.png", 1);
		}
		else if (skillName == "weaponexchange")
		{
			player->SetSkill(1, &(PlayerSkill::Marksman));
			player->GetPlayerHUD()->LoadIcon("resources/graphics/icons/gunner/dualwielder.png", 1);
			player->SetSkill(2, &(PlayerSkill::WeaponExchange));
			player->GetPlayerHUD()->LoadIcon("resources/graphics/icons/gunner/machinegun.png", 2);
		}
		else if (skillName == "hunter")
		{
			player->SetSkill(1, &(PlayerSkill::Claymore));
			player->GetPlayerHUD()->LoadIcon("resources/graphics/icons/gunner/scout.png", 1);
			player->SetSkill(2, &(PlayerSkill::Hunter));
			player->GetPlayerHUD()->LoadIcon("resources/graphics/icons/gunner/sniper.png", 2);
		}
		else if (skillName == "cannonfire")
		{
			player->SetSkill(1, &(PlayerSkill::Marksman));
			player->GetPlayerHUD()->LoadIcon("resources/graphics/icons/gunner/dualshot.png", 1);
			player->SetSkill(2, &(PlayerSkill::CannonFire));
			player->GetPlayerHUD()->LoadIcon("resources/graphics/icons/gunner/dualwielder.png", 2);
		}
		else if (skillName == "camper")
		{
			player->SetSkill(1, &(PlayerSkill::Claymore));
			player->GetPlayerHUD()->LoadIcon("resources/graphics/icons/gunner/scout.png", 1);
			player->SetSkill(2, &(PlayerSkill::Camper));
			player->GetPlayerHUD()->LoadIcon("resources/graphics/icons/gunner/camper.png", 2);
		}
		break;
	default:
		break;
	}

}

//Music
bool PlayGameState::LoadBackgroundMusic(unsigned int _level)
{
	//create the string
	std::string fileName = "resources/xml/levelSound.xml";

	//document
	TiXmlDocument doc;

	//load file
	if (doc.LoadFile(fileName.c_str()) == false)
		return false;

	//level!?
	std::stringstream levelDoc;
	levelDoc << "level" << _level;

	//access root
	TiXmlElement* pRoot = doc.RootElement();

	//access 
	if (!pRoot)
		return false;

	TiXmlElement* levelSounds = pRoot->FirstChildElement(levelDoc.str().c_str());
	TiXmlElement* backgroundFile = levelSounds->FirstChildElement("background");
	//correct?
	while (backgroundFile)
	{
		const char* path = backgroundFile->GetText();

		std::string fullPath = "resources/audio/background music/";
		fullPath += path;
		fullPath += ".xwm";


		AudioQueue::GetInstance()->AddSound(SGD::AudioManager::GetInstance()->LoadAudio(fullPath.c_str()), true);

		//next sibling
		backgroundFile = backgroundFile->NextSiblingElement("background");
	}

	//success!
	return true;
}

