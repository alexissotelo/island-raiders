#include "DefenseAI.h"

/**************************************************************
|	Function:
|	Purpose:
|	Parameter:
|	Return Value:
**************************************************************/
void DefenseAI::Update(float dt, AIStamp& stamp)
{
	Character* ptr = (Character*)stamp.owner;

	//switch animations to guard
	ptr->GetTimeStamp()->SwitchAnimation("guard");

	//counter timer
	stamp.fPause -= dt;

	//correct color 
	ptr->GetTimeStamp()->m_clrRender = SGD::Color(255, 255, (unsigned char)(ptr->GetStamina() * 255), 0);

	//no moving allowed
	ptr->SetVelocity({ 0, 0 });

	//increment timer
	stamp.fStartTimer += dt;

	float maxTime = stamp.tier < 2 ? 4.50f : 0.75f;
	//defend himself for a full second
	if (stamp.fStartTimer >= maxTime)
	{
		if (stamp.tier < 2)
		{
			unsigned int number = rand() % 7;
			if (number < 3)
				stamp.SwitchAIMode(EnemyAIType::Flee);
			else if (number < 5)
				stamp.SwitchAIMode(EnemyAIType::Combat);
			else
				stamp.SwitchAIMode(EnemyAIType::Pursuit);
		}
		else
			//fight back
			stamp.SwitchAIMode(EnemyAIType::Combat);
		
	}

}
