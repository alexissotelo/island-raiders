#pragma once
#include "Entity.h"
#include <string>

class Collision : public Entity
{
	bool destroyAfterCollision = false;
	int section;
	std::string eventID;

	int effectCounter = 0;
	int effectMax = 120;

public:
	Collision(int _section);
	~Collision();

	virtual bool HandleCollision(const IEntity* other) override;
	virtual void Render()override;

	void SetTrigger(const char *eventName);
	int GetSection()const { return section; }
};

