#pragma once
#include "IGameState.h"
#include <vector>
#include <string>
#include "../SGD Wrappers/SGD_AudioManager.h"
#include "../SGD Wrappers/SGD_GraphicsManager.h"
#include "../SGD Wrappers/SGD_InputManager.h"

class BitmapFont;
class Button;

class CreditsGameState : public IGameState
{
public:
	static CreditsGameState* GetInstance(void);

	static void DeleteInstance(void);

	virtual void Enter();
	virtual void Exit();
	virtual bool Update(float dt);
	virtual void Render(float dt);
	void SetPositions(void);

private:

	//can't make a empty arry inside a structure... why?!?!?
	

	struct words
	{
		float wordPosition;
		const char* wordToDisplay;
		float wordSize = 1.0f;
		float wordX;
	};

	int arrSize;
	float botScreen;

	std::vector<Button*> menuButtons;
	std::vector<words> credits;
	
	static CreditsGameState* pInstance;
	const BitmapFont* pFont = nullptr;
	SGD::GraphicsManager* pGraphics = nullptr;
	SGD::AudioManager* pAudio = nullptr;
	SGD::InputManager* pInput = nullptr;
	SGD::HTexture backGround = SGD::INVALID_HANDLE;
	SGD::HTexture titleScreen = SGD::INVALID_HANDLE;
	CreditsGameState() = default;
	~CreditsGameState() = default;
	CreditsGameState(const CreditsGameState&) = delete;
	CreditsGameState operator=(const CreditsGameState&) = delete;
	float timer = 0.0f;
	float timerFreq = 0.3f;
	int nCursor = 0;
	void ArcadeCursorMovement(float dt);

#pragma region No look pl0x	
	bool hidden = false;
	float hiddenTimerMax = 30.0f;
	float hiddenTimer = 0.0f;
#pragma endregion

};

