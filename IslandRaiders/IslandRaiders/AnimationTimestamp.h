#pragma once

#include "../SGD Wrappers/SGD_Color.h"
#include <string>

class Entity;

class AnimationTimestamp
{
private:
	Entity* owner = nullptr;

public:

	//current animation
	std::string m_sOwnerName; //the character name
	std::string m_sAction; //the name of the action to play

	//complete
	bool isDone = false; //is the animation running complete?
	bool bForwards = true; //is the character facing the original directon of the sheet?
	bool bLoop = false; //does the animation running loop?
	
	float m_fFrameTimer = 0.f; //frame timer
	unsigned int m_CurrentFrame = 0; //the current frame in the animation to display and update

	void SwitchAnimation(const char* _action); //switch to another animation
	//void SwitchAnimation(std::string _action); //switch to another animation
	Entity* GetOwner(void) const { return owner; } //accessor
	void SetOwner(Entity* _character) { owner = _character; } //mutator

	SGD::Color m_clrRender;
};

