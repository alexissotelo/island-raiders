#pragma once
//#include <vector>
//#include "Entity.h"
//#include "Enemy.h"
#include "Player.h"
#include "../SGD Wrappers/SGD_Geometry.h"
#include <map>
#include <utility> //std::pair structure

typedef std::pair<const char*, float>	EnemySkill;

class Enemy;
class Entity;
enum class EnemyAIType
{
	Idle, Pursuit, Combat, Defense, Ability, Flee, Flinch, Knockback, Stun, Boss, Count
};
enum class CombatType
{
	Melee, Range, Support, Familiar
};
/************************************************************
| Class: AIStamp
| Purpose: structure that keeps record of the current state of
|			the AI during the update period
************************************************************/
struct AIStamp
{
	//constructor and destructor
	AIStamp();
	~AIStamp();

	//FUNCTIONS
	void SwitchAIMode(EnemyAIType _aiType); //switch between states

	//MEMBERS
	CombatType combatType; //combat type of the character
	EnemyAIType aiType = EnemyAIType::Idle; //the AI enum value to determine which AI to update with
	PlayerType preferedTarget = PlayerType::none; //preferred player to target

	Character* owner = nullptr; //the entity the stamp will represent
	Entity* target = nullptr; //a target for the entity

	

	float fPause = -1.0f; //a pause timer for the enenmy action 
	float fStartTimer = 0.0f; //a timer to use during update
	short tier = 1;

	SGD::Vector vKnock; //unit vector in the direction to fall
	SGD::Point ptWaypoint;
	EnemySkill enemySkills[3];

	void SetAttackSound(const char* const _sound);
	const char* const GetAttackSound(void) const { return attackSoundFilePath; }

private:
	char* attackSoundFilePath = nullptr;
};

/************************************************************
| Class: EnemyAI
| Purpose: Abstract Base class for all AI structures
************************************************************/
class EnemyAI
{
protected:
	friend class Enemy;
public:
	EnemyAI() = default;
	virtual ~EnemyAI() = default;

	//pure virtuals
	virtual void Update(float dt, AIStamp& _timeStamp) = 0;
	virtual EnemyAIType GetAIType() const = 0;
};

/************************************************************
| Class: AIManager
| Purpose: singleton class that stores all AI types and updates
|			enemy entities  using the AIStamp structure
************************************************************/
class AIManager
{
public:
	//singletons
	static AIManager* GetInstance(void);
	static void DeleteInstance(void);

	//update - update all enemies 
	void Update(float dt, AIStamp& _timeStamp);

	//init and terminate
	static void Initialize();
	void Terminate(void);
	//helpers
	static void SetOwner(Enemy* _owner, AIStamp& stamp);
	static void SetTarget(Entity* _target, AIStamp& stamp);
	static bool FindPreferredTarget(AIStamp& _aiStamp);
	static bool FindClosestTarget(AIStamp& stamp);
	static bool FindWeakestTarget(AIStamp& stamp);
	//static bool FindClosestEnemy(AIStamp& stamp);
	//static bool FindWeakestEnemy(AIStamp& stamp);


private:
	AIManager() = default;
	~AIManager() = default;
	AIManager(const AIManager&) = delete;
	AIManager& operator=(const AIManager&) = delete;

	//instance - dynamic
	static AIManager* pInstance;
	std::map<EnemyAIType, EnemyAI*> vEnemyAI;

};