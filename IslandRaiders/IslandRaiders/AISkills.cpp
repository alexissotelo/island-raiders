#include "AISkills.h"
#include "Enemy.h"
#include "PlayerSkillStamp.h"

#include "CreateAnimatedParticle.h"

#include "PlayGameState.h"
#include "EntityManager.h"
#include "AudioQueue.h"

#include "../SGD Wrappers/SGD_MessageManager.h"
#include "../SGD Wrappers/SGD_AudioManager.h"
#include <vector>


/**************************************************************
|	Member: AISkill::pInstance [static][private]
|	Purpose: the sole instance for the singleton class
**************************************************************/
AISkill* AISkill::pInstance = nullptr;


/**************************************************************
|	Function: AISKill::GetInstance [static]
|	Purpose: instantiate and return the sole instance of the class
|	Parameter: void
|	Return Value: AISkill* - the sole instance of the class
**************************************************************/
AISkill* AISkill::GetInstance(void)
{
	if (!pInstance)
		pInstance = new AISkill();

	return pInstance;
}

/**************************************************************
|	Function: AISkill::DeleteInstance
|	Purpose: delete the sole instance of the class and set it to null
|	Parameter: void
|	Return Value: void
**************************************************************/
void AISkill::DeleteInstance(void)
{
	delete pInstance;
	pInstance = nullptr;
}

/**************************************************************
|	Function: AISkill::Update
|	Purpose: search for a skill by name and update the enemy based 
|			on the function pointer found, return the results of the skill
|	Parameter: 
|			float dt - the a mount of time that has elapsed since the last update
|			Enemy* enemy - a pointer to the enemy character to update
|			EnemySkill* enemySkill - a pointer to the skill structure for enemies
|	Return Value: bool - results of the skill that has updated
|				return false when no skill is found or if the skill return false,
|				otherwise return skill
**************************************************************/
bool AISkill::Update(float dt, Enemy* enemy, EnemySkill* enemySkill)
{
	//no name?
	if(!enemySkill->first)
		return false;

	//function pointer
	bool(*ptr)(float, Enemy*, EnemySkill*) = nullptr; //local pointer

	//set pointer
	FindSkill(enemySkill->first, &ptr);

	//no pointer
	if (!ptr)
		return false;

	//run function
	return ptr(dt, enemy, enemySkill);
}

/**************************************************************
|	Function: AISkill::FindSSkill
|	Purpose: appoint the function pointer based on the name of the skill
|	Parameter: 
|				const char* actionName - the name of the skill to search
|				bool(**funcPtr)(float, Enemy*, EnemySkill*) - a pointer to a function 
|								pointer that should be set depending on the name of the 
|								action sent [output]
|	Return Value: void
**************************************************************/
void AISkill::FindSkill(const char* actionName, bool(**funcPtr)(float, Enemy*, EnemySkill*))
{
	if (!actionName)
		return;

	//string comparing
	std::string nameOfAction = actionName;

	if (nameOfAction == "placeholder")
		*funcPtr = (&PlaceHolder);
	else if (nameOfAction == "healparty")
		*funcPtr = &HealParty;
	else if (nameOfAction == "healmember"/*strcmp(actionName, "healmember") == 0*/)
		*funcPtr = &HealMember;
	else if (nameOfAction == "charge")
		*funcPtr = &Charge;
	else if (nameOfAction == "sandwarp"/*strcmp(actionName, "sandwarp") == 0*/)
		*funcPtr = &SandWarp;
	else if (nameOfAction == "special1")
		*funcPtr = &GenericSkillOne;
	else if (nameOfAction == "special2")
		*funcPtr = &GenericSkillTwo;
	else if (nameOfAction == "special3")
		*funcPtr = &GenericSkillThree;
	else
		*funcPtr = nullptr;
}

/**************************************************************
|	Function: AISkill::PlaceHolder [static]
|	Purpose: test enemy skills
|	Parameter: 
|				float dt - the amount of time elapsed
|				Enemy* enemy - pointer to the enemy to update
|				EnemySkill* enemySkill* - pointer to the skill structure for 
|								enemy update information
|	Return Value: bool - return true for testing purposes
**************************************************************/
bool AISkill::PlaceHolder(float dt, Enemy* ptr, EnemySkill* enemySkill) //"placeholder"
{
	//healing place holder
	ptr->GetSkillStamp()->m_fSkillTimer += dt;
	return true;
}

/**************************************************************
|	Function: AISkill::HealParty [static]
|	Purpose: heals all members of the AI's party members
|	Parameter: 
|			float dt - the amount of time elapsed since the last update
|			Enemy* enemy -pointer to the enemy to update
|			EnemySkill* enemySkill - pointer to the enemy skill structure 
|							regardingupdate information
|	Return Value: return false when the cooldown is not complete, 
|				otherwise return true
**************************************************************/
bool AISkill::HealParty(float dt, Enemy* ptr, EnemySkill* enemySkill)
{
	const float cooldown = 10.0f;
	//static bool for when to play the sound only once
	static bool powerStart = true;

	//local ai stamp
	AIStamp* stamp = ptr->GetAIStamp();

	//local timer
	float timer = enemySkill->second;
	//increment tiemr
	timer -= dt;

	if (timer > 0.0f)
		return false;

	if (powerStart)
	{
		AudioQueue::GetInstance()->AddSound(SGD::AudioManager::GetInstance()->LoadAudio(L"resources/audio/sound effects/heal.wav"));
		powerStart = false;
}
	//allies
	std::vector<IEntity*> allyVector = PlayGameState::GetInstance()->GetManager()->GetEntityVector(ptr->GetType());

	//time up?
	if (timer <= -7.0f)
	{
		//reset timer
		timer = 10.0f;
		//set timer
		enemySkill->second = cooldown;
		//sound for next time
		powerStart = true;
		//we're done here -- go away
		return true;
	}
	//set the timer
	enemySkill->second = timer;



	// targets to heal
	for (unsigned int index = 0; index < allyVector.size(); ++index)
	{
		//local enemy
		Character* other = (Character*)allyVector[index];
		//up health
		float hp = ptr->GetDamage() * dt * 0.2f;
		//add onto the players current health
		hp += other->GetHealth();

		//max?
		if (hp >= other->GetMaxHealth())
			//cap
			hp = other->GetMaxHealth();

		//set the health
		other->SetHealth(hp);
	}

	//success!
	return true;
}

/**************************************************************
|	Function: AISkill:HealMember [static]
|	Purpose: target a member and heal them for 5 seconds, switch targets
|			of the original target dies
|	Parameter: 
|			float dt - the time elapsed since the last update
|			Enemy* ptr - pointer to the enemy to update
|			EnemySkill* enemySkill - pointer to the skill structure
|						containing the information to update with
|	Return Value: return false when the timer is not complete from cooldown
|				or there is no valid target, otherwise return true 
**************************************************************/
bool AISkill::HealMember(float dt, Enemy* ptr, EnemySkill* enemySkill)
{
	const float cooldown = 10.0f;

	//timer for healing animations
	static float animHeal;
	//static bool for when to play the sound only once
	//static bool powerStart = true;

	//local ai stamp
	AIStamp* stamp = ptr->GetAIStamp();

	//local timer
	float timer = enemySkill->second;
	//increment tiemr
	timer -= dt;
	//set the timer
	enemySkill->second = timer;

	//cannot activate ability yet
	if (timer > 0.0f)
		return false;

	//if (powerStart == true)
	//{
	//	AudioQueue::GetInstance()->AddSound(SGD::AudioManager::GetInstance()->LoadAudio(L"resources/audio/sound effects/heal.wav"));
	//	powerStart = false;
	//}

	//allies
	std::vector<IEntity*> allyVector = PlayGameState::GetInstance()->GetManager()->GetEntityVector(ptr->GetType());

	//time up?
	if (timer <= -5.50f )
	{
		//reset timer
		timer = 10.0f;
		//set timer
		enemySkill->second = cooldown;
		//play sound next time
		//powerStart = true;
		//we're done here -- go away
		return true;
	}

	//target to heal?
	if (stamp->fPause == -1)
	{
		//AudioQueue::GetInstance()->AddSound(SGD::AudioManager::GetInstance()->LoadAudio(L"resources/audio/sound effects/heal.wav"));

		//percentage of health
		float healthGauge = 0.55f;
		//start
		animHeal = 0.0f;
		// targets to heal
		for (unsigned int index = 0; index < allyVector.size(); ++index)
		{
			//local enemy
			Character* other = (Character*)allyVector[index];
			//up health
			float hp = other->GetHealth() / other->GetMaxHealth();
			//low enough
			if (hp < healthGauge)
			{
				healthGauge = hp;
				stamp->fPause = (float)index;
			}
		}
	}
	else //heal target
	{
	//index value
		unsigned int index = (unsigned int)stamp->fPause;
		//local enemy
		Character* other = index < allyVector.size() ? (Character*)allyVector[index] : nullptr;

		//bad pointer?
		if (!other)
		{
			stamp->fPause = -1.0f;
			return false;
		}

		//up health
		float hp = ptr->GetDamage() * dt ;
		//add onto the players current health
		hp += other->GetHealth();

		//max?
		if (hp >= other->GetMaxHealth())
		{
			//cap
			hp = other->GetMaxHealth();
			//were done here
			stamp->fPause = -1.0f;
			timer = 10.0f;
			//set timer
			enemySkill->second = timer;
			//stop!
			return false;
		}

		//switch to the proper animation
		ptr->GetTimeStamp()->SwitchAnimation("heal");
		//no moving allowed
		ptr->SetVelocity({ 0.f, 0.f });

		if (animHeal == 0.0f)
		{
			CreateAnimatedParticle* msg = new CreateAnimatedParticle(other, "heal", 5.0f, { 150, 255, 255, 255 });
			SGD::MessageManager::GetInstance()->QueueMessage(msg);
		}

		//update timer
		animHeal += dt;

		if (animHeal >= 2.0f)
			animHeal = 0.0f;

		//set the health
		other->SetHealth(hp);

		//success!
		return true;
	}

	//did we fail?
	return stamp->fPause != -1;
}

/**************************************************************
|	Function: AISkill::Charge [static]
|	Purpose: an ability that allows the enemy to run to an oppoents
|			position and knock them over without flinching to any 
|			damage
|	Parameter:
|			float dt - the time elapsed since the last update
|			Enemy* ptr - pointer to the enemy to update
|			EnemySkill* enemySkill - pointer to the skill structure
|						containing the information to update with
|	Return Value:
**************************************************************/
bool AISkill::Charge(float dt, Enemy* ptr, EnemySkill* enemySkill)
{
	//local ai stamp
	AIStamp* stamp = ptr->GetAIStamp();

	//local timer
	float timer = enemySkill->second;
	//increment tiemr
	timer -= dt;
	//set the timer
	enemySkill->second = timer;

	//cannot activate ability yet
	if (timer > 0.0f)
		return false;

	//no position to charge
	if (stamp->fPause == -1.0f)
	{
		//current target
		stamp->ptWaypoint = stamp->target->GetPosition();
		//do not look for another position
		stamp->fPause = 0.0f;
		//success
		return true;
	}

	//are we done?
	SGD::Vector toWaypoint = stamp->ptWaypoint - ptr->GetPosition();
	//distance
	float distance = toWaypoint.ComputeLength();

	//close enough to our destination?
	if (distance <= 20.0f)
	{
		//stun the enemy
		ptr->GetCharacterStatus()->StartEffect(Status::STUN, 4.0f, 0.0f);
		//idle
		stamp->SwitchAIMode(EnemyAIType::Idle);
		////reset timer
		//timer = -10.0f;
		////set timer
		//enemySkill->second = timer;

		//we're done here -- go away
		return true;
	}

	//about time to charge?
	if (fabs(toWaypoint.y) > 20.0f)
	{
		//move until the y's match
		toWaypoint.x = 0;
		toWaypoint.Normalize();

		//scale to move
		toWaypoint.y *= ptr->GetMoveSpeed();

		//set the velocity -- ie move motherfucker
		ptr->SetVelocity(toWaypoint);

		//success
		return true;
	}

	//now CHARGE!!
	toWaypoint.y = 0;
	toWaypoint.Normalize();

	//scale to move
	toWaypoint.x *= ptr->GetMoveSpeed() * 1.75f;

	//set the velocity to move
	ptr->SetVelocity(toWaypoint);

	//set animations
	ptr->GetTimeStamp()->SwitchAnimation("charge");

	//good job!
	return true;

	//set the position to charge 
	//change animation to run until position has reached
	//leave user stunned for small time
	//no fliching
	//knock down any opposing threats
	//take damage
	//return to idle afterwards
}

bool AISkill::SandWarp(float dt, Enemy* ptr, EnemySkill* enemySkill)
{
	const float cooldown = 10.0f;

	//local ai stamp
	AIStamp* stamp = ptr->GetAIStamp();

	//local timer
	float timer = enemySkill->second;
	//increment tiemr
	timer -= dt;
	//set the timer
	enemySkill->second = timer;

	//cannot activate ability yet
	if (timer > 0.0f)
		return false;

	if (ptr->GetTimeStamp()->isDone)
	{
		//reset timer
		timer = 10.0f;
		//set timer
		enemySkill->second = timer;
		//we're done here -- go away
		return false;
	}

	ptr->SetVelocity({ 0.f, 0.f });
	ptr->GetTimeStamp()->SwitchAnimation("sandwarp");
	return true;

}

bool AISkill::GenericSkillOne(float dt, Enemy* ptr, EnemySkill* enemySkill)
{
	const float cooldown = 10.0f;

	//local ai stamp
	AIStamp* stamp = ptr->GetAIStamp();

	//local timer
	float timer = enemySkill->second;
	//increment tiemr
	timer -= dt;
	//set the timer
	enemySkill->second = timer;

	//cannot activate ability yet
	if (timer > 0.0f)
		return false;

	if (ptr->GetTimeStamp()->isDone)
	{
		//reset timer
		timer = 10.0f;
		//set timer
		enemySkill->second = cooldown; 
		ptr->GetTimeStamp()->SwitchAnimation("idle");

		//we're done here -- go away
		return false;
	}

	ptr->SetVelocity({ 0.f, 0.f });
	ptr->GetTimeStamp()->SwitchAnimation("special1");
	return true;

}
bool AISkill::GenericSkillTwo(float dt, Enemy* ptr, EnemySkill* enemySkill)
{
	const float cooldown = 10.0f;

	//local ai stamp
	AIStamp* stamp = ptr->GetAIStamp();

	//local timer
	float timer = enemySkill->second;
	//increment tiemr
	timer -= dt;
	//set the timer
	enemySkill->second = timer;

	//cannot activate ability yet
	if (timer > 0.0f)
		return false;

	if (ptr->GetTimeStamp()->isDone)
	{
		//reset timer
		timer = 10.0f;
		//set timer
		enemySkill->second = cooldown;
		ptr->GetTimeStamp()->SwitchAnimation("idle");

		//we're done here -- go away
		return false;
	}

	ptr->SetVelocity({ 0.f, 0.f });
	ptr->GetTimeStamp()->SwitchAnimation("special2");
	return true;

}
bool AISkill::GenericSkillThree(float dt, Enemy* ptr, EnemySkill* enemySkill)
{
	const float cooldown = 10.0f;

	//local ai stamp
	AIStamp* stamp = ptr->GetAIStamp();

	//local timer
	float timer = enemySkill->second;
	//increment tiemr
	timer -= dt;
	//set the timer
	enemySkill->second = timer;

	//cannot activate ability yet
	if (timer > 0.0f)
		return false;

	if (ptr->GetTimeStamp()->isDone)
	{
		//reset timer
		timer = 10.0f;
		//set timer
		enemySkill->second = cooldown;
		ptr->GetTimeStamp()->SwitchAnimation("idle");

		//we're done here -- go away
		return false;
	}

	ptr->SetVelocity({ 0.f, 0.f });
	ptr->GetTimeStamp()->SwitchAnimation("special3");
	return true;

}

