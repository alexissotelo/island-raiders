#pragma once
#include "EnemyAI.h"

class DefenseAI : public EnemyAI
{
public:
	DefenseAI() = default;
	~DefenseAI() = default;

	virtual void Update(float dt, AIStamp& _timeStamp);
	EnemyAIType GetAIType() const { return EnemyAIType::Defense; }

};

