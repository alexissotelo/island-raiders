#include "Familiar.h"
#include "EnemyAI.h"

#include "DestroyEntityMessage.h"
#include "../SGD Wrappers/SGD_MessageManager.h"

#include "../SGD Wrappers/SGD_Event.h"
#include "../SGD Wrappers/SGD_EventManager.h"

#include "PlayGameState.h"

Familiar::Familiar(const char* name, Character* owner) : Character(name)
{
	//safe null initialization
	m_pOwner = owner;

	//set the name for animations
	SetName(name);
	//set the owner
	SetOwner(owner);
	//m_pOwner = owner;
	//set the AI
	aiStamp.owner = this;
	aiStamp.combatType = CombatType::Familiar;
	aiStamp.aiType = EnemyAIType::Idle;
	aiStamp.tier = 1;
	//set animations
	aTimeStamp.SwitchAnimation("idle");

	//set size?
	SetSize({ 20.f, 20.f });
}


Familiar::~Familiar()
{
}

void Familiar::Update(float dt)
{
	//Update all
	Character::Update(dt);

	StayInWorld();
	//AI Manager
	AIManager::GetInstance()->Update(dt, aiStamp);
}


bool Familiar::HandleCollision(const IEntity* ptr)
{
	//safe check
	if (!ptr)
		return true;

	if (ptr->GetType() == this->GetType())
		return true;


	switch (ptr->GetType())
	{
	case EntityType::ENT_PLAYER:
	case EntityType::ENT_ENEMY:
	{
								  //convert to a character pointer
								  const Character* other = dynamic_cast<const Character*>(ptr);

								  const SGD::Rectangle attack = this->GetActiveRect();
								  const SGD::Rectangle enemyCol = other->GetCollisionRect();

#if 0
								  //create a damage event
								  SGD::Event* counter = new SGD::Event( "PARTNER", (void*)other, this );
								  //send it out
								  SGD::EventManager::GetInstance()->SendEventNow(counter, m_pOwner);
#endif

#if 1
								  if (attack.IsIntersecting(enemyCol) && aTimeStamp.m_sAction == "attack2")
								  {
									  //create a damage event
									  SGD::Event counter = { "KNOCKBACK", nullptr, this };
									  //send it out
									  counter.SendEventNow(other);

								  }
#endif
	}
		break;
	}

	return Character::HandleCollision(ptr);
	return false;

}

void Familiar::HandleEvent(const SGD::Event* pEvent)
{
	Character::HandleEvent(pEvent);

#pragma region HP Check
	//am i dead?
	if (stat.health <= 0)
	{
		//cap at zero
		stat.health = 0;

		SGD::Event down = { "RIP", nullptr, this };
		down.SendEventNow(m_pOwner);

		//kill yourself
		DestroyEntityMessage* destroy = new DestroyEntityMessage(this);
		SGD::MessageManager::GetInstance()->QueueMessage(destroy);

		//stop looking at me!!
		PlayGameState::GetInstance()->ClearCharacterTargets(this);
	}

#pragma endregion
}

void Familiar::SetOwner(Character* ptr)
{
	if (m_pOwner)
	{

		m_pOwner->Release();
		m_pOwner = nullptr;

	}

	m_pOwner = ptr;

	if (m_pOwner)
		m_pOwner->AddRef();
}

unsigned int Familiar::GetType() const
{
	//return the owner type or just return character type
	return (m_pOwner) ? m_pOwner->GetType() : ENT_CHARACTER;
}
