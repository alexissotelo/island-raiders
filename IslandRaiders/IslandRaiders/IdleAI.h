#pragma once
#include "EnemyAI.h"
/****************************************************
| File:	IdleAI
| Purpose: IdleAI will define the behavior of Enemy types 
|			that are in waiting and idling based on their tier
****************************************************/

class IdleAI : public EnemyAI
{
public:
	IdleAI() = default;
	~IdleAI() = default;

	void Update(float dt, AIStamp& _timeStamp) ;
	virtual EnemyAIType GetAIType() const  { return EnemyAIType::Idle; }

private:
	void TierZeroWait(float dt, AIStamp& _timeStamp);
	void TierOneWait(float dt, AIStamp& _timeStamp);
	void TierTwoWait(float dt, AIStamp& _timeStamp);
	void BossWait(float dt, AIStamp& _timeStamp);
	void FamiliarIdle(float dt, AIStamp& _timestamp);

};

