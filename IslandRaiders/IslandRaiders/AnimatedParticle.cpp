#include "AnimatedParticle.h"
#include "AnimationSystem.h"
#include "PlayGameState.h"
#include "Character.h"
#include "Animation.h"
#include "Frame.h"

#include "DestroyEntityMessage.h"
#include "../SGD Wrappers/SGD_Event.h"
#include "../SGD Wrappers/SGD_Geometry.h"
#include "../SGD Wrappers/SGD_MessageManager.h"
#include "../SGD Wrappers/SGD_EventManager.h"


/**************************************************************
|	Function:
|	Purpose:
|	Parameter:
**************************************************************/
AnimatedParticle::AnimatedParticle(Character* _owner, const char* _name, float _span, SGD::Point position, SGD::Vector vel) : Entity(_name)
{
	//set all values
	m_pOwner = nullptr;
	SetOwner(_owner);
	SetLifeSpan(_span);
	aTimeStamp.SwitchAnimation("animate");

	m_bMotionless = !(position.x == 0 && position.y == 0); // && vel.x != 0 && vel.y != 0);

	SetVelocity(vel);
	SetPosition({ position.x, position.y + 1 });
}

/**************************************************************
|	Function:
|	Purpose:
|	Parameter:
**************************************************************/
AnimatedParticle::~AnimatedParticle()
{
	if (GetName() == "target")
	{
		//event
		SGD::Event evt = { "MARKED", nullptr, this };
		//unmark
		evt.SendEventNow(GetOwner());
	}

	SetOwner(nullptr);

}

/**************************************************************
|	Function:
|	Purpose:
|	Parameter:
|	Return Value:
**************************************************************/
void AnimatedParticle::Update(float dt)
{

	if (m_fLifespan <= 0.0f || aTimeStamp.isDone || !m_pOwner)
	{
		//kill yourself
		DestroyEntityMessage* msg = new DestroyEntityMessage(this);
		SGD::MessageManager::GetInstance()->QueueMessage(msg);
	}

	//stay on the player
	if (m_pOwner && !m_bMotionless)
	{
		SGD::Point pos = m_pOwner->GetPosition();
		pos.y += 1;
		SetPosition(pos);
	}

	if (velocity.x != 0 || velocity.y != 0)
		position += velocity;
	//update animation
	AnimationSystem::GetInstance()->Update(dt, &aTimeStamp);

	//lower the time
	m_fLifespan -= dt;

}

/**************************************************************
|	Function:
|	Purpose:
|	Parameter:
|	Return Value:
**************************************************************/
void AnimatedParticle::Render()
{
	SGD::Point camPos = PlayGameState::GetInstance()->GetCameraPosition();

	//if (!aTimeStamp.isDone)
	AnimationSystem::GetInstance()->Render(&aTimeStamp,
	{ position.x - camPos.x, position.y - camPos.y });

}

/**************************************************************
|	Function:
|	Purpose:
|	Parameter:
|	Return Value:
**************************************************************/
bool AnimatedParticle::HandleCollision(const IEntity* ptr)
{

#pragma region Owner Inflicting

	if (GetName() == "bugburn")
	{
		if (ptr == GetOwner())
		{
			//damage to deal
			float dmg = -1.0f;
			//event to the colliding entity
			SGD::Event* evt = new SGD::Event("DAMAGE", &dmg, this);
			evt->SendEventNow(m_pOwner);
			delete evt;
			return false;
		}
		return true;
	}
	else if (GetName() == "lighting")
	{
		if (ptr == GetOwner())
		{
			//damage to deal
			float dmg = -5.0f;
			//event to the colliding entity
			SGD::Event* evt = new SGD::Event("DAMAGE", &dmg, this);
			evt->SendEventNow(m_pOwner);
			delete evt;

			aTimeStamp.m_CurrentFrame++;
			aTimeStamp.m_fFrameTimer = 0;
			return false;
		}
		return true;
	}
	else if (GetName() == "snakes")
	{
		if (ptr == GetOwner())
		{
			//damage to deal
			float dmg[] = { 7.0f, 3.0f };
			//event to the colliding entity
			SGD::Event* evt = new SGD::Event("POISON", &dmg, this);
			evt->SendEventNow(m_pOwner);
			delete evt;
			return false;
		}
		return true;
	}
	//else if (GetName() == "target")
	//{
	//	//cast to an entity for damage
	//	const Missile* ent = dynamic_cast<const Missile*>(ptr);

	//	//missile hitting?
	//	if (!ent || !m_pOwner || ent->GetOwner()->GetType() != ENT_PLAYER)
	//		return false;

	//	//damage value
	//	float damn = ent->GetDamage() * 2.0f;
	//	//send to the owner
	//	SGD::Event* ouch = new SGD::Event("SMACK", &damn, this);
	//	ouch->SendEventNow(m_pOwner);
	//	delete ouch;
	//	ouch = nullptr;
	//	return false;
	//}
#pragma endregion


	if (this == ptr || GetOwner() == ptr || ptr->GetType() == this->GetType())
		return true;

	if (m_pOwner)
	{
		const Missile* missilePtr = dynamic_cast<const Missile*>(ptr);
		if (missilePtr && missilePtr->GetOwner() && missilePtr->GetOwner()->GetType() == this->GetOwner()->GetType())
		{
			if (missilePtr->GetName() != "pinkmissile" /*|| missilePtr->GetName() == "target"*/)
				return true;
		}

		const AnimatedParticle* animPtr = dynamic_cast<const AnimatedParticle*>(ptr);
		if (animPtr && animPtr->GetOwner() && animPtr->GetOwner()->GetType() == this->GetOwner()->GetType())
			return true;


		if (ptr->GetType() == m_pOwner->GetType())
			return true;

		SGD::Event* evt = new SGD::Event("POKE", (void*)ptr, this);
		evt->SendEventNow(m_pOwner);
		delete evt;
		evt = nullptr;

	}
	return false;
}

/**************************************************************
|	Function:
|	Purpose:
|	Parameter:
|	Return Value:
**************************************************************/
void AnimatedParticle::HandleEvent(const SGD::Event* pEvent)
{
#pragma region MOVE
	if (pEvent->GetEventID() == "MOVE")
	{
		//move 
		SGD::Vector forwards = SGD::Vector(1, 0);
		//directions
		if (!aTimeStamp.bForwards)
			forwards.x *= -1;

		//scale
		forwards *= 10;

		//set velocity
		SetVelocity(forwards);
	}
	else if (pEvent->GetEventID() == "STOP")
	{
		SetVelocity({ 0.f, 0.f });
	}
#pragma endregion

}

void AnimatedParticle::SetOwner(Character* ptr)
{
	if (m_pOwner)
	{
		m_pOwner->Release();
		m_pOwner = nullptr;
	}
	//set pointer
	m_pOwner = ptr;

	if (m_pOwner)
		m_pOwner->AddRef();
}

/**************************************************************
|	Function:
|	Purpose:
|	Parameter:
|	Return Value:
**************************************************************/
//SGD::Rectangle	AnimatedParticle::GetCollisionRect(void) const
//{
//	//current actions name
//	std::string act = aTimeStamp.m_sOwnerName;
//	act += "_";
//	act += aTimeStamp.m_sAction;
//
//	//dead?
//	if (aTimeStamp.m_sAction == "die" || aTimeStamp.m_sAction == "stand")
//		return SGD::Rectangle(0, 0, 0, 0);
//
//	//position 
//	SGD::Point topLeft = GetPosition();
//	//current animation
//	const Animation* currentAniamtion = (AnimationSystem::GetInstance()->GetAnimation(act));
//	const Frame* currentFrame = (currentAniamtion->operator[](aTimeStamp.m_CurrentFrame));
//	const SGD::Rectangle collisionRect = currentFrame->m_rCollision;
//
//	//if (collisionRect.)
//	//	return SGD::Rectangle(0, 0, 0, 0);
//
//	//size
//	SGD::Size size;
//	size.width = collisionRect.right;
//	size.height = collisionRect.bottom;
//
//	//offset position
//	SGD::Point offset = currentFrame->m_ptCollisionOffset;
//	topLeft.x += currentAniamtion->IsSheetBackwards() ? -offset.x - size.width : offset.x;
//	topLeft.y += offset.y;
//
//	//turn around
//	if (!aTimeStamp.bForwards /*&& currentAniamtion->IsSheetBackwards() == false*/)
//	{
//		if (!currentAniamtion->IsSheetBackwards())
//		{
//			//inverse the render position
//			topLeft.x = GetPosition().x - offset.x;
//			topLeft.x -= size.width;
//		}
//		else
//			//inverse the render position
//			topLeft.x = GetPosition().x + offset.x;
//	}
//
//
//	//return the rect
//	return SGD::Rectangle(topLeft, size);
//}

///**************************************************************
//|	Function:
//|	Purpose:
//|	Parameter:
//|	Return Value:
//**************************************************************/
//SGD::Rectangle	AnimatedParticle::GetActiveRect(void) const
//{
//	//current actions name
//	std::string act = aTimeStamp.m_sOwnerName;
//	act += "_";
//	act += aTimeStamp.m_sAction;
//
//	//dead?
//	if (aTimeStamp.m_sAction == "die" || aTimeStamp.m_sAction == "stand")
//		return SGD::Rectangle(0, 0, 0, 0);
//
//	//current animation
//	const Animation* currentAniamtion = (AnimationSystem::GetInstance()->GetAnimation(act));
//	const Frame* currentFrame = (currentAniamtion->operator[](aTimeStamp.m_CurrentFrame));
//	const SGD::Rectangle activeRect = currentFrame->m_rActive;
//
//	//if (collisionRect.)
//	//	return SGD::Rectangle(0, 0, 0, 0);
//
//	//size
//	SGD::Size size;
//	size.width = activeRect.right;
//	size.height = activeRect.bottom;
//
//	//top left
//	SGD::Point topLeft = GetPosition();
//	//offset position
//	SGD::Point offset = currentFrame->m_ptActiveOffset;
//	topLeft.x += currentAniamtion->IsSheetBackwards() ? -offset.x - size.width : offset.x;
//	topLeft.y += offset.y;
//
//	//turn around
//	if (!aTimeStamp.bForwards /*&& currentAniamtion->IsSheetBackwards() == false*/)
//	{
//		if (!currentAniamtion->IsSheetBackwards())
//		{
//			//inverse the render position
//			topLeft.x = GetPosition().x - offset.x;
//			topLeft.x -= size.width;
//		}
//		else
//			//inverse the render position
//			topLeft.x = GetPosition().x + offset.x;
//	}
//
//
//	//return the rect
//	return SGD::Rectangle(topLeft, size);
//}

