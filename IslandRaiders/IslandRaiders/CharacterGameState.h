#pragma once
#include "IGameState.h"
#include "../SGD Wrappers/SGD_AudioManager.h"
#include "../SGD Wrappers/SGD_GraphicsManager.h"
#include "../SGD Wrappers/SGD_InputManager.h"
#include <vector>

#define NUM_CHARACTERS 6

class BitmapFont;
class GameInfo;

struct DataPacket
{
	//controller number
	unsigned int m_numController;
	//character to select
	int m_numCharacter;
	//confirmation
	bool m_bConfirm = false;
	//include
	bool m_bInclude = false;
	//name
	const char* m_cName = nullptr;

};

class CharacterGameState :
	public IGameState
{
public:

	void CreateParty();
	virtual void Enter();
	virtual void Exit();
	virtual bool Update(float dt);
	virtual void Render(float dt);
	static CharacterGameState* GetInstance(void);
	static void DeleteInstance(void);
	void UnloadTextures(void);

	// accessor
	void GetPlayerPacket(std::vector<DataPacket>& vec) const;
	unsigned int GetNumPlayers(void) const { return numPlayers; }
private:
	void RenderKeyboard();
	void RenderController();
	bool controller = false;
	int currPartySize = 0;
	CharacterGameState() = default;
	~CharacterGameState() = default;
	CharacterGameState(const CharacterGameState&) = delete;
	CharacterGameState operator=(const CharacterGameState&) = delete;

	const BitmapFont* font = nullptr;
	static CharacterGameState* pInstance;
	SGD::HTexture backGround = SGD::INVALID_HANDLE;

	//array of character portraits
	SGD::HTexture icons[NUM_CHARACTERS];
	std::string names[NUM_CHARACTERS];
	std::string classNames[NUM_CHARACTERS];
	SGD::HTexture X = SGD::INVALID_HANDLE;
	DataPacket data[4];

	unsigned int numPlayers = 0;
	unsigned int numPlayersConfirmed = 0;

	float timer = 0.f;
	float timerFreq = 0.5f;
	void ArcadeCursorMovement(float dt);
	bool ArcadeCharacterSelection(float dt);
	void RenderArcade();

	SGD::InputManager* pInput = SGD::InputManager::GetInstance();


	/**********************************************************/
	// Cursor Index
	int		m_nCursor = 1;
	void CharacterGameState::SelectCharacter(int controllerNumber, int characterNumber);
};

