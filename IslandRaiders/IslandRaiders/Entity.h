#pragma once
#include "IEntity.h"
#include "../SGD Wrappers/SGD_GraphicsManager.h"
#include "..\SGD Wrappers\SGD_Geometry.h"
#include "../SGD Wrappers/SGD_Listener.h"
#include "../SGD Wrappers/SGD_Handle.h"
#include "AnimationTimestamp.h"
#include <vector>

enum EntityType
{
	ENT_BASE, ENT_INVULNERABLE,//BACKGROUND
	ENT_CHARACTER, ENT_PLAYER, ENT_ENEMY, ENT_MISSILE, ENT_ANIM_PARTICLE, ENT_ITEMS, ENT_COUNT
	//FOREGROUND
};



class Entity : public IEntity , public SGD::Listener
{
public:
	
	virtual void Update(float dt)override;
	virtual void Render()override;

	virtual bool HandleCollision(const IEntity* other) override;
	virtual void AddRef() final;
	virtual void Release()final;
	virtual void HandleEvent(const SGD::Event* pEvent);

	//accessors
	virtual unsigned int GetType()	const override	{ return type; }
	SGD::Rectangle GetRect()		const override;
	std::string		GetName()			const			{ return name; }
	SGD::Point	   GetPosition()	const			{ return position; }
	SGD::Vector	   GetVelocity()	const			{ return velocity; }
	SGD::Size	   GetSize()		const			{ return size; }
	SGD::HTexture GetImage()		const			{ return image; }
	float GetRotation()				const			{ return rotation; }

	virtual SGD::Rectangle GetRenderRect(void) const;
	virtual SGD::Rectangle GetCollisionRect(void) const;
	virtual SGD::Rectangle GetActiveRect(void) const;
	virtual bool GetSpawnPoint(SGD::Point) const;


	//mutators
	virtual void SetType(unsigned int t)		{ type = t; }
	void SetPosition(SGD::Point p)				{ position = p; }
	void SetVelocity(SGD::Vector vel)			{ velocity = vel; }
	void SetSize(SGD::Size SIZE)				{ size = SIZE; }
	void SetImage(SGD::HTexture img)			{ image = img; }
	void SetRotation(float rotate)				{ rotation = rotate; }
	void SetName(std::string name);
	void SetName(const char* name);


	//accessor for timestamp
	AnimationTimestamp* GetTimeStamp(void) { return &aTimeStamp; } //modifiable timestamp
	const AnimationTimestamp* GetTimeStamp(void) const { return &aTimeStamp; } //read-only timestamp

	Entity(const char* = nullptr);
	virtual ~Entity();

protected:

	std::string name;

	SGD::Point	position;
	SGD::Vector velocity;
	SGD::Size	size;
	SGD::HTexture image = SGD::INVALID_HANDLE;
	float rotation;
	unsigned int type;
	unsigned int refCount = 1;

	//all entities are animated
	AnimationTimestamp aTimeStamp;

};

