#pragma once
#include "SkillStamp.h"
#include <map>

//class container for SkillStamp
struct PlayerSkillStamp
{
	//conatiner for ALL information needed for a player
	std::map<unsigned int, SkillStamp> m_mapSkills;

	//operator overloading
	SkillStamp& operator[]( unsigned int index);
	float SkillCooldown(int index){ return m_mapSkills[index].m_fSkillTimer / m_mapSkills[index].m_fSkillCooldown; }

};


