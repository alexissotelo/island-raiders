#include "ParticleEmitter.h"
#include "ParticleManager.h"
#include <math.h>

ParticleEmitter::ParticleEmitter(ParticleFlyWeight* flyweight,
	int _maxParticles, float _spawnRate, SGD::Size siz)
{
	particleFlyWeight = flyweight;
	maxParticles = _maxParticles;
	spawnRate = _spawnRate;

	size = siz;

	for (int i = 0; i < maxParticles; i++)
		pool.push_back(new Particle(this));
}
ParticleEmitter::ParticleEmitter(
	int FlyweightIndex, int _maxParticles, float _spawnRate, SGD::Size siz)
{
	particleFlyWeight = ParticleManager::GetInstance()->GetFlyWeight(FlyweightIndex);
	maxParticles = _maxParticles;
	spawnRate = _spawnRate;

	size = siz;
	for (int i = 0; i < maxParticles; i++)
		pool.push_back(new Particle(this));
}

ParticleEmitter::~ParticleEmitter()
{
	for (unsigned int i = 0; i < pool.size(); i++)
	{
		delete pool[i];
		pool[i] = nullptr;
	}
	pool.clear();
}
bool ParticleEmitter::Update(float dt)
{
	if (emitTime > 0)
	{
		emitTimer += dt;
		if (emitTimer > emitTime)
		{
			emit = false;
			if (playOnce && emitTimer > emitTime + 1.f)
			{
				return true;//remove
			}
		}
	}

	//update emitter
	position += velocity;

#if 0
	//if time to spawn a particle
	spawnTimer += dt;
	if (spawnTimer > spawnRate && emit)
	{
		//find first disabled particle and enable it
		spawnTimer = 0;
		for (unsigned int i = 0; i < pool.size(); i++)
		{
			if (!pool[i]->enabled)
			{
				pool[i]->Enable();
				break;
			}
		}
	}
#endif
	//if time to spawn a particle
	spawnTimer = 0;
	if (emit)
	{
		//find first disabled particle and enable it
		spawnTimer = 0;
		for (unsigned int i = 0; i < pool.size(); i++)
		{
			if (!pool[i]->enabled)
			{
				pool[i]->Enable();
				spawnTimer++;
			}
			if (spawnTimer >= spawnRate)
				break;
		}
	}

	//update particles
	for (unsigned int i = 0; i < pool.size(); i++)
	{
		pool[i]->Update(dt);
	}
	return false;
}
void ParticleEmitter::Render()
{
	//only renders the particles based on the enableCount
	for (unsigned int i = 0; i < pool.size(); i++)
		pool[i]->Render();
}
