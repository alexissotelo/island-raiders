#include "../Source/Game.h"
#include "Animation.h"
#include "AnimationTimestamp.h"
#include "AnimationSystem.h"
#include "Frame.h"
#include "PlayGameState.h"
#include "../SGD Wrappers/SGD_GraphicsManager.h"
#include "../SGD Wrappers/SGD_AudioManager.h"
#include "../SGD Wrappers/SGD_MessageManager.h"
#include "../SGD Wrappers/SGD_EventManager.h"
#include "../SGD Wrappers/SGD_Event.h"
#include "CreateMissileMessage.h"
#include "CreateAnimatedParticle.h"
#include "AnimatedParticle.h"
#include "AudioQueue.h"

#include <vector>
#include <string>

/**************************************************************
|	Function: Animation Constructor
|	Purpose: to initialize any variables
|	Parameter: (void)
**************************************************************/
Animation::Animation(void)
{}

/**************************************************************
|	Function: Animation Destrctor
|	Purpose: to remove from memory all class variables
|	Parameter: void
**************************************************************/
Animation::~Animation(void)
{
	//delete all frames
	for (unsigned int i = 0; i < m_vFrames.size(); ++i)
	{
		delete m_vFrames[i];
		m_vFrames[i] = nullptr;
	}

	//clear the vector
m_vFrames.clear();
}

/**************************************************************
|	Function: Animation::Render
|	Purpose: display to the screen the current entity image using
|			the position as its anchor point
|	Parameter:
|			SGD::HTexture _texture - the texture to use for the characters image
|			unsigned int _frame - the current frame in the animation to render
|			SGD::Point _pos - the position/anchor point of the character
|			bool _forwards - the direction of the character (true when the character is facing right)
|			float scale _scale - a value used to scale the characters image
|	Return Value: void
**************************************************************/
void Animation::Render(SGD::HTexture _texture, unsigned int _frame, SGD::Point _pos, bool _forwards, SGD::Color color, float _scale) const
{
	//local graphics pointer
	SGD::GraphicsManager* pGraphics = SGD::GraphicsManager::GetInstance();

	//frame to render
	Frame* frame = m_vFrames[_frame];

	//calcluate the render rectangle
	SGD::Point topLeft;
	// X = anchorPoint + offest
	topLeft.x = _pos.x + frame->m_ptRenderOffset.x;
	//Y = anchorPoint + offset
	topLeft.y = _pos.y + frame->m_ptRenderOffset.y;

	//Render Size -- 
	SGD::Size size;
	size.width = frame->m_rRender.right;
	size.height = frame->m_rRender.bottom;

	//render rect
	SGD::Rectangle sourceRect = frame->m_rRender;

	//scale size
	SGD::Size scale = SGD::Size(_scale, _scale);
	//turn around
	if (!_forwards)
	{
		//inverse the render position
		topLeft.x = _pos.x - frame->m_ptRenderOffset.x;
		//inverse scale
		scale.width *= -1.0f;

	}

	//vector
	SGD::Size frameSize;
	frameSize.width = frame->m_rRender.right - frame->m_rRender.left;
	frameSize.height = frame->m_rRender.bottom - frame->m_rRender.top;

	SGD::Vector flipPooint = SGD::Vector(frameSize.width *0.5f, frameSize.height * 0.5f);

	//render
	pGraphics->DrawTextureSection(_texture, topLeft, sourceRect, 0.f, flipPooint,
		color, scale);

#pragma region DEBUG
	if (Game::GetInstance()->bDebug)
	{

		//shade the character for events
#pragma region EVENTS Rect
		if (frame->m_sEvent != "NONE" && frame->m_sEvent != "")
		{
			//render rect
			topLeft.x = _pos.x + frame->m_ptRenderOffset.x;
			topLeft.y = _pos.y + frame->m_ptRenderOffset.y;
			//draw event
			pGraphics->DrawRectangle(SGD::Rectangle(topLeft, frameSize), SGD::Color(50, 0, 150, 150));
		}

#pragma endregion
		//render the collision box in BLUE
#pragma region Collision Rect
		topLeft.x = _pos.x + frame->m_ptCollisionOffset.x;
		topLeft.y = _pos.y + frame->m_ptCollisionOffset.y;

		//render rect
		SGD::Rectangle rect;
		//Render Size -- 
		SGD::Size size;
		size.width = frame->m_rCollision.right;// -frame->m_rCollision.left;
		size.height = frame->m_rCollision.bottom;// -frame->m_rCollision.top;

		if (size.width != 0 && size.height != 0)
		{
			//scale size
			SGD::Size scale = SGD::Size(_scale, _scale);
			//turn around
			if (!_forwards)
			{
				//inverse the render position
				topLeft.x = _pos.x - frame->m_ptCollisionOffset.x;
				topLeft.x -= size.width;
			}

			//rectangle
			rect = SGD::Rectangle(topLeft, size);
			if (!rect.IsEmpty())
				//draw rect collision
				pGraphics->DrawRectangle(rect, SGD::Color(50, 0, 0, 255));
		}
#pragma endregion
		//render the active box in RED
#pragma region Active Rect

		//render the collision box in BLUE
		topLeft.x = _pos.x + frame->m_ptActiveOffset.x;
		topLeft.y = _pos.y + frame->m_ptActiveOffset.y;

		//Render Size -- 
		size.width = frame->m_rActive.right;
		size.height = frame->m_rActive.bottom;

		if (size.width != 0 && size.height != 0)
		{
			//scale size
			//SGD::Size scale = SGD::Size(_scale, _scale);

			//turn around
			if (!_forwards)
			{
				//inverse the render position
				topLeft.x = _pos.x - frame->m_ptActiveOffset.x;
				topLeft.x -= size.width;
			}

			//rectangle
			rect = SGD::Rectangle(topLeft, size);
			//draw rect collision
			pGraphics->DrawRectangle(rect, SGD::Color(50, 255, 0, 0));
		}
#pragma endregion

	}
#pragma endregion
}

/**************************************************************
|	Function: Animation::AddFrame
|	Purpose: include a frame into the animations vector
|	Parameter:
|				Frame* _frame - pointer to the frame to add into the vector
|	Return Value: void
**************************************************************/
void Animation::AddFrame(Frame* _frame)
{
	//add into the vector
	m_vFrames.push_back(_frame);
}

/**************************************************************
|	Function: Animation::Update
|	Purpose: update the current animation based on the previous status
|			of the character kept within the _timeStamp
|	Parameter:
|				float dt - the tiem that has elapsed in seconds since the last update
|				AnimationTimestamp* _timeStamp - pointer to the character's timstamp
|						holding all information of its current status in animations
|	Return Value: void
**************************************************************/
void Animation::Update(float dt, AnimationTimestamp* _timeStamp)
{
	//update the timer
	_timeStamp->m_fFrameTimer += dt;
	//update the loop
	_timeStamp->bLoop = m_bLoop;

	//local index for the frame
	unsigned int index = _timeStamp->m_CurrentFrame;
	//local ref to the current frame
	Frame* frame = m_vFrames[index];

	//has the current animation reached its limit?
	if (_timeStamp->m_fFrameTimer >= frame->m_fDuration)
	{
		//update the current frame --increment
		++index;
		//reset timer
		_timeStamp->m_fFrameTimer = 0.f;

		//have we reached the end of the animation
		if (index >= m_vFrames.size())
		{

			//looping?
			if (m_bLoop)
				//reset frame
				index = 0;
			else
			{
				--index;
				_timeStamp->isDone = true;
			}
		}
		if (index != m_vFrames.size())
			DoEvent(_timeStamp, m_vFrames[index]);
	}

	//update the timestamp
	_timeStamp->m_CurrentFrame = index;
}

/**************************************************************
|	Function: Animation::Loop [accessor][constant]
|	Purpose: return whether or not the current animation loops
|	Parameter: void
|	Return Value: bool - returns true if the animation loops, returns false otherwise
**************************************************************/
bool Animation::Loop(void) const { return m_bLoop; }

/**************************************************************
|	Function: Animation::operator[]
|	Purpose: return a read-only frame located at the required index
|	Parameter:
|			unsigned int _index - an index to search the animations
|					vector
|	Return Value: Frame* - pointer to the frame found within the vector
**************************************************************/
Frame* Animation::operator[](unsigned int _index) const
{
	if (_index >= m_vFrames.size())
		return nullptr;
	return m_vFrames[_index];
}

/**************************************************************
|	Function: Animation::operator[] [constant]
|	Purpose: return a frame located at the required index
|	Parameter:
|			unsigned int _index - an index to search the animatins vector
|	Return Value: Frame* - pointer to the frame found within the vector
**************************************************************/
Frame* Animation::operator[](unsigned int _index)
{
	if (_index >= m_vFrames.size())
		return nullptr;
	return m_vFrames[_index];
}

/**************************************************************
|	Function: Animation::SetLoop [mutator]
|	Purpose: set the lopoing value of the animation
|	Parameter:
|			bool _loop: a value to set the animations loop to
|						true if the animation is meant to loop
|	Return Value: void
**************************************************************/
void Animation::SetLoop(bool _loop) { m_bLoop = _loop; }

/**************************************************************
|	Function: Animation::SetName [mutator]
|	Purpose: set the name of the animations
|	Parameter:
|			const char* _name: a name to set the animations to
|	Return Value: void
**************************************************************/
void Animation::SetName(const char* _name)
{
	m_sName.clear();
	m_sName = _name;
}

/**************************************************************
|	Function: Animation::SetOrient [mutator]
|	Purpose: set the direction of the sprite sheet used in the animations
|	Parameter:
|				bool _orient: a value to set the sheet's direction to
|						true if the character faces to left
|	Return Value: void
**************************************************************/
void Animation::SetOrient(bool _orient)
{
	m_bSheetInverse = _orient;
}

void Animation::DoEvent(AnimationTimestamp* stamp, Frame* frame)
{
#pragma region COMMENTED OUT
	////create a projectile
	//if (frame->m_sEvent == "SHOOT")
	//{
	//	//position offset to spawn the projectile relative to the anchor
	//	SGD::Point spawnAt;
	//	//y
	//	spawnAt.y = frame->m_ptSpawnOffset.y;
	//	//x
	//	if (stamp->bForwards)
	//		spawnAt.x = frame->m_ptSpawnOffset.x;
	//	else
	//		spawnAt.x = -frame->m_ptSpawnOffset.x;

	//	//camera offset
	//	spawnAt.x -= PlayGameState::GetInstance()->GetCameraPosition().x;

	//	//create a missile
	//	CreateMissileMessage* msg = new CreateMissileMessage((Character*)stamp->GetOwner(), spawnAt);
	//	//queue the message
	//	SGD::MessageManager::GetInstance()->QueueMessage(msg);
	//	//safety
	//	msg = nullptr;
	//}

#pragma endregion

	if (frame->m_sEvent == "NONE" || frame->m_sEvent == "")
		return;

	if (frame->m_sEvent == "sound" || frame->m_sEvent == "frog")
	{
		//play sound
		std::string effectSound = "resources/audio/sound effects/" + frame->m_sEvent + '_' + stamp->m_sOwnerName + ".wav";
		//queue sound
		AudioQueue::GetInstance()->AddSound(SGD::AudioManager::GetInstance()->LoadAudio(effectSound.c_str()));
	}

	CreateMissile(stamp, frame);
	CreateLaser(stamp, frame);
	CreateBeam(stamp, frame);
	CreateParticles(stamp, frame);


#pragma region MOVE, STOP, BACK & WARP

	if (frame->m_sEvent == "MOVE" || frame->m_sEvent == "STOP" || frame->m_sEvent == "WARP" || frame->m_sEvent == "BACK")
	{
		const SGD::Event* move = new SGD::Event(frame->m_sEvent.c_str());
		move->SendEventNow(stamp->GetOwner());

		delete move;
		move = nullptr;
	}
#pragma endregion

#pragma region

	if (frame->m_sEvent == "TRANSFORM")
	{
		AnimatedParticle* ptr = (AnimatedParticle*)stamp->GetOwner();
		
		const SGD::Event* msg = new SGD::Event ( frame->m_sEvent.c_str(), nullptr, ptr );
		SGD::EventManager::GetInstance()->QueueEvent(msg, ptr->GetOwner());
	}

#pragma endregion
	//BREAK?  - remove once all sounds have been added
	if (stamp->GetOwner()->GetType() == ENT_ENEMY)
		return;

#pragma region PLAY SOUNDS

	//play a sound for events
	if (frame->m_sEvent == "ATTACK" || frame->m_sEvent == "SHOOT")
	{
		//SOUND FILE
		std::string soundclip = "resources/audio/sound effects/";
		soundclip += stamp->m_sOwnerName;
		soundclip += "_";
		soundclip += stamp->m_sAction;
		soundclip += ".wav";

		//ATTACK SOUND
		SGD::Event k = { "ATTACK", &soundclip[0] };
		//send event
		//k.SendEventNow();
	}

#pragma endregion


}

bool Animation::CreateMissile(AnimationTimestamp* stamp, Frame* frame)
{
	//check if the word is available
	bool found = false;

	//is the name a missile?
	const std::vector<std::string>* missileList = AnimationSystem::GetInstance()->GetMissileNames();
	for (unsigned int index = 0; index < missileList->size(); ++index)
	{
		if ((*missileList)[index] == frame->m_sEvent)
			found = true;
	}

	//find it?
	if (!found)
		return false;

	//item found -- create a missile message
	SGD::Point spawnAt;
	spawnAt.y = frame->m_ptSpawnOffset.y;

	spawnAt.x = IsSheetBackwards() == stamp->bForwards ? -frame->m_ptSpawnOffset.x : frame->m_ptSpawnOffset.x;

	//position
	spawnAt.x += stamp->GetOwner()->GetPosition().x;
	spawnAt.y += stamp->GetOwner()->GetPosition().y;


	//camera offset
	spawnAt.x -= PlayGameState::GetInstance()->GetCameraPosition().x;
	spawnAt.y -= PlayGameState::GetInstance()->GetCameraPosition().y;

	//type cast to character pointer to access certain data members
	Character* creator = dynamic_cast<Character*>(stamp->GetOwner());

	if (!creator)
	{
		AnimatedParticle* animP = dynamic_cast<AnimatedParticle*>(stamp->GetOwner());
		Missile* missilePtr = dynamic_cast<Missile*>(stamp->GetOwner());

		if (animP)
			creator = dynamic_cast<Character*>(animP->GetOwner());
		else if (missilePtr)
			creator = dynamic_cast<Character*>(missilePtr->GetOwner());
	}


	//create a missile
	CreateMissileMessage* msg = new CreateMissileMessage((Character*)creator, spawnAt, ProjectileType::NORMAL,
		frame->m_sEvent, creator->GetProjectileSpeed());

	//queue the message
	SGD::MessageManager::GetInstance()->QueueMessage(msg);
	//safety
	msg = nullptr;

	//success!
	return true;
}

bool Animation::CreateLaser(AnimationTimestamp* stamp, Frame* frame)
{
	bool found = false;

	//is the name a missile?
	const std::vector<std::string>* missileList = AnimationSystem::GetInstance()->GetLaserNames();
	for (unsigned int index = 0; index < missileList->size(); ++index)
	{
		if ((*missileList)[index] == frame->m_sEvent)
			found = true;
	}
	//find it?
	if (!found)
		return false;

	//item found -- create a missile message
	SGD::Point spawnAt;
	spawnAt.y = frame->m_ptSpawnOffset.y;

	spawnAt.x = IsSheetBackwards() == stamp->bForwards ? -frame->m_ptSpawnOffset.x : frame->m_ptSpawnOffset.x;

	//position
	spawnAt.x += stamp->GetOwner()->GetPosition().x;
	spawnAt.y += stamp->GetOwner()->GetPosition().y;


	//camera offset
	spawnAt.x -= PlayGameState::GetInstance()->GetCameraPosition().x;
	spawnAt.y -= PlayGameState::GetInstance()->GetCameraPosition().y;

	//type cast to character pointer to access certain data members
	Character* creator = (Character*)stamp->GetOwner();

	//direction 

	//create a missile
	CreateMissileMessage* msg = new CreateMissileMessage(creator, spawnAt, ProjectileType::LASER, frame->m_sEvent, creator->GetProjectileSpeed());

	//queue the message
	SGD::MessageManager::GetInstance()->QueueMessage(msg);
	//safety
	msg = nullptr;

	//success!
	return true;
}

bool Animation::CreateBeam(AnimationTimestamp* stamp, Frame* frame)
{
	bool found = false;

	//is the name a missile?
	const std::vector<std::string>* missileList = AnimationSystem::GetInstance()->GetBeamNames();
	for (unsigned int index = 0; index < missileList->size(); ++index)
	{
		if ((*missileList)[index] == frame->m_sEvent)
			found = true;
	}
	//find it?
	if (!found)
		return false;

	//item found -- create a missile message
	SGD::Point spawnAt;
	spawnAt.y = frame->m_ptSpawnOffset.y;
	spawnAt.x = (stamp->bForwards) ? frame->m_ptSpawnOffset.x : -frame->m_ptSpawnOffset.x;
	//position
	spawnAt.x += stamp->GetOwner()->GetPosition().x;
	spawnAt.y += stamp->GetOwner()->GetPosition().y;

	//camera offset
	spawnAt.x -= PlayGameState::GetInstance()->GetCameraPosition().x;
	spawnAt.y -= PlayGameState::GetInstance()->GetCameraPosition().y;

	//type cast to character pointer to access certain data members
	Character* creator = (Character*)stamp->GetOwner();

	//create a missile
	CreateMissileMessage* msg = new CreateMissileMessage(creator, spawnAt, ProjectileType::BEAM,
		frame->m_sEvent, creator->GetProjectileSpeed());

	//queue the message
	SGD::MessageManager::GetInstance()->QueueMessage(msg);
	//safety
	msg = nullptr;

	//success!
	return true;
}

bool Animation::CreateParticles(AnimationTimestamp* stamp, Frame* frame)
{
	bool found = false;

	//is the name a missile?
	const std::vector<std::string>* missileList = AnimationSystem::GetInstance()->GetParticleNames();
	for (unsigned int index = 0; index < missileList->size(); ++index)
	{
		if ((*missileList)[index] == frame->m_sEvent)
			found = true;
	}
	//find it?
	if (!found)
		return false;

	//item found -- create a missile message
	SGD::Point spawnAt;
	spawnAt.y = frame->m_ptSpawnOffset.y;
	spawnAt.x = (stamp->bForwards) ? frame->m_ptSpawnOffset.x : -frame->m_ptSpawnOffset.x;

	//position
	spawnAt.x += stamp->GetOwner()->GetPosition().x;
	spawnAt.y += stamp->GetOwner()->GetPosition().y;

	//type cast to character pointer to access certain data members
	Character* creator = dynamic_cast<Character*>(stamp->GetOwner());

	if (!creator)
	{
		AnimatedParticle* animP = dynamic_cast<AnimatedParticle*>(stamp->GetOwner());
		if (animP)
			creator = animP->GetOwner();
		

		Missile* missilePtr = dynamic_cast<Missile*>(stamp->GetOwner());
		if (missilePtr)
			creator = missilePtr->GetOwner();
	}

	//create a particle
	CreateAnimatedParticle* msg = new CreateAnimatedParticle(creator, frame->m_sEvent, 3.0f, { 255, 255, 255, 255 }, spawnAt);

	//queue the message
	SGD::MessageManager::GetInstance()->QueueMessage(msg);
	//safety
	msg = nullptr;

	//success
	return true;
}

