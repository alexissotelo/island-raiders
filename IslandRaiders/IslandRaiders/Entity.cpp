#include "Entity.h"
#include "PlayGameState.h"

#include "../SGD Wrappers/SGD_Geometry.h"
#include "../SGD Wrappers/SGD_Event.h"
#include "../SGD Wrappers/SGD_Handle.h"
#include "../SGD Wrappers/SGD_EventManager.h"
#include "../SGD Wrappers/SGD_AudioManager.h"
#include "AnimationSystem.h"
#include "Animation.h"
#include "Frame.h"
#include "AudioQueue.h"
#include <cassert>


Entity::Entity(const char* _name) : Listener(this)
{
	//set the name
	SetName(_name);
	//owner
	aTimeStamp.SetOwner(this);
	//coloring
	aTimeStamp.m_clrRender = SGD::Color(255, 255, 255, 255);

	Listener::RegisterForEvent("MOVE");
	Listener::RegisterForEvent("STOP");

}
Entity::~Entity()
{
	_CrtCheckMemory();
}

void Entity::Update(float dt)
{
	if (aTimeStamp.GetOwner())
	//update animations
	AnimationSystem::GetInstance()->Update(dt, &aTimeStamp);

	//eulers
	position += velocity * dt;
}

void Entity::Render()
{
	//camera offset
	SGD::Point camPos = PlayGameState::GetInstance()->GetCameraPosition();

	//render animations as long as it exists
	if (aTimeStamp.GetOwner())
		AnimationSystem::GetInstance()->Render(&aTimeStamp,
		{ position.x - camPos.x, position.y - camPos.y });

	//image but no animations
	else if (image != SGD::INVALID_HANDLE)
	{
		//onscreen position
		SGD::Point pos = { position.x - camPos.x, position.y - camPos.y };
		//render to the screen
		SGD::GraphicsManager::GetInstance()->DrawTexture(image, pos);
	}
	//nothins but space
	else 
	{
		SGD::Point pos = { position.x - camPos.x, position.y - camPos.y };
		SGD::GraphicsManager::GetInstance()->DrawRectangle(SGD::Rectangle(pos, size), { 100, 255, 255, 255 });
	}

}

bool	Entity::HandleCollision(const IEntity* other)
{
	return false;
}
/**************************************************************/
// AddRef
//	- increase the reference count
/*virtual*/ void Entity::AddRef(void)
{
	assert(refCount != 0xFFFFFFFF && "Entity::AddRef - maximum reference count has been exceeded");
	++refCount;
}
/**************************************************************/
// Release
//	- decrease the reference count
//	- self-destruct when the count is 0
/*virtual*/ void Entity::Release(void)
{

	--refCount;
	if (refCount == 0)
	{
		delete this;
	}

}

SGD::Rectangle	Entity::GetRect() const
{
	return SGD::Rectangle{ position, size };
}

void Entity::HandleEvent(const SGD::Event* pEvent)
{
	if (pEvent->GetEventID() == "ATTACK")
	{

		//use the sent string
		const char* sound = (const char*)pEvent->GetData();
		//load audio file
		SGD::HAudio sfx = SGD::AudioManager::GetInstance()->LoadAudio(sound);
		//play audio file
		SGD::AudioManager::GetInstance()->PlayAudio(sfx);
		//unload
		AudioQueue::GetInstance()->AddSound(sfx);
	}
}

/**************************************************************
|	Function: Character::GetRenderRect [constant]
|	Purpose: retrieve the rectangle that renders the image
|	Parameter: void
|	Return Value: SGD::Rectangle - rectangle in screen space that
|						the image is rendering into
**************************************************************/
SGD::Rectangle Entity::GetRenderRect(void) const
{
	//current actions name
	std::string act = GetName();
	act += "_";
	act += aTimeStamp.m_sAction;

	//current animation
	const Animation* currentAniamtion = (AnimationSystem::GetInstance()->GetAnimation(act));
	const Frame* currentFrame = (currentAniamtion->operator[](aTimeStamp.m_CurrentFrame));
	const SGD::Rectangle renderRect = currentFrame->m_rRender;

	//dead?
	if (aTimeStamp.m_sAction == "die" || aTimeStamp.m_sAction == "stand")
		return SGD::Rectangle(0, 0, 0, 0);

	//size
	SGD::Size size;
	size.width = renderRect.right - renderRect.left;
	size.height = renderRect.bottom - renderRect.top;

	//top left
	SGD::Point topLeft = GetPosition();
	//offset position
	SGD::Point offset = currentFrame->m_ptRenderOffset;
	topLeft.x += currentAniamtion->IsSheetBackwards() ? -offset.x - size.width : offset.x;
	topLeft.y += offset.y;

	//turn around
	if (!aTimeStamp.bForwards /*&& currentAniamtion->IsSheetBackwards() == false*/)
	{
		if (!currentAniamtion->IsSheetBackwards())
		{
			//inverse the render position
			topLeft.x = GetPosition().x - offset.x;
			topLeft.x -= size.width;
		}
		else
			//inverse the render position
			topLeft.x = GetPosition().x + offset.x;
	}

	//return the rect
	return SGD::Rectangle(topLeft, size);
}

/**************************************************************
|	Function: Character::GetCollisionRect [constant]
|	Purpose: retrieve the rectangle that is used to check collision
|	Parameter: void
|	Return Value: SGD::Rectangle - rectangle in screen space that
|						the image is using to collide
**************************************************************/
SGD::Rectangle Entity::GetCollisionRect(void) const
{
	//current actions name
	std::string act = GetName();
	act += "_";
	act += aTimeStamp.m_sAction;

	//dead?
	if (aTimeStamp.m_sAction == "die" || aTimeStamp.m_sAction == "stand")
		return SGD::Rectangle(0, 0, 0, 0);

	//position 
	SGD::Point topLeft = GetPosition();
	//current animation
	const Animation* currentAniamtion = (AnimationSystem::GetInstance()->GetAnimation(act));
	const Frame* currentFrame = (currentAniamtion->operator[](aTimeStamp.m_CurrentFrame));
	const SGD::Rectangle collisionRect = currentFrame->m_rCollision;

	//size
	SGD::Size size;
	size.width = collisionRect.right;
	size.height = collisionRect.bottom;

	//offset position
	SGD::Point offset = currentFrame->m_ptCollisionOffset;
	topLeft.x += currentAniamtion->IsSheetBackwards() ? -offset.x - size.width : offset.x;
	topLeft.y += offset.y;

	//turn around
	if (!aTimeStamp.bForwards /*&& currentAniamtion->IsSheetBackwards() == false*/)
	{
		if (!currentAniamtion->IsSheetBackwards())
		{
			//inverse the render position
			topLeft.x = GetPosition().x - offset.x;
			topLeft.x -= size.width;
		}
		else
			//inverse the render position
			topLeft.x = GetPosition().x + offset.x;
	}


	//return the rect
	return SGD::Rectangle(topLeft, size);
}

/**************************************************************
|	Function: Character::GetRenderRect [constant]
|	Purpose: retrieve the rectangle that character uses to attack
|	Parameter: void
|	Return Value: SGD::Rectangle - rectangle in screen space that
|						the image is using to attack
**************************************************************/
SGD::Rectangle Entity::GetActiveRect(void) const
{
	//current actions name
	std::string act = GetName();
	act += "_";
	act += aTimeStamp.m_sAction;

	//current animation
	const Animation* currentAniamtion = (AnimationSystem::GetInstance()->GetAnimation(act));
	const Frame* currentFrame = (currentAniamtion->operator[](aTimeStamp.m_CurrentFrame));
	const SGD::Rectangle activeRect = currentFrame->m_rActive;

	//size
	SGD::Size size;
	size.width = activeRect.right;
	size.height = activeRect.bottom;

	//top left
	SGD::Point topLeft = GetPosition();
	//offset position
	SGD::Point offset = currentFrame->m_ptActiveOffset;
	topLeft.x += currentAniamtion->IsSheetBackwards() ? -offset.x - size.width : offset.x;
	topLeft.y += offset.y;

	//turn around
	if (!aTimeStamp.bForwards /*&& currentAniamtion->IsSheetBackwards() == false*/)
	{
		if (!currentAniamtion->IsSheetBackwards())
		{
			//inverse the render position
			topLeft.x = GetPosition().x - offset.x;
			topLeft.x -= size.width;
		}
		else
			//inverse the render position
			topLeft.x = GetPosition().x + offset.x;
	}


	//return the rect
	return SGD::Rectangle(topLeft, size);
}

/**************************************************************
|	Function: Character::GetSpawnPoint [constant]
|	Purpose: retrieve the point to spawn an object at
|	Parameter:
|			SGD::Point -  an output parameter to store the onscreen
|						point to spawn at [screen coords]
|	Return Value: bool return true if the point exists
**************************************************************/
bool Entity::GetSpawnPoint(SGD::Point _spawn) const
{
	//current actions name
	std::string act = GetName();
	act += "_";
	act += aTimeStamp.m_sAction;

	//dead?
	if (aTimeStamp.m_sAction == "die" || aTimeStamp.m_sAction == "stand")
		return false;

	//current animation
	const Animation* currentAniamtion = (AnimationSystem::GetInstance()->GetAnimation(act));
	const Frame* currentFrame = (currentAniamtion->operator[](aTimeStamp.m_CurrentFrame));
	const SGD::Point spawnOffset = currentFrame->m_ptSpawnOffset;

	if (spawnOffset.x == 0 && spawnOffset.y == 0)
		return false;

	_spawn.x = GetPosition().x + spawnOffset.x;
	_spawn.y = GetPosition().y + spawnOffset.y;

	return true;
}

/**************************************************************
|	Function: Character::SetName [constant]
|	Purpose: set the name of the player and animation owner
|	Parameter:
|			std::string _name - the name to assign the data member
|					for the entity and animation
|	Return Value: void
**************************************************************/
void Entity::SetName(std::string _name)
{
	//set the name member
	name = _name;
	//set the name in the animations
	aTimeStamp.m_sOwnerName = _name;

}

/**************************************************************
|	Function: Character::SetName [constant]
|	Purpose: set the name of the player and animation owner
|	Parameter:
|			const char* _name - the name to assign the data member
|					for the entity and animation
|	Return Value: void
**************************************************************/
void Entity::SetName(const char* _name)
{
	if (_name)
	{
		//set the name member
		name = _name;
		//set the name in the animations
		aTimeStamp.m_sOwnerName = _name;
	}
}

