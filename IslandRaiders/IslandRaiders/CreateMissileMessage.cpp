#include "CreateMissileMessage.h"
#include "Character.h"
#include "MessageID.h"
#include "../SGD Wrappers/SGD_Geometry.h"

CreateMissileMessage::CreateMissileMessage(Character* owner, SGD::Point spawn, ProjectileType _type, std::string _name, float _move, float _timer, SGD::Vector _vel)
: Message(MessageID::MSG_CREATE_MISSILE)
{
	postMan = owner;

	if (postMan != nullptr)
		postMan->AddRef();

	ptSpawn = spawn;
	type = _type;
	name = _name;
	timer = _timer;
	moveSpeed = _move;
	m_vVelocity = _vel;
}


CreateMissileMessage::~CreateMissileMessage()
{
	postMan->Release();
	postMan = nullptr;
}
