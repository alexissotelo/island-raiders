#pragma once
#include "..\SGD Wrappers\SGD_Message.h"
class Enemy;

class EnemyDownMessage : public SGD::Message
{
public:
	EnemyDownMessage(Enemy* owner);
	~EnemyDownMessage();

	Enemy* GetOwner(void) const { return postMan; }

private:
	Enemy* postMan;
};

