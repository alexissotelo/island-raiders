#include "Particle.h"
#include "../SGD Wrappers/SGD_GraphicsManager.h"

#include "PlayGameState.h"

namespace{

	float RandomFloat(float min, float max)
	{
		return min + static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / (max - min)));;
	}
}

Particle::Particle(ParticleEmitter *_emitter)
{
	emitter = _emitter;
}
Particle::~Particle()
{
}

bool Particle::Update(float dt)
{
	if (!enabled)return false;

	lifeTimer += dt;
	if (lifeTimer > lifeTime)
		Disable();

	float t = lifeTimer / lifeTime;
	currentColor = SGD::Color::Lerp(currentColor, emitter->particleFlyWeight->targetColor, t);
	currentScale += emitter->particleFlyWeight->deltaScale * dt;
	if (currentScale < 0) currentScale = 0;
	rotation += emitter->particleFlyWeight->deltaRotation * dt;

	position += velocity;

	return true;
}
void Particle::Render()
{
	if (!enabled)return;

	SGD::Point camPos = PlayGameState::GetInstance()->GetCameraPosition();
	SGD::Point pos = { position.x - camPos.x, position.y - camPos.y };

	SGD::GraphicsManager::GetInstance()->DrawTexture(emitter->particleFlyWeight->image,
		pos, rotation, emitter->particleFlyWeight->rotationOffset, currentColor, { currentScale, currentScale });
}
void Particle::Enable()
{
	emitter->enabledCount++;
	enabled = true;
	lifeTimer = 0;
	
	position.x = RandomFloat(emitter->position.x, emitter->position.x + emitter->size.width);
	position.y = RandomFloat(emitter->position.y, emitter->position.y + emitter->size.height);

	lifeTime = emitter->particleFlyWeight->GetRandomLifeTime();
	rotation = emitter->particleFlyWeight->GetRandomRotation();
	velocity = emitter->particleFlyWeight->velocity + emitter->particleFlyWeight->GetRandomVelocity();
	velocity *= emitter->particleFlyWeight->GetRandomSpeed();
	
	currentScale = emitter->particleFlyWeight->GetRandomScale();
	currentColor = emitter->particleFlyWeight->startColor;

	/*
					pool[i]->SetVelocity(
					RandomFloat(0, 2*speed) - speed,
					RandomFloat(0, 2*speed) - speed
					);
	*/
}
void Particle::Disable()
{
	enabled = false;
	emitter->enabledCount--;
}
