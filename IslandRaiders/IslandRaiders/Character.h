#pragma once
#include "../SGD Wrappers/SGD_Declarations.h"
#include "Status.h"

#include "..\SGD Wrappers\SGD_Handle.h"
#include "Entity.h"

#include <string>
class Items;

struct AttackInfo
{
	float damage;
	bool isCriticalHit;
};
struct CharacterStats
{
	//initialize all
	void InitializeStats(float dmg, float maxHP, float moveSpd, float firefreq,
	float def = 0.80f, float projSpeed = 1.50f, float quickBoost = 0.70f, float heavyBoost = 1.30f);

	void CopyStats(const CharacterStats& stat);

	float damage; //the original amount of damage a character will deal to another character
	float health;
	float maxHealth; // the maximum value of the current character's health
	float moveSpeed; //the amount of pixels per second the character will move across the screen
	float fireFreq; //the amount of time needed to pass before shooting another projectile
	float fireTimer; //the amount of time that has passed since last shooting a projectile
	float defense; //the percentage of damage reduced
	float stamina; //the percentage of stamina remaining on a character
	float staminaRegen; //a timer to determine when to regen a quarter of the characters 
	float projectileSpeed; //a percentage relative to the players movement speed
	float atkBoost; //the percentage of extra base damage to be done

	float quickPercent = 0.80f; //a percentage of damage the quick attack will deal according to the damage
	float heavyPercent = 1.30f; //a percentage of damage the heavy atk will deal according to the damage
	bool  bQuickAttack = true; //apply the quick attack or heavy damage

};


class Character : public Entity
{
public:

	virtual void Update(float dt)override;
	virtual void Render()override;
	virtual unsigned int  GetType()		const override { return ENT_CHARACTER; };
	virtual bool HandleCollision(const IEntity* other)override;

	virtual SGD::Rectangle	Character::GetRect() const override;

	void StayInWorld();
	virtual void Attack(const Character *other); 
	void Heal(int hp); //break? - remove
	void Hurt(int dmg); //break? - remove
	void DrawHealthBar();
	void Die(); //break? - remove

	Character(const char*); //all characters MUST have a name to animate them
	virtual ~Character();

	//accessors
	float GetDamage() const;
	float GetHealth() const					{ return stat.health; }
	float GetMaxHealth() const				{ return stat.maxHealth; }
	float GetMoveSpeed() const				{ return stat.moveSpeed; }
	float GetFireFreq(void) const			{ return stat.fireFreq; }
	float GetFireTime(void) const			{ return stat.fireTimer; }
	float GetProjectileSpeed(void) const	{ return stat.projectileSpeed; }
	int	  GetNumItemsHeld(void) const		{ return numItemsHeld; }
	float GetDefensiveStat(void) const		{ return stat.defense; }
	float GetStamina(void) const			{ return stat.stamina; }
	float GetAttackBoost(void) const		{ return stat.atkBoost; }
	float GetBaseDamage(void) const			{ return stat.damage; }
	float tempMoveSpeed;


	//SGD::Rectangle GetRenderRect(void) const;
	//SGD::Rectangle GetCollisionRect(void) const;
	//SGD::Rectangle GetActiveRect(void) const;
	//bool GetSpawnPoint(SGD::Point) const;

	//mutators
	void SetDamage(float dam)				{ stat.damage = dam; }
	void SetHealth(float hp)				{ stat.health = hp; }
	void SetMaxHealth(float maxHP)			{ stat.maxHealth = maxHP; }
	void SetMoveSpeed(float ms)				{ stat.moveSpeed = ms; }
	void SetFireFreq(float f)				{ stat.fireFreq = f; }
	void SetFireTimer(float f)				{ stat.fireTimer = f; }
	void SetStamina(float _stamina)			{ stat.stamina = _stamina; }
	void SetProjectileSpeed(float _speed)	{ stat.projectileSpeed = _speed; }
	void SetNumItemsHeld(int i)				{ numItemsHeld = i; }
	void SetDefense(float f)				{ stat.defense = f; }
	void SetAttackBoost(float atk)			{ stat.atkBoost = atk; }

	Status* GetCharacterStatus(void)  { return &sEffectStatus; }
	const Status* GetCharacterStatus(void) const { return &sEffectStatus; }
	CharacterStats* GetCharacterStats(void) { return &stat; }
	const CharacterStats* GetCharacterStats(void) const { return &stat; }
	//Event Handling
	virtual void HandleEvent(const SGD::Event* pEvent);


	std::vector<Items*> utilityBelt;
	bool skillSelectConfirm = false;
	int skillSelectCursor = 0;

protected:

#pragma region COMMENTS
//	float damage; //the original amount of damage a character will deal to another character
//	float health; 
//	float maxHealth; // the maximum value of the current character's health
//	float moveSpeed; //the amount of pixels per second the character will move across the screen
//	float fireFreq; //the amount of time needed to pass before shooting another projectile
//	float fireTimer = 0.0f; //the amount of time that has passed since last shooting a projectile
//	float defense = 0.80f; //the percentage of damage reduced
//	float stamina = 1.0f; //the percentage of stamina remaining on a character
//	float staminaRegen = 0.0f; //a timer to determine when to regen a quarter of the characters 
//	float projectileSpeed = 1.5f; //a percentage relative to the players movement speed
//	float atkBoost = 1.0f; //the percentage of extra base damage to be done
//	float quickPercent = 0.80f; //a percentage of damage the quick attack will deal according to the damage
//	float heavyPercent = 1.30f; //a percentage of damage the heavy atk will deal according to the damage
//	bool  bQuickAttack = true; //apply the quick attack or heavy damage

#pragma endregion

	//structure for all character stats
	CharacterStats stat;
	//status effect
	Status sEffectStatus;

	//number of items the player holds
	int numItemsHeld = 0;
		//helper 
	void Knockback(Entity* source);

private :
	SGD::Point prevPosition;
	bool stuck = false;
};

