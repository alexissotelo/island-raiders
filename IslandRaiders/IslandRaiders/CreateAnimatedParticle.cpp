#include "CreateAnimatedParticle.h"
#include "../SGD Wrappers/SGD_Message.h"
#include "MessageID.h"
#include "Character.h"

CreateAnimatedParticle::CreateAnimatedParticle(Character* _owner, std::string _name, float _span, SGD::Color _clr, SGD::Point _pos, SGD::Vector _vel)
: SGD::Message(MessageID::MSG_CREATE_ANIM_PARTICLE)
{
	//name
	m_sName = _name;
	//life span
	m_fLifeSpan = _span;
	//color
	m_clrRender = _clr;
	//position
	m_ptPosition = _pos;
	//velocity
	m_vVelocity = _vel;
	//owenr -- ref counting
	m_pOwner = _owner;

	if (m_pOwner)
		m_pOwner->AddRef();

}


CreateAnimatedParticle::~CreateAnimatedParticle()
{
	if (m_pOwner)
	{
		m_pOwner->Release();
		m_pOwner = nullptr;
	}
}
