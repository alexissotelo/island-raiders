#include "ParticleManager.h"
#include "../SGD Wrappers/SGD_GraphicsManager.h"
#include "LoadingManager.h"
#include <sstream>

ParticleManager* ParticleManager::s_pInstance = nullptr;
ParticleManager* ParticleManager::GetInstance(void)
{
	if (s_pInstance == nullptr)
		s_pInstance = new ParticleManager;
	return s_pInstance;
}
void ParticleManager::DeleteInstance(void)
{
	delete s_pInstance;
	s_pInstance = nullptr;
}
void ParticleManager::Initialize()
{
	//must be before loading player, because blood fx
#pragma region FLYWEIGHTS

	//for blizzard
	ParticleFlyWeight *fly1 = new ParticleFlyWeight("snow.png");
	fly1->minLife = 3.f;
	fly1->maxLife = 5.f;


	fly1->startColor  = { 0, 255, 255, 255 };
	fly1->targetColor = { 255, 255, 255 };

	fly1->minScale = 0.125f;
	fly1->maxScale = 0.25f;
	fly1->deltaScale = 0.f;

	fly1->minRotation = 0.f;
	fly1->maxRotation = 90.f;
	fly1->deltaRotation = 10.f;

	fly1->velocity = { -1, 1 };
	ParticleManager::GetInstance()->AddFlyWeight(fly1);

	//for blood
	ParticleFlyWeight *fly2 = new ParticleFlyWeight("square.png");
	fly2->minLife = 0.f;
	fly2->maxLife = 1.f;

	fly2->startColor = { 255, 0, 0 };
	fly2->targetColor = { 128, 0, 0 };

	fly2->minScale = 0.5f;
	fly2->maxScale = 1.f;
	fly2->deltaScale = 0.5f;

	fly2->minRotation = 0.f;
	fly2->maxRotation = 90.f;
	fly2->deltaRotation = 0.f;

	fly2->minSpeed = 1.f;
	fly2->maxSpeed = 2.f;

	fly2->velocity = { 0, -1 };
	fly2->randomVelocity = { 1.5f, 0 };
	ParticleManager::GetInstance()->AddFlyWeight(fly2);

	//for lava pits
	fly2 = new ParticleFlyWeight("square.png");
	fly2->minLife = 0.f;
	fly2->maxLife = 1.5f;

	fly2->startColor = { 255, 0, 0 };
	fly2->targetColor = { 255, 255, 0 };

	fly2->minScale = 1.f;
	fly2->maxScale = 1.5f;
	fly2->deltaScale = -2.0f;

	fly2->minRotation = 0.f;
	fly2->maxRotation = 90.f;
	fly2->deltaRotation = 0.f;

	fly2->minSpeed = 0.5f;
	fly2->maxSpeed = 1.0f;

	fly2->velocity = { 0, -1 };
	fly2->randomVelocity = { 0, 0 };
	ParticleManager::GetInstance()->AddFlyWeight(fly2);

	//for trails
	fly2 = new ParticleFlyWeight("square.png");
	fly2->minLife = 0.f;
	fly2->maxLife = 1.5f;

	fly2->startColor = { 0, 255, 0 };
	fly2->targetColor = { 0, 255, 255 };

	fly2->minScale = 0.5f;
	fly2->maxScale = 1.f;
	fly2->deltaScale = -1.0f;

	fly2->minRotation = 0.f;
	fly2->maxRotation = 90.f;
	fly2->deltaRotation = 0.f;

	fly2->minSpeed = 0.5f;
	fly2->maxSpeed = 1.0f;

	fly2->velocity = { 0, 0 };
	fly2->randomVelocity = { 1.f, 1.f };
	ParticleManager::GetInstance()->AddFlyWeight(fly2);

	//for snow area//4
	fly1 = new ParticleFlyWeight("snow.png");
	fly1->minLife = 0.f;
	fly1->maxLife = 1.f;

	fly1->startColor = { 0, 255, 255, 255 };
	fly1->targetColor = { 255, 255, 255 };

	fly1->minScale = 0.125f;
	fly1->maxScale = 0.25f;
	fly1->deltaScale = 0.f;

	fly1->minRotation = 0.f;
	fly1->maxRotation = 90.f;
	fly1->deltaRotation = 10.f;

	fly1->velocity = { 0, -1 };
	ParticleManager::GetInstance()->AddFlyWeight(fly1);


	//for rain
	fly1 = new ParticleFlyWeight("raindrop.png");
	fly1->minLife = 1.f;
	fly1->maxLife = 1.2f;

	fly1->startColor = { 120, 120 , 120};
	fly1->targetColor = { 50,120, 120, 120 };

	fly1->minScale = 0.125f;
	fly1->maxScale = 0.25f;
	fly1->deltaScale = 0.f;

	fly1->minRotation = 45.f;
	fly1->maxRotation = 45.f;
	fly1->deltaRotation = 0.f;

	fly1->minSpeed = 7.f;
	fly1->maxSpeed = 10.f;
	fly1->velocity = { -1, 1 };
	fly1->rotationOffset = {64,64};

	ParticleManager::GetInstance()->AddFlyWeight(fly1);
#pragma endregion

	//inform a thread is done
	ThreadData* threadData = ThreadData::GetInstance();
	threadData->countMutex.lock();
	--threadData->numberOfThreads;
	threadData->countCondition.notify_all();
	threadData->countMutex.unlock();

}
void ParticleManager::Terminate()
{
	ClearParticles();
	for (unsigned int i = 0; i < flyweights.size(); i++)
		delete flyweights[i];
	flyweights.clear();
}
//**********END SINGLETON
void ParticleManager::ClearParticles()
{
#if 0
	for (unsigned int i = 0; i < emitters.size(); ++i)
	{
		delete emitters[i];
		emitters[i] = nullptr;
	}
#else
	for (auto iter = emitters.begin(); iter != emitters.end() ; ++iter)
	{
		delete *iter;
	}
#endif
	emitters.clear();
}
int ParticleManager::AddEmitter(ParticleEmitter *emitter, bool emit)
{
	emitters.push_back(emitter);
	emitter->Emit(emit);

	return emitters.size()-1;
}
void ParticleManager::UpdateEmitters(float dt)
{
	//for (unsigned int i = 0; i < emitters.size(); ++i)
	//{
	//	emitters[i]->Update(dt);
	//}
	std::list<ParticleEmitter*>::iterator iter = emitters.begin();
	for (; iter != emitters.end(); )
	{
		if ((*iter)->Update(dt))
		{
			delete *iter;
			iter = emitters.erase(iter);
		}
		else
			++iter;
	}
}
void ParticleManager::RenderEmitters()
{
	//for (unsigned int i = 0; i < emitters.size(); ++i)
	//	emitters[i]->Render();
	for (auto iter = emitters.begin(); iter != emitters.end(); ++iter)
	{
		(*iter)->Render();
	}
}
