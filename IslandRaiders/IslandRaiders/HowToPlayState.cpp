#include "HowToPlayState.h"

#include "OptionsGameState.h"
#include "BitmapFont.h"
#include "GUI.h"
#include "..\Source\Game.h"
#include "MenuGameState.h"

HowToPlayState* HowToPlayState::pInstance = nullptr;


HowToPlayState* HowToPlayState::GetInstance(void)
{
	if (pInstance == nullptr)
		pInstance = new HowToPlayState;

	return pInstance;
}

void HowToPlayState::DeleteInstance(void)
{
	delete pInstance;
	pInstance = nullptr;
}

void HowToPlayState::Enter()
{
	pFont = GUIManager::GetInstance()->GetFont();
	pGraphics = SGD::GraphicsManager::GetInstance();
	pAudio = SGD::AudioManager::GetInstance();
	pInput = SGD::InputManager::GetInstance();
	backGround = pGraphics->LoadTexture(L"resource/Graphics/placeholder images/map.png");


}
void HowToPlayState::Exit()
{
	pGraphics->UnloadTexture(backGround);
	DeleteInstance();

}
bool HowToPlayState::Update(float dt)
{
	if (pInput->IsKeyPressed(SGD::Key::Escape) || pInput->IsButtonPressed(0,2))
	{
		Game::GetInstance()->ChangeState(MenuGameState::GetInstance());
		return true;
	}

	return true;
}
void HowToPlayState::Render(float dt)
{
	pGraphics->DrawTexture(backGround, { 0.0f, 0.0f }, {}, {}, {}, { 4.0f, 4.0f });

	float width = Game::GetInstance()->GetScreenWidth();
	pFont->DrawXML("How To play Screen", { ((width - (4 * 42 * 4.0f)) / 2) + 0, 50 }, 2.0f, { 255, 255, 255 });
}

