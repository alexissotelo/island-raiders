#include "AnimationTimestamp.h"
#include "AnimationSystem.h"

#include <string>

/**************************************************************
|	Function: AnimationTimestamp::SwitchAnimation
|	Purpose: change and reset an action if the action is not already active
|	Parameter:
|				std::string _action - a string for the name of the
|									action to switch to
|	Return Value: void
**************************************************************/
#if 1
void AnimationTimestamp::SwitchAnimation(const char * _action)
{
	if (m_sAction == _action)
		return;

	this->isDone = false;
	this->m_CurrentFrame = 0;
	this->m_fFrameTimer = 0.f;
	this->m_sAction = _action;

	//loop 
	std::string  animName = this->m_sOwnerName + "_" + _action;
	//does the animation exist??
	const Animation* animation = AnimationSystem::GetInstance()->GetAnimation(animName);
	//set the loop value only when one exists
	if (animation) this->bLoop = animation->Loop();
}

#else
void AnimationTimestamp::SwitchAnimation(std::string _action)
{
	if (m_sAction == _action)
		return;

	this->isDone = false;
	this->m_CurrentFrame = 0;
	this->m_fFrameTimer = 0.f;
	this->m_sAction = _action;

	//loop 
	std::string  animName = this->m_sOwnerName + "_" + _action;
	//does the animation exist??
	const Animation* animation = AnimationSystem::GetInstance()->GetAnimation(animName);
	//set the loop value only when one exists
	if (animation) this->bLoop =animation->Loop();
}
#endif