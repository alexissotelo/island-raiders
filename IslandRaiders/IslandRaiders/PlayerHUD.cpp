#include "PlayerHUD.h"
#include "Player.h"
#include "SkillStamp.h"

SGD::Color hudColors[] = { { 158, 11, 15 }, {60,74,255}, {255,255,43} };

PlayerHUD::PlayerHUD(Player* p)
{
	player = p;
	for (int i = 0; i < NUMBER_OF_SKILLS; i++)
		skillIcons[i] = SGD::INVALID_HANDLE;

	SGD::GraphicsManager* pGraphics = SGD::GraphicsManager::GetInstance();
	if (p->GetName() == "mihawk")
		portrait = pGraphics->LoadTexture(L"resources/graphics/icons/mihawk.png");
	if (p->GetName() == "mystogan")
		portrait = pGraphics->LoadTexture(L"resources/graphics/icons/mystogan.png");
	if (p->GetName() == "lal")
		portrait = pGraphics->LoadTexture(L"resources/graphics/icons/lal.png");
	if (p->GetName() == "erza")
		portrait = pGraphics->LoadTexture(L"resources/graphics/icons/erza.png");
	if (p->GetName() == "fran")
		portrait = pGraphics->LoadTexture(L"resources/graphics/icons/fran.png");
	if (p->GetName() == "xanxus")
		portrait = pGraphics->LoadTexture(L"resources/graphics/icons/xanxus.png");

	HUDposition = { 0, 520 };
}
PlayerHUD::~PlayerHUD()
{
	SGD::GraphicsManager *pGraphics = SGD::GraphicsManager::GetInstance();
	pGraphics->UnloadTexture(portrait);
	for (int i = 0; i < NUMBER_OF_SKILLS; i++)
		pGraphics->UnloadTexture(skillIcons[i]);

	player = nullptr;
}
void PlayerHUD::LoadIcon(std::string filePath, int skillIndex)
{
	skillIcons[skillIndex] =
		SGD::GraphicsManager::GetInstance()->LoadTexture(filePath.c_str());
}
void PlayerHUD::DrawHUD()
{
	SGD::GraphicsManager *pGraphics = SGD::GraphicsManager::GetInstance();

	int posIndex = player->GetControllerNumber();
	HUDposition = { 267.f * posIndex, 520 };
	SGD::Point position = HUDposition;

	//HUD CANVAS
	pGraphics->DrawRectangle(
		SGD::Rectangle(position.x, position.y, position.x + 266.f, 600.f),
		hudColors[posIndex]);

	//draw hp bar
	SGD::Rectangle hpbar = SGD::Rectangle(position.x, HUDposition.y, position.x + 266.f, 534.f);
	pGraphics->DrawRectangle(hpbar, { 100, 100, 100 });
	if (player->GetHealth() > 0)
	{
		hpbar.right = hpbar.left + (266.f * (player->GetHealth()*1.f / player->GetMaxHealth()));
		pGraphics->DrawRectangle(hpbar, { 146, 227, 37 });
	}

	//draw skills
	position = HUDposition;
	int skillPos[NUMBER_OF_SKILLS] = { /*9,*/ 74, 138, 201 };
	position.y += 20;
	float scale = 56.f / 128;

	//portrait
	pGraphics->DrawTexture(portrait, { position.x + 9, position.y },
		0, {}, {}, { scale, scale });
	//skill slots
	for (int i = 0; i < NUMBER_OF_SKILLS; i++)
	{
		pGraphics->DrawRectangle(
			SGD::Rectangle({ position.x + skillPos[i], position.y }, SGD::Size(56, 56)),
			{ 100,0, 0, 0 });//draw slot
		if (skillIcons[i] != SGD::INVALID_HANDLE)
		{
			float cur = player->GetSkillStamp()->SkillCooldown(i);

			pGraphics->DrawTexture(skillIcons[i], { position.x + skillPos[i], position.y },
				0, {}, {}, { 1.8f, 1.8f });

			if (cur > 0.f)
			{
				pGraphics->DrawRectangle(
					SGD::Rectangle({ position.x + skillPos[i], position.y },
					SGD::Size(56 * cur, 56)),
					{ 100, 0, 0, 0 });//coolbar
			}
		}
	}
}