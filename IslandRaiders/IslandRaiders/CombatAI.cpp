#include "CombatAI.h"
#include "../SGD Wrappers/SGD_AudioManager.h"
#include "AudioQueue.h"

/**************************************************************
|	Function:
|	Purpose:
|	Parameter:
|	Return Value:
**************************************************************/

void CombatAI::Update(float dt, AIStamp& stamp)
{

	//no target? 
	if (!stamp.target)
	{
		//find one
		stamp.SwitchAIMode(EnemyAIType::Idle);
		//leave -- you'll get em next time
		return;
	}

	//incremenet fire timer
	float fireTimer = stamp.owner->GetFireTime();
	fireTimer += dt;
	stamp.owner->SetFireTimer(fireTimer);

	//wait based on the enemy's tier
	switch (stamp.combatType)
	{
	case CombatType::Melee:
		MeleeUpdate(dt, stamp);
		break;
	case CombatType::Range:
		RangeUpdate(dt, stamp);
		break;
	case CombatType::Support:
		SupportUpdate(dt, stamp);
		break;
	case CombatType::Familiar:
		FamiliarUpdate(dt, stamp);
		break;
	default:
		break;
	}
}

/**************************************************************
|	Function:
|	Purpose:
|	Parameter:
|	Return Value:
**************************************************************/
void CombatAI::MeleeUpdate(float dt, AIStamp& stamp)
{
	//wait based on the enemy's tier
	switch (stamp.tier)
	{
	case 0:
		TierZeroMelee(dt, stamp);
		break;
	case 1:
		TierOneMelee(dt, stamp);
		break;
	case 2:TierTwoMelee(dt, stamp);
		break;
	case 3:
		break;
	default:
		break;
	}
}

/**************************************************************
|	Function:
|	Purpose:
|	Parameter:
|	Return Value:
**************************************************************/
void CombatAI::TierZeroMelee(float dt, AIStamp& stamp)
{

	//return to waiting once the attack is done
	if (stamp.owner->GetTimeStamp()->isDone)
		//return to idle
		stamp.SwitchAIMode(EnemyAIType::Idle);

	//attack the player 
	if (stamp.owner->GetFireTime() > 0.75f)
	{
		stamp.owner->GetTimeStamp()->SwitchAnimation("attack");

		if (stamp.GetAttackSound())
		AudioQueue::GetInstance()->AddSound(SGD::AudioManager::GetInstance()->LoadAudio(stamp.GetAttackSound()));

		stamp.owner->SetFireTimer(0.f);
	}

}

/**************************************************************
|	Function:
|	Purpose:
|	Parameter:
|	Return Value:
**************************************************************/
void CombatAI::TierOneMelee(float dt, AIStamp& stamp)
{
	//target
	if (stamp.target == nullptr)
	{
		//return to idle
		stamp.SwitchAIMode(EnemyAIType::Idle);
		//skip
		return;
	}

	//fire timer
	float fireTimer = stamp.owner->GetFireTime();

	//fire timer
	if (fireTimer >= stamp.owner->GetFireFreq())
	{	//return to waiting once the attack is done
		if (stamp.owner->GetTimeStamp()->isDone)
		{
			//player stunned?
			if (stamp.target->GetTimeStamp()->m_sAction == "flinch")
			{
				if (rand() % 3 == 1)
				{
					//reset the animatiosn
					stamp.owner->GetTimeStamp()->SwitchAnimation("idle");
					stamp.owner->GetTimeStamp()->SwitchAnimation("attack2");
					if (stamp.GetAttackSound())

					AudioQueue::GetInstance()->AddSound(SGD::AudioManager::GetInstance()->LoadAudio(stamp.GetAttackSound()));
				}
				else
					//return to idle
					stamp.SwitchAIMode(EnemyAIType::Idle);
			}
			else
				//return to idle
				stamp.SwitchAIMode(EnemyAIType::Idle);
		}
		else if (fireTimer >= stamp.owner->GetFireFreq())
		{
			//zero
			fireTimer = 0.f;
			//attack the player 
			stamp.owner->GetTimeStamp()->SwitchAnimation("attack");
			if (stamp.GetAttackSound())
			AudioQueue::GetInstance()->AddSound(SGD::AudioManager::GetInstance()->LoadAudio(stamp.GetAttackSound()));
		}
	}

	//set the timer
	stamp.owner->SetFireTimer(fireTimer);
}

/**************************************************************
|	Function:
|	Purpose:
|	Parameter:
|	Return Value:
**************************************************************/
void CombatAI::TierTwoMelee(float dt, AIStamp& stamp)
{

	if (stamp.target == nullptr)
	{
		//return to idle
		stamp.SwitchAIMode(EnemyAIType::Idle);
		return;
	}

	//player stunned?
	if (stamp.target->GetTimeStamp()->m_sAction == "flinch")
	{
		stamp.owner->GetTimeStamp()->SwitchAnimation("attack2");
		return;
	}

	//fire timer
	float fireTimer = stamp.owner->GetFireTime();

	//time to attack
	if (fireTimer >= stamp.owner->GetFireFreq())
	{
		//increase timer
		fireTimer = 0.f;

		//player down?
		if (stamp.target->GetTimeStamp()->m_sAction == "knockback")
			//go away
			stamp.SwitchAIMode(EnemyAIType::Pursuit);

		//return to waiting once the attack is done
		if (stamp.owner->GetTimeStamp()->isDone)

			//return to idle
			stamp.SwitchAIMode(EnemyAIType::Idle);

		//attack the player 
		stamp.owner->GetTimeStamp()->SwitchAnimation("attack");
		if (stamp.GetAttackSound())
		AudioQueue::GetInstance()->AddSound(SGD::AudioManager::GetInstance()->LoadAudio(stamp.GetAttackSound()));
	}


	stamp.owner->SetFireTimer(fireTimer);
}

//range
/**************************************************************
|	Function:
|	Purpose:
|	Parameter:
|	Return Value:
**************************************************************/
void CombatAI::RangeUpdate(float dt, AIStamp& stamp)
{
	//wait based on the enemy's tier
	switch (stamp.tier)
	{
	case 0:
		TierZeroRange(dt, stamp);
		break;
	case 1:
		TierOneRange(dt, stamp);
		break;
	case 2:
		break;
	case 3:
		break;
	default:
		break;
	}
}

/**************************************************************
|	Function:
|	Purpose:
|	Parameter:
|	Return Value:
**************************************************************/
void CombatAI::TierZeroRange(float dt, AIStamp& stamp)
{
	//timer to shoot
	float fireTimer = stamp.owner->GetFireTime();

	//can shoot?
	if (fireTimer >= stamp.owner->GetFireFreq())
	{
		//reset timer
		fireTimer = 0.f;


		//return to waiting once the attack is done
		if (stamp.owner->GetTimeStamp()->isDone)
			//return to idle
			stamp.SwitchAIMode(EnemyAIType::Idle);
		else
		{
			//attack the player 
			stamp.owner->GetTimeStamp()->SwitchAnimation("attack");
			//play sound
			if (stamp.GetAttackSound())
			AudioQueue::GetInstance()->AddSound(SGD::AudioManager::GetInstance()->LoadAudio(stamp.GetAttackSound()));
		}

	}
	//set the timer to shoot again
	stamp.owner->SetFireTimer(fireTimer);

}

/**************************************************************
|	Function:
|	Purpose:
|	Parameter:
|	Return Value:
**************************************************************/
void CombatAI::TierOneRange(float dt, AIStamp& stamp)
{
	//return to waiting once the attack is done
	if (stamp.owner->GetTimeStamp()->isDone)
	{
		//shoot again
		if (stamp.owner->GetTimeStamp()->m_sAction == "attack")
			stamp.owner->GetTimeStamp()->SwitchAnimation("attack2");
		else
		{
			//return to idle
			stamp.SwitchAIMode(EnemyAIType::Pursuit);
			stamp.owner->GetTimeStamp()->SwitchAnimation("idle");
		}
		return;
	}

	if (stamp.owner->GetTimeStamp()->m_sAction != "attack2")
		//attack the player 
		stamp.owner->GetTimeStamp()->SwitchAnimation("attack");

}

//support
/**************************************************************
|	Function:
|	Purpose:
|	Parameter:
|	Return Value:
**************************************************************/
void CombatAI::SupportUpdate(float dt, AIStamp& stamp)
{
	//wait based on the enemy's tier
	switch (stamp.tier)
	{
	case 1:
		TierOneSupport(dt, stamp);
		break;
	case 2:
		break;
	case 3:
		break;
	default:
		break;
	}
}

/**************************************************************
|	Function:
|	Purpose:
|	Parameter:
|	Return Value:
**************************************************************/
void CombatAI::TierOneSupport(float dt, AIStamp& stamp)
{
	//return to waiting once the attack is done
	if (stamp.owner->GetTimeStamp()->isDone)
	{
		//return to idle
		stamp.SwitchAIMode(EnemyAIType::Pursuit);
		stamp.owner->GetTimeStamp()->SwitchAnimation("idle");
		return;
	}

	//update fire timer
	float fireTimer = stamp.owner->GetFireTime();

	if (fireTimer >= stamp.owner->GetFireFreq())
	{
		//reset timer
		fireTimer = 0.f;

		if (stamp.GetAttackSound())
		//audio queue
		AudioQueue::GetInstance()->AddSound(SGD::AudioManager::GetInstance()->LoadAudio(stamp.GetAttackSound()));

		//vector to player
		SGD::Vector toTarget = stamp.target->GetPosition() - stamp.owner->GetPosition();

		//distance
		float distance = toTarget.ComputeLength();

		if (distance <= 100 && fabs(toTarget.y) <= 20)
			//attack the player 
			stamp.owner->GetTimeStamp()->SwitchAnimation("attack2");
		else
			//attack the player 
			stamp.owner->GetTimeStamp()->SwitchAnimation("attack");
	}
	//set the timer to shoot again
	stamp.owner->SetFireTimer(fireTimer);

}


void CombatAI::FamiliarUpdate(float dt, AIStamp& stamp)
{

	//return to waiting once the attack is done
	if (stamp.owner->GetTimeStamp()->isDone)
	{
		//shoot again
		if (stamp.owner->GetTimeStamp()->m_sAction == "attack")
		{
			if (stamp.target->GetTimeStamp()->m_sAction == "flinch")
				stamp.owner->GetTimeStamp()->SwitchAnimation("attack2");
			else
				stamp.SwitchAIMode(EnemyAIType::Idle);

		}
		else
		{
			//return to idle
			stamp.SwitchAIMode(EnemyAIType::Pursuit);
			stamp.owner->GetTimeStamp()->SwitchAnimation("idle");
		}
		return;
	}

	if (stamp.owner->GetTimeStamp()->m_sAction != "attack2")
		//attack the player 
		stamp.owner->GetTimeStamp()->SwitchAnimation("attack");


#if 0
	////return to waiting once the attack is done
	//if (stamp.owner->GetTimeStamp()->isDone)
	//{
	//	//player stunned?
	//	if (stamp.target->GetTimeStamp()->m_sAction == "flinch")
	//	{

	//		//reset the animatiosn
	//		stamp.owner->GetTimeStamp()->SwitchAnimation("idle");

	//		if (rand() % 2)
	//			stamp.owner->GetTimeStamp()->SwitchAnimation("attack2");
	//		else
	//			//return to idle
	//			stamp.SwitchAIMode(EnemyAIType::Pursuit);
	//	}
	//	else if (stamp.target->GetTimeStamp()->m_sAction == "flinch")
	//		stamp.owner->GetTimeStamp()->SwitchAnimation("attack2");
	//	else
	//	{
	//		//return to idle
	//		stamp.SwitchAIMode(EnemyAIType::Pursuit);
	//		//reset the animatiosn
	//		stamp.owner->GetTimeStamp()->SwitchAnimation("idle");
	//	}
	//}
	//else
	//{
	//	//attack success!
	//	stamp.owner->SetFireTimer(0.0f);
	//	//attack the player 
	//	stamp.owner->GetTimeStamp()->SwitchAnimation("attack");
	//}
#endif
}

