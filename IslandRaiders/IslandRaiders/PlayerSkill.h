#pragma once

class Player;

//public namespace given access to all entities who may use it
namespace PlayerSkill
{
	/************************************************************************************
		all functions ALL must have the following:
			-boolean return value
			-float for the elapsed time
			-Player pointer in which to enable the behavior on
			-boolean to determine whether or not the player desires to fully update
	************************************************************************************/

	bool Basic_AllyHealing(float elapsedTime, Player* player, bool bStart);
	bool Basic_StunEnemy(float elapsedTime, Player* player, bool bStart);
	bool Basic_SmokeScreen(float elpasedTime, Player* player, bool bStart);


	//level 2 - size 6 __OK
	bool Fencer(float elapsedTime, Player* owner, bool bStart); //swordsman
	bool Berserker(float elapsedTime, Player* owner, bool bStart); //swordsman
	bool Marksman(float elapsedTime, Player* player, bool bStart); //gunner
	bool Claymore(float elapsedTime, Player* player, bool bStart); //gunner
	bool Summoner(float elapsedTime, Player* player, bool bStart); //mage
	bool PoisonFog(float elapsedTime, Player* player, bool bStart); //mage

	//level 3 - size 12 __ In Progess  50% complete
	bool Captain(float elapsedTime, Player* player, bool bStart);	 //swordsman
	bool Hammer(float elapsedTime, Player* player, bool bStart);	 //swordsman
	bool Admiral(float elapsedTime, Player* player, bool bStart);	 //swordsman
	bool SeaKnight(float elapsedTime, Player* player, bool bStart);	 //swordsman

	bool WitchDoctor(float elapsedTime, Player* player, bool bStart);	//mage
	bool BeastMaster(float elapsedTime, Player* player, bool bStart);	//mage
	bool Pyromaniac(float elapsedTime, Player* player, bool bStart);	//mage
	bool OmniKnight(float elapsedTime, Player* player, bool bStart);	//mage

	bool Camper(float elapsedTime, Player* player, bool bStart);			//gunner
	bool CannonFire(float elapsedTime, Player* player, bool bStart);		//gunner
	bool WeaponExchange(float elapsedTime, Player* player, bool bStart);	//gunner
	bool Hunter(float elapsedTime, Player* player, bool bStart);			//gunner

	bool SnakeTrap(float elapsedTime, Player* player, bool bStart);
	bool BlazeFire(float elapsedTime, Player* player, bool bStart);
};
