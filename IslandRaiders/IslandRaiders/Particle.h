#pragma once

#include "..\SGD Wrappers\SGD_Handle.h"
#include "..\SGD Wrappers\SGD_Color.h"
#include "..\SGD Wrappers\SGD_Geometry.h"

#include "ParticleEmitter.h"
#include "ParticleFlyWeight.h"

class Particle
{
	friend class ParticleEmitter;
	friend class ParticleFlyWeight;

private:
	ParticleEmitter *emitter;

	bool enabled = false;

	float lifeTimer;
	float lifeTime;
	SGD::Color currentColor;

	SGD::Vector velocity;
	SGD::Point  position;
	//SGD::Size   size;

	float currentScale =1.f;
	float targetScale;

	float rotation;
public:
	Particle(ParticleEmitter *_emitter);
	~Particle();

	bool Update(float dt);
	void Render();

	void Enable();
	void Disable();
};

