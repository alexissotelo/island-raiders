#include "GUISkin.h"

GUISkin::GUISkin(const BitmapFont *_font, SGD::Color _textColor,
	SGD::Color _backColor)
{
	hoverEnabled = false;

	font = _font;
	textColor = _textColor;
	backColor = _backColor;
}

GUISkin::GUISkin(const BitmapFont *_font, SGD::Color _textColor,
	SGD::Color _backColor, SGD::Color _hoverColor)
{
	hoverEnabled = true;

	font = _font;
	textColor  = _textColor;
	backColor  = _backColor;
	hoverColor = _hoverColor;
}
GUISkin::~GUISkin()
{
}
