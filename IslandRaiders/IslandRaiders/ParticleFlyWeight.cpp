#include "ParticleFlyWeight.h"
#include "../Source/Game.h"

namespace{

	float RandomFloat(float min, float max)
	{
		return min + static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / (max - min)));;
	}
}

ParticleFlyWeight::ParticleFlyWeight(const char* imgPath)
{
	std::string path = "resources/graphics/particles/";
	path += imgPath;

	image = SGD::GraphicsManager::GetInstance()->LoadTexture(path.c_str());
}
ParticleFlyWeight::~ParticleFlyWeight()
{
	SGD::GraphicsManager::GetInstance()->UnloadTexture(image);
}

float ParticleFlyWeight::GetRandomLifeTime()
{
	return RandomFloat(minLife,maxLife);
}
float ParticleFlyWeight::GetRandomRotation()
{
	return RandomFloat(minRotation, maxRotation);
}
float ParticleFlyWeight::GetRandomScale()
{
	return RandomFloat(minScale, maxScale);
}
float ParticleFlyWeight::GetRandomSpeed()
{
	return RandomFloat(minSpeed, maxSpeed);
}
SGD::Vector ParticleFlyWeight::GetRandomVelocity()
{
	return{ 
		RandomFloat(-randomVelocity.x, randomVelocity.x),
		RandomFloat(-randomVelocity.y, randomVelocity.y) };
}
