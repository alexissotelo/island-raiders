#pragma once
#include "Character.h"
#include "SkillStamp.h"
#include "EnemyAI.h"

//familiars are owned by another character runned by an AI 
//could potentially be owned by an enemy


class Familiar : public Character
{

private:
	AIStamp aiStamp;
	SkillStamp skillStamp;
	Character* m_pOwner;

public:
	Familiar(const char* name, Character* owner);
	~Familiar();

	virtual void   Update(float dt) override;
	virtual bool   HandleCollision(const IEntity* other) override;
	virtual void   HandleEvent(const SGD::Event* pEvent);

	virtual unsigned int  GetType() const;

	//accessors
	const SkillStamp* GetSkillStamp(void) const { return &skillStamp; }
	SkillStamp* GetSkillStamp(void) { return &skillStamp; }
	const AIStamp* GetAIStamp(void) const { return &aiStamp; }
	AIStamp* GetAIStamp(void) { return &aiStamp; }
	Character* GetOwner(void) const { return m_pOwner; }

	//mutators
	void SetOwner(Character* ptr);

};

