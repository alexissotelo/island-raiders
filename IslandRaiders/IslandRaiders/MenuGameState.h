#pragma once
#include "IGameState.h"
#include <vector>
#include <string>
#include "../SGD Wrappers/SGD_AudioManager.h"
#include "../SGD Wrappers/SGD_GraphicsManager.h"
#include "../SGD Wrappers/SGD_InputManager.h"

class BitmapFont;
class Button;
class ButtonGroup;

class MenuGameState : public IGameState
{
public:
	static MenuGameState* GetInstance(void);
	static void DeleteInstance(void);

	virtual void Enter();
	virtual void Exit();
	virtual bool Update(float dt);
	virtual void Render(float dt);
	void ArcadeCursorMovement(float dt);

private:
	SGD::HAudio song = SGD::INVALID_HANDLE;
	SGD::HAudio buttonSound = SGD::INVALID_HANDLE;

	//std::vector<Button*> menuButtons;
	ButtonGroup *menuButtons = nullptr;
	ButtonGroup *slotButtons = nullptr;

	MenuGameState() = default;
	~MenuGameState() = default;
	MenuGameState(const MenuGameState&) = delete;
	MenuGameState operator=(const MenuGameState&) = delete;

	static MenuGameState* pInstance;

	SGD::GraphicsManager* pGraphics = nullptr;
	SGD::AudioManager* pAudio = nullptr;
	SGD::InputManager* pInput = nullptr;
	SGD::HTexture backGround = SGD::INVALID_HANDLE;
	SGD::HTexture teamlogo = SGD::INVALID_HANDLE;

	const BitmapFont* xfont = nullptr;
	int nCursor = 0;
	float timer = 0.0f;
	float timerFreq = 0.3f;

};

