#include "KnockbackAI.h"
#include "Enemy.h"


/**************************************************************
|	Function:
|	Purpose:
|	Parameter:
|	Return Value:
**************************************************************/
void KnockbackAI::Update(float dt, AIStamp& stamp)
{
	switch (stamp.tier)
	{
	case 0: 
		ExtraFlinch(dt, stamp);
		break;
	case 1:
	case 2:
		FallDown(dt, stamp);
		break;
	case 3:
		stamp.SwitchAIMode(EnemyAIType::Pursuit);
	default:
		break;
	}
}

/**************************************************************
|	Function:
|	Purpose:
|	Parameter:
|	Return Value:
**************************************************************/
void KnockbackAI::FallDown(float dt, AIStamp& stamp)
{
	//knockback
	stamp.owner->GetTimeStamp()->SwitchAnimation("knockback");

	//timer
	stamp.fStartTimer += dt;

	//animations done?
	if (stamp.owner->GetTimeStamp()->isDone)
		stamp.SwitchAIMode(EnemyAIType::Pursuit);

	//time up?
	//if (stamp.fStartTimer >= 2.0f)
	//	stamp.SwitchAIMode(EnemyAIType::Pursuit);

	//scale the velocity
	stamp.vKnock *= 0.90f;

	//check
	if (fabsf(stamp.vKnock.x) <= 3.0f)
		stamp.vKnock.x = 0.0f;

	//flip
	if (stamp.vKnock.x > 0)
		stamp.owner->GetTimeStamp()->bForwards = false;
	else if (stamp.vKnock.x < 0)
		stamp.owner->GetTimeStamp()->bForwards = true;

	//set velocity
	stamp.owner->SetVelocity(stamp.vKnock);
}

/**************************************************************
|	Function:
|	Purpose:
|	Parameter:
|	Return Value:
**************************************************************/
void KnockbackAI::ExtraFlinch(float dt, AIStamp& stamp)
{
	//access the timestamp
	Enemy* owner = dynamic_cast<Enemy*>(stamp.owner);
	AnimationTimestamp* timeStamp = owner->GetTimeStamp();

	//switch to flinching animations
	timeStamp->SwitchAnimation("flinch");

	//don't let them move
	stamp.fStartTimer += dt;

	//switch after a second
	if (stamp.fStartTimer >= 0.80f)
	{
		//timeStamp->SwitchAnimation("idle");
		stamp.SwitchAIMode(EnemyAIType::Flee);
	}

	//no moving
	owner->SetVelocity({ 0.0f, 0.0f });
}
