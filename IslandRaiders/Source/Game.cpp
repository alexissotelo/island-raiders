/***************************************************************
|	File:		Game.cpp
|	Author:		alexis sotelo
|	Course:		SGD 1409
|	Purpose:	Game class controls the SGD wrappers
|				& runs the game logic
***************************************************************/

#include "Game.h"


#include "../SGD Wrappers/SGD_EventManager.h"
#include "../SGD Wrappers/SGD_MessageManager.h"
#include "../SGD Wrappers/SGD_Message.h"
#include "../SGD Wrappers/SGD_String.h"
#include "../SGD Wrappers/SGD_Geometry.h"
#include "../SGD Wrappers/SGD_Event.h"
#include "../IslandRaiders/AnimationSystem.h" 

#define WIN32_LEAN_AND_MEAN
#define DEBUG_ENABLE 1

#include <Windows.h>

#include <ctime>
#include <cstdlib>
#include <cassert>
#include "..\IslandRaiders\IGameState.h"
#include "..\IslandRaiders\MenuGameState.h"
#include "..\IslandRaiders\LevelManager.h"
#include "..\IslandRaiders\PlayGameState.h"
#include "..\IslandRaiders\SaveLoadState.h"

#include "../IslandRaiders/GUISkin.h"
#include "../IslandRaiders/GUI.h"

#include "../IslandRaiders/ParticleManager.h"
#include "../IslandRaiders/GameInfo.h"
#include "../IslandRaiders/AudioQueue.h"
#include "../IslandRaiders/LoadingManager.h"

/**************************************************************/
// Singleton
//	- instantiate the static member
Game* Game::s_pInstance = nullptr;

// GetInstance
//	- allocate the ONE instance & return it
Game* Game::GetInstance(void)
{
	if (s_pInstance == nullptr)
		s_pInstance = new Game;

	return s_pInstance;
}

// DeleteInstance
//	- deallocate the ONE instance
void Game::DeleteInstance(void)
{
	delete s_pInstance;
	s_pInstance = nullptr;
}

/**************************************************************/
// Initialize
//	- initialize the SGD wrappers
//	- load resources / assets
//	- allocate & initialize the game entities
bool Game::Initialize(float width, float height)
{
	// Seed First!
	srand((unsigned int)time(nullptr));
	srand(static_cast<unsigned int>(time(0))); 
	rand();
	// Store the size parameters
	m_fScreenWidth = width;
	m_fScreenHeight = height;
	pAudio = SGD::AudioManager::GetInstance();

	// Local pointers to the SGD singletons
	SGD::AudioManager*	  pAudio = SGD::AudioManager::GetInstance();
	SGD::GraphicsManager* pGraphics = SGD::GraphicsManager::GetInstance();
	SGD::InputManager*	  pInput = SGD::InputManager::GetInstance();

	// Initialize each singleton
	if (pAudio->Initialize() == false
		|| pGraphics->Initialize() == false
		|| pInput->Initialize() == false)
	{
		return false;
	}

#pragma region Loading Prefab


#if 0
	//textures and time
	SGD::HTexture loadingTextures[6];
	loadingTextures[0] = SGD::GraphicsManager::GetInstance()->LoadTexture(L"resources/graphics/backgrounds/erzaloading.png");
	loadingTextures[1] = SGD::GraphicsManager::GetInstance()->LoadTexture(L"resources/graphics/backgrounds/franLoading.png");
	loadingTextures[2] = SGD::GraphicsManager::GetInstance()->LoadTexture(L"resources/graphics/backgrounds/lalLoading.png");
	loadingTextures[3] = SGD::GraphicsManager::GetInstance()->LoadTexture(L"resources/graphics/backgrounds/mihawkloading.png");
	loadingTextures[4] = SGD::GraphicsManager::GetInstance()->LoadTexture(L"resources/graphics/backgrounds/mystoganLoading.png");
	loadingTextures[5] = SGD::GraphicsManager::GetInstance()->LoadTexture(L"resources/graphics/backgrounds/xanxusLoading.png");

	// Current time
	unsigned long now = GetTickCount();
	float elapsedTime = (now - m_ulGameTime) / 1000.0f;
	m_ulGameTime = now;

	//current loading image
	unsigned int currFrame = 0;

	// Cap the elapsed time to 1/8th of a second
	if (elapsedTime > 0.125f)
		elapsedTime = 0.125f;

	for (unsigned int index = 0; index < 6; ++index)
		SGD::GraphicsManager::GetInstance()->UnloadTexture(loadingTextures[index]);
#pragma endregion

	//SGD::HTexture l = pGraphics->LoadTexture("resource/Graphics/placeholder images/loading.png");
	//SGD::HTexture l1 = pGraphics->LoadTexture("resource/Graphics/placeholder images/loading1.png");
	//SGD::HTexture l2 = pGraphics->LoadTexture("resource/Graphics/placeholder images/loading2.png");
	//SGD::HTexture l3 = pGraphics->LoadTexture("resource/Graphics/placeholder images/loading3.png");

	unsigned int index = 0; 
	unsigned char alpha = 255;
#endif

	AnimationSystem::GetInstance();
	AIManager::GetInstance();
	GUIManager::GetInstance();
	ParticleManager::GetInstance();
	LevelManager *levelManager = LevelManager::GetInstance();
	//game singleton 
	GameInfo::GetInstance();


	void(*function[4])();
	function[0] = &AnimationSystem::Initialize;
	function[1] = &AIManager::Initialize;
	function[2] = &GUIManager::Initialize;
	function[3] = &ParticleManager::Initialize;

	LoadingManager::GetInstance()->AddThread(&LevelManager::LoadEnemies, "resources/xml/enemies.xml");
	LoadingManager::GetInstance()->AddThread(&LevelManager::LoadEntities, "resources/xml/groups.xml");
	LoadingManager::GetInstance()->Update(function, 4);

	// Initialize the Event & Message Managers
	SGD::EventManager::GetInstance()->Initialize();
	SGD::MessageManager::GetInstance()->Initialize(&PlayGameState::MessageProc);
	
	LoadingManager::GetInstance()->Terminate();
	LoadingManager::DeleteInstance();
	ChangeState(MenuGameState::GetInstance());
	m_ulGameTime = GetTickCount();

	//for (unsigned int index = 0; index < 6; ++index)
	//	SGD::GraphicsManager::GetInstance()->UnloadTexture(loadingTextures[index]);

	return true;	// success!
}

/**************************************************************/
// Update
//	- update the SGD wrappers
//	- update the game entities
//	- render the game entities
int Game::Update(void)
{
	// Local pointers to the SGD singletons
	SGD::AudioManager*	  pAudio = SGD::AudioManager::GetInstance();
	SGD::GraphicsManager* pGraphics = SGD::GraphicsManager::GetInstance();
	SGD::InputManager*	  pInput = SGD::InputManager::GetInstance();
	//debug mode

#if DEBUG_ENABLE
	if (pInput->IsKeyPressed(SGD::Key::F4))
		bDebug = !bDebug;
#endif

	// Update each wrapper
	if (pAudio->Update() == false
		|| pGraphics->Update() == false
		|| pInput->Update() == false)
	{
		return -10;		// abort!
	}

	SGD::EventManager::GetInstance()->Update();
	// Current time
	unsigned long now = GetTickCount();
	float elapsedTime = (now - m_ulGameTime) / 1000.0f;
	m_ulGameTime = now;

	// Cap the elapsed time to 1/8th of a second
	if (elapsedTime > 0.125f)
		elapsedTime = 0.125f;

		if (pCurrState->Update(elapsedTime) == false)
			return 1; // exit success


		if (pInput->IsKeyDown(SGD::Key::Alt))
		{
			if (pInput->IsKeyPressed(SGD::Key::F4))
			{
				ChangeState(nullptr);
				return -1;
			}
		}

	pCurrState->Render(elapsedTime);
	GUIManager::GetInstance()->Update(elapsedTime);

	return 0;		// keep playing!
}

void Game::ArcadeCursorMovement(float dt)
{

}

/**************************************************************/
// Terminate
//	- deallocate game entities
//	- unload resources / assets
//	- terminate the SGD wrappers
void Game::Terminate(void)
{
	ChangeState(nullptr);//HAS TO BE CALLED FIRST

	LevelManager::DeleteInstance();
	//PlayGameState::DeleteInstance();

	//begone!
	GameInfo::GetInstance()->Terminate();
	GameInfo::DeleteInstance();

	//delete playerTest;
	AnimationSystem::GetInstance()->Terminate();
	AnimationSystem::DeleteInstance();

	AIManager::GetInstance()->Terminate();
	AIManager::DeleteInstance();

	// Local pointers to the SGD singletons
	SGD::AudioManager*	  pAudio = SGD::AudioManager::GetInstance();
	SGD::GraphicsManager* pGraphics = SGD::GraphicsManager::GetInstance();
	SGD::InputManager*	  pInput = SGD::InputManager::GetInstance();
	
	
	//pAudio->UnloadAudio(BackgroundMusic);
	SGD::Event event = { "GAME_OVER", nullptr, nullptr };
	event.SendEventNow();

	AudioQueue::GetInstance()->clear();
	AudioQueue::DeleteInstance();

	ParticleManager::GetInstance()->Terminate();
	ParticleManager::GetInstance()->DeleteInstance();

	GUIManager::GetInstance()->Terminate();
	GUIManager::GetInstance()->DeleteInstance();

	// Terminate & deallocate the SGD wrappers
	SGD::EventManager::GetInstance()->Terminate();
	SGD::EventManager::DeleteInstance();

	SGD::MessageManager::GetInstance()->Terminate();
	SGD::MessageManager::DeleteInstance();

	pAudio->Terminate();
	pAudio = nullptr;
	SGD::AudioManager::DeleteInstance();

	pGraphics->Terminate();
	pGraphics = nullptr;
	SGD::GraphicsManager::DeleteInstance();

	pInput->Terminate();
	pInput = nullptr;
	SGD::InputManager::DeleteInstance();
}

void Game::ChangeState(IGameState* newState)
{

	if (pCurrState != nullptr)
		pCurrState->Exit();

	pCurrState = newState;

	if (pCurrState != nullptr)
		pCurrState->Enter();
}

void Game::Save()
{
	switch (GetMasterSlot())
	{
	case 0:
		GameInfo::GetInstance()->SaveProfile("slotone");
		break;
	case 1:
		GameInfo::GetInstance()->SaveProfile("slottwo");
		break;
	case 2:
		GameInfo::GetInstance()->SaveProfile("slotthree");
		break;
	}
}
