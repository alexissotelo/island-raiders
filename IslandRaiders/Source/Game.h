/***************************************************************
|	File:		Game.h
|	Author:		Alexis Sotelo
|	Course:		SGD 1409
|	Purpose:	Game class controls the SGD wrappers
|				& runs the game logic
***************************************************************/
#ifndef GAME_H
#define GAME_H




#include "../SGD Wrappers/SGD_Declarations.h"
#include "../SGD Wrappers/SGD_Handle.h"
#include "../SGD Wrappers/SGD_Geometry.h"
#include "../IslandRaiders/AnimationTimestamp.h"
#include "../SGD Wrappers/SGD_AudioManager.h"
#include "../SGD Wrappers/SGD_GraphicsManager.h"
#include "../SGD Wrappers/SGD_InputManager.h"

/**************************************************************/
// Forward class declaration
//	- tells the compiler that the type exists
//	- allows us to create pointers or references
class Entity;
class IGameState;
//class BitmapFont;
#include "..\IslandRaiders\BitmapFont.h"

class GUISkin;
class Player;

#define DEBUG_MODE 0

/**************************************************************/
// Game class
//	- runs the game logic
//	- controls the SGD wrappers
//	- SINGLETON!
//		- only ONE object is created
//		- prevents access to constructors
//		- object could be dynamically-allocated or global object
//		- class provides an accessor for the object
//		- use Game::GetInstance() to access the object
class Game
{
public:
	bool ARCADEMODE = false;
	int NUMARCADEPLAYERS = 0;
	/**********************************************************/
	// Singleton Accessors:
	static Game*	GetInstance		( void );
	static void		DeleteInstance	( void );
	bool PAUSED = false;
	
	/**********************************************************/
	// Setup, Play, Cleanup:
	bool Initialize	( float width, float height );
	int	 Update		( void );
	void Terminate	( void );
	
	
	/**********************************************************/
	// Screen Size Accessors:
	float	GetScreenWidth	( void ) const	{	return m_fScreenWidth;	}
	float	GetScreenHeight ( void ) const	{	return m_fScreenHeight;	} 

	void ArcadeCursorMovement(float dt);

	void ChangeState(IGameState* newState);
	
	SGD::HAudio BackgroundMusic = SGD::INVALID_HANDLE;

	bool wantToSave = false;
	bool bDebug = false;

	const std::vector<Player*>* PlayerParty()const { return &playerParty; }

	void Save();
	int GetMasterSlot(void) const { return MASTERSLOT; }
	void SetMasterSlot(int x) { MASTERSLOT = x; }

private:
	std::vector<Player*> playerParty;
	/**********************************************************/
	// Singleton Object:
	static Game*	s_pInstance;
	SGD::AudioManager* pAudio = nullptr;
	float timer = 0.0f;
	float timerFreq = 0.4f;

	// Private constructor / destructor to prevent multiple objects
	Game( void )	= default;		// default constructor
	~Game( void )	= default;		// destructor

	Game( const Game& )				= delete;	// copy constructor
	Game& operator= ( const Game& )	= delete;	// assignment operator

	/**********************************************************/
	// Screen Size
	float					m_fScreenWidth	= 1;
	float					m_fScreenHeight = 1;

	/**********************************************************/
	// Game Time
	unsigned long			m_ulGameTime	= 0;

	IGameState* pCurrState = nullptr;
	int MASTERSLOT = -1;
};

#endif //GAME_H


