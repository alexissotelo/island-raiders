﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AnimationEditor
{
   public  class GraphicPanel : Panel
    {
      public  GraphicPanel()
       {
           //double buffer
           this.DoubleBuffered = true;
           this.SetStyle(ControlStyles.ResizeRedraw, true);
       }

    }
}
