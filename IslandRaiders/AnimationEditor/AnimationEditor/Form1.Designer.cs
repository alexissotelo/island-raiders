﻿namespace AnimationEditor
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.importXMLFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.saveAsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.loadSpriteImageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.previewBox = new System.Windows.Forms.GroupBox();
            this.eventText = new System.Windows.Forms.TextBox();
            this.spawnBox = new System.Windows.Forms.CheckBox();
            this.label3 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.previousFrame = new System.Windows.Forms.Button();
            this.activeBox = new System.Windows.Forms.CheckBox();
            this.collisionBox = new System.Windows.Forms.CheckBox();
            this.upDownDuration = new System.Windows.Forms.NumericUpDown();
            this.frameDurationLabel = new System.Windows.Forms.Label();
            this.ResetPreviewButton = new System.Windows.Forms.Button();
            this.StopButton = new System.Windows.Forms.Button();
            this.playButton = new System.Windows.Forms.Button();
            this.Animation = new System.Windows.Forms.GroupBox();
            this.sheetBox = new System.Windows.Forms.CheckBox();
            this.loopCheck = new System.Windows.Forms.CheckBox();
            this.deselectAnim = new System.Windows.Forms.Button();
            this.removeAnimation = new System.Windows.Forms.Button();
            this.addAnimation = new System.Windows.Forms.Button();
            this.listBox2 = new System.Windows.Forms.ListBox();
            this.animationName = new System.Windows.Forms.TextBox();
            this.animName = new System.Windows.Forms.Label();
            this.Frames = new System.Windows.Forms.GroupBox();
            this.spawnPoint = new System.Windows.Forms.RadioButton();
            this.upDownHeight = new System.Windows.Forms.NumericUpDown();
            this.upDownWidth = new System.Windows.Forms.NumericUpDown();
            this.upDownY = new System.Windows.Forms.NumericUpDown();
            this.upDownX = new System.Windows.Forms.NumericUpDown();
            this.boxHeight = new System.Windows.Forms.Label();
            this.boxWidth = new System.Windows.Forms.Label();
            this.boxPositionX = new System.Windows.Forms.Label();
            this.boxPositionY = new System.Windows.Forms.Label();
            this.moveFrameDown = new System.Windows.Forms.Button();
            this.deselectFrame = new System.Windows.Forms.Button();
            this.none = new System.Windows.Forms.RadioButton();
            this.activeRect = new System.Windows.Forms.RadioButton();
            this.collisionRect = new System.Windows.Forms.RadioButton();
            this.renderRect = new System.Windows.Forms.RadioButton();
            this.anchorPoint = new System.Windows.Forms.RadioButton();
            this.moveFrameUp = new System.Windows.Forms.Button();
            this.frameRemove = new System.Windows.Forms.Button();
            this.frameAdd = new System.Windows.Forms.Button();
            this.currentFrame = new System.Windows.Forms.Label();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.FrameLabel = new System.Windows.Forms.Label();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.graphicPanel1 = new AnimationEditor.GraphicPanel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.splitContainer3 = new System.Windows.Forms.SplitContainer();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.previewBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.upDownDuration)).BeginInit();
            this.Animation.SuspendLayout();
            this.Frames.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.upDownHeight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.upDownWidth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.upDownY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.upDownX)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            this.graphicPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).BeginInit();
            this.splitContainer3.Panel1.SuspendLayout();
            this.splitContainer3.Panel2.SuspendLayout();
            this.splitContainer3.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1411, 28);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newToolStripMenuItem,
            this.importXMLFileToolStripMenuItem,
            this.toolStripSeparator,
            this.saveAsToolStripMenuItem,
            this.toolStripSeparator1,
            this.loadSpriteImageToolStripMenuItem,
            this.toolStripSeparator2,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(44, 24);
            this.fileToolStripMenuItem.Text = "&File";
            // 
            // newToolStripMenuItem
            // 
            this.newToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("newToolStripMenuItem.Image")));
            this.newToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.newToolStripMenuItem.Name = "newToolStripMenuItem";
            this.newToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this.newToolStripMenuItem.Size = new System.Drawing.Size(183, 24);
            this.newToolStripMenuItem.Text = "&New";
            this.newToolStripMenuItem.Click += new System.EventHandler(this.newToolStripMenuItem_Click);
            // 
            // importXMLFileToolStripMenuItem
            // 
            this.importXMLFileToolStripMenuItem.Name = "importXMLFileToolStripMenuItem";
            this.importXMLFileToolStripMenuItem.Size = new System.Drawing.Size(183, 24);
            this.importXMLFileToolStripMenuItem.Text = "&Import XML File";
            this.importXMLFileToolStripMenuItem.Click += new System.EventHandler(this.importXMLFileToolStripMenuItem_Click);
            // 
            // toolStripSeparator
            // 
            this.toolStripSeparator.Name = "toolStripSeparator";
            this.toolStripSeparator.Size = new System.Drawing.Size(180, 6);
            // 
            // saveAsToolStripMenuItem
            // 
            this.saveAsToolStripMenuItem.Name = "saveAsToolStripMenuItem";
            this.saveAsToolStripMenuItem.Size = new System.Drawing.Size(183, 24);
            this.saveAsToolStripMenuItem.Text = "Save &As";
            this.saveAsToolStripMenuItem.Click += new System.EventHandler(this.saveAsToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(180, 6);
            // 
            // loadSpriteImageToolStripMenuItem
            // 
            this.loadSpriteImageToolStripMenuItem.Name = "loadSpriteImageToolStripMenuItem";
            this.loadSpriteImageToolStripMenuItem.Size = new System.Drawing.Size(183, 24);
            this.loadSpriteImageToolStripMenuItem.Text = "&Import Sheet";
            this.loadSpriteImageToolStripMenuItem.Click += new System.EventHandler(this.loadSpriteImageToolStripMenuItem_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(180, 6);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.ShowShortcutKeys = false;
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(183, 24);
            this.exitToolStripMenuItem.Text = "&Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // splitContainer1
            // 
            this.splitContainer1.AllowDrop = true;
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 28);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.BackColor = System.Drawing.SystemColors.Control;
            this.splitContainer1.Panel1.Controls.Add(this.previewBox);
            this.splitContainer1.Panel1.Controls.Add(this.Animation);
            this.splitContainer1.Panel1.Controls.Add(this.Frames);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.splitContainer2);
            this.splitContainer1.Size = new System.Drawing.Size(1411, 876);
            this.splitContainer1.SplitterDistance = 463;
            this.splitContainer1.TabIndex = 1;
            // 
            // previewBox
            // 
            this.previewBox.Controls.Add(this.eventText);
            this.previewBox.Controls.Add(this.spawnBox);
            this.previewBox.Controls.Add(this.label3);
            this.previewBox.Controls.Add(this.button1);
            this.previewBox.Controls.Add(this.previousFrame);
            this.previewBox.Controls.Add(this.activeBox);
            this.previewBox.Controls.Add(this.collisionBox);
            this.previewBox.Controls.Add(this.upDownDuration);
            this.previewBox.Controls.Add(this.frameDurationLabel);
            this.previewBox.Controls.Add(this.ResetPreviewButton);
            this.previewBox.Controls.Add(this.StopButton);
            this.previewBox.Controls.Add(this.playButton);
            this.previewBox.Location = new System.Drawing.Point(33, 607);
            this.previewBox.Name = "previewBox";
            this.previewBox.Size = new System.Drawing.Size(397, 244);
            this.previewBox.TabIndex = 2;
            this.previewBox.TabStop = false;
            this.previewBox.Text = "Preview";
            // 
            // eventText
            // 
            this.eventText.Location = new System.Drawing.Point(123, 66);
            this.eventText.Name = "eventText";
            this.eventText.Size = new System.Drawing.Size(112, 22);
            this.eventText.TabIndex = 14;
            // 
            // spawnBox
            // 
            this.spawnBox.AutoSize = true;
            this.spawnBox.Location = new System.Drawing.Point(14, 164);
            this.spawnBox.Name = "spawnBox";
            this.spawnBox.Size = new System.Drawing.Size(108, 21);
            this.spawnBox.TabIndex = 13;
            this.spawnBox.Text = "Spawn Point";
            this.spawnBox.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(14, 66);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(44, 17);
            this.label3.TabIndex = 11;
            this.label3.Text = "Event";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(276, 155);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(99, 37);
            this.button1.TabIndex = 10;
            this.button1.Text = "Next Frame";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // previousFrame
            // 
            this.previousFrame.Location = new System.Drawing.Point(276, 198);
            this.previousFrame.Name = "previousFrame";
            this.previousFrame.Size = new System.Drawing.Size(99, 37);
            this.previousFrame.TabIndex = 9;
            this.previousFrame.Text = "Last Frame";
            this.previousFrame.UseVisualStyleBackColor = true;
            this.previousFrame.Click += new System.EventHandler(this.previousFrame_Click);
            // 
            // activeBox
            // 
            this.activeBox.AutoSize = true;
            this.activeBox.Location = new System.Drawing.Point(17, 139);
            this.activeBox.Name = "activeBox";
            this.activeBox.Size = new System.Drawing.Size(68, 21);
            this.activeBox.TabIndex = 8;
            this.activeBox.Text = "Active";
            this.activeBox.UseVisualStyleBackColor = true;
            // 
            // collisionBox
            // 
            this.collisionBox.AutoSize = true;
            this.collisionBox.Location = new System.Drawing.Point(17, 112);
            this.collisionBox.Name = "collisionBox";
            this.collisionBox.Size = new System.Drawing.Size(82, 21);
            this.collisionBox.TabIndex = 7;
            this.collisionBox.Text = "Collision";
            this.collisionBox.UseVisualStyleBackColor = true;
            // 
            // upDownDuration
            // 
            this.upDownDuration.DecimalPlaces = 2;
            this.upDownDuration.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.upDownDuration.Location = new System.Drawing.Point(123, 34);
            this.upDownDuration.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.upDownDuration.Name = "upDownDuration";
            this.upDownDuration.Size = new System.Drawing.Size(120, 22);
            this.upDownDuration.TabIndex = 6;
            this.upDownDuration.Value = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.upDownDuration.ValueChanged += new System.EventHandler(this.upDownDuration_ValueChanged);
            // 
            // frameDurationLabel
            // 
            this.frameDurationLabel.AutoSize = true;
            this.frameDurationLabel.Location = new System.Drawing.Point(11, 34);
            this.frameDurationLabel.Name = "frameDurationLabel";
            this.frameDurationLabel.Size = new System.Drawing.Size(106, 17);
            this.frameDurationLabel.TabIndex = 5;
            this.frameDurationLabel.Text = "Frame Duration";
            // 
            // ResetPreviewButton
            // 
            this.ResetPreviewButton.Location = new System.Drawing.Point(276, 112);
            this.ResetPreviewButton.Name = "ResetPreviewButton";
            this.ResetPreviewButton.Size = new System.Drawing.Size(99, 37);
            this.ResetPreviewButton.TabIndex = 2;
            this.ResetPreviewButton.Text = "Reset";
            this.ResetPreviewButton.UseVisualStyleBackColor = true;
            this.ResetPreviewButton.Click += new System.EventHandler(this.ResetPreviewButton_Click);
            // 
            // StopButton
            // 
            this.StopButton.Location = new System.Drawing.Point(277, 69);
            this.StopButton.Name = "StopButton";
            this.StopButton.Size = new System.Drawing.Size(99, 37);
            this.StopButton.TabIndex = 1;
            this.StopButton.Text = "Pause";
            this.StopButton.UseVisualStyleBackColor = true;
            this.StopButton.Click += new System.EventHandler(this.StopButton_Click);
            // 
            // playButton
            // 
            this.playButton.Location = new System.Drawing.Point(277, 26);
            this.playButton.Name = "playButton";
            this.playButton.Size = new System.Drawing.Size(99, 37);
            this.playButton.TabIndex = 0;
            this.playButton.Text = "Play";
            this.playButton.UseVisualStyleBackColor = true;
            this.playButton.Click += new System.EventHandler(this.playButton_Click);
            // 
            // Animation
            // 
            this.Animation.Controls.Add(this.sheetBox);
            this.Animation.Controls.Add(this.loopCheck);
            this.Animation.Controls.Add(this.deselectAnim);
            this.Animation.Controls.Add(this.removeAnimation);
            this.Animation.Controls.Add(this.addAnimation);
            this.Animation.Controls.Add(this.listBox2);
            this.Animation.Controls.Add(this.animationName);
            this.Animation.Controls.Add(this.animName);
            this.Animation.Location = new System.Drawing.Point(21, 377);
            this.Animation.Name = "Animation";
            this.Animation.Size = new System.Drawing.Size(439, 209);
            this.Animation.TabIndex = 1;
            this.Animation.TabStop = false;
            this.Animation.Text = "Animation";
            // 
            // sheetBox
            // 
            this.sheetBox.AutoSize = true;
            this.sheetBox.Location = new System.Drawing.Point(13, 170);
            this.sheetBox.Name = "sheetBox";
            this.sheetBox.Size = new System.Drawing.Size(139, 21);
            this.sheetBox.TabIndex = 7;
            this.sheetBox.Text = "Sheet Backwards";
            this.sheetBox.UseVisualStyleBackColor = true;
            // 
            // loopCheck
            // 
            this.loopCheck.AutoSize = true;
            this.loopCheck.Location = new System.Drawing.Point(12, 148);
            this.loopCheck.Name = "loopCheck";
            this.loopCheck.Size = new System.Drawing.Size(62, 21);
            this.loopCheck.TabIndex = 6;
            this.loopCheck.Text = "Loop";
            this.loopCheck.UseVisualStyleBackColor = true;
            // 
            // deselectAnim
            // 
            this.deselectAnim.Location = new System.Drawing.Point(214, 170);
            this.deselectAnim.Name = "deselectAnim";
            this.deselectAnim.Size = new System.Drawing.Size(208, 33);
            this.deselectAnim.TabIndex = 5;
            this.deselectAnim.Text = "Unselect Animation";
            this.deselectAnim.UseVisualStyleBackColor = true;
            this.deselectAnim.Click += new System.EventHandler(this.deselectAnim_Click);
            // 
            // removeAnimation
            // 
            this.removeAnimation.Location = new System.Drawing.Point(13, 99);
            this.removeAnimation.Name = "removeAnimation";
            this.removeAnimation.Size = new System.Drawing.Size(195, 33);
            this.removeAnimation.TabIndex = 4;
            this.removeAnimation.Text = "Remove Animation";
            this.removeAnimation.UseVisualStyleBackColor = true;
            this.removeAnimation.Click += new System.EventHandler(this.removeAnimation_Click);
            // 
            // addAnimation
            // 
            this.addAnimation.Location = new System.Drawing.Point(12, 60);
            this.addAnimation.Name = "addAnimation";
            this.addAnimation.Size = new System.Drawing.Size(196, 33);
            this.addAnimation.TabIndex = 3;
            this.addAnimation.Text = "Add Animation";
            this.addAnimation.UseVisualStyleBackColor = true;
            this.addAnimation.Click += new System.EventHandler(this.addAnimation_Click);
            // 
            // listBox2
            // 
            this.listBox2.FormattingEnabled = true;
            this.listBox2.ItemHeight = 16;
            this.listBox2.Location = new System.Drawing.Point(214, 22);
            this.listBox2.Name = "listBox2";
            this.listBox2.Size = new System.Drawing.Size(208, 148);
            this.listBox2.TabIndex = 2;
            this.listBox2.SelectedIndexChanged += new System.EventHandler(this.listBox2_SelectedIndexChanged);
            // 
            // animationName
            // 
            this.animationName.Location = new System.Drawing.Point(61, 22);
            this.animationName.Name = "animationName";
            this.animationName.Size = new System.Drawing.Size(147, 22);
            this.animationName.TabIndex = 1;
            // 
            // animName
            // 
            this.animName.AutoSize = true;
            this.animName.Location = new System.Drawing.Point(9, 22);
            this.animName.Name = "animName";
            this.animName.Size = new System.Drawing.Size(45, 17);
            this.animName.TabIndex = 0;
            this.animName.Text = "Name";
            // 
            // Frames
            // 
            this.Frames.Controls.Add(this.spawnPoint);
            this.Frames.Controls.Add(this.upDownHeight);
            this.Frames.Controls.Add(this.upDownWidth);
            this.Frames.Controls.Add(this.upDownY);
            this.Frames.Controls.Add(this.upDownX);
            this.Frames.Controls.Add(this.boxHeight);
            this.Frames.Controls.Add(this.boxWidth);
            this.Frames.Controls.Add(this.boxPositionX);
            this.Frames.Controls.Add(this.boxPositionY);
            this.Frames.Controls.Add(this.moveFrameDown);
            this.Frames.Controls.Add(this.deselectFrame);
            this.Frames.Controls.Add(this.none);
            this.Frames.Controls.Add(this.activeRect);
            this.Frames.Controls.Add(this.collisionRect);
            this.Frames.Controls.Add(this.renderRect);
            this.Frames.Controls.Add(this.anchorPoint);
            this.Frames.Controls.Add(this.moveFrameUp);
            this.Frames.Controls.Add(this.frameRemove);
            this.Frames.Controls.Add(this.frameAdd);
            this.Frames.Controls.Add(this.currentFrame);
            this.Frames.Controls.Add(this.listBox1);
            this.Frames.Controls.Add(this.FrameLabel);
            this.Frames.Location = new System.Drawing.Point(12, 3);
            this.Frames.Name = "Frames";
            this.Frames.Size = new System.Drawing.Size(440, 349);
            this.Frames.TabIndex = 0;
            this.Frames.TabStop = false;
            this.Frames.Text = "Frames";
            // 
            // spawnPoint
            // 
            this.spawnPoint.AutoSize = true;
            this.spawnPoint.Location = new System.Drawing.Point(10, 322);
            this.spawnPoint.Name = "spawnPoint";
            this.spawnPoint.Size = new System.Drawing.Size(107, 21);
            this.spawnPoint.TabIndex = 21;
            this.spawnPoint.TabStop = true;
            this.spawnPoint.Text = "Spawn Point";
            this.spawnPoint.UseVisualStyleBackColor = true;
            this.spawnPoint.CheckedChanged += new System.EventHandler(this.spawnPoint_CheckedChanged);
            // 
            // upDownHeight
            // 
            this.upDownHeight.Location = new System.Drawing.Point(262, 298);
            this.upDownHeight.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.upDownHeight.Name = "upDownHeight";
            this.upDownHeight.Size = new System.Drawing.Size(75, 22);
            this.upDownHeight.TabIndex = 20;
            this.upDownHeight.ValueChanged += new System.EventHandler(this.upDownHeight_ValueChanged);
            // 
            // upDownWidth
            // 
            this.upDownWidth.Location = new System.Drawing.Point(262, 270);
            this.upDownWidth.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.upDownWidth.Name = "upDownWidth";
            this.upDownWidth.Size = new System.Drawing.Size(75, 22);
            this.upDownWidth.TabIndex = 19;
            this.upDownWidth.ValueChanged += new System.EventHandler(this.upDownWidth_ValueChanged);
            // 
            // upDownY
            // 
            this.upDownY.Location = new System.Drawing.Point(262, 242);
            this.upDownY.Maximum = new decimal(new int[] {
            9999,
            0,
            0,
            0});
            this.upDownY.Name = "upDownY";
            this.upDownY.Size = new System.Drawing.Size(75, 22);
            this.upDownY.TabIndex = 18;
            this.upDownY.ValueChanged += new System.EventHandler(this.upDownY_ValueChanged);
            // 
            // upDownX
            // 
            this.upDownX.Location = new System.Drawing.Point(262, 214);
            this.upDownX.Maximum = new decimal(new int[] {
            9999,
            0,
            0,
            0});
            this.upDownX.Name = "upDownX";
            this.upDownX.Size = new System.Drawing.Size(75, 22);
            this.upDownX.TabIndex = 17;
            this.upDownX.ValueChanged += new System.EventHandler(this.upDownX_ValueChanged);
            // 
            // boxHeight
            // 
            this.boxHeight.AutoSize = true;
            this.boxHeight.Location = new System.Drawing.Point(207, 298);
            this.boxHeight.Name = "boxHeight";
            this.boxHeight.Size = new System.Drawing.Size(49, 17);
            this.boxHeight.TabIndex = 16;
            this.boxHeight.Text = "Height";
            // 
            // boxWidth
            // 
            this.boxWidth.AutoSize = true;
            this.boxWidth.Location = new System.Drawing.Point(207, 270);
            this.boxWidth.Name = "boxWidth";
            this.boxWidth.Size = new System.Drawing.Size(44, 17);
            this.boxWidth.TabIndex = 15;
            this.boxWidth.Text = "Width";
            // 
            // boxPositionX
            // 
            this.boxPositionX.AutoSize = true;
            this.boxPositionX.Location = new System.Drawing.Point(207, 242);
            this.boxPositionX.Name = "boxPositionX";
            this.boxPositionX.Size = new System.Drawing.Size(17, 17);
            this.boxPositionX.TabIndex = 14;
            this.boxPositionX.Text = "Y";
            // 
            // boxPositionY
            // 
            this.boxPositionY.AutoSize = true;
            this.boxPositionY.Location = new System.Drawing.Point(207, 214);
            this.boxPositionY.Name = "boxPositionY";
            this.boxPositionY.Size = new System.Drawing.Size(17, 17);
            this.boxPositionY.TabIndex = 13;
            this.boxPositionY.Text = "X";
            // 
            // moveFrameDown
            // 
            this.moveFrameDown.Location = new System.Drawing.Point(6, 122);
            this.moveFrameDown.Name = "moveFrameDown";
            this.moveFrameDown.Size = new System.Drawing.Size(153, 27);
            this.moveFrameDown.TabIndex = 12;
            this.moveFrameDown.Text = "Move Down";
            this.moveFrameDown.UseVisualStyleBackColor = true;
            this.moveFrameDown.Click += new System.EventHandler(this.moveFrameDown_Click);
            // 
            // deselectFrame
            // 
            this.deselectFrame.Location = new System.Drawing.Point(200, 152);
            this.deselectFrame.Name = "deselectFrame";
            this.deselectFrame.Size = new System.Drawing.Size(228, 27);
            this.deselectFrame.TabIndex = 11;
            this.deselectFrame.Text = "Unselect Frame";
            this.deselectFrame.UseVisualStyleBackColor = true;
            this.deselectFrame.Click += new System.EventHandler(this.deselectFrame_Click);
            // 
            // none
            // 
            this.none.AutoSize = true;
            this.none.Checked = true;
            this.none.Location = new System.Drawing.Point(8, 188);
            this.none.Name = "none";
            this.none.Size = new System.Drawing.Size(63, 21);
            this.none.TabIndex = 10;
            this.none.TabStop = true;
            this.none.Text = "None";
            this.none.UseVisualStyleBackColor = true;
            // 
            // activeRect
            // 
            this.activeRect.AutoSize = true;
            this.activeRect.Location = new System.Drawing.Point(9, 296);
            this.activeRect.Name = "activeRect";
            this.activeRect.Size = new System.Drawing.Size(67, 21);
            this.activeRect.TabIndex = 9;
            this.activeRect.TabStop = true;
            this.activeRect.Text = "Active";
            this.activeRect.UseVisualStyleBackColor = true;
            this.activeRect.CheckedChanged += new System.EventHandler(this.activeRect_CheckedChanged);
            // 
            // collisionRect
            // 
            this.collisionRect.AutoSize = true;
            this.collisionRect.Location = new System.Drawing.Point(9, 269);
            this.collisionRect.Name = "collisionRect";
            this.collisionRect.Size = new System.Drawing.Size(81, 21);
            this.collisionRect.TabIndex = 8;
            this.collisionRect.TabStop = true;
            this.collisionRect.Text = "Collision";
            this.collisionRect.UseVisualStyleBackColor = true;
            this.collisionRect.CheckedChanged += new System.EventHandler(this.collisionRect_CheckedChanged);
            // 
            // renderRect
            // 
            this.renderRect.AutoSize = true;
            this.renderRect.Location = new System.Drawing.Point(9, 242);
            this.renderRect.Name = "renderRect";
            this.renderRect.Size = new System.Drawing.Size(76, 21);
            this.renderRect.TabIndex = 7;
            this.renderRect.Text = "Render";
            this.renderRect.UseVisualStyleBackColor = true;
            this.renderRect.CheckedChanged += new System.EventHandler(this.renderRect_CheckedChanged);
            // 
            // anchorPoint
            // 
            this.anchorPoint.AutoSize = true;
            this.anchorPoint.Location = new System.Drawing.Point(9, 215);
            this.anchorPoint.Name = "anchorPoint";
            this.anchorPoint.Size = new System.Drawing.Size(110, 21);
            this.anchorPoint.TabIndex = 6;
            this.anchorPoint.Text = "Anchor Point";
            this.anchorPoint.UseVisualStyleBackColor = true;
            this.anchorPoint.CheckedChanged += new System.EventHandler(this.anchorPoint_CheckedChanged);
            // 
            // moveFrameUp
            // 
            this.moveFrameUp.Location = new System.Drawing.Point(6, 89);
            this.moveFrameUp.Name = "moveFrameUp";
            this.moveFrameUp.Size = new System.Drawing.Size(153, 27);
            this.moveFrameUp.TabIndex = 5;
            this.moveFrameUp.Text = "Move Up";
            this.moveFrameUp.UseVisualStyleBackColor = true;
            this.moveFrameUp.Click += new System.EventHandler(this.moveFrameUp_Click);
            // 
            // frameRemove
            // 
            this.frameRemove.Location = new System.Drawing.Point(6, 56);
            this.frameRemove.Name = "frameRemove";
            this.frameRemove.Size = new System.Drawing.Size(153, 27);
            this.frameRemove.TabIndex = 4;
            this.frameRemove.Text = "Remove Frame";
            this.frameRemove.UseVisualStyleBackColor = true;
            this.frameRemove.Click += new System.EventHandler(this.frameRemove_Click);
            // 
            // frameAdd
            // 
            this.frameAdd.Location = new System.Drawing.Point(6, 23);
            this.frameAdd.Name = "frameAdd";
            this.frameAdd.Size = new System.Drawing.Size(153, 27);
            this.frameAdd.TabIndex = 3;
            this.frameAdd.Text = "Add Frame";
            this.frameAdd.UseVisualStyleBackColor = true;
            this.frameAdd.Click += new System.EventHandler(this.frameAdd_Click);
            // 
            // currentFrame
            // 
            this.currentFrame.AutoSize = true;
            this.currentFrame.Location = new System.Drawing.Point(332, 182);
            this.currentFrame.Name = "currentFrame";
            this.currentFrame.Size = new System.Drawing.Size(16, 17);
            this.currentFrame.TabIndex = 2;
            this.currentFrame.Text = "0";
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.ItemHeight = 16;
            this.listBox1.Location = new System.Drawing.Point(200, 17);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(228, 132);
            this.listBox1.TabIndex = 1;
            this.listBox1.SelectedIndexChanged += new System.EventHandler(this.listBox1_SelectedIndexChanged);
            // 
            // FrameLabel
            // 
            this.FrameLabel.AutoSize = true;
            this.FrameLabel.Location = new System.Drawing.Point(234, 182);
            this.FrameLabel.Name = "FrameLabel";
            this.FrameLabel.Size = new System.Drawing.Size(103, 17);
            this.FrameLabel.TabIndex = 0;
            this.FrameLabel.Text = "Current Frame:";
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            this.splitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.graphicPanel1);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.splitContainer3);
            this.splitContainer2.Size = new System.Drawing.Size(944, 876);
            this.splitContainer2.SplitterDistance = 603;
            this.splitContainer2.TabIndex = 0;
            // 
            // graphicPanel1
            // 
            this.graphicPanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.graphicPanel1.Controls.Add(this.panel1);
            this.graphicPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.graphicPanel1.Location = new System.Drawing.Point(0, 0);
            this.graphicPanel1.Name = "graphicPanel1";
            this.graphicPanel1.Size = new System.Drawing.Size(944, 603);
            this.graphicPanel1.TabIndex = 0;
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(942, 601);
            this.panel1.TabIndex = 0;
            this.panel1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseDown);
            this.panel1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseMove);
            this.panel1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseUp);
            // 
            // splitContainer3
            // 
            this.splitContainer3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer3.Location = new System.Drawing.Point(0, 0);
            this.splitContainer3.Name = "splitContainer3";
            // 
            // splitContainer3.Panel1
            // 
            this.splitContainer3.Panel1.Controls.Add(this.panel2);
            // 
            // splitContainer3.Panel2
            // 
            this.splitContainer3.Panel2.Controls.Add(this.panel3);
            this.splitContainer3.Size = new System.Drawing.Size(944, 269);
            this.splitContainer3.SplitterDistance = 488;
            this.splitContainer3.TabIndex = 0;
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.label1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(488, 269);
            this.panel2.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 2);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(66, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "RENDER";
            // 
            // panel3
            // 
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.label2);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(452, 269);
            this.panel3.TabIndex = 0;
            this.panel3.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panel3_MouseDown);
            this.panel3.MouseMove += new System.Windows.Forms.MouseEventHandler(this.panel3_MouseMove);
            this.panel3.MouseUp += new System.Windows.Forms.MouseEventHandler(this.panel3_MouseUp);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(4, 2);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(70, 17);
            this.label2.TabIndex = 0;
            this.label2.Text = "PREVIEW";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1411, 904);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Animation Editor";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.previewBox.ResumeLayout(false);
            this.previewBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.upDownDuration)).EndInit();
            this.Animation.ResumeLayout(false);
            this.Animation.PerformLayout();
            this.Frames.ResumeLayout(false);
            this.Frames.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.upDownHeight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.upDownWidth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.upDownY)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.upDownX)).EndInit();
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            this.graphicPanel1.ResumeLayout(false);
            this.splitContainer3.Panel1.ResumeLayout(false);
            this.splitContainer3.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).EndInit();
            this.splitContainer3.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator;
        private System.Windows.Forms.ToolStripMenuItem saveAsToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadSpriteImageToolStripMenuItem;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.GroupBox Frames;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.Label FrameLabel;
        private System.Windows.Forms.Label currentFrame;
        private System.Windows.Forms.Button moveFrameUp;
        private System.Windows.Forms.Button frameRemove;
        private System.Windows.Forms.Button frameAdd;
        private System.Windows.Forms.RadioButton none;
        private System.Windows.Forms.RadioButton activeRect;
        private System.Windows.Forms.RadioButton collisionRect;
        private System.Windows.Forms.RadioButton renderRect;
        private System.Windows.Forms.RadioButton anchorPoint;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.SplitContainer splitContainer3;
        private GraphicPanel graphicPanel1;
        private System.Windows.Forms.Button deselectFrame;
        private System.Windows.Forms.Button moveFrameDown;
        private System.Windows.Forms.GroupBox Animation;
        private System.Windows.Forms.Button deselectAnim;
        private System.Windows.Forms.Button removeAnimation;
        private System.Windows.Forms.Button addAnimation;
        private System.Windows.Forms.ListBox listBox2;
        private System.Windows.Forms.TextBox animationName;
        private System.Windows.Forms.Label animName;
        private System.Windows.Forms.GroupBox previewBox;
        private System.Windows.Forms.Button ResetPreviewButton;
        private System.Windows.Forms.Button StopButton;
        private System.Windows.Forms.Button playButton;
        private System.Windows.Forms.CheckBox loopCheck;
        private System.Windows.Forms.ToolStripMenuItem importXMLFileToolStripMenuItem;
        private System.Windows.Forms.Label boxHeight;
        private System.Windows.Forms.Label boxWidth;
        private System.Windows.Forms.Label boxPositionX;
        private System.Windows.Forms.Label boxPositionY;
        private System.Windows.Forms.NumericUpDown upDownHeight;
        private System.Windows.Forms.NumericUpDown upDownWidth;
        private System.Windows.Forms.NumericUpDown upDownY;
        private System.Windows.Forms.NumericUpDown upDownX;
        private System.Windows.Forms.NumericUpDown upDownDuration;
        private System.Windows.Forms.Label frameDurationLabel;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckBox activeBox;
        private System.Windows.Forms.CheckBox collisionBox;
        private System.Windows.Forms.Button previousFrame;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.CheckBox sheetBox;
        private System.Windows.Forms.RadioButton spawnPoint;
        private System.Windows.Forms.CheckBox spawnBox;
        private System.Windows.Forms.TextBox eventText;
    }
}

