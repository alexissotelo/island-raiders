﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Threading.Tasks;

namespace AnimationEditor
{
    public class Frame
    {
        //duration for the frame
        public float duration;
        //position - anchor
       public Point anchor;
       public Point spawn; //offset from the anchor to spawn something
        //rende rect
       public Rectangle render;
       public Point renderOffset;
        //collision rect
       public Rectangle collision;
       public Point collisionOffset;
        //active rect
       public Rectangle active;
       public Point activeOffset;
        //event to throw
       public string eventText;

    }

}
