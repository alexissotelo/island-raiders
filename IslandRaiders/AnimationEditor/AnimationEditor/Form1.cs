﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Linq;

namespace AnimationEditor
{
    public partial class Form1 : Form
    {
        //replace image on new
        //
        SGP.CSGP_Direct3D D3D = SGP.CSGP_Direct3D.GetInstance();
        SGP.CSGP_TextureManager tManager = SGP.CSGP_TextureManager.GetInstance();
        int imageID;

        //running loop
        bool looping;
        //TODO saving and loading 
        //TODO preview screen

        //current sprite sheet
        Bitmap spriteSheet;
        //drawing points
        Point mouseDown;
        Point mouseUp;
        Point mouseCurr;
        Point previewMouse;
        Point previewAnchor;

        //input check
        bool holding;
        bool updateFrame;
        bool updateAnimation;
        bool updatePreview;
        bool playPreview;
        bool frameOn;
        bool imageSaved;
        int frameIndex;
        int animIndex;
        int currPreview;
        float runTime;

        //class rectangle info
        Point anchor;
        Point spawn;
        Rectangle render;
        Rectangle collision;
        Rectangle active;

        //sprite sheet info
        string imageFileName;
        public string fileFolderName;
        //string saveFileName;

        //list of frames
        List<Frame> frameList;
        List<Animation> animList;

        //Timers
        DateTime tick;

        public Form1()
        {
            InitializeComponent();


            //initalize the wrappers
            D3D.Initialize(panel1, true);
            D3D.AddRenderTarget(panel2);
            D3D.AddRenderTarget(panel3);
            tManager.Initialize(D3D.Device, D3D.Sprite);

            //D3D.Resize(panel3, panel3.ClientRectangle.Width, panel3.ClientRectangle.Height, true);

            //image id number
            imageID = -1;

            //time
            tick = new DateTime();
            tick = DateTime.Now;

            //turn on
            looping = true;
            //null bitmap
            spriteSheet = null;
            imageSaved = false;
            //initialize the list
            frameList = new List<Frame>();
            animList = new List<Animation>();

            Reset();
            frameIndex = 0;
            currPreview = 0;
            animIndex = -1;
            runTime = 0.0f;
            updatePreview = false;
            playPreview = false;
            frameOn = false;


            //render anchor in the center of the screen
            previewAnchor.X = panel3.Size.Width / 2;
            previewAnchor.Y = panel3.Size.Height / 2;

            //tool tips
            CreateToolTips();
            int index = frameIndex + 1;
            currentFrame.Text = index.ToString();

            //combo box
            eventText.Text = "";
        }
        //loop
        public bool Looping
        {
            get { return looping; }
            set { looping = value; }
        }
        //ToolTips
        private void CreateToolTips()
        {
            //create a tool tip
            ToolTip tip = new ToolTip();

            //Set up delay for the ToolTip
            tip.InitialDelay = 1000;

            //Set up ToolTip for Buttons
            tip.SetToolTip(this.frameAdd, "Add Frame\nAdd a Frame into the list.\nFrame MUST have atleast a render rectangle and anchor point.");
            tip.SetToolTip(this.frameRemove, "Remove Frame\nDeletes a frame from the current frame list");
            tip.SetToolTip(this.moveFrameUp, "Move Up\nMove a frame an index higher in the list");
            tip.SetToolTip(this.moveFrameDown, "Move Up\nMove a frame an index lower in the list");
            tip.SetToolTip(this.deselectFrame, "Unselect Frame\n Deselect all frames in the list");

            tip.SetToolTip(this.anchorPoint, "Anchor Point\n");
            tip.SetToolTip(this.renderRect, "Render Rectangle\n");
            tip.SetToolTip(this.collisionRect, "Collision Rectangle\n");
            tip.SetToolTip(this.activeRect, "Active Rectangle\n");

            tip.SetToolTip(this.addAnimation, "Add Animation\n Add an animation into the list.\nEach animation must have a unique name\nand at least one frame.");
            tip.SetToolTip(this.removeAnimation, "Remove Animation\nRemoves an animations from the list.");
            tip.SetToolTip(this.loopCheck, "Loop\n");
            tip.SetToolTip(this.deselectAnim, "Unselect Animation\nDeselect all animations in the list.");
        }
        //Load Sprite Image
        private void loadSpriteImageToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //load in an image and display the entire sheet into the splitcontainer 1.1

            //open the dialog the box
            OpenFileDialog dlg = new OpenFileDialog();

            //filters
            dlg.Filter = "All Files|*.*|JPG|*.jpg*|Bitmap|*.bmp|PNG|*.png";
            dlg.FilterIndex = 4;

            if (DialogResult.OK == dlg.ShowDialog())
            {
                //new bitmap
                spriteSheet = new Bitmap(dlg.FileName);
                imageFileName = dlg.SafeFileName;
                //wrapeprs
                imageID = tManager.LoadTexture(dlg.FileName);

                if (imageID == -1)
                {
                    this.Close();
                    return;
                }
                Size size = new Size(tManager.GetTextureWidth(imageID), tManager.GetTextureHeight(imageID));
                panel1.AutoScrollMinSize = spriteSheet.Size;
                D3D.Resize(panel1, panel1.ClientRectangle.Width, panel1.ClientRectangle.Height, true);

                // spriteSheet.Dispose();
                // spriteSheet = null;
            }
        }
        //Exit
        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //exit the application
            this.Close();
            looping = false;
        }
        private void frameAdd_Click(object sender, EventArgs e)
        {
            //anchor and render at least
            if (render.X == -1 || anchor.X == -1 || listBox1.SelectedIndex != -1)
                return;

            //last index
            frameIndex = listBox1.Items.Count + 1;

            //create a frame instance and add it into the list
            Frame frame = new Frame();

            //fill out frame information -point
            frame.anchor = anchor;


            //render rectangle - local to the anchor
            frame.renderOffset.X = render.X - anchor.X;
            frame.renderOffset.Y = render.Y - anchor.Y;

            //render rectangle
            frame.render = render;
            frame.spawn = spawn;

            //colllision rectangle - local to the anchor point
            if (collision.X != -1)
            {
                //rectangle offset
                frame.collisionOffset.X = collision.X - anchor.X;
                frame.collisionOffset.Y = collision.Y - anchor.Y;
                //rectanlge for collision
                frame.collision = collision;
            }


            //active rectangle-- local to the anchor point
            if (active.X != -1)
            {
                //rectangle offset
                frame.activeOffset.X = active.X - anchor.X;
                frame.activeOffset.Y = active.Y - anchor.Y;
                //active rectangle
                frame.active = active;
            }

            //duration
            frame.duration = (float)upDownDuration.Value;
            //event
            frame.eventText = eventText.Text;

            //set the label
            string label;
            label = "Frame ";
            label += frameIndex.ToString();

            //new index to render in preview
            currPreview = listBox1.Items.Count;
            frameIndex = listBox1.Items.Count;
            //event
            eventText.Text = "";

            //add into the list / listBox
            listBox1.Items.Add(label);
            frameList.Add(frame);

            //update
            frameOn = false;

            if (updateAnimation)
                UpdateCurrentAnimation();
            //reset
            Reset();

        }
        //select frame
        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            //selected index
            int boxIndex = listBox1.SelectedIndex;

            if (updateFrame)
                UpdateCurrentFrame();

            //check the value
            if (boxIndex != -1)
            {

                //display current frame
                int txt = boxIndex + 1;
                currentFrame.Text = txt.ToString();

                frameOn = true;

                //frame
                Frame curr = frameList[boxIndex];
                Rectangle rect;
                //draw eveything

                //anchor
                anchor = curr.anchor;
                spawn = curr.spawn;
                //event
                eventText.Text = curr.eventText;

                //render
                rect = new Rectangle(curr.render.X + curr.anchor.X, curr.render.Y + curr.anchor.Y, curr.render.Width, curr.render.Height);
                render = curr.render;

                //collision
                rect = new Rectangle(curr.collision.X + curr.anchor.X, curr.collision.Y + curr.anchor.Y,
                    curr.collision.Width, curr.collision.Height);
                collision = curr.collision;

                //active
                rect = new Rectangle(curr.active.X + curr.anchor.X, curr.active.Y + curr.anchor.Y,
                    curr.active.Width, curr.active.Height);
                active = curr.active;

                //update
                updateFrame = true;
                frameIndex = boxIndex;
                currPreview = frameIndex;

                upDownDuration.Value = (decimal)frameList[currPreview].duration;

                if (anchorPoint.Checked)
                {
                    upDownX.Value = anchor.X;
                    upDownY.Value = anchor.Y;
                }
                else if (renderRect.Checked)
                {
                    upDownX.Value = render.X;
                    upDownY.Value = render.Y;
                    upDownWidth.Value = render.Width;
                    upDownHeight.Value = render.Height;
                }
                else if (collisionRect.Checked)
                {
                    upDownX.Value = collision.X;
                    upDownY.Value = collision.Y;
                    upDownWidth.Value = collision.Width;
                    upDownHeight.Value = collision.Height;
                }
                else if (activeRect.Checked)
                {
                    upDownX.Value = active.X;
                    upDownY.Value = active.Y;
                    upDownWidth.Value = active.Width;
                    upDownHeight.Value = active.Height;
                }
                else
                {
                    upDownX.Value = 0;
                    upDownY.Value = 0;
                    upDownWidth.Value = 0;
                    upDownHeight.Value = 0;

                }
                //event text
                //eventText.Text = "";
            }

        }
        //udpate frame
        private void UpdateCurrentFrame()
        {
            //current frame
            Frame frame = frameList[frameIndex];

            //set the current sizes

            //anchor
            frame.anchor = anchor;
            frame.spawn = spawn;
            //event
            frame.eventText = eventText.Text;


            //update offsets
            frame.renderOffset.X = render.X - anchor.X;
            frame.renderOffset.Y = render.Y - anchor.Y;

            //render rect
            frame.render = render;


            //colllision rectangle - local to the anchor point
            if (collision.X != -1)
            {
                //update offsets
                frame.collisionOffset.X = collision.X - anchor.X;
                frame.collisionOffset.Y = collision.Y - anchor.Y;
                //collision rect
                frame.collision = collision;
            }


            //active rectangle-- local to the anchor point
            if (active.X != -1)
            {
                //update offsets
                frame.activeOffset.X = active.X - anchor.X;
                frame.activeOffset.Y = active.Y - anchor.Y;
                //active rectangle
                frame.active = active;
            }

            //duration
            frame.duration = (float)upDownDuration.Value;

            if (updateAnimation)
                UpdateCurrentAnimation();

        }
        //Update animation
        private void UpdateCurrentAnimation()
        {
            //index check
            if (animIndex == -1 || animIndex == listBox2.Items.Count)
                return;


            //add a list of animations to the list
            Animation anim = new Animation();


            //set the list
            anim.frameList = new List<Frame>();
            for (int i = 0; i < frameList.Count; ++i)
                anim.frameList.Add(frameList[i]);

            //set the name
            anim.name = animationName.Text;
            //loop
            anim.loop = loopCheck.Checked;

            //add into the list
            animList[animIndex] = anim;

            //reset all names
            listBox2.Items.RemoveAt(animIndex);
            listBox2.Items.Insert(animIndex, anim.name);
            listBox2.SelectedIndex = animIndex;
            //frameOn = false;
        }
        //reset
        private void Reset()
        {
            //set all to -1
            anchor.X = anchor.Y = -1;
            spawn.X = spawn.Y = -1;
            render = new Rectangle(-1, -1, 0, 0);
            collision = new Rectangle(-1, -1, 0, 0);
            active = new Rectangle(-1, -1, 0, 0);

            //reset up down values
            upDownX.Value = 0;
            upDownY.Value = 0;
            upDownWidth.Value = 0;
            upDownHeight.Value = 0;

            upDownDuration.Value = (decimal)0.1;
            eventText.Text = "";
        }
        //remove frame
        private void frameRemove_Click(object sender, EventArgs e)
        {
            //selected index
            int fIndex = listBox1.SelectedIndex;

            if (fIndex == -1)
                return;
            //stop updating info
            updateFrame = false;

            //object at index
            Frame curr = frameList[fIndex];

            //remove from the list box and the list
            frameList.Remove(curr);

            //remove
            int remove = fIndex + 1;
            string tag = "Frame ";
            tag += remove.ToString();
            listBox1.Items.Remove(tag);

            //update the label for all
            for (int i = fIndex; i < listBox1.Items.Count; ++i)
            {
                //set the label
                string label;
                int ind = i + 1;
                label = "Frame ";
                label += ind.ToString();

                listBox1.Items[i] = label;
            }


            //reset
            Reset();
            //reset the selected index
            listBox1.ClearSelected();
            //update the frame index
            frameIndex = -1;
            //reset previewed frame
            currPreview = 0;
        }
        //deselect frame
        private void deselectFrame_Click(object sender, EventArgs e)
        {

            //clear listbox
            listBox1.ClearSelected();
            //new index
            frameIndex = listBox1.Items.Count;
            currPreview = 0;

            //display current frame
            int txt = frameIndex + 1;
            currentFrame.Text = txt.ToString();

            //no update
            updateFrame = false;
            frameOn = false;

            //reset
            Reset();
        }
        //move frame up
        private void moveFrameUp_Click(object sender, EventArgs e)
        {
            //move a frame up unless its the top

            //current frame to move
            int fIndex = listBox1.SelectedIndex;

            //safety
            if (fIndex == -1 || fIndex == 0)
                return;

            //swap the current index and the one above
            Frame tFrame = frameList[fIndex];
            frameList[fIndex] = frameList[fIndex - 1];
            frameList[fIndex - 1] = tFrame;

            //update index
            --frameIndex;
            listBox1.SelectedIndex = frameIndex;

        }
        //move frame down
        private void moveFrameDown_Click(object sender, EventArgs e)
        {
            //move frame down unless its at the bottom
            //current frame to move
            int fIndex = listBox1.SelectedIndex;

            //safety
            if (fIndex == -1 || fIndex == listBox1.Items.Count - 1)
                return;

            //swap the current index and the one above
            Frame tFrame = frameList[fIndex];
            frameList[fIndex] = frameList[fIndex + 1];
            frameList[fIndex + 1] = tFrame;

            //update index
            ++frameIndex;
            listBox1.SelectedIndex = frameIndex;
        }
        //add animation
        private void addAnimation_Click(object sender, EventArgs e)
        {
            //no updating
            updateFrame = false;

            //at least one frame
            if (frameList.Count == 0)
                return;

            //do not allow unless it is named
            if (animationName.Text == "")
                return;


            //must have unique name
            for (int i = 0; i < animList.Count; ++i)
            {
                if (animList[i].name == animationName.Text)
                    return;
            }


            //add a list of animations to the list
            Animation anim = new Animation();

            //set the list
            anim.frameList = new List<Frame>();
            for (int i = 0; i < frameList.Count; ++i)
                anim.frameList.Add(frameList[i]);
            //set the name
            anim.name = animationName.Text;
            //loop
            anim.loop = loopCheck.Checked;

            //add into the list
            animList.Add(anim);

            //add into the list box
            listBox2.Items.Add(anim.name);

            //clear the frame list
            frameList.Clear();
            listBox1.Items.Clear();

            //reset index
            frameIndex = 0;

            //reset
            Reset();
            //reset text
            animationName.Text = "";

        }
        //select animation
        private void listBox2_SelectedIndexChanged(object sender, EventArgs e)
        {

            //selected index
            int sIndex = listBox2.SelectedIndex;
            playPreview = false;

            //safety check
            if (sIndex == -1 || imageID == -1 || sIndex == animIndex)
                return;

            frameOn = true;

            //save the current 
            if (updateAnimation)
                UpdateCurrentAnimation();

            Reset();

            //update any chages to the animation
            updateAnimation = true;

            //update index
            animIndex = sIndex;
            listBox2.SelectedIndex = animIndex;

            //clear the current frame list
            frameList.Clear();
            listBox1.Items.Clear();

            //current list of frames
            Animation curr = animList[sIndex];
            List<Frame> fList = curr.frameList;
            //fill the current list with the animations
            for (int i = 0; i < fList.Count; ++i)
            {
                //add frame
                frameList.Add(fList[i]);

                //add name
                string title = "Frame ";
                int index = i + 1;
                title += index.ToString();
                listBox1.Items.Add(title);

            }

            //set the name
            animationName.Text = curr.name;
            //loop
            loopCheck.Checked = curr.loop;

            //re-render
            frameIndex = 0;
            currPreview = 0;
            //render = frameList[frameIndex].render;
            listBox1.ClearSelected();
            updateFrame = false;
            //Reset();

        }
        //deselect animation
        private void deselectAnim_Click(object sender, EventArgs e)
        {
            UpdateCurrentAnimation();

            //no update
            updateFrame = false;
            updateAnimation = false;
            frameOn = false;

            //clear listbox
            listBox2.ClearSelected();
            listBox1.ClearSelected();

            //clear the frame list
            frameList.Clear();
            listBox1.Items.Clear();

            ////reset index
            frameIndex = -1;
            animIndex = listBox2.Items.Count;

            //reset
            Reset();
            //reset text
            animationName.Text = "";
            loopCheck.Checked = false;

            playPreview = false;
            currPreview = 0;

        }
        //remove
        private void removeAnimation_Click(object sender, EventArgs e)
        {
            //selected index
            int fIndex = listBox2.SelectedIndex;
            if (fIndex == -1)
                return;
            //object at index
            Animation curr = animList[fIndex];

            //remove from the list box and the list
            animList.Remove(curr);
            //remove
            listBox2.Items.Remove(curr.name);


            //reset
            listBox1.Items.Clear();
            Reset();

            //update selected frame
            frameIndex = listBox1.Items.Count;
            //no updating
            updateFrame = false;
            updateAnimation = false;

        }
        //unused
        private void insertFrame_Click(object sender, EventArgs e)
        {
            //insert the current frame into the list and list box

            //anchor and render at least
            if (render.X == -1 || anchor.X == -1)
                return;

            //last index
            frameIndex = listBox1.SelectedIndex;

            //create a frame instance and add it into the list
            Frame frame = new Frame();

            //fill out frame information -point
            frame.anchor = anchor;
            frame.spawn = spawn;
            //rectangle info needed
            Point pt = new Point(0, 0);
            Size sz = new Size(0, 0);
            Rectangle rect = new Rectangle(0, 0, 0, 0);

            //render rectangle - local to the anchor
            frame.renderOffset.X = render.X - anchor.X; // -render.X;
            frame.renderOffset.Y = render.Y - anchor.Y; // -render.Y;
            //render rect
            frame.render = render;

            //colllision rectangle - local to the anchor point
            if (collision.X != -1)
            {
                //offset
                frame.collisionOffset.X = collision.X - anchor.X;
                frame.collisionOffset.Y = collision.Y - anchor.Y;
                //collision rect
                frame.collision = collision;
            }


            //active rectangle-- local to the anchor point
            if (active.X != -1)
            {
                //offsets
                frame.activeOffset.X = active.X - anchor.X;
                frame.activeOffset.Y = active.Y - anchor.Y;
                //active rect
                frame.active = rect;
            }


            //insert
            frameList.Insert(frameIndex, frame);
            //shift
            for (int i = frameIndex; i < listBox1.Items.Count; ++i)
            {
                //set the label
                string label;
                label = "Frame ";
                label += i.ToString();

                //add into the list / listBox
                listBox1.Items[i] = label;

            }
            //reset
            Reset();
        }
        //save xml
        private void saveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //save the form with all the animations

            //create a save file dialog
            SaveFileDialog dlg = new SaveFileDialog();

            //filters for xml 
            dlg.Filter = "All Files|*.*|Xml Files|*.xml";
            dlg.FilterIndex = 2;
            dlg.DefaultExt = ".xml";


            //pressed ok?
            if (DialogResult.OK == dlg.ShowDialog())
            {
                //create a root element using XElement Class
                XElement root = new XElement("list");

                //image used
                XAttribute fName = new XAttribute("image", imageFileName);
                //XAttribute fName = new XAttribute("image", imageFileName);
                root.Add(fName);

                //backwards sheet
                XAttribute sheetBack;
                int sheet;
                //set loop value  -- true(1) false(0)
                if (sheetBox.Checked)
                    sheet = 1;
                else sheet = 0;
                //create attribute
                sheetBack = new XAttribute("sheet", sheet);
                //add attribute
                root.Add(sheetBack);


                foreach (Animation curr in animList)
                {
                    //animations : frames, loop

                    //animation
                    XElement xAnim = new XElement("animation");

                    //loop
                    XAttribute bLoop;
                    int loop;
                    //set loop value  -- true(1) false(0)
                    if (curr.loop)
                        loop = 1;
                    else loop = 0;
                    //create attribute
                    bLoop = new XAttribute("loop", loop);
                    //add attribute
                    xAnim.Add(bLoop);

                    //name

                    XAttribute name = new XAttribute("name", curr.name);
                    xAnim.Add(name);

                    //frames
                    foreach (Frame frame in curr.frameList)
                    {
                        //element for frames
                        XElement xFrame = new XElement("frame");

                        XAttribute xFrameDuration = new XAttribute("duration", frame.duration);
                        xFrame.Add(xFrameDuration);

                        //frame : anchor, render, collision, active
                        XElement anchor = new XElement("anchor");
                        XAttribute anchorPosX = new XAttribute("x", frame.anchor.X);
                        XAttribute anchorPosY = new XAttribute("y", frame.anchor.Y);
                        //attach position
                        anchor.Add(anchorPosX);
                        anchor.Add(anchorPosY);
                        //attach to frame
                        xFrame.Add(anchor);

                        //spawning point
                        XElement xSpawn = new XElement("spawn");

                        int spawnX = frame.spawn.X - frame.anchor.X;
                        int spawnY = frame.spawn.Y - frame.anchor.Y;

                        if (spawnX == -frame.anchor.X)
                            spawnX = 0;
                        if (spawnY == -frame.anchor.Y)
                            spawnY = 0;

                        XAttribute spawnPosX = new XAttribute("x", spawnX); ;
                        XAttribute spawnPosY = new XAttribute("y", spawnY); ;


                        //attach the position
                        xSpawn.Add(spawnPosX);
                        xSpawn.Add(spawnPosY);
                        //attach to frame
                        xFrame.Add(xSpawn);


                        XElement xRender = new XElement("render");
                        XAttribute offsetX = new XAttribute("offsetX", frame.renderOffset.X);
                        XAttribute offsetY = new XAttribute("offsetY", frame.renderOffset.Y);
                        XAttribute renderPosX = new XAttribute("x", frame.render.X);
                        XAttribute renderPosY = new XAttribute("y", frame.render.Y);
                        XAttribute renderWidth = new XAttribute("width", frame.render.Width);
                        XAttribute renderHeight = new XAttribute("height", frame.render.Height);
                        //attach position and size
                        xRender.Add(offsetX);
                        xRender.Add(offsetY);
                        xRender.Add(renderPosX);
                        xRender.Add(renderPosY);
                        xRender.Add(renderWidth);
                        xRender.Add(renderHeight);
                        //attach to frame
                        xFrame.Add(xRender);

                        //collision
                        XElement xCollision = new XElement("collision");
                        offsetX = new XAttribute("offsetX", frame.collisionOffset.X);
                        offsetY = new XAttribute("offsetY", frame.collisionOffset.Y);
                        XAttribute collisionPosX = new XAttribute("x", frame.collision.X);
                        XAttribute collisionPosY = new XAttribute("y", frame.collision.Y);
                        XAttribute collisionWidth = new XAttribute("width", frame.collision.Width);
                        XAttribute collisionHeight = new XAttribute("height", frame.collision.Height);
                        //attach position and size
                        xCollision.Add(offsetX);
                        xCollision.Add(offsetY);
                        xCollision.Add(collisionPosX);
                        xCollision.Add(collisionPosY);
                        xCollision.Add(collisionWidth);
                        xCollision.Add(collisionHeight);
                        //attach to frame
                        xFrame.Add(xCollision);

                        //active
                        XElement xActive = new XElement("active");
                        offsetX = new XAttribute("offsetX", frame.activeOffset.X);
                        offsetY = new XAttribute("offsetY", frame.activeOffset.Y);
                        XAttribute activePosX = new XAttribute("x", frame.active.X);
                        XAttribute activePosY = new XAttribute("y", frame.active.Y);
                        XAttribute activeWidth = new XAttribute("width", frame.active.Width);
                        XAttribute activeHeight = new XAttribute("height", frame.active.Height);
                        //attach position and size
                        xActive.Add(offsetX);
                        xActive.Add(offsetY);
                        xActive.Add(activePosX);
                        xActive.Add(activePosY);
                        xActive.Add(activeWidth);
                        xActive.Add(activeHeight);
                        //attach to frame
                        xFrame.Add(xActive);


                        //XElement xEvent = new XElement("event"); //
                        XAttribute xEventText = new XAttribute("event", frame.eventText);
                        //xEvent.Add(xEventText);
                        //add to frame
                        xFrame.Add(xEventText);

                        //attach frame to animation
                        xAnim.Add(xFrame);
                    }

                    //add the element into the list
                    root.Add(xAnim);
                }

                //save file
                root.Save(dlg.FileName);

                if (!imageSaved)
                {
                    //save into the resources folder
                    string initPath = fileFolderName;
                    initPath += "/";
                    initPath += imageFileName;
                    spriteSheet.Save(initPath);
                }

            }
        }
        //load xml file
        private void importXMLFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //open dialog box
            OpenFileDialog dlg = new OpenFileDialog();

            //specify filters
            dlg.Filter = "All Files|*.*|Xml Files|*xml";
            dlg.FilterIndex = 2;

            if (DialogResult.OK == dlg.ShowDialog())
            {
                //clear ALL containers
                frameList.Clear();
                animList.Clear();
                listBox1.Items.Clear();
                listBox2.Items.Clear();

                //root
                XElement root = XElement.Load(dlg.FileName);


                //load in the image used for it
                XAttribute xImage = root.Attribute("image");
                imageFileName = Convert.ToString(xImage.Value);

                imageSaved = true;


                //loop attribute
                XAttribute xSheet = root.Attribute("sheet");
                if (xSheet != null)
                {
                    int sheetBool = Convert.ToInt32(xSheet.Value);
                    sheetBox.Checked = (sheetBool == 1);
                }

                //container of elements for animations
                IEnumerable<XElement> xAnim = root.Elements();

                foreach (XElement elementAnim in xAnim)
                {
                    //new animation
                    Animation temp = new Animation();
                    temp.frameList = new List<Frame>();

                    //name attribute
                    XAttribute xName = elementAnim.Attribute("name");
                    temp.name = Convert.ToString(xName.Value);

                    //loop attribute
                    XAttribute xLoop = elementAnim.Attribute("loop");
                    int loopC = Convert.ToInt32(xLoop.Value);
                    if (loopC == 1) temp.loop = true;
                    else temp.loop = false;

                    //frames
                    IEnumerable<XElement> xFrames = elementAnim.Elements();

                    int counter = 1;

                    //for each frame
                    foreach (XElement elementFrame in xFrames)
                    {
                        //new frame
                        Frame tFrame = new Frame();
                        tFrame.render = new Rectangle();
                        tFrame.collision = new Rectangle();
                        tFrame.active = new Rectangle();

                        //attributes - x,y,w,h]
                        XAttribute xDuration = elementFrame.Attribute("duration");
                        tFrame.duration = (float)Convert.ToDouble(xDuration.Value);

                        //frame element
                        XElement xAnchor = elementFrame.Element("anchor");
                        //x
                        XAttribute info = xAnchor.Attribute("x");
                        tFrame.anchor.X = Convert.ToInt32(info.Value);
                        //y
                        info = xAnchor.Attribute("y");
                        tFrame.anchor.Y = Convert.ToInt32(info.Value);

                        //spawn point
                        XElement xSpawn = elementFrame.Element("spawn");
                        if (xSpawn != null)
                        {
                            info = xSpawn.Attribute("x");
                            tFrame.spawn.X = Convert.ToInt32(info.Value);
                            info = xSpawn.Attribute("y");
                            tFrame.spawn.Y = Convert.ToInt32(info.Value);

                            if (tFrame.spawn.X != 0)
                                tFrame.spawn.X += tFrame.anchor.X;
                            if (tFrame.spawn.Y != 0)
                                tFrame.spawn.Y += tFrame.anchor.Y;
                        }

                        //render
                        XElement xRender = elementFrame.Element("render");
                        //offest x
                        info = xRender.Attribute("offsetX");
                        tFrame.renderOffset.X = Convert.ToInt32(info.Value);
                        //offset y
                        info = xRender.Attribute("offsetY");
                        tFrame.renderOffset.Y = Convert.ToInt32(info.Value);
                        //x
                        info = xRender.Attribute("x");
                        tFrame.render.X = Convert.ToInt32(info.Value);
                        //y
                        info = xRender.Attribute("y");
                        tFrame.render.Y = Convert.ToInt32(info.Value);
                        //width
                        info = xRender.Attribute("width");
                        tFrame.render.Width = Convert.ToInt32(info.Value);
                        //height
                        info = xRender.Attribute("height");
                        tFrame.render.Height = Convert.ToInt32(info.Value);


                        //collision
                        XElement xCollision = elementFrame.Element("collision");
                        //offest x
                        info = xCollision.Attribute("offsetX");
                        tFrame.collisionOffset.X = Convert.ToInt32(info.Value);
                        //offset y
                        info = xCollision.Attribute("offsetY");
                        tFrame.collisionOffset.Y = Convert.ToInt32(info.Value);
                        //x
                        info = xCollision.Attribute("x");
                        tFrame.collision.X = Convert.ToInt32(info.Value);
                        //y
                        info = xCollision.Attribute("y");
                        tFrame.collision.Y = Convert.ToInt32(info.Value);
                        //width
                        info = xCollision.Attribute("width");
                        tFrame.collision.Width = Convert.ToInt32(info.Value);
                        //height
                        info = xCollision.Attribute("height");
                        tFrame.collision.Height = Convert.ToInt32(info.Value);

                        //active
                        XElement xActive = elementFrame.Element("active");
                        //offest x
                        info = xActive.Attribute("offsetX");
                        tFrame.activeOffset.X = Convert.ToInt32(info.Value);
                        //offset y
                        info = xActive.Attribute("offsetY");
                        tFrame.activeOffset.Y = Convert.ToInt32(info.Value);
                        //x
                        info = xActive.Attribute("x");
                        tFrame.active.X = Convert.ToInt32(info.Value);
                        //y
                        info = xActive.Attribute("y");
                        tFrame.active.Y = Convert.ToInt32(info.Value);
                        //width
                        info = xActive.Attribute("width");
                        tFrame.active.Width = Convert.ToInt32(info.Value);
                        //height
                        info = xActive.Attribute("height");
                        tFrame.active.Height = Convert.ToInt32(info.Value);


                        //event
                        info = elementFrame.Attribute("event");
                        string currEvent = Convert.ToString(info.Value);

                        //info = 
                        tFrame.eventText = currEvent;
                        //add into the list
                        temp.frameList.Add(tFrame);

                        //list string
                        string txt = "Frame ";
                        txt += counter.ToString();
                        //add to listbox
                        listBox1.Items.Add(txt);
                        ++counter;
                    }

                    //add the animation to the list
                    animList.Add(temp);
                    listBox2.Items.Add(temp.name);

                    //clear the frame list
                    listBox1.Items.Clear();
                }

                updateFrame = false;
                animIndex = -1;
                frameIndex = -1;
                //invalidate the panel
                Reset();

                //load the image
                string initPath = fileFolderName;
                initPath += "\\";
                initPath += imageFileName;

                spriteSheet = new Bitmap(initPath);
                imageID = tManager.LoadTexture(initPath);

                Size size = new Size(tManager.GetTextureWidth(imageID), tManager.GetTextureHeight(imageID));
                panel1.AutoScrollMinSize = spriteSheet.Size;
                D3D.Resize(panel1, panel1.ClientRectangle.Width, panel1.ClientRectangle.Height, true);

            }
        }
        //preview update
        private void UpdatePreview(float dt)
        {
            if (currPreview != -1 && playPreview && listBox1.Items.Count != 0)
            {
                //frame time
                float frameTime = frameList[currPreview].duration;
                //increase the timer
                runTime += dt;

                //is it time to move?
                if (runTime >= frameTime)
                {
                    runTime = 0.0f;
                    ++currPreview;

                    //reached the end?
                    if (currPreview == frameList.Count)
                    {

                        if (loopCheck.Checked) //break?
                            currPreview = 0;
                        else
                        {
                            //stop
                            --currPreview;
                            playPreview = false;

                        }
                    }
                }
            }
        }
        //closing form
        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            looping = false;
        }
        //Up-Down X
        private void upDownX_ValueChanged(object sender, EventArgs e)
        {

            if (anchorPoint.Checked)
                //update point
                anchor.X = (int)upDownX.Value;
            else if (renderRect.Checked)
                //update boxes
                render.X = (int)upDownX.Value;
            else if (collisionRect.Checked)
                //update boxes
                collision.X = (int)upDownX.Value;
            else if (activeRect.Checked)
                //update boxes
                active.X = (int)upDownX.Value;
            else if (spawnPoint.Checked)
                spawn.X = (int)upDownX.Value;

            //invalidate
            //graphicPanel1.Invalidate();
        }
        //Up-Down Y
        private void upDownY_ValueChanged(object sender, EventArgs e)
        {

            if (anchorPoint.Checked)
                //update point
                anchor.Y = (int)upDownY.Value;
            else if (renderRect.Checked)
                //update boxes
                render.Y = (int)upDownY.Value;
            else if (collisionRect.Checked)
                //update boxes
                collision.Y = (int)upDownY.Value;
            else if (activeRect.Checked)
                //update boxes
                active.Y = (int)upDownY.Value;
            else if (spawnPoint.Checked)
                spawn.Y = (int)upDownY.Value;

        }
        //Up-Down Width
        private void upDownWidth_ValueChanged(object sender, EventArgs e)
        {

            if (renderRect.Checked)
                //update boxes
                render.Width = (int)upDownWidth.Value;
            else if (collisionRect.Checked)
                //update boxes
                collision.Width = (int)upDownWidth.Value;
            else if (activeRect.Checked)
                //update boxes
                active.Width = (int)upDownWidth.Value;

            //invalidate
            //graphicPanel1.Invalidate();

        }
        //Up-Down Heigh
        private void upDownHeight_ValueChanged(object sender, EventArgs e)
        {
            if (renderRect.Checked)
                //update boxes
                render.Height = (int)upDownHeight.Value;
            else if (collisionRect.Checked)
                //update boxes
                collision.Height = (int)upDownHeight.Value;
            else if (activeRect.Checked)
                //update boxes
                active.Height = (int)upDownHeight.Value;

            //invalidate
            //graphicPanel1.Invalidate();

        }
        //New
        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {
            listBox1.Items.Clear();
            listBox2.Items.Clear();
            animList.Clear();
            frameList.Clear();
            //anchorPoint.Checked = false;

            tManager.UnloadTexture(imageID);
            imageID = -1;

            //null bitmap
            spriteSheet = null;
            imageSaved = false;
            sheetBox.Checked = false;

            Reset();
            frameIndex = 0;
            currPreview = 0;
            animIndex = -1;
            runTime = 0.0f;
            updatePreview = false;
            playPreview = false;
            frameOn = false;


            //render anchor in the center of the screen
            previewAnchor.X = panel3.Size.Width / 2;
            previewAnchor.Y = panel3.Size.Height / 2;

            //tool tips
            //CreateToolTips();
            int index = frameIndex + 1;
            currentFrame.Text = index.ToString();

            //combo box
            //eventBox.DataSource = Enum.GetValues(typeof(EVENT_TYPE));

        }
        //Up-Down duration
        private void upDownDuration_ValueChanged(object sender, EventArgs e)
        {
            if (currPreview == -1 || listBox1.Items.Count == 0)
                return;

            frameList[currPreview].duration = (float)upDownDuration.Value;
        }
        //play
        private void playButton_Click(object sender, EventArgs e)
        {
            if (currPreview != -1 && listBox1.Items.Count != 0)
            {
                //update
                playPreview = true;
                //index
                currPreview = 0;
            }
        }
        //pause
        private void StopButton_Click(object sender, EventArgs e)
        {
            playPreview = false;
        }
        //update
        public new void Update()
        {
            //display current frame
            int index = frameIndex + 1;
            currentFrame.Text = index.ToString();            //local date time for current time
            DateTime now = new DateTime();
            now = DateTime.Now;

            //delta time
            TimeSpan deltaT = new TimeSpan();
            deltaT = now - tick;
            float dt = (float)deltaT.TotalSeconds;
            tick = now;
            //update the preview
            UpdatePreview(dt);
        }
        //render
        public void Render()
        {
            Panel1Render();
            Panel2Render();
            Panel3Render();
        }
        //panel 1
        private void Panel1Render()
        {

            D3D.Clear(panel1, Color.White);

            if (imageID != -1)
            {
                // return;
                //clear
                D3D.DeviceBegin();
                D3D.SpriteBegin();


                //display the current sprite sheet iff one exists
                Point point = new Point(0, 0);

                //adjust for bitmap
                point.X += panel1.AutoScrollPosition.X;
                point.Y += panel1.AutoScrollPosition.Y;

                //draw the sheet
                tManager.Draw(imageID, point.X, point.Y);

                //render the updating rect
                if (holding && anchorPoint.Checked == false)
                {
                    if (mouseCurr.X < 0)
                        mouseCurr.X = 0;
                    if (mouseCurr.X > panel1.Size.Width - point.X)
                        mouseCurr.X = panel1.Size.Width - point.X;
                    if (mouseCurr.Y < 0)
                        mouseCurr.Y = 0;
                    if (mouseCurr.Y > panel1.Size.Height - point.Y)
                        mouseCurr.Y = panel1.Size.Height - point.Y;

                    //size
                    int _width = (mouseDown.X > mouseCurr.X) ? mouseDown.X - mouseCurr.X : mouseCurr.X - mouseDown.X;
                    int _height = (mouseDown.Y > mouseCurr.Y) ? mouseDown.Y - mouseCurr.Y : mouseCurr.Y - mouseDown.Y;
                    //topleft
                    Point p = new Point();
                    p.X = (mouseDown.X < mouseCurr.X) ? mouseDown.X : mouseCurr.X;
                    p.Y = (mouseDown.Y < mouseCurr.Y) ? mouseDown.Y : mouseCurr.Y;
                    p.X += point.X;
                    p.Y += point.Y;
                    //rectangle
                    Rectangle draw = new Rectangle(p.X, p.Y, _width, _height);
                    //draw
                    D3D.DrawHollowRect(draw, Color.Orange, 1);
                }

                //anchor point
                Point pt = new Point(anchor.X - 2, anchor.Y - 2);
                pt.X += point.X;
                pt.Y += point.Y;
                Rectangle rect = new Rectangle(pt, new Size(5, 5));
                D3D.DrawRect(rect, Color.Red);

                //spawn point
                pt.X = (spawn.X - 2) + point.X;
                pt.Y = (spawn.Y - 2) + point.Y;
                rect = new Rectangle(pt, new Size(5, 5));
                D3D.DrawRect(rect, Color.Black);

                //render rect
                Rectangle r = new Rectangle(render.X + point.X, render.Y + point.Y, render.Width, render.Height);
                //render the render rect on the image
                D3D.DrawHollowRect(r, Color.Purple, 1);

                //collision rect
                r = new Rectangle(collision.X + point.X, collision.Y + point.Y, collision.Width, collision.Height);
                //render
                D3D.DrawHollowRect(r, Color.Blue, 1);

                //collision rect
                r = new Rectangle(active.X + point.X, active.Y + point.Y, active.Width, active.Height);
                //render
                D3D.DrawHollowRect(r, Color.Red, 1);



                //end
                D3D.SpriteEnd();
                D3D.DeviceEnd();
            }
            D3D.Present();

        }
        //panel 1 mouse down
        private void panel1_MouseDown(object sender, MouseEventArgs e)
        {

            //update input
            holding = true;

            //set the mouse while down
            mouseDown = e.Location;
            mouseDown.X -= panel1.AutoScrollPosition.X;
            mouseDown.Y -= panel1.AutoScrollPosition.Y;

            upDownX.Value = 0;
            upDownY.Value = 0;
            upDownWidth.Value = 0;
            upDownHeight.Value = 0;


            //check the type
            if (anchorPoint.Checked == true)
            {
                //anchor at mouse position
                anchor = mouseDown;
                //anchor
                upDownX.Value = anchor.X;
                upDownY.Value = anchor.Y;

                if (updateFrame)
                    UpdateCurrentFrame();

            }
            else if (spawnPoint.Checked == true)
            {
                //spawn point at the moiuse
                spawn = mouseDown;
                //spawn
                upDownX.Value = spawn.X;
                upDownY.Value = spawn.Y;

                if (updateFrame)
                    UpdateCurrentFrame();
            }
            else if (renderRect.Checked)
            {
                //update boxes
                upDownX.Value = mouseDown.X;
                upDownY.Value = mouseDown.Y;
            }
            else if (collisionRect.Checked)
            {
                //update boxes
                upDownX.Value = mouseDown.X;
                upDownY.Value = mouseDown.Y;
            }
            else if (activeRect.Checked)
            {
                //update boxes
                upDownX.Value = mouseDown.X;
                upDownY.Value = mouseDown.Y;
            }

        }
        //panel 1 mouse move
        private void panel1_MouseMove(object sender, MouseEventArgs e)
        {
            //set the curr location
            mouseCurr = e.Location;
            mouseCurr.X -= panel1.AutoScrollPosition.X;
            mouseCurr.Y -= panel1.AutoScrollPosition.Y;

            //anchor
            //check the type
            if (anchorPoint.Checked == true && holding && anchor.X != -1 && anchor.Y != -1)
            {
                anchor = mouseCurr;
                upDownX.Value = anchor.X;
                upDownY.Value = anchor.Y;

                if (updateFrame)
                    UpdateCurrentFrame();
            }
            if (spawnPoint.Checked == true && holding && spawn.X != -1 && spawn.Y != -1)
            {
                spawn = mouseCurr;
                upDownX.Value = spawn.X;
                upDownY.Value = spawn.Y;

                if (updateFrame)
                    UpdateCurrentFrame();
            }

        }
        //panel 1 mouse up
        private void panel1_MouseUp(object sender, MouseEventArgs e)
        {
            //do not render while an image does not exist
            if (imageID == -1)
                return;

            //update input
            holding = false;
            frameOn = true;

            //set the mouse while down
            mouseUp = e.Location;
            mouseUp.X -= panel1.AutoScrollPosition.X;
            mouseUp.Y -= panel1.AutoScrollPosition.Y;

            if (mouseUp.X < 0)
                mouseUp.X = 0;
            if (mouseUp.X > panel1.Size.Width - panel1.AutoScrollPosition.X)
                mouseUp.X = panel1.Size.Width - panel1.AutoScrollPosition.X;
            if (mouseUp.Y < 0)
                mouseUp.Y = 0;
            if (mouseUp.Y > panel1.Size.Height - panel1.AutoScrollPosition.Y)
                mouseUp.Y = panel1.Size.Height - panel1.AutoScrollPosition.Y;

            //check correct dimension
            if (mouseDown.X > mouseUp.X)
            {
                int temp = mouseDown.X;
                mouseDown.X = mouseUp.X;
                mouseUp.X = temp;
            }

            if (mouseDown.Y > mouseUp.Y)
            {
                int temp = mouseDown.Y;
                mouseDown.Y = mouseUp.Y;
                mouseUp.Y = temp;
            }

            //calculate size
            int width = mouseUp.X - mouseDown.X;
            int height = mouseUp.Y - mouseDown.Y;

            //check the type
            if (renderRect.Checked == true)
            {
                //set the rectangle dimensions
                render = new Rectangle(mouseDown, new Size(width, height));

                //set the size
                //graphicPanel2.AutoScrollMinSize = render.Size;

                //update boxes
                upDownX.Value = render.X;
                upDownY.Value = render.Y;
                upDownWidth.Value = render.Width;
                upDownHeight.Value = render.Height;

            }
            else if (collisionRect.Checked == true)
            {
                collision = new Rectangle(mouseDown, new Size(width, height));

                //update boxes
                upDownX.Value = collision.X;
                upDownY.Value = collision.Y;
                upDownWidth.Value = collision.Width;
                upDownHeight.Value = collision.Height;

            }
            else if (activeRect.Checked == true)
            {
                active = new Rectangle(mouseDown, new Size(width, height));
                //update boxes
                upDownX.Value = active.X;
                upDownY.Value = active.Y;
                upDownWidth.Value = active.Width;
                upDownHeight.Value = active.Height;
            }


            if (updateFrame)
                UpdateCurrentFrame();


        }
        //panel 2
        private void Panel2Render()
        {

            //resize to fit the image
            int width = render.Width;
            int height = render.Height;
            //resize
            panel2.AutoScrollMinSize = new Size(width, height); 
            D3D.Resize(panel2, panel2.ClientRectangle.Width, panel2.ClientRectangle.Height, true);

            D3D.Clear(panel2, Color.White);

            if (frameOn && imageID != -1)
            { // return;

                //clear
                D3D.DeviceBegin();
                D3D.SpriteBegin();


                //display the current sprite sheet iff one exists
                Point point = new Point(0, 0);

                //adjust for bitmap
                point.X += panel2.AutoScrollPosition.X;
                point.Y += panel2.AutoScrollPosition.Y;

                //size of the image
                int screenWidth = panel2.Size.Width;
                int screenHeight = panel2.Size.Height;

                //render everything inside of the render rect

                //destination
                Point pos = new Point(0, 0);
                pos.X = screenWidth / 2;
                pos.Y = screenHeight / 2;

                //update based on image size
                pos.X -= render.Width / 2;
                pos.Y -= render.Height / 2;

                //update for scrolling
                pos.X += point.X;
                pos.Y += point.Y;

                //Destination Rectangle
                Rectangle dest = new Rectangle(pos, render.Size);

                //render image
                tManager.Draw(imageID, pos.X, pos.Y, 1.0f, 1.0f, render);



                //end
                D3D.SpriteEnd();
                D3D.DeviceEnd();
            }
            D3D.Present();
        }
        //panel 3
        private void Panel3Render()
        {
            //clear
            D3D.Clear(panel3, Color.White);
            D3D.DeviceBegin();
            D3D.SpriteBegin();

            //render the anchor point for adjusting
            Point renderAnc = new Point(0, 0);
            //render based off of anchor
            renderAnc.X = previewAnchor.X - 2;
            renderAnc.Y = previewAnchor.Y - 2;


            //rectangle 
            Rectangle rect = new Rectangle(renderAnc, new Size(5, 5));

            //render the current frame list based on the anchor point
            if (frameList.Count != 0)
            {
                //current frame
                Frame frame = frameList[currPreview];

                //source rectangle
                Point ptDest = new Point(frame.renderOffset.X + renderAnc.X, frame.renderOffset.Y + renderAnc.Y);
                //render the current frame\
                Rectangle dest = new Rectangle(ptDest, frame.render.Size);

                //image, destination rect, source, rect, GraphicsUnit.Pixel
                tManager.Draw(imageID, ptDest.X, ptDest.Y, 1.0f, 1.0f, frame.render);

                //collision
                if (collisionBox.Checked && frame.collision.Width != 0)
                {
                    //local rect to draw
                    Point ptCollision = new Point();
                    ptCollision.X = frame.collisionOffset.X + renderAnc.X;
                    ptCollision.Y = frame.collisionOffset.Y + renderAnc.Y;

                    Rectangle rtCollision = new Rectangle(ptCollision, frame.collision.Size);

                    //draw the rect
                    D3D.DrawHollowRect(rtCollision, Color.Blue, 1);
                }


                //active
                if (activeBox.Checked && frame.active.Width != 0)
                {
                    //local rect to draw
                    Point ptActive = new Point();
                    ptActive.X = frame.activeOffset.X + previewAnchor.X;
                    ptActive.Y = frame.activeOffset.Y + previewAnchor.Y;

                    Rectangle rtActive = new Rectangle(ptActive, frame.active.Size);

                    //draw the rect
                    D3D.DrawHollowRect(rtActive, Color.Red, 1);
                }

                //spawn
                if (spawnBox.Checked == true)
                {
                    Point renderSpawn = new Point(frame.spawn.X - frame.anchor.X, frame.spawn.Y - frame.anchor.Y);
                    renderSpawn.X += previewAnchor.X;
                    renderSpawn.Y += previewAnchor.Y;
                    renderSpawn.X -= 2;
                    renderSpawn.Y -= 2;

                    //rectangle 
                    Rectangle r = new Rectangle(renderSpawn, new Size(5, 5));
                    //draw
                    D3D.DrawRect(r, Color.Black);
                }
            }

            //render the anchor point on top
            D3D.DrawRect(rect, Color.Blue);



            //end
            D3D.SpriteEnd();
            D3D.DeviceEnd();
            D3D.Present();

        }

        private void anchorPoint_CheckedChanged(object sender, EventArgs e)
        {
            if (anchorPoint.Checked && anchor.X != -1)
            {
                upDownX.Value = anchor.X;
                upDownY.Value = anchor.Y;
            }
        }

        private void renderRect_CheckedChanged(object sender, EventArgs e)
        {
            if (renderRect.Checked && render.X != -1)
            {
                upDownX.Value = render.X;
                upDownY.Value = render.Y;
                upDownWidth.Value = render.Width;
                upDownHeight.Value = render.Height;
            }
        }

        private void collisionRect_CheckedChanged(object sender, EventArgs e)
        {
            if (collisionRect.Checked && collision.X != -1)
            {
                upDownX.Value = collision.X;
                upDownY.Value = collision.Y;
                upDownWidth.Value = collision.Width;
                upDownHeight.Value = collision.Height;
            }

        }

        private void activeRect_CheckedChanged(object sender, EventArgs e)
        {
            if (activeRect.Checked && active.X != -1)
            {
                upDownX.Value = active.X;
                upDownY.Value = active.Y;
                upDownWidth.Value = active.Width;
                upDownHeight.Value = active.Height;
            }
        }
        //stop - rset
        private void ResetPreviewButton_Click(object sender, EventArgs e)
        {
            playPreview = false;
            currPreview = 0;
        }
        //mouse down
        private void panel3_MouseDown(object sender, MouseEventArgs e)
        {
            //update input
            updatePreview = true;

            //set the mouse while down
            previewMouse = e.Location;
            previewAnchor = previewMouse;

        }

        private void panel3_MouseMove(object sender, MouseEventArgs e)
        {
            if (updatePreview)
            {
                //set the mouse while down
                previewMouse = e.Location;
                previewAnchor = previewMouse;
            }
        }

        private void panel3_MouseUp(object sender, MouseEventArgs e)
        {
            //update input
            updatePreview = false;
        }



        private void button1_Click(object sender, EventArgs e)
        {
            ++currPreview;
            if (currPreview == frameList.Count)
                currPreview = 0;
        }

        private void previousFrame_Click(object sender, EventArgs e)
        {
            --currPreview;
            if (currPreview == -1)
                currPreview = frameList.Count - 1;

        }

        private void spawnPoint_CheckedChanged(object sender, EventArgs e)
        {
            if (spawnPoint.Checked && spawn.X != -1)
            {
                upDownX.Value = spawn.X;
                upDownY.Value = spawn.Y;
            }
        }

    }
}
