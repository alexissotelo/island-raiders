﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.Xml.Linq;
using System.IO;

namespace LeverEditor
{
    public partial class Form1 : Form
    {
        //D3D
        SGP.CSGP_Direct3D D3D = SGP.CSGP_Direct3D.GetInstance();
        SGP.CSGP_TextureManager TM = SGP.CSGP_TextureManager.GetInstance();
        private bool looping = true;

        //CAMERA
        private const int CamWidth = 800;
        private const int CamHeight = 600;
        private float zoom = 0.2f;
        float relativeWidth;
        float relativeHeight;
        Size cameraSize;

        SectionEditor sectionTool;
        EntityEditor entityTool;
        public static Level level;
        public static int currentSection = 0;

        Enemy currentEnemy = null;
        public static List<Enemy> enemies;

        public static List<Group> groups;
        int currentGroup;

        //helper
        Point defaultOffset;
        Point cursorOffset;
        LevelSection selectedSection;

        public string resourcesPath;
        public Form1()
        {
            InitializeComponent();

            //D3D and TM initialize
            D3D.Initialize(splitContainer1.Panel2, true);
            TM.Initialize(D3D.Device, D3D.Sprite);
        }
        public void Initialize()
        {
            //camera
            cameraSize = new Size(CamWidth, CamHeight);
            defaultOffset = new Point(0, 245);
            ZoomChange();

            //load groups
            groups = new List<Group>();
            bool groupsResult = LoadGroups(resourcesPath + "XML\\groups.xml");

            //load enemies
            enemies = new List<Enemy>();
            bool enemiesResult = LoadEnemies(resourcesPath + "XML\\enemies.xml");

            //load level
            level = new Level("Level1");
            textBox1.Text = level.levelName;
            //if (groupsResult && enemiesResult)
            //LoadLevel("level1", resourcesPath + "XML\\level1.xml");

            sectionBox.Items.AddRange(level.levelSections.ToArray());
            sectionBox.SelectedIndex = 0;
            UpdateSection();
        }
        public bool Looping
        {
            get { return looping; }
            //set { looping = value; }
        }

        #region ENEMY
        //ENEMY CHANGE
        private void enemyListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (enemyListBox.SelectedIndex < 0) return;
            if (enemies[enemyListBox.SelectedIndex] == null) return;

            currentEnemy = enemies[enemyListBox.SelectedIndex];//(Enemy)enemyListBox.SelectedItem;
            if (currentEnemy != null) groupBox3.Enabled = true;


            //refresh Enemy Information GroupBox
            textBox3.Text = currentEnemy.name;
            nudDmg1.Value = currentEnemy.damage[0];
            nudHp1.Value  = currentEnemy.health[0];
            nudDmg2.Value = currentEnemy.damage[1];
            nudHp2.Value  = currentEnemy.health[1];
            nudDmg3.Value = currentEnemy.damage[2];
            nudHp3.Value  = currentEnemy.health[2];

            numericUpDown3.Value = currentEnemy.moveSpeed;
            numericUpDown4.Value = currentEnemy.droprate;

            numericUpDown6.Value = (decimal)currentEnemy.renderRect.Width;
            numericUpDown7.Value = (decimal)currentEnemy.renderRect.Height;

            nudTier.Value = currentEnemy.tier;
            comboBox3.SelectedIndex = currentEnemy.combatType;
            if (currentEnemy.combatType == (int)CombatType.Melee)
            {
                textBox2.Text = null;
                textBox2.Enabled = false;
            }
            textBox2.Text = currentEnemy.missileName;
            textBox5.Text = currentEnemy.skillName;
        }

        //ADD ENEMY
        private void button3_Click(object sender, EventArgs e)
        {
            enemies.Add(new Enemy());
            UpdateEnemyListBox();
        }
        //DELETE ENEMY
        private void button4_Click(object sender, EventArgs e)
        {
            if (enemyListBox.SelectedIndex > 0)
            {
                enemies.RemoveAt(enemyListBox.SelectedIndex);
                enemyListBox.Items.RemoveAt(enemyListBox.SelectedIndex);
            }
        }

        //enemy name
        private void textBox3_TextChanged(object sender, EventArgs e)
        {
            currentEnemy.name = textBox3.Text;
            UpdateEnemyListBox();
        }
        //missile name
        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            currentEnemy.missileName = textBox2.Text;
        }
        //skillname
        private void textBox5_TextChanged(object sender, EventArgs e)
        {
            currentEnemy.skillName = (textBox5.Text.Length > 0) ? textBox5.Text : null;
        }

        //combat type
        private void comboBox3_SelectedIndexChanged(object sender, EventArgs e)
        {
            currentEnemy.combatType = comboBox3.SelectedIndex;

            bool ranged = (CombatType)currentEnemy.combatType != CombatType.Melee;
            textBox2.Enabled = ranged;
            if (ranged &&
                currentEnemy.combatType != comboBox3.SelectedIndex)
            //needs to be different than last
            {
                textBox2.Text = currentEnemy.missileName = "";
            }
        }
        
        #region NUDs
        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {//enemy damage
            currentEnemy.damage[0] = (int)nudDmg1.Value;
        }

        private void nudDmg2_ValueChanged(object sender, EventArgs e)
        {
            currentEnemy.damage[1] = (int)nudDmg2.Value;

        }
        private void nudDmg3_ValueChanged(object sender, EventArgs e)
        {
            currentEnemy.damage[2] = (int)nudDmg3.Value;

        }
        private void numericUpDown2_ValueChanged(object sender, EventArgs e)
        {
            currentEnemy.health[0] = (int)nudHp1.Value;
        }
        private void nudHp2_ValueChanged(object sender, EventArgs e)
        {
            currentEnemy.health[1] = (int)nudHp2.Value;

        }
        private void nudHp3_ValueChanged(object sender, EventArgs e)
        {
            currentEnemy.health[2] = (int)nudHp3.Value;

        }
        //speed
        private void numericUpDown3_ValueChanged(object sender, EventArgs e)
        {
            currentEnemy.moveSpeed = (int)numericUpDown3.Value;
        }
        //drop rate
        private void numericUpDown4_ValueChanged(object sender, EventArgs e)
        {
            currentEnemy.droprate = (int)numericUpDown4.Value;
        }
        //width
        private void numericUpDown6_ValueChanged(object sender, EventArgs e)
        {
            currentEnemy.renderRect.Width = (int)numericUpDown6.Value;
        }
        //height
        private void numericUpDown7_ValueChanged(object sender, EventArgs e)
        {
            currentEnemy.renderRect.Height = (int)numericUpDown7.Value;
        }
        private void nudTier_ValueChanged(object sender, EventArgs e)
        {
            currentEnemy.tier = (int)nudTier.Value;
        }
        #endregion

        //UPDATE ENEMY LISTBOX
        private void UpdateEnemyListBox()
        {
            enemyListBox.Items.Clear();
            //enemyBox.Items.Clear();
            for (int i = 0; i < enemies.Count; i++)
            {
                string name = enemies[i].name;
                enemyListBox.Items.Add(name);
                //enemyBox.Items.Add(name);
            }
        }

        //SAVE ENEMIES
        private void SaveEnemies(object sender, EventArgs e)
        {
            SaveFileDialog dlg = new SaveFileDialog();
            dlg.Filter = "All Files|*.*|XML Files|*.xml";
            dlg.FilterIndex = 2;
            dlg.DefaultExt = "xml";

            if (DialogResult.OK == dlg.ShowDialog())
            {
                SaveEnemies(dlg.FileName);
            }
        }
        private void SaveEnemies(string filepath)
        {
            XElement xRoot = new XElement("Enemies");

            for (int i = 0; i < enemies.Count; i++)
            {
                XElement xEnemy = new XElement("Enemy");
                xRoot.Add(xEnemy);

                //atributes
                Enemy en = enemies[i];

                xEnemy.Add(new XAttribute("name", en.name));

                xEnemy.Add(new XAttribute("damage0", en.damage[0]));
                xEnemy.Add(new XAttribute("health0", en.health[0]));

                xEnemy.Add(new XAttribute("damage1", en.damage[1]));
                xEnemy.Add(new XAttribute("health1", en.health[1]));

                xEnemy.Add(new XAttribute("damage2", en.damage[2]));
                xEnemy.Add(new XAttribute("health2", en.health[2]));

                xEnemy.Add(new XAttribute("moveSpeed", en.moveSpeed));

                //size
                XElement xeSize = new XElement("size");
                xeSize.Add(new XAttribute("w", en.renderRect.Width));
                xeSize.Add(new XAttribute("h", en.renderRect.Height));
                xEnemy.Add(xeSize);

                //----------------
                XAttribute eCombat = new XAttribute("combatType", (int)en.combatType);
                xEnemy.Add(eCombat);

                if (en.missileName != null)
                {
                    xEnemy.Add(new XElement("missile", en.missileName));
                }
                if (en.skillName != null)
                {
                    xEnemy.Add(new XElement("skill", en.skillName));
                }

                xEnemy.Add(new XAttribute("AI", en.tier));
                xEnemy.Add(new XAttribute("DropRate", en.droprate));
            }

            xRoot.Save(filepath);
        }

        //LOAD ENEMIES
        private void LoadEnemies(object sender, EventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Filter = "All Files|*.*|XML Files|*.xml";
            dlg.FilterIndex = 2;

            if (DialogResult.OK == dlg.ShowDialog())
            {
                LoadEnemies(dlg.FileName);
            }
        }
        private bool LoadEnemies(string filename)
        {
            enemies.Clear();
            XElement xRoot = XElement.Load(filename);
            if (xRoot == null) return false;

            IEnumerable<XElement> xTiles = xRoot.Elements();
            foreach (XElement xEnemy in xTiles)
            {
                Enemy enemy = new Enemy();

                enemy.name = Convert.ToString(xEnemy.Attribute("name").Value);

                enemy.damage[0] = int.Parse(xEnemy.Attribute("damage0").Value);
                enemy.health[0] = int.Parse(xEnemy.Attribute("health0").Value);

                enemy.damage[1] = int.Parse(xEnemy.Attribute("damage1").Value);
                enemy.health[1] = int.Parse(xEnemy.Attribute("health1").Value);

                enemy.damage[2] = int.Parse(xEnemy.Attribute("damage2").Value);
                enemy.health[2] = int.Parse(xEnemy.Attribute("health2").Value);

                enemy.moveSpeed = int.Parse(xEnemy.Attribute("moveSpeed").Value);

                //enemy.size
                XElement xeSize = xEnemy.Element("size");
                int w, h;
                w = int.Parse(xeSize.Attribute("w").Value);
                h = int.Parse(xeSize.Attribute("h").Value);
                enemy.renderRect = new Rectangle(0, 0, w, h);

                enemy.tier = int.Parse(xEnemy.Attribute("AI").Value);
                enemy.combatType = int.Parse(xEnemy.Attribute("combatType").Value);
                //missile
                XElement xMissile = xEnemy.Element("missile");
                if (xMissile != null)
                {
                    string missile = xMissile.Value.ToString();
                    if (missile != null)
                        enemy.missileName = missile;
                }
                //missile
                XElement xSkill = xEnemy.Element("skill");
                if (xSkill != null)
                {
                    string skill = xSkill.Value.ToString();
                    if (skill != null)
                        enemy.skillName = skill;
                }

                enemy.droprate = int.Parse(xEnemy.Attribute("DropRate").Value);

                enemies.Add(enemy);
            }

            splitContainer1.Panel2.Invalidate();
            UpdateEnemyListBox();
            return true;
        }

        #endregion

        #region LEVEL
        //IMPORT LEVEL
        private void button2_Click(object sender, EventArgs e)
        {
            //textBox1.Text = level.levelName;
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Filter = "All Files|*.*|XML Files|*.xml";
            dlg.FilterIndex = 2;

            if (DialogResult.OK == dlg.ShowDialog())
            {
                LoadLevel(dlg.SafeFileName.Split('.')[0], dlg.FileName);
            }
        }
        private void LoadLevel(string filename, string filepath)
        {
            currentSection = 0;
            level = new Level(filename);
            level.levelSections.Clear();

            XElement xRoot = XElement.Load(filepath);
            XAttribute xBg = xRoot.Attribute("background");
            if (xBg != null)
            {
                level.backgroundImage = xBg.Value;
                TM.LoadTexture(resourcesPath + level.backgroundImage, level.backgroundImage);
            }
            IEnumerable<XElement> xSections = xRoot.Elements();
            foreach (XElement xSection in xSections)
            {
                LevelSection section = new LevelSection();

                section.position.X = int.Parse(xSection.Attribute("X").Value);
                section.position.Y = int.Parse(xSection.Attribute("Y").Value);

                XElement xEnemies = xSection.Element("Enemies");
                section.amountOfEnemies = int.Parse(xEnemies.Attribute("amountOfEnemies").Value);

                //add world enemies
                foreach (XElement xEnemy in xEnemies.Elements())
                {
                    Entity enemy = new Entity();
                    enemy.position.X = int.Parse(xEnemy.Attribute("X").Value);
                    enemy.position.Y = int.Parse(xEnemy.Attribute("Y").Value);

                    //search for enemy in enemy collection
                    //apply size and name base on enemy found
                    //to do: show message if not found
                    string nameInCollection = Convert.ToString(xEnemy.Attribute("name").Value);
                    for (int i = 0; i < enemies.Count; i++)
                    {
                        if (enemies[i].name == nameInCollection)
                        {
                            //founded enemy in collection
                            //take name and size values
                            enemy.name = enemies[i].name;
                            enemy.renderRect = enemies[i].renderRect;
                            break;
                        }
                    }
                    section.enemies.Add(enemy);
                }
                //layers
                section.layers.Clear();
                XElement xLayers = xSection.Element("Layers");
                foreach (XElement xLayer in xLayers.Elements())
                {
                    Layer layer = new Layer();
                    foreach (XElement xEntity in xLayer.Elements())
                    {
                        Entity entity = new Entity();
                        entity.position.X = int.Parse(xEntity.Attribute("X").Value);
                        entity.position.Y = int.Parse(xEntity.Attribute("Y").Value);
                        entity.scale.Width = float.Parse(xEntity.Attribute("scaleX").Value);
                        entity.scale.Height = float.Parse(xEntity.Attribute("scaleY").Value);

                        //search entity in groups
                        //todo: optimize by storing group index, 
                        //so dont have to loop through all groups
                        string nameInCollection = Convert.ToString(xEntity.Attribute("name").Value);
                        bool founded = false;
                        for (int i = 0; i < groups.Count; i++)
                        {
                            for (int j = 0; j < groups[i].entities.Count; j++)
                            {
                                if (groups[i].entities[j].name == nameInCollection)
                                {
                                    entity.imageIndex = groups[i].entities[j].imageIndex;
                                    entity.name = groups[i].entities[j].name;
                                    entity.renderRect = groups[i].entities[j].renderRect;
                                    founded = true;
                                    break;
                                }
                            }
                            if (founded) break;
                        }
                        layer.entities.Add(entity);
                    }
                    section.layers.Add(layer);
                }
                //COLLISIONS
                XElement xCollisions = xSection.Element("Collisions");
                foreach (XElement xColl in xCollisions.Elements())
                {
                    Point pos = new Point();
                    Size siz = new Size();
                    pos.X = int.Parse(xColl.Attribute("X").Value);
                    pos.Y = int.Parse(xColl.Attribute("Y").Value);
                    siz.Width = int.Parse(xColl.Attribute("Width").Value);
                    siz.Height = int.Parse(xColl.Attribute("Height").Value);
                    CollisionRect coll = new CollisionRect(new Rectangle(pos, siz));

                    XElement xEvent = xColl.Element("event");
                    if (xEvent != null)
                    {
                        coll.eventName = xEvent.Attribute("name").Value;
                    }
                    section.collisions.Add(coll);
                }

                level.levelSections.Add(section);
            }

            textBox1.Text = filename;
            UpdateSection();
        }
        //EXPORT LEVEL
        private void button6_Click(object sender, EventArgs e)
        {
            SaveLevel();
        }
        private void SaveLevel()
        {

            SaveFileDialog dlg = new SaveFileDialog();
            dlg.Filter = "All Files|*.*|XML Files|*.xml";
            dlg.FilterIndex = 2;
            dlg.DefaultExt = "xml";

            if (DialogResult.OK == dlg.ShowDialog())
            {
                XElement xRoot = new XElement("Level");
                if (level.backgroundImage != null)
                    xRoot.Add(new XAttribute("background", level.backgroundImage));

                for (int i = 0; i < level.levelSections.Count; i++)
                {
                    XElement xSection = new XElement("Section");
                    xRoot.Add(xSection);

                    //position
                    LevelSection section = level.levelSections[i];
                    xSection.Add(new XAttribute("X", section.position.X));
                    xSection.Add(new XAttribute("Y", section.position.Y));

                    //enemies
                    XElement xEnemies = new XElement("Enemies");
                    xEnemies.Add(new XAttribute("amountOfEnemies", section.amountOfEnemies));
                    for (int j = 0; j < section.enemies.Count; j++)
                    {
                        XElement xEnt = new XElement("enemy");
                        xEnt.Add(new XAttribute("name", section.enemies[j].name));

                        xEnt.Add(new XAttribute("X", section.enemies[j].position.X));
                        xEnt.Add(new XAttribute("Y", section.enemies[j].position.Y));

                        xEnemies.Add(xEnt);
                    }
                    xSection.Add(xEnemies);
                    //layers
                    XElement xLayers = new XElement("Layers");
                    for (int j = 0; j < section.layers.Count; j++)
                    {
                        XElement xLayer = new XElement("Layer");
                        for (int k = 0; k < section.layers[j].entities.Count; k++)
                        {
                            XElement xEnt = new XElement("entity");
                            xEnt.Add(new XAttribute("name", section.layers[j].entities[k].name));
                            xEnt.Add(new XAttribute("textureIndex", section.layers[j].entities[k].imageIndex));

                            xEnt.Add(new XAttribute("X", section.layers[j].entities[k].position.X));
                            xEnt.Add(new XAttribute("Y", section.layers[j].entities[k].position.Y));

                            xEnt.Add(new XAttribute("scaleX", section.layers[j].entities[k].scale.Width));
                            xEnt.Add(new XAttribute("scaleY", section.layers[j].entities[k].scale.Height));

                            xLayer.Add(xEnt);
                        }
                        xLayers.Add(xLayer);
                    }
                    xSection.Add(xLayers);
                    //COLLISION
                    XElement xCollisions = new XElement("Collisions");
                    for (int k = 0; k < section.collisions.Count; k++)
                    {
                        XElement xColl = new XElement("collision");
                        if (section.collisions[k].eventName != null)
                        {
                            XElement xEvent = new XElement("event");
                            xEvent.Add(new XAttribute("name", section.collisions[k].eventName));
                            xColl.Add(xEvent);
                        }
                        xColl.Add(new XAttribute("X", section.collisions[k].rect.X));
                        xColl.Add(new XAttribute("Y", section.collisions[k].rect.Y));
                        xColl.Add(new XAttribute("Width", section.collisions[k].rect.Width));
                        xColl.Add(new XAttribute("Height", section.collisions[k].rect.Height));
                        xCollisions.Add(xColl);
                    }
                    xSection.Add(xCollisions);
                }
                xRoot.Save(dlg.FileName);
            }
        }
        //LEVEL NAME
        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            level.levelName = textBox1.Text;
        }
        //LOAD BACKGROUND
        private void button8_Click(object sender, EventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();

            if (DialogResult.OK == dlg.ShowDialog())
            {
                level.backgroundImage = dlg.SafeFileName;
                TM.LoadTexture(resourcesPath + level.backgroundImage, level.backgroundImage);
            }
        }
        #endregion

        #region SECTION
        //OPEN SECTION EDITOR
        private void button5_Click(object sender, EventArgs e)
        {
            if (groups.Count < 1)
            {
                MessageBox.Show("No entity groups found\nEntities tab: Add Group");
                return;
            }

            if (sectionTool == null)
            {
                sectionTool = new SectionEditor();
                sectionTool.FormClosed += sectionTool_FormClosed; //add event
                //sectionTool.UpdateButtonClick += tool_UpdateButtonClick;
                //tool.Owner = this;//parenting
                sectionTool.Show(this);//modal
                //sectionTool.Show(this);
            }
        }
        void sectionTool_FormClosed(object sender, FormClosedEventArgs e)
        {
            sectionTool = null;
        }

        //CHANGE SECTION
        private void sectionBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Level Section
            if (sectionBox.SelectedIndex < 0) return;

            currentSection = sectionBox.SelectedIndex;
            UpdateSection(false);
        }

        //ADD SECTION
        private void button1_Click(object sender, EventArgs e)
        {
            level.AddSection();
            currentSection = level.levelSections.Count - 1;

            ResizePanel2();
            UpdateSection();
        }

        //DELETE SECTION
        private void button7_Click(object sender, EventArgs e)
        {
            LevelSection section = level.levelSections[currentSection];
            if (level.levelSections.Count == 1)
            {
                //clear section 1
                section.amountOfEnemies = 0;
                section.layers.Clear();
            }
            else
            {
                //erase
                level.levelSections.RemoveAt(currentSection);
                currentSection = level.levelSections.Count - 1;
            }
            UpdateSection();
        }
        void UpdateSection(bool updateSectionBox = true)
        {
            LevelSection section = level.levelSections[currentSection];
            //resize combobox
            if (updateSectionBox)
            {
                sectionBox.Items.Clear();
                for (int i = 0; i < level.levelSections.Count; i++)
                {
                    sectionBox.Items.Add(level.levelName + "-" + (i + 1));
                }
                sectionBox.SelectedIndex = currentSection;
            }

            nudSectionX.Value = section.position.X;
            nudSectionY.Value = section.position.Y;
            //numericUpDown5.Value = section.amountOfEnemies;
            ////update enemies list
            //sectionListBox.Items.Clear();
            //sectionListBox.Items.AddRange(section.enemies.ToArray());

            splitContainer1.Panel2.Invalidate();
        }

        private void numericUpDown5_ValueChanged(object sender, EventArgs e)
        {
            //CHANGE AMOUNT OF ENEMIES
            //level.levelSections[currentSection].amountOfEnemies = (int)numericUpDown5.Value;
        }

        private void numericUpDown8_ValueChanged(object sender, EventArgs e)
        {
            LevelSection section = level.levelSections[currentSection];
            section.position.X = (int)nudSectionX.Value;

            splitContainer1.Panel2.Invalidate();
        }
        private void numericUpDown9_ValueChanged(object sender, EventArgs e)
        {
            LevelSection section = level.levelSections[currentSection];
            section.position.Y = (int)nudSectionY.Value;

            splitContainer1.Panel2.Invalidate();
        }
        #endregion

        //DRAW//public new void Update(){}
        public void Render()
        {
            D3D.Resize(splitContainer1.Panel2, true);
            Point offset = defaultOffset;
            offset.X += splitContainer1.Panel2.AutoScrollPosition.X;
            offset.Y += splitContainer1.Panel2.AutoScrollPosition.Y;

            if (sectionTool != null)
            {
                sectionTool.Render();
                return;
            }
            if (entityTool != null)
            {
                entityTool.Render();
                return;
            }

            //PANEL2
            D3D.Clear(splitContainer1.Panel2, Color.DimGray);
            D3D.DeviceBegin();
            D3D.SpriteBegin();
            //background
            if (level.backgroundImage != null)
            {
                //TM.LoadTexture(level.backgroundImage, "background");
                Point p = GetScreenPos(level.levelSections[currentSection].position);
                TM.Draw(level.backgroundImage, p.X, p.Y, zoom, zoom, new Rectangle(0, 0, CamWidth, CamHeight));
            }
            for (int sectionIndex = 0; sectionIndex < level.levelSections.Count; sectionIndex++)
            {
                LevelSection _section = Form1.level.levelSections[sectionIndex];
                for (int i = 0; i < _section.layers.Count; i++)
                {
                    for (int j = 0; j < _section.layers[i].entities.Count; j++)
                    {
                        Entity we = _section.layers[i].entities[j];
                        Point pos = GetScreenPos(we.position);
                        TM.Draw(we.imageIndex, pos.X, pos.Y,
                            we.scale.Width * zoom, we.scale.Height * zoom, we.renderRect);
                    }
                }
            }

            #region RENDER SECTIONS

            for (int i = 0; i < level.levelSections.Count; i++)
            {
                PointF screenPos = GetScreenPos(level.levelSections[i].position);
                Color color = (currentSection == i) ? Color.Red : Color.IndianRed;

                D3D.DrawHollowRect(screenPos.X, screenPos.Y,
                    relativeWidth, relativeHeight, color, 3);

                //center square
                color = (currentSection == i && selectedSection != null) ?
                    Color.Red : Color.IndianRed;
                int radius = 8;
                screenPos.X += (relativeWidth * 0.5f) - radius;
                screenPos.Y += (relativeHeight * 0.5f) - radius;
                D3D.DrawRect(screenPos.X, screenPos.Y,
                        radius * 2, radius * 2, color);

                if (i > 0)
                {
                    PointF prevPos = GetScreenPos(level.levelSections[i - 1].position);
                    prevPos.X += (relativeWidth * 0.5f);
                    prevPos.Y += (relativeHeight * 0.5f);

                    screenPos.X += radius;
                    screenPos.Y += radius;
                    D3D.DrawLine(
                        (int)prevPos.X, (int)prevPos.Y,
                        (int)screenPos.X, (int)screenPos.Y,
                        Color.IndianRed, 3);
                }
            }
            #endregion

            D3D.SpriteEnd();
            D3D.DeviceEnd();
            D3D.Present();
        }
        #region CAMERA

        private void splitContainer1_Panel2_MouseDown(object sender, MouseEventArgs e)
        {
            Point offset = defaultOffset;//new Point();//
            offset.X += splitContainer1.Panel2.AutoScrollPosition.X;
            offset.Y += splitContainer1.Panel2.AutoScrollPosition.Y;

            for (int i = 0; i < level.levelSections.Count; i++)
            {
                PointF screenPos = GetScreenPos(level.levelSections[i].position);
                int radius = 12;
                screenPos.X += (relativeWidth * 0.5f) - radius;
                screenPos.Y += (relativeHeight * 0.5f) - radius;

                Rectangle rect = new Rectangle(
                    (int)screenPos.X, (int)screenPos.Y, radius * 2, radius * 2);

                if (rect.Contains(e.X,//- splitContainer1.Panel2.AutoScrollPosition.X,
                                  e.Y //- splitContainer1.Panel2.AutoScrollPosition.Y
                                  ))
                {
                    sectionBox.SelectedIndex = currentSection = i;
                    UpdateSection(false);
                    selectedSection = level.levelSections[i];

                    //calculate cursor offset
                    cursorOffset = e.Location;
                    screenPos = GetScreenPos(level.levelSections[i].position);
                    cursorOffset.X -= (int)screenPos.X;
                    cursorOffset.Y -= (int)screenPos.Y;
                    break;
                }
            }
        }
        private void splitContainer1_Panel2_MouseMove(object sender, MouseEventArgs e)
        {
            if (selectedSection != null)
            {
                PointF relativePos = e.Location;

                relativePos.X -= defaultOffset.X + cursorOffset.X + splitContainer1.Panel2.AutoScrollPosition.X;
                relativePos.Y -= defaultOffset.Y + cursorOffset.Y + splitContainer1.Panel2.AutoScrollPosition.Y;

                relativePos.X /= zoom;
                relativePos.Y /= zoom;

                if (relativePos.X < 0) relativePos.X = 0;

                selectedSection.position.X = (int)relativePos.X;
                selectedSection.position.Y = (int)relativePos.Y;

                nudSectionX.Value = selectedSection.position.X;
                nudSectionY.Value = selectedSection.position.Y;

                Render();
            }
        }

        private void splitContainer1_Panel2_MouseUp(object sender, MouseEventArgs e)
        {
            ResizePanel2();
            selectedSection = null;
        }
        private void splitContainer1_Panel2_Scroll(object sender, ScrollEventArgs e)
        {
            Render();
        }

        private Point GetScreenPos(Point entityPosition)
        {
            Point offset = defaultOffset;
            offset.X += splitContainer1.Panel2.AutoScrollPosition.X;
            offset.Y += splitContainer1.Panel2.AutoScrollPosition.Y;

            PointF relativePos = entityPosition;
            relativePos.X *= zoom;
            relativePos.Y *= zoom;
            relativePos.X += offset.X;
            relativePos.Y += offset.Y;

            Point p = new Point();
            p.X = (int)relativePos.X;
            p.Y = (int)relativePos.Y;
            return p;
        }
        private void ZoomChange()
        {
            relativeWidth = cameraSize.Width * zoom;
            relativeHeight = cameraSize.Height * zoom;
        }
        private void ResizePanel2()
        {
            //Point offset = splitContainer1.Panel2.AutoScrollPosition;

            Point screenPos = GetScreenPos(level.levelSections[0].position);
            int maxX = screenPos.X;
            int maxY = screenPos.Y;
            for (int i = 1; i < level.levelSections.Count; i++)
            {
                Point sp = GetScreenPos(level.levelSections[i].position);
                if (sp.X > maxX) maxX = sp.X;
                if (sp.Y > maxY) maxY = sp.Y;
            }

            maxX += (int)relativeWidth;
            maxY += (int)relativeHeight;

            //maxX = Math.Max(maxX, splitContainer1.Panel2.AutoScrollMinSize.Width);
            //maxY = Math.Max(maxY, splitContainer1.Panel2.AutoScrollMinSize.Height);
            //Size newMinSize = splitContainer1.Panel2.AutoScrollMinSize;//new Size(CamWidth, CamHeight);
            //if (maxX > newMinSize.Width)
            //{
            //    newMinSize.Width = maxX;
            //}
            //if (maxY > newMinSize.Height)
            //{
            //    newMinSize.Height = maxY;
            //}
            splitContainer1.Panel2.AutoScrollMinSize = new Size(maxX, maxY);// newMinSize;
        }
        #endregion

        #region ENTITIES
        //ENTITY GROUP
        private void bAddGroup_Click(object sender, EventArgs e)
        {
            if (textBox4.Text.Length > 0)
            {
                Group g = new Group();
                g.name = textBox4.Text;
                groups.Add(g);
                UpdateGroupNames();
                groupListBox.SelectedIndex =
                    currentGroup =
                    groups.Count - 1;
            }
        }
        private void bDeleteGroup_Click(object sender, EventArgs e)
        {
            var confirmResult = MessageBox.Show("Are you sure to delete this item ??",
                                     "Confirm Delete",
                                     MessageBoxButtons.YesNo);
            if (confirmResult == DialogResult.Yes)
            {
                if (groupListBox.SelectedIndex >= 0)
                {
                    //groups[groupListBox.SelectedIndex].entities.Clear();
                    groups.RemoveAt(groupListBox.SelectedIndex);
                    groupListBox.Items.RemoveAt(groupListBox.SelectedIndex);
                    UpdateGroupNames();
                    currentGroup = 0;
                    //UpdateEntityNames();
                }
            }
        }
        private void UpdateGroupNames()
        {
            groupListBox.Items.Clear();
            int i = 0;
            for (; i < groups.Count; i++)
            {
                groupListBox.Items.Add(groups[i].name);
            }
            if (groups.Count > 0)
            {
                groupListBox.SelectedIndex =
                    currentGroup = 0;
            }
        }
        //ADD ENTITY
        private void bAddEntity_Click(object sender, EventArgs e)
        {
#if false
            if (textBox5.Text.Length > 0)
            {
                if (currentImage != null)
                {
                    Entity ent = new Entity();
                    ent.name = textBox5.Text;
                    ent.size.Width = (int)numericUpDown4.Value;
                    ent.size.Height = (int)numericUpDown5.Value;
                    ent.imageIndex = currentImage;
                    groups[groupBox.SelectedIndex].entities.Add(ent);

                    //textures.Add(imgLabel.Text);
                    currentImage = null;
                }
                else
                {
                    MessageBox.Show("WARNING\nYou must load an image.");
                }
            }
            UpdateEntityNames();
#endif
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Filter = "All Files|*.*|PNG Files|*.png";
            dlg.FilterIndex = 2;

            if (DialogResult.OK == dlg.ShowDialog())
            {
                if (entityTool == null)
                {
                    entityTool = new EntityEditor();
                    entityTool.FormClosed += entityTool_FormClosed;
                    entityTool.eAddEntity += entityTool_eAddEntity;

                    TM.LoadTexture(dlg.FileName, dlg.SafeFileName);
                    entityTool.currentImage = dlg.SafeFileName;
                    entityTool.currentGroup = currentGroup;
                    entityTool.Initialize();

                    entityTool.Show(this);//modal
                }
            }
        }

        void entityTool_eAddEntity(object sender, EventArgs e)
        {
            UpdateEntityNames();
        }

        void entityTool_FormClosed(object sender, FormClosedEventArgs e)
        {
            entityTool = null;
        }
        //DELETE ENTITY
        private void bDeleteEntity_Click(object sender, EventArgs e)
        {
            if (entityListBox.SelectedIndex >= 0
                && currentGroup >= 0)
            {
                TM.UnloadTexture(groups[currentGroup].entities[entityListBox.SelectedIndex].imageIndex);
                groups[currentGroup].entities.RemoveAt(entityListBox.SelectedIndex);
                entityListBox.Items.RemoveAt(entityListBox.SelectedIndex);
                UpdateEntityNames();
            }
            Render();
        }
        public void UpdateEntityNames()
        {
            if (currentGroup < 0) return;

            entityListBox.Items.Clear();
            int i = 0;
            for (; i < groups[currentGroup].entities.Count; i++)
            {
                entityListBox.Items.Add(groups[currentGroup].entities[i].name);
            }
        }
        #endregion

        //EXPORT GROUP LIST
        private void SaveGroups(string filepath)//button9_Click(object sender, EventArgs e)
        {

            XElement xRoot = new XElement("Groups");
            for (int i = 0; i < groups.Count; i++)
            {
                XElement xGroup = new XElement("Group");
                xGroup.Add(new XAttribute("name", groups[i].name));
                xRoot.Add(xGroup);

                //entities
                for (int j = 0; j < groups[i].entities.Count; j++)
                {
                    XElement xEnt = new XElement("entity");
                    xEnt.Add(new XAttribute("name", groups[i].entities[j].name));
                    xEnt.Add(new XAttribute("imageFilename", groups[i].entities[j].imageIndex));

                    xEnt.Add(new XAttribute("X", groups[i].entities[j].renderRect.X));
                    xEnt.Add(new XAttribute("Y", groups[i].entities[j].renderRect.Y));
                    xEnt.Add(new XAttribute("Width", groups[i].entities[j].renderRect.Width));
                    xEnt.Add(new XAttribute("Height", groups[i].entities[j].renderRect.Height));

                    xGroup.Add(xEnt);
                }
                xRoot.Save(filepath);
            }
        }
        private bool LoadGroups(string filepath)
        {

            groups.Clear();
            XElement xRoot = XElement.Load(filepath);
            if (xRoot == null) return false;

            foreach (XElement xGroup in xRoot.Elements())
            {
                Group group = new Group();
                group.name = Convert.ToString(xGroup.Attribute("name").Value);

                foreach (XElement xEntity in xGroup.Elements())
                {
                    Entity ent = new Entity();
                    ent.name = Convert.ToString(xEntity.Attribute("name").Value);
                    string imageFilename = Convert.ToString(xEntity.Attribute("imageFilename").Value);
                    ent.imageIndex = imageFilename;
                    TM.LoadTexture(resourcesPath + imageFilename, imageFilename);

                    int x, y, w, h;
                    x = int.Parse(xEntity.Attribute("X").Value);
                    y = int.Parse(xEntity.Attribute("Y").Value);
                    w = int.Parse(xEntity.Attribute("Width").Value);
                    h = int.Parse(xEntity.Attribute("Height").Value);
                    ent.renderRect = new Rectangle(x, y, w, h);
                    group.entities.Add(ent);
                }
                groups.Add(group);
            }
            UpdateGroupNames();
            UpdateEntityNames();
            return true;
        }

        //CLOSE
        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            var confirmResult = MessageBox.Show("Would you like to save the level?",
                         "Save Level",
                         MessageBoxButtons.YesNo);
            if (confirmResult == DialogResult.Yes)
            {
                SaveLevel();
            }
            SaveGroups(resourcesPath + "XML\\groups.xml");
            SaveEnemies(resourcesPath + "XML\\enemies.xml");
            looping = false;
        }

        private void groupListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            currentGroup = groupListBox.SelectedIndex;
            UpdateEntityNames();
        }
        private void trackBar1_Scroll(object sender, EventArgs e)
        {
            zoom = trackBar1.Value / 100f;
            ZoomChange();
            ResizePanel2();
        }

        private void saveToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            SaveGroups(resourcesPath + "XML\\groups.xml");
            SaveEnemies(resourcesPath + "XML\\enemies.xml");
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("made by: Alexis Sotelo");
        }

    }
}
