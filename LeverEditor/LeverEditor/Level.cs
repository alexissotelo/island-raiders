﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace LeverEditor
{
    public class Layer
    {
        //Use to allocate entities and place them in a level section
        public List<Entity> entities;

        public Layer()
        {
            entities = new List<Entity>();
        }
        public void Clear()
        {
            entities.Clear();
        }
    }
    public class CollisionRect
    {
        public string eventName;
        public Rectangle rect;

        public CollisionRect(Rectangle rec)
        {
            rect = rec;
        }
        public CollisionRect(int x,int y, int w, int h)
        {
            rect = new Rectangle(x,y,w,h);
        }

        public override string ToString()
        {
            return eventName + " [" + rect.X + "," + rect.Y + "," + rect.Width + "," + rect.Height + "]";
        }
    }
    public class LevelSection
    {
        public Point position;

        public List<Layer> layers;

        public int amountOfEnemies;
        public List<Entity> enemies;
        public List<CollisionRect> collisions;

        public LevelSection()
        {
            position = new Point(0, 0);

            amountOfEnemies = 0;
            layers = new List<Layer>();
            layers.Add(new Layer());
            layers.Add(new Layer());
            layers.Add(new Layer());
            layers.Add(new Layer());

            enemies = new List<Entity>();
            collisions = new List<CollisionRect>();
        }
    }
    public class Level
    {
        public string backgroundImage;

        public string levelName;
        public List<LevelSection> levelSections;

        public Level(string name)
        {
            levelName = name;
            levelSections = new List<LevelSection>();
            levelSections.Add(new LevelSection());
        }
        public void AddSection()
        {
            LevelSection temp = new LevelSection();
            temp.position.X = 800 * levelSections.Count;
            levelSections.Add(temp);

        }
        public override string ToString()
        {
            return levelName;
        }
    }

    public class Group
    {
        public string name;
        public List<Entity> entities;

        public Group(string n = "new group")
        {
            name = n;
            entities = new List<Entity>();
        }
    }
}
