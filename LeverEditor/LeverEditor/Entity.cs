﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace LeverEditor
{
    //public enum EntityType
    //{
    //    image, obstacle, enemy
    //}
    public class Entity
    {
        //public string groupName;
        
        public string name;
        //public int imageIndex;
        public string imageIndex;

        public Point position;
        //public Size size;
        public Rectangle renderRect;
        public SizeF scale;

        public Entity()
        {
            scale.Width = scale.Height = 1f;
        }
        public Rectangle GetRect()
        {
            return new Rectangle(position,renderRect.Size);
        }
        public override string ToString()
        {
            return name;
        }
    }

    #region ENEMY
    public class Enemy : Entity
    {
        public int[] damage     = new int[3];
        public int[] health     = new int[3];
        public int moveSpeed;

        public int combatType;
        public string missileName;
        public string skillName;

        public int tier;
        public int droprate;

        public Enemy()
        {
            name = "Enemy";

            damage[0] = 1;
            health[0] = 50;
            moveSpeed = 60;

            position = new Point();
            renderRect = new Rectangle(0,0,42, 16);

            combatType = (int)CombatType.Melee;
            tier  = 0;

            droprate = 0;
        }
    }
    enum PlayerType
    {
        swordsman, gunner, alchemist
    }

    enum CombatType
    {
        Melee, Range, AOE
    }
    #endregion
}
