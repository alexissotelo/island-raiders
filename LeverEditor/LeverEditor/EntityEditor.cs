﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LeverEditor
{

    public partial class EntityEditor : Form
    {
        public event EventHandler eAddEntity;

        SGP.CSGP_Direct3D D3D = SGP.CSGP_Direct3D.GetInstance();
        SGP.CSGP_TextureManager TM = SGP.CSGP_TextureManager.GetInstance();

        public string currentImage;
        public int currentGroup;

        Entity entity;

        int Clamp(int n, int min, int max)
        {
            if (n < min) return min;
            if (n > max) return max;
            return n;
        }

        public EntityEditor()
        {
            InitializeComponent();

            entity = new Entity();
            D3D.AddRenderTarget(splitContainer1.Panel2);
        }
        public void Initialize()
        {
            groupBox4.Name = entity.imageIndex = currentImage;

            nudWidth.Value = entity.renderRect.Width = (int)TM.GetTextureWidth(currentImage);
            nudHeight.Value = entity.renderRect.Height = (int)TM.GetTextureHeight(currentImage);
            splitContainer1.Panel2.AutoScrollMinSize = entity.renderRect.Size;

            groupBox.Items.Clear();
            int i = 0;
            for (; i < Form1.groups.Count; i++)
            {
                groupBox.Items.Add(Form1.groups[i].name);
            }
            if (Form1.groups.Count > 0)
            {
                groupBox.SelectedIndex = 0;
            }
        }
        private void nudX_ValueChanged(object sender, EventArgs e)
        {
            entity.renderRect.X = (int)nudX.Value;
        }
        private void nudY_ValueChanged(object sender, EventArgs e)
        {
            entity.renderRect.Y = (int)nudY.Value;
        }
        private void nudWidth_ValueChanged(object sender, EventArgs e)
        {
            entity.renderRect.Width = (int)nudWidth.Value;

        }
        private void nudHeight_ValueChanged(object sender, EventArgs e)
        {
            entity.renderRect.Height = (int)nudHeight.Value;
        }
        private void textBox5_TextChanged(object sender, EventArgs e)
        {
            entity.name = textBox5.Text;
        }
        private void groupBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            currentGroup = groupBox.SelectedIndex;
        }
        public void Render()
        {
            D3D.Resize(splitContainer1.Panel2, true);
            D3D.Clear(splitContainer1.Panel2, Color.White);
            D3D.DeviceBegin();
            D3D.SpriteBegin();

            Point offset = splitContainer1.Panel2.AutoScrollPosition;
            if (currentImage != null)
            {
                TM.Draw(currentImage, offset.X, offset.Y);
                Rectangle rect = entity.renderRect;
                rect.Offset(offset);
                D3D.DrawRect(rect, Color.FromArgb(100, Color.CadetBlue));
            }

            D3D.SpriteEnd();
            D3D.DeviceEnd();
            D3D.Present();
        }
        bool isDrawing;
        private void splitContainer1_Panel2_MouseDown(object sender, MouseEventArgs e)
        {
            isDrawing = true;
            Point newPos = e.Location;
            newPos.X -= splitContainer1.Panel2.AutoScrollPosition.X;
            newPos.Y -= splitContainer1.Panel2.AutoScrollPosition.Y;
            entity.renderRect.Location = newPos;

            nudX.Value = newPos.X;
            nudY.Value = newPos.Y;
        }
        private void splitContainer1_Panel2_MouseMove(object sender, MouseEventArgs e)
        {
            if (isDrawing)
            {
                Point newPos = e.Location;
                newPos.X -= splitContainer1.Panel2.AutoScrollPosition.X;
                newPos.Y -= splitContainer1.Panel2.AutoScrollPosition.Y;

                Size newSize = new Size(newPos.X - entity.renderRect.X, newPos.Y - entity.renderRect.Y);
                newSize.Width = Clamp(newSize.Width, (int)nudWidth.Minimum, (int)nudWidth.Maximum);
                newSize.Height = Clamp(newSize.Height, (int)nudHeight.Minimum, (int)nudHeight.Maximum);

                entity.renderRect.Size = newSize;
                nudWidth.Value = newSize.Width;
                nudHeight.Value = newSize.Height;
            }

        }
        private void splitContainer1_Panel2_MouseUp(object sender, MouseEventArgs e)
        {
            isDrawing = false;
        }
        private void bAddEntity_Click(object sender, EventArgs e)
        {
            if (textBox5.Text.Length == 0)
            {
                MessageBox.Show("Entity must have a name");
                return;
            }

            for (int i = 0; i < Form1.groups.Count; i++)
            {
                for (int j = 0; j < Form1.groups[i].entities.Count; j++)
                {
                    Entity ent = Form1.groups[i].entities[j];
                    if (ent.name == entity.name)
                    {
                        MessageBox.Show("There is already an entity with that name.");
                        return;
                    }
                }
            }
            Form1.groups[currentGroup].entities.Add(entity);
            if (eAddEntity != null)
            {
                eAddEntity(this, e);
            }
            this.Close();
        }

    }
}
