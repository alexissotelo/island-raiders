﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LeverEditor
{
    public partial class SectionEditor : Form
    {
        SGP.CSGP_Direct3D D3D = SGP.CSGP_Direct3D.GetInstance();
        SGP.CSGP_TextureManager TM = SGP.CSGP_TextureManager.GetInstance();

        string[] eventList = { "-", "SPAWNENEMIES", "CAM2SECTION","LAVAPIT","SLOWAREA","THUNDERAREA" };

        //DRAG
        Entity selectedEntity = null;
        Point cursorOffset;
        Size toolSize = new Size(20, 20);
        bool isScaling;
        bool isMoving;

        //SECTION
        LevelSection section;
        int sectionIndex;
        int currentLayer;

        //CAMERA
        int amountOfTicks = 3;
        Point CameraPosition;
        const int CamWidth = 800;
        const int CamHeight = 600;

        //group
        int currentGroup;
        int currentEntity;

        //collision
        int currentCollision = -1;
        bool moveCollision;

        Point Lerp(Point a, Point b, float t)
        {
            Point bMinusA = new Point(b.X - a.X, b.Y- a.Y);
            float cX = bMinusA.X* t;
            float cY = bMinusA.Y * t;
            a.Offset((int)cX,(int)cY);
            return a;
        }

        public SectionEditor()
        {
            InitializeComponent();
            splitContainer1.Panel2.AutoScrollMinSize = new Size(800, 600);
            this.Text = /*"Edit Section - " +*/ Form1.level.levelName + "-" + (Form1.currentSection + 1);

            //entities
            //populate group box
            for (int i = 0; i < Form1.groups.Count; i++)
            {
                groupBox.Items.Add(Form1.groups[i].name);
            }
            groupBox.SelectedIndex = currentGroup = 0;
            //populate entity list box
            UpdateEntityListBox();
            //populate enemy list
            for (int i = 0; i < Form1.enemies.Count; i++)
            {
                enemyBox.Items.Add(Form1.enemies[i].name);
            }
            enemyBox.SelectedIndex = 0;

            //populate layers
            sectionIndex = Form1.currentSection;
            section = Form1.level.levelSections[sectionIndex];
            CameraPosition = section.position;

            if (sectionIndex == 0 || sectionIndex == (Form1.level.levelSections.Count - 1))
            {
                //if first of last section only show 2 ticks
                amountOfTicks = 2;
                trackBar1.Value = sectionIndex == 0 ? 0 : 10;
                trackBar1.Maximum = 1 * trackBar1.TickFrequency;
            }

            UpdateLayerListbox();
            layersListBox.SelectedIndex = currentLayer = 0;
            UpdateEntitiesListbox();
            nudAmountEnemies.Value = section.amountOfEnemies;
            nudAmountEnemies.Maximum = section.enemies.Count;
            UpdateEnemyListBox();
            UpdateCollisionListBox();
            for (int i = 0; i < eventList.Length; i++)
            {
                eventBox.Items.Add(eventList[i]);
            }
            D3D.AddRenderTarget(splitContainer1.Panel2);
        }
        #region ENEMY
        //ADD ENEMY
        private void button5_Click(object sender, EventArgs e)
        {
            Entity en = new Entity();
            //make copy
            en.name = Form1.enemies[enemyBox.SelectedIndex].name;
            en.position = section.position;
            en.position.X += 400;
            en.position.Y += 300;//Form1.enemies[enemyBox.SelectedIndex].position;
            en.renderRect = Form1.enemies[enemyBox.SelectedIndex].renderRect;

            section.enemies.Add(en);
            enemyListBox.Items.Add(en.name + " " + en.position);
            nudAmountEnemies.Maximum = section.enemies.Count;
            nudAmountEnemies.Value++;
        }
        //DELETE ENEMY
        private void button2_Click(object sender, EventArgs e)
        {
            if (enemyListBox.SelectedIndex < 0) return;
            section.enemies.RemoveAt(enemyListBox.SelectedIndex);
            enemyListBox.Items.RemoveAt(enemyListBox.SelectedIndex);

            nudAmountEnemies.Value--;
            nudAmountEnemies.Maximum = section.enemies.Count;
        }
        void UpdateEnemyListBox()
        {
            enemyListBox.Items.Clear();
            for (int i = 0; i < section.enemies.Count; i++)
            {
                enemyListBox.Items.Add(
                    section.enemies[i].name + " " + section.enemies[i].position);
            }
        }
        private void nudAmountEnemies_ValueChanged(object sender, EventArgs e)
        {
            section.amountOfEnemies = (int)nudAmountEnemies.Value;
        }
        private void enemyListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(enemyListBox.SelectedIndex > 0)
            {
                selectedEntity = section.enemies[enemyListBox.SelectedIndex];
            }
        }
        #endregion

        #region DESIGN
        //CHANGE LAYER
        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            currentLayer = layersListBox.SelectedIndex;
            selectedEntity = null;

            //refresh listbox2 - layer entities
            Layer layer = section.layers[currentLayer];
            entitiesListBox.Items.Clear();
            for (int i = 0; i < layer.entities.Count; i++)
            {
                entitiesListBox.Items.Add(layer.entities[i].name + " " + layer.entities[i].position);
            }
        }
        //ADD LAYER
        private void bAddLayer_Click(object sender, EventArgs e)
        {
            section.layers.Add(new Layer());
            layersListBox.Items.Add("Layer " + section.layers.Count);
        }
        //DELETE LAYER
        private void bDeleteLayer_Click(object sender, EventArgs e)
        {
            var confirmResult = MessageBox.Show("Are you sure to delete this item ??",
                         "Confirm Delete",
                         MessageBoxButtons.YesNo);
            if (confirmResult == DialogResult.Yes)
            {
                if (layersListBox.SelectedIndex >= 0)
                {
                    section.layers.RemoveAt(layersListBox.SelectedIndex);
                    layersListBox.SelectedIndex = currentLayer = 0;
                    UpdateLayerListbox();
                    UpdateEntitiesListbox();
                }
            }
        }
        //GROUP
        private void groupBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            currentGroup = groupBox.SelectedIndex;
            UpdateEntityListBox();
        }
        private void UpdateEntityListBox()
        {
            groupentityListBox.Items.Clear();
            for (int i = 0; i < Form1.groups[currentGroup].entities.Count; i++)
            {
                groupentityListBox.Items.Add(Form1.groups[currentGroup].entities[i].name);
            }
        }
        private void bAddEntity_Click(object sender, EventArgs e)
        {
            if (currentGroup >= 0 && currentEntity >= 0 && currentLayer >= 0)
            {
                Entity entity = Form1.groups[currentGroup].entities[currentEntity];
                Entity clone = new Entity();
                clone.name = entity.name;
                clone.imageIndex = entity.imageIndex;
                clone.renderRect = entity.renderRect;

                clone.position = section.position;
                clone.position.X += 400;
                clone.position.Y += 300;

                section.layers[currentLayer].entities.Add(clone);
                entitiesListBox.Items.Add(clone.name + " " + clone.position);
            }
        }
        private void entityListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            currentEntity = groupentityListBox.SelectedIndex;
        }
        private void bDeleteEntity_Click(object sender, EventArgs e)
        {
            if (entitiesListBox.SelectedIndex >= 0)
            {
                section.layers[currentLayer].entities.RemoveAt(entitiesListBox.SelectedIndex);
                selectedEntity = null;
                UpdateEntitiesListbox();
                Render();
            }
        }
        private void UpdateEntitiesListbox()
        {
            if (currentLayer >= 0)
            {
                entitiesListBox.Items.Clear();
                Layer layer = section.layers[currentLayer];
                for (int i = 0; i < layer.entities.Count; i++)
                {
                    entitiesListBox.Items.Add(
                        layer.entities[i].name + " " + layer.entities[i].position);
                }
            }
        }
        private void UpdateLayerListbox()
        {
            layersListBox.Items.Clear();
            for (int i = 0; i < section.layers.Count; i++)
            {
                layersListBox.Items.Add("Layer " + (i + 1));
            }
        }
        private void nupX_ValueChanged(object sender, EventArgs e)
        {
            if (selectedEntity != null)
            {
                selectedEntity.position.X = (int)nupX.Value;
            }
        }
        private void nupY_ValueChanged(object sender, EventArgs e)
        {
            if (selectedEntity != null)
            {
                selectedEntity.position.Y = (int)nupY.Value;
            }
        }
        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            if (selectedEntity != null)
            {
                selectedEntity.scale.Width = (float)nudScaleX.Value;
            }
        }
        private void nudScaleY_ValueChanged(object sender, EventArgs e)
        {
            if (selectedEntity != null)
            {
                selectedEntity.scale.Height = (float)nudScaleY.Value;
            }
        }
        private void entitiesListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (entitiesListBox.SelectedIndex >= 0)
            {
                selectedEntity = section.layers[currentLayer].entities[entitiesListBox.SelectedIndex];
                nupX.Value = selectedEntity.position.X;
                nupY.Value = selectedEntity.position.Y;
                nudScaleX.Value = (decimal)selectedEntity.scale.Width;
                nudScaleY.Value = (decimal)selectedEntity.scale.Height;
            }
        }

        #endregion
        void DrawSection(int index)
        {
            Point offset = splitContainer1.Panel2.AutoScrollPosition;
            offset.X -= CameraPosition.X;
            offset.Y -= CameraPosition.Y;

            LevelSection _section = Form1.level.levelSections[index];

            for (int i = 0; i < _section.layers.Count; i++)
            {
                for (int j = 0; j < _section.layers[i].entities.Count; j++)
                {
                    Entity we = _section.layers[i].entities[j];
                    Point pos = we.position;
                    pos.X += offset.X;
                    pos.Y += offset.Y;

                    TM.Draw(we.imageIndex, pos.X, pos.Y,
                        we.scale.Width, we.scale.Height, we.renderRect);
                }
            }
        }
        public void Render()
        {
            D3D.Resize(splitContainer1.Panel2, true);
            D3D.Clear(splitContainer1.Panel2, Color.White);
            D3D.DeviceBegin();
            D3D.SpriteBegin();

            Point offset = splitContainer1.Panel2.AutoScrollPosition;
            if (Form1.level.backgroundImage != null)
                TM.Draw(Form1.level.backgroundImage, offset.X, offset.Y);
            offset.X -= CameraPosition.X;
            offset.Y -= CameraPosition.Y;

            int prevSection = Form1.currentSection - 1;
            if (prevSection >= 0)
                DrawSection(prevSection);

            //DRAW ENTITIES
            for (int i = 0; i < section.layers.Count; i++)
            {
                for (int j = 0; j < section.layers[i].entities.Count; j++)
                {
                    Entity we = section.layers[i].entities[j];
                    Point pos = we.position;
                    pos.X += offset.X;
                    pos.Y += offset.Y;

                    bool currentTab = (tabControl1.SelectedIndex == 1);
                    if (currentTab)
                    {
                        D3D.DrawRect(pos.X, pos.Y, we.renderRect.Size.Width * we.scale.Width,
                            we.renderRect.Size.Height * we.scale.Height,
                            Color.FromArgb(100, Color.LightGreen));
                    }
                    //TM.Draw(we.imageIndex, pos.X, pos.Y, we.scale.Width, we.scale.Height);
                    TM.Draw(we.imageIndex, pos.X, pos.Y, we.scale.Width, we.scale.Height, we.renderRect);
                    if (checkBox1.Checked)
                        D3D.DrawText(we.name, pos.X, pos.Y, Color.Black);
                }
            }
            //DRAW NEXT SECTION
            int nextSection = Form1.currentSection + 1;
            if (nextSection < Form1.level.levelSections.Count)
                DrawSection(nextSection);
            //DRAW COLLISIONS
            if (tabControl1.SelectedIndex == 2)
            {
                for (int i = 0; i < section.collisions.Count; i++)
                {
                    Rectangle rect = section.collisions[i].rect;
                    rect.Offset(offset);
                    Color color = (section.collisions[i].eventName == null) ?
                        Color.Blue : Color.BlueViolet;

                    if (i == currentCollision)
                    {
                        D3D.DrawHollowRect(rect, color, 2);

                        Point pos = rect.Location - toolSize;
                        D3D.DrawRect(new Rectangle(pos + rect.Size, toolSize), color);
                    }
                    if (section.collisions[i].eventName == null)
                    {
                        D3D.DrawRect(rect, Color.FromArgb(100, color));
                    }
                    else
                    {
                        D3D.DrawRect(rect, Color.FromArgb(100, color));
                        D3D.DrawText(section.collisions[i].eventName, rect.X, rect.Y, Color.Black);
                    }
                }
            }
            //DRAW ENEMIES
            for (int j = 0; j < section.enemies.Count; j++)
            {
                Entity we = section.enemies[j];
                Point pos = we.position;
                pos.X += offset.X;
                pos.Y += offset.Y;

                D3D.DrawRect(new Rectangle(pos, we.renderRect.Size), Color.FromArgb(100, Color.IndianRed));
                if (checkBox1.Checked)
                    D3D.DrawText(we.name, pos.X, pos.Y, Color.Black);
            }

            if (selectedEntity != null)
            {
                Rectangle rect = selectedEntity.GetRect();
                rect.X += offset.X;
                rect.Y += offset.Y;
                D3D.DrawHollowRect(rect.X, rect.Y,
                    rect.Width * selectedEntity.scale.Width,
                    rect.Height * selectedEntity.scale.Height,
                    Color.Red, 2);
                {
                    Point pos = rect.Location - toolSize;
                    float width = rect.Size.Width;
                    float height = rect.Size.Height;
                    width *= selectedEntity.scale.Width;
                    height *= selectedEntity.scale.Height;
                    pos.X += (int)width;
                    pos.Y += (int)height;

                    D3D.DrawRect(new Rectangle(pos, toolSize), Color.LightGreen);
                }
            }

            //DRAW HUD
            if (checkHUD.Checked)
            {
                D3D.DrawRect(0, 520, 800, 80, Color.LightGray);
            }
            offset.Offset(section.position);
            D3D.DrawHollowRect(offset.X, offset.Y, CamWidth, CamHeight, Color.Red, 2);

            D3D.SpriteEnd();
            D3D.DeviceEnd();
            D3D.Present();
        }

        //**********/MOUSE/**********/
        private void splitContainer1_Panel2_MouseMove(object sender, MouseEventArgs e)
        {
            //MOVE COLLISION
            if (tabControl1.SelectedIndex == 2
                && currentCollision >= 0)
            {
                Point location = section.collisions[currentCollision].rect.Location;
                if (moveCollision)
                {
                    location.X = e.X - cursorOffset.X;// +section.position.X;
                    location.Y = e.Y - cursorOffset.Y;// +section.position.Y;
                    if (location.X < nudRectX.Minimum)
                        location.X = (int)nudRectX.Minimum;
                    section.collisions[currentCollision].rect.Location = location;
                }
                if (isScaling)
                {
                    Point newSize = e.Location;
                    newSize.X -= location.X - CameraPosition.X;
                    newSize.Y -= location.Y - CameraPosition.Y;
                    if (newSize.X < 4) newSize.X = 4;
                    if (newSize.Y < 4) newSize.Y = 4;
                    if (newSize.X > 800) newSize.X = 800;
                    if (newSize.Y > 600) newSize.Y = 600;

                    section.collisions[currentCollision].rect.Size = new Size(newSize.X, newSize.Y);
                }

                UpdateCollisionGroup();
                Render();
                return;
            }

            if (selectedEntity != null && isMoving)
            {
                selectedEntity.position.X = e.X - cursorOffset.X;// +section.position.X;
                selectedEntity.position.Y = e.Y - cursorOffset.Y;// +section.position.Y;

                //clamp pos
                if (selectedEntity.position.X < nupX.Minimum)
                    selectedEntity.position.X = (int)nupX.Minimum;

                nupX.Value = selectedEntity.position.X;
                nupY.Value = selectedEntity.position.Y;

                Render();
            }
        }
        private void splitContainer1_Panel2_MouseDown(object sender, MouseEventArgs e)
        {
            selectedEntity = null;
            Point offset = splitContainer1.Panel2.AutoScrollPosition;
            offset.X -=  CameraPosition.X;
            offset.Y -=  CameraPosition.Y;

            switch (tabControl1.SelectedIndex)
            {
                case 0://SELECT ENEMIES
                    for (int j = section.enemies.Count -1 ; j >= 0; j--)
                    {
                        Entity we = section.enemies[j];
                        if (we.GetRect().Contains(
                            e.X + CameraPosition.X, e.Y + CameraPosition.Y))
                        {
                            cursorOffset = e.Location;
                            cursorOffset.X -= we.position.X;
                            cursorOffset.Y -= we.position.Y;

                            nupX.Value = we.position.X;
                            nupY.Value = we.position.Y;
                            nudScaleX.Value = (decimal)we.scale.Width;
                            nudScaleY.Value = (decimal)we.scale.Height;

                            selectedEntity = we;
                            isMoving = true;
                            return;
                        }
                    }
                    break;
                case 1://SELECT DESIGN
                    if (currentLayer >= 0)
                    {
                        for (int j = section.layers[currentLayer].entities.Count -1; j >= 0; j--)
                        {
                            Entity we = section.layers[currentLayer].entities[j];
                            int rWidth  = (int) (we.renderRect.Size.Width * we.scale.Width);
                            int rHeight = (int) (we.renderRect.Size.Height * we.scale.Height);
                            Rectangle rect = new Rectangle(
                                we.position.X, we.position.Y,
                                Math.Abs(rWidth),
                                Math.Abs(rHeight));
                            if (rWidth < 0) rect.Offset(rWidth, 0);
                            if (rHeight < 0) rect.Offset(rWidth, 0);

                            if (rect.Contains(e.X + CameraPosition.X, e.Y + CameraPosition.Y))
                            {
                                cursorOffset = e.Location;
                                cursorOffset.X -= we.position.X;
                                cursorOffset.Y -= we.position.Y;

                                nupX.Value = we.position.X;
                                nupY.Value = we.position.Y;
                                nudScaleX.Value = (decimal)we.scale.Width;
                                nudScaleY.Value = (decimal)we.scale.Height;

                                selectedEntity = we;
                                isMoving = true;
                                return;
                            }
                        }
                    }
                    break;
                case 2://SELECT COLL RECT
                    for (int j = section.collisions.Count - 1; j >= 0; j--)
                    {
                        Rectangle rect = section.collisions[j].rect;
                        if (rect.Contains(e.X + CameraPosition.X, e.Y + CameraPosition.Y))
                        {
                            cursorOffset = e.Location;
                            cursorOffset.X -= rect.Location.X;
                            cursorOffset.Y -= rect.Location.Y;

                            collisionListBox.SelectedIndex = currentCollision = j;

                            rect.Location += section.collisions[j].rect.Size - toolSize;
                            rect.Size = toolSize;
                            if (rect.Contains(e.X +CameraPosition.X, e.Y +CameraPosition.Y))
                            {
                                isScaling = true;
                            }
                            else
                                moveCollision = true;
                            return;
                        }
                    }
                    break;
            }
        }
        private void splitContainer1_Panel2_MouseUp(object sender, MouseEventArgs e)
        {
            UpdateEntitiesListbox();
            UpdateEnemyListBox();
            //selectedEntity = null;
            isMoving =
            isScaling = moveCollision = false;
            Render();
        }

        #region COLLISION
        private void bAddCollision_Click(object sender, EventArgs e)
        {
            Point pos = section.position;
            pos.X += 400;
            pos.Y += 300;
            section.collisions.Add(new CollisionRect(pos.X, pos.Y, 64, 64));
            currentCollision = section.collisions.Count - 1;
            collisionListBox.Items.Add(section.collisions[currentCollision]);
        }
        private void bDeleteCollision_Click(object sender, EventArgs e)
        {
            if (collisionListBox.SelectedIndex >= 0)
            {
                section.collisions.RemoveAt(collisionListBox.SelectedIndex);
                currentCollision = -1;
                UpdateCollisionListBox();
            }
        }
        private void UpdateCollisionListBox()
        {
            collisionListBox.Items.Clear();
            for (int i = 0; i < section.collisions.Count; i++)
            {
                collisionListBox.Items.Add(section.collisions[i]);
            }
        }
        private void collisionListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            currentCollision = collisionListBox.SelectedIndex;
            UpdateCollisionGroup();
        }
        void UpdateCollisionGroup()
        {
            if (collisionListBox.SelectedIndex >= 0)
            {
                nudRectX.Value = section.collisions[currentCollision].rect.X;
                nudRectY.Value = section.collisions[currentCollision].rect.Y;
                nudRectW.Value = section.collisions[currentCollision].rect.Width;
                nudRectH.Value = section.collisions[currentCollision].rect.Height;

                textBox1.Text = section.collisions[currentCollision].eventName;
            }
        }

        private void nudRectX_ValueChanged(object sender, EventArgs e)
        {
            if (currentCollision >= 0)
            {
                Point pos = section.collisions[currentCollision].rect.Location;
                Size siz = section.collisions[currentCollision].rect.Size;
                pos.X = (int)nudRectX.Value;
                section.collisions[currentCollision].rect = new Rectangle(pos, siz);
            }
        }
        private void nudRectY_ValueChanged(object sender, EventArgs e)
        {
            if (currentCollision >= 0)
            {
                Point pos = section.collisions[currentCollision].rect.Location;
                Size siz = section.collisions[currentCollision].rect.Size;
                pos.Y = (int)nudRectY.Value;
                section.collisions[currentCollision].rect = new Rectangle(pos, siz);
            }
        }
        private void nudRectW_ValueChanged(object sender, EventArgs e)
        {
            if (currentCollision >= 0)
            {
                Point pos = section.collisions[currentCollision].rect.Location;
                Size siz = section.collisions[currentCollision].rect.Size;
                siz.Width = (int)nudRectW.Value;
                section.collisions[currentCollision].rect = new Rectangle(pos, siz);
            }
        }
        private void nudRectH_ValueChanged(object sender, EventArgs e)
        {
            if (currentCollision >= 0)
            {
                Point pos = section.collisions[currentCollision].rect.Location;
                Size siz = section.collisions[currentCollision].rect.Size;
                siz.Height = (int)nudRectH.Value;
                section.collisions[currentCollision].rect = new Rectangle(pos, siz);
            }
        }
        //EVENT
        private void bAddEvent_Click(object sender, EventArgs e)
        {
            if (textBox1.Text.Length > 0)
                eventBox.Items.Add(textBox1.Text);
        }
        private void eventBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (currentCollision >= 0)
            {
                if (eventBox.SelectedIndex > 0)
                {
                    section.collisions[currentCollision].eventName = (string)eventBox.Items[eventBox.SelectedIndex];
                }
                else
                    section.collisions[currentCollision].eventName = null;
                UpdateCollisionListBox();
            }
        }
        #endregion
        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (tabControl1.SelectedIndex)
            {
                case 0://enemies
                    currentCollision = -1;
                    selectedEntity = null;
                    break;
                case 1://design
                    currentCollision = -1;
                    break;
                case 2://collision
                    selectedEntity = null;
                    break;
            }
        }

        private void trackBar1_Scroll(object sender, EventArgs e)
        {
            //update CameraPosition
            float t = trackBar1.Value *1f / trackBar1.Maximum;
            if (Form1.level.levelSections.Count > 1)
            {
                if (amountOfTicks == 2)
                {
                    if (sectionIndex == 0)
                    {
                        CameraPosition = Lerp(section.position, Form1.level.levelSections[sectionIndex + 1].position, t);
                    }
                    else
                    {
                        CameraPosition = Lerp(Form1.level.levelSections[sectionIndex - 1].position, section.position, t);
                    }
                }
                else
                {
                    if (t < 0.5)
                    {
                        LevelSection prevSection = Form1.level.levelSections[sectionIndex - 1];
                        CameraPosition =
                            Lerp(prevSection.position, section.position, t * 2f);
                    }
                    else
                    {
                        t -= 0.5f;
                        LevelSection nextSection = Form1.level.levelSections[sectionIndex + 1];
                        CameraPosition = Lerp(section.position, nextSection.position, t * 2f);
                    }
                }
            }
        }
    }
}
