﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace LeverEditor
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            //Application.Run(new Form1());
            
            Form1 form = new Form1();
            FolderBrowserDialog dlg = new FolderBrowserDialog();
            dlg.SelectedPath = Path.GetDirectoryName(Application.ExecutablePath);//Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            if (dlg.ShowDialog() == DialogResult.OK)
            {
                //form
                form.resourcesPath = dlg.SelectedPath + "\\";
                form.Initialize();
                //show
                form.Show();

                while (form.Looping)
                {
                    form.Update();
                    form.Render();
                    Application.DoEvents();
                }
            }
            form.Close();
        }
    }
}
