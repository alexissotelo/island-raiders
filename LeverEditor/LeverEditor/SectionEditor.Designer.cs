﻿namespace LeverEditor
{
    partial class SectionEditor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.label11 = new System.Windows.Forms.Label();
            this.nudScaleY = new System.Windows.Forms.NumericUpDown();
            this.nudScaleX = new System.Windows.Forms.NumericUpDown();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.checkHUD = new System.Windows.Forms.CheckBox();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.nupY = new System.Windows.Forms.NumericUpDown();
            this.nupX = new System.Windows.Forms.NumericUpDown();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.button2 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.enemyListBox = new System.Windows.Forms.ListBox();
            this.nudAmountEnemies = new System.Windows.Forms.NumericUpDown();
            this.button5 = new System.Windows.Forms.Button();
            this.enemyBox = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.groupBox = new System.Windows.Forms.ComboBox();
            this.bAddLayer = new System.Windows.Forms.Button();
            this.bAddEntity = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.groupentityListBox = new System.Windows.Forms.ListBox();
            this.bDeleteEntity = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.bDeleteLayer = new System.Windows.Forms.Button();
            this.entitiesListBox = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.layersListBox = new System.Windows.Forms.ListBox();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.bAddEvent = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label8 = new System.Windows.Forms.Label();
            this.eventBox = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.nudRectX = new System.Windows.Forms.NumericUpDown();
            this.nudRectH = new System.Windows.Forms.NumericUpDown();
            this.nudRectY = new System.Windows.Forms.NumericUpDown();
            this.nudRectW = new System.Windows.Forms.NumericUpDown();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.collisionListBox = new System.Windows.Forms.ListBox();
            this.bDeleteCollision = new System.Windows.Forms.Button();
            this.bAddCollision = new System.Windows.Forms.Button();
            this.trackBar1 = new System.Windows.Forms.TrackBar();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudScaleY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudScaleX)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nupY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nupX)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudAmountEnemies)).BeginInit();
            this.tabPage1.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudRectX)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudRectH)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudRectY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudRectW)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).BeginInit();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.label11);
            this.splitContainer1.Panel1.Controls.Add(this.nudScaleY);
            this.splitContainer1.Panel1.Controls.Add(this.nudScaleX);
            this.splitContainer1.Panel1.Controls.Add(this.label10);
            this.splitContainer1.Panel1.Controls.Add(this.label9);
            this.splitContainer1.Panel1.Controls.Add(this.checkHUD);
            this.splitContainer1.Panel1.Controls.Add(this.checkBox1);
            this.splitContainer1.Panel1.Controls.Add(this.nupY);
            this.splitContainer1.Panel1.Controls.Add(this.nupX);
            this.splitContainer1.Panel1.Controls.Add(this.tabControl1);
            this.splitContainer1.Panel1.Controls.Add(this.trackBar1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.AutoScroll = true;
            this.splitContainer1.Panel2.AutoScrollMinSize = new System.Drawing.Size(800, 600);
            this.splitContainer1.Panel2.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.splitContainer1.Panel2.MouseDown += new System.Windows.Forms.MouseEventHandler(this.splitContainer1_Panel2_MouseDown);
            this.splitContainer1.Panel2.MouseMove += new System.Windows.Forms.MouseEventHandler(this.splitContainer1_Panel2_MouseMove);
            this.splitContainer1.Panel2.MouseUp += new System.Windows.Forms.MouseEventHandler(this.splitContainer1_Panel2_MouseUp);
            this.splitContainer1.Size = new System.Drawing.Size(1114, 637);
            this.splitContainer1.SplitterDistance = 288;
            this.splitContainer1.TabIndex = 0;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(18, 553);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(57, 17);
            this.label11.TabIndex = 10;
            this.label11.Text = "Camera";
            // 
            // nudScaleY
            // 
            this.nudScaleY.DecimalPlaces = 3;
            this.nudScaleY.Increment = new decimal(new int[] {
            5,
            0,
            0,
            131072});
            this.nudScaleY.Location = new System.Drawing.Point(178, 516);
            this.nudScaleY.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.nudScaleY.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            this.nudScaleY.Name = "nudScaleY";
            this.nudScaleY.Size = new System.Drawing.Size(70, 22);
            this.nudScaleY.TabIndex = 8;
            this.nudScaleY.ValueChanged += new System.EventHandler(this.nudScaleY_ValueChanged);
            // 
            // nudScaleX
            // 
            this.nudScaleX.DecimalPlaces = 3;
            this.nudScaleX.Increment = new decimal(new int[] {
            5,
            0,
            0,
            131072});
            this.nudScaleX.Location = new System.Drawing.Point(102, 515);
            this.nudScaleX.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.nudScaleX.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            this.nudScaleX.Name = "nudScaleX";
            this.nudScaleX.Size = new System.Drawing.Size(70, 22);
            this.nudScaleX.TabIndex = 7;
            this.nudScaleX.ValueChanged += new System.EventHandler(this.numericUpDown1_ValueChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(17, 517);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(43, 17);
            this.label10.TabIndex = 6;
            this.label10.Text = "Scale";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(17, 486);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(58, 17);
            this.label9.TabIndex = 5;
            this.label9.Text = "Position";
            // 
            // checkHUD
            // 
            this.checkHUD.AutoSize = true;
            this.checkHUD.Checked = true;
            this.checkHUD.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkHUD.Location = new System.Drawing.Point(154, 585);
            this.checkHUD.Name = "checkHUD";
            this.checkHUD.Size = new System.Drawing.Size(98, 21);
            this.checkHUD.TabIndex = 4;
            this.checkHUD.Text = "Show HUD";
            this.checkHUD.UseVisualStyleBackColor = true;
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(21, 585);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(112, 21);
            this.checkBox1.TabIndex = 3;
            this.checkBox1.Text = "Show Names";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // nupY
            // 
            this.nupY.Location = new System.Drawing.Point(178, 484);
            this.nupY.Maximum = new decimal(new int[] {
            4000,
            0,
            0,
            0});
            this.nupY.Minimum = new decimal(new int[] {
            4000,
            0,
            0,
            -2147483648});
            this.nupY.Name = "nupY";
            this.nupY.Size = new System.Drawing.Size(70, 22);
            this.nupY.TabIndex = 2;
            this.nupY.ValueChanged += new System.EventHandler(this.nupY_ValueChanged);
            // 
            // nupX
            // 
            this.nupX.Location = new System.Drawing.Point(102, 484);
            this.nupX.Maximum = new decimal(new int[] {
            12000,
            0,
            0,
            0});
            this.nupX.Minimum = new decimal(new int[] {
            800,
            0,
            0,
            -2147483648});
            this.nupX.Name = "nupX";
            this.nupX.Size = new System.Drawing.Size(70, 22);
            this.nupX.TabIndex = 1;
            this.nupX.ValueChanged += new System.EventHandler(this.nupX_ValueChanged);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Location = new System.Drawing.Point(12, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(273, 466);
            this.tabControl1.TabIndex = 0;
            this.tabControl1.SelectedIndexChanged += new System.EventHandler(this.tabControl1_SelectedIndexChanged);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.button2);
            this.tabPage2.Controls.Add(this.label2);
            this.tabPage2.Controls.Add(this.enemyListBox);
            this.tabPage2.Controls.Add(this.nudAmountEnemies);
            this.tabPage2.Controls.Add(this.button5);
            this.tabPage2.Controls.Add(this.enemyBox);
            this.tabPage2.Controls.Add(this.label4);
            this.tabPage2.Location = new System.Drawing.Point(4, 25);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(265, 437);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Enemies";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(7, 140);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(64, 23);
            this.button2.TabIndex = 16;
            this.button2.Text = "Delete";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 14);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(62, 17);
            this.label2.TabIndex = 15;
            this.label2.Text = "Enemies";
            // 
            // enemyListBox
            // 
            this.enemyListBox.FormattingEnabled = true;
            this.enemyListBox.ItemHeight = 16;
            this.enemyListBox.Location = new System.Drawing.Point(76, 76);
            this.enemyListBox.Name = "enemyListBox";
            this.enemyListBox.Size = new System.Drawing.Size(171, 164);
            this.enemyListBox.TabIndex = 11;
            this.enemyListBox.SelectedIndexChanged += new System.EventHandler(this.enemyListBox_SelectedIndexChanged);
            // 
            // nudAmountEnemies
            // 
            this.nudAmountEnemies.Location = new System.Drawing.Point(143, 245);
            this.nudAmountEnemies.Name = "nudAmountEnemies";
            this.nudAmountEnemies.Size = new System.Drawing.Size(84, 22);
            this.nudAmountEnemies.TabIndex = 14;
            this.nudAmountEnemies.ValueChanged += new System.EventHandler(this.nudAmountEnemies_ValueChanged);
            // 
            // button5
            // 
            this.button5.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button5.Location = new System.Drawing.Point(90, 41);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(137, 29);
            this.button5.TabIndex = 13;
            this.button5.Text = "Add Enemy";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // enemyBox
            // 
            this.enemyBox.FormattingEnabled = true;
            this.enemyBox.Location = new System.Drawing.Point(91, 11);
            this.enemyBox.Name = "enemyBox";
            this.enemyBox.Size = new System.Drawing.Size(136, 24);
            this.enemyBox.TabIndex = 12;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(7, 247);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(130, 17);
            this.label4.TabIndex = 10;
            this.label4.Text = "Amount of Enemies";
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.groupBox);
            this.tabPage1.Controls.Add(this.bAddLayer);
            this.tabPage1.Controls.Add(this.bAddEntity);
            this.tabPage1.Controls.Add(this.label5);
            this.tabPage1.Controls.Add(this.groupentityListBox);
            this.tabPage1.Controls.Add(this.bDeleteEntity);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.bDeleteLayer);
            this.tabPage1.Controls.Add(this.entitiesListBox);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Controls.Add(this.layersListBox);
            this.tabPage1.Location = new System.Drawing.Point(4, 25);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(265, 437);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Design";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // groupBox
            // 
            this.groupBox.FormattingEnabled = true;
            this.groupBox.Location = new System.Drawing.Point(107, 11);
            this.groupBox.Name = "groupBox";
            this.groupBox.Size = new System.Drawing.Size(145, 24);
            this.groupBox.TabIndex = 8;
            this.groupBox.SelectedIndexChanged += new System.EventHandler(this.groupBox_SelectedIndexChanged);
            // 
            // bAddLayer
            // 
            this.bAddLayer.Location = new System.Drawing.Point(60, 404);
            this.bAddLayer.Name = "bAddLayer";
            this.bAddLayer.Size = new System.Drawing.Size(50, 26);
            this.bAddLayer.TabIndex = 11;
            this.bAddLayer.Text = "Add";
            this.bAddLayer.UseVisualStyleBackColor = true;
            this.bAddLayer.Click += new System.EventHandler(this.bAddLayer_Click);
            // 
            // bAddEntity
            // 
            this.bAddEntity.Location = new System.Drawing.Point(177, 100);
            this.bAddEntity.Name = "bAddEntity";
            this.bAddEntity.Size = new System.Drawing.Size(75, 47);
            this.bAddEntity.TabIndex = 3;
            this.bAddEntity.Text = "Add Entity";
            this.bAddEntity.UseVisualStyleBackColor = true;
            this.bAddEntity.Click += new System.EventHandler(this.bAddEntity_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(162, 210);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(54, 17);
            this.label5.TabIndex = 10;
            this.label5.Text = "Entities";
            // 
            // groupentityListBox
            // 
            this.groupentityListBox.FormattingEnabled = true;
            this.groupentityListBox.ItemHeight = 16;
            this.groupentityListBox.Location = new System.Drawing.Point(16, 43);
            this.groupentityListBox.Name = "groupentityListBox";
            this.groupentityListBox.Size = new System.Drawing.Size(155, 164);
            this.groupentityListBox.TabIndex = 9;
            this.groupentityListBox.SelectedIndexChanged += new System.EventHandler(this.entityListBox_SelectedIndexChanged);
            // 
            // bDeleteEntity
            // 
            this.bDeleteEntity.Location = new System.Drawing.Point(144, 404);
            this.bDeleteEntity.Name = "bDeleteEntity";
            this.bDeleteEntity.Size = new System.Drawing.Size(90, 26);
            this.bDeleteEntity.TabIndex = 7;
            this.bDeleteEntity.Text = "Delete";
            this.bDeleteEntity.UseVisualStyleBackColor = true;
            this.bDeleteEntity.Click += new System.EventHandler(this.bDeleteEntity_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(42, 14);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(48, 17);
            this.label3.TabIndex = 5;
            this.label3.Text = "Group";
            // 
            // bDeleteLayer
            // 
            this.bDeleteLayer.Location = new System.Drawing.Point(0, 404);
            this.bDeleteLayer.Name = "bDeleteLayer";
            this.bDeleteLayer.Size = new System.Drawing.Size(58, 26);
            this.bDeleteLayer.TabIndex = 4;
            this.bDeleteLayer.Text = "Delete";
            this.bDeleteLayer.UseVisualStyleBackColor = true;
            this.bDeleteLayer.Click += new System.EventHandler(this.bDeleteLayer_Click);
            // 
            // entitiesListBox
            // 
            this.entitiesListBox.FormattingEnabled = true;
            this.entitiesListBox.ItemHeight = 16;
            this.entitiesListBox.Location = new System.Drawing.Point(107, 231);
            this.entitiesListBox.Name = "entitiesListBox";
            this.entitiesListBox.Size = new System.Drawing.Size(155, 164);
            this.entitiesListBox.TabIndex = 2;
            this.entitiesListBox.SelectedIndexChanged += new System.EventHandler(this.entitiesListBox_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(30, 210);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(51, 17);
            this.label1.TabIndex = 1;
            this.label1.Text = "Layers";
            // 
            // layersListBox
            // 
            this.layersListBox.FormattingEnabled = true;
            this.layersListBox.ItemHeight = 16;
            this.layersListBox.Location = new System.Drawing.Point(4, 231);
            this.layersListBox.Name = "layersListBox";
            this.layersListBox.Size = new System.Drawing.Size(98, 164);
            this.layersListBox.TabIndex = 0;
            this.layersListBox.SelectedIndexChanged += new System.EventHandler(this.listBox1_SelectedIndexChanged);
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.bAddEvent);
            this.tabPage3.Controls.Add(this.textBox1);
            this.tabPage3.Controls.Add(this.groupBox1);
            this.tabPage3.Controls.Add(this.groupBox2);
            this.tabPage3.Location = new System.Drawing.Point(4, 25);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(265, 437);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Collision";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // bAddEvent
            // 
            this.bAddEvent.Location = new System.Drawing.Point(127, 331);
            this.bAddEvent.Name = "bAddEvent";
            this.bAddEvent.Size = new System.Drawing.Size(108, 23);
            this.bAddEvent.TabIndex = 9;
            this.bAddEvent.Text = "Add Event";
            this.bAddEvent.UseVisualStyleBackColor = true;
            this.bAddEvent.Click += new System.EventHandler(this.bAddEvent_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(19, 331);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(97, 22);
            this.textBox1.TabIndex = 8;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.eventBox);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.nudRectX);
            this.groupBox1.Controls.Add(this.nudRectH);
            this.groupBox1.Controls.Add(this.nudRectY);
            this.groupBox1.Controls.Add(this.nudRectW);
            this.groupBox1.Location = new System.Drawing.Point(19, 206);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(221, 119);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Edit Rectangle";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(9, 91);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(85, 17);
            this.label8.TabIndex = 7;
            this.label8.Text = "Event Name";
            // 
            // eventBox
            // 
            this.eventBox.FormattingEnabled = true;
            this.eventBox.Location = new System.Drawing.Point(102, 88);
            this.eventBox.Name = "eventBox";
            this.eventBox.Size = new System.Drawing.Size(114, 24);
            this.eventBox.TabIndex = 9;
            this.eventBox.SelectedIndexChanged += new System.EventHandler(this.eventBox_SelectedIndexChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 62);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(35, 17);
            this.label7.TabIndex = 6;
            this.label7.Text = "Size";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 34);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(58, 17);
            this.label6.TabIndex = 5;
            this.label6.Text = "Position";
            // 
            // nudRectX
            // 
            this.nudRectX.Location = new System.Drawing.Point(69, 32);
            this.nudRectX.Maximum = new decimal(new int[] {
            80000,
            0,
            0,
            0});
            this.nudRectX.Name = "nudRectX";
            this.nudRectX.Size = new System.Drawing.Size(70, 22);
            this.nudRectX.TabIndex = 1;
            this.nudRectX.ValueChanged += new System.EventHandler(this.nudRectX_ValueChanged);
            // 
            // nudRectH
            // 
            this.nudRectH.Location = new System.Drawing.Point(145, 60);
            this.nudRectH.Maximum = new decimal(new int[] {
            600,
            0,
            0,
            0});
            this.nudRectH.Name = "nudRectH";
            this.nudRectH.Size = new System.Drawing.Size(70, 22);
            this.nudRectH.TabIndex = 4;
            this.nudRectH.ValueChanged += new System.EventHandler(this.nudRectH_ValueChanged);
            // 
            // nudRectY
            // 
            this.nudRectY.Location = new System.Drawing.Point(145, 32);
            this.nudRectY.Maximum = new decimal(new int[] {
            3000,
            0,
            0,
            0});
            this.nudRectY.Minimum = new decimal(new int[] {
            3000,
            0,
            0,
            -2147483648});
            this.nudRectY.Name = "nudRectY";
            this.nudRectY.Size = new System.Drawing.Size(70, 22);
            this.nudRectY.TabIndex = 2;
            this.nudRectY.ValueChanged += new System.EventHandler(this.nudRectY_ValueChanged);
            // 
            // nudRectW
            // 
            this.nudRectW.Location = new System.Drawing.Point(69, 60);
            this.nudRectW.Maximum = new decimal(new int[] {
            1200,
            0,
            0,
            0});
            this.nudRectW.Name = "nudRectW";
            this.nudRectW.Size = new System.Drawing.Size(70, 22);
            this.nudRectW.TabIndex = 3;
            this.nudRectW.ValueChanged += new System.EventHandler(this.nudRectW_ValueChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.collisionListBox);
            this.groupBox2.Controls.Add(this.bDeleteCollision);
            this.groupBox2.Controls.Add(this.bAddCollision);
            this.groupBox2.Location = new System.Drawing.Point(19, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(221, 188);
            this.groupBox2.TabIndex = 8;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Rectangles";
            // 
            // collisionListBox
            // 
            this.collisionListBox.FormattingEnabled = true;
            this.collisionListBox.ItemHeight = 16;
            this.collisionListBox.Location = new System.Drawing.Point(6, 21);
            this.collisionListBox.Name = "collisionListBox";
            this.collisionListBox.Size = new System.Drawing.Size(209, 132);
            this.collisionListBox.TabIndex = 0;
            this.collisionListBox.SelectedIndexChanged += new System.EventHandler(this.collisionListBox_SelectedIndexChanged);
            // 
            // bDeleteCollision
            // 
            this.bDeleteCollision.Location = new System.Drawing.Point(12, 158);
            this.bDeleteCollision.Name = "bDeleteCollision";
            this.bDeleteCollision.Size = new System.Drawing.Size(96, 23);
            this.bDeleteCollision.TabIndex = 7;
            this.bDeleteCollision.Text = "Delete";
            this.bDeleteCollision.UseVisualStyleBackColor = true;
            this.bDeleteCollision.Click += new System.EventHandler(this.bDeleteCollision_Click);
            // 
            // bAddCollision
            // 
            this.bAddCollision.Location = new System.Drawing.Point(114, 158);
            this.bAddCollision.Name = "bAddCollision";
            this.bAddCollision.Size = new System.Drawing.Size(96, 23);
            this.bAddCollision.TabIndex = 6;
            this.bAddCollision.Text = "Add";
            this.bAddCollision.UseVisualStyleBackColor = true;
            this.bAddCollision.Click += new System.EventHandler(this.bAddCollision_Click);
            // 
            // trackBar1
            // 
            this.trackBar1.LargeChange = 1;
            this.trackBar1.Location = new System.Drawing.Point(93, 550);
            this.trackBar1.Maximum = 20;
            this.trackBar1.Name = "trackBar1";
            this.trackBar1.Size = new System.Drawing.Size(155, 56);
            this.trackBar1.TabIndex = 9;
            this.trackBar1.TickFrequency = 10;
            this.trackBar1.Value = 10;
            this.trackBar1.Scroll += new System.EventHandler(this.trackBar1_Scroll);
            // 
            // SectionEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1114, 637);
            this.Controls.Add(this.splitContainer1);
            this.Name = "SectionEditor";
            this.Text = "Edit Section";
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.nudScaleY)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudScaleX)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nupY)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nupX)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudAmountEnemies)).EndInit();
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudRectX)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudRectH)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudRectY)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudRectW)).EndInit();
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.NumericUpDown nudAmountEnemies;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.ComboBox enemyBox;
        private System.Windows.Forms.ListBox enemyListBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListBox layersListBox;
        private System.Windows.Forms.Button bAddEntity;
        private System.Windows.Forms.ListBox entitiesListBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button bDeleteLayer;
        private System.Windows.Forms.Button bDeleteEntity;
        private System.Windows.Forms.ComboBox groupBox;
        private System.Windows.Forms.ListBox groupentityListBox;
        private System.Windows.Forms.NumericUpDown nupY;
        private System.Windows.Forms.NumericUpDown nupX;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button bAddLayer;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.NumericUpDown nudRectX;
        private System.Windows.Forms.NumericUpDown nudRectH;
        private System.Windows.Forms.NumericUpDown nudRectY;
        private System.Windows.Forms.NumericUpDown nudRectW;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ListBox collisionListBox;
        private System.Windows.Forms.Button bDeleteCollision;
        private System.Windows.Forms.Button bAddCollision;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.CheckBox checkHUD;
        private System.Windows.Forms.NumericUpDown nudScaleX;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.NumericUpDown nudScaleY;
        private System.Windows.Forms.TrackBar trackBar1;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button bAddEvent;
        private System.Windows.Forms.ComboBox eventBox;
    }
}